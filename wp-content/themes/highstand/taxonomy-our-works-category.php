<?php
/**
 * (c) king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$post = highstand::globe('post');
$highstand = highstand::globe();

get_header();

	if ( $highstand->cfg['our_works_breadcrumb'] ) {
		highstand_incl_core( $highstand->cfg['our_works_breadcrumb'] );
	}

	wp_enqueue_style( 'highstand-cubeportfolio-min' );
	wp_enqueue_script( 'highstand-cubeportfolio' );
	wp_enqueue_script( 'highstand-cubeportfolio-main' );

?>

	<div class="content_fullwidth less4">
		<div id="js-grid-juicy-projects" class="cbp js-grid-juicy-projects-layout-4" data-cols="4" data-gap="0">

			<?php
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					$image = $highstand->get_featured_image( $post );
					$image_thumb = highstand_createLinkImage($image, '476x420xc');

					$work_cf = get_post_meta( $post->ID , '_highstand_post_meta_options', TRUE );
					$link = !empty($work_cf['link']) ? $work_cf['link'] : get_permalink( $post->ID );
			?>

					<div class="cbp-item">
						<div class="cbp-caption">
							<div class="cbp-caption-defaultWrap">
								<img src="<?php echo esc_url($image_thumb); ?>" alt="" />
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignCenter">
									<div class="cbp-l-caption-body">

										<a href="<?php echo esc_url( $link ); ?>" class="cbp-l-caption-buttonLeft" rel="nofollow"><?php echo esc_html__( 'more info', 'highstand' ); ?></a>

										<?php if ( isset( $work_cf['video_link'] ) && !empty( $work_cf['video_link'] ) ): ?>
											<a href="<?php echo esc_url( $work_cf['video_link'] ) ?>" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?php the_title(); ?><br>by <?php echo esc_html($work_cf['outhor']); ?>"><?php echo esc_html__( 'view video', 'highstand' ); ?></a>
										<?php else: ?>
											<a href="<?php echo esc_url($image); ?>" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?php the_title(); ?><br>by <?php echo esc_html($work_cf['outhor']); ?>"><?php esc_html_e( 'view larger', 'highstand' ); ?></a>
										<?php endif ?>
									</div>
								</div>
							</div>
						</div>
					</div><!-- end item -->

			<?php
					endwhile;
				endif;
			?>
		</div>
	</div>

	<?php $highstand->pagination(); ?>

<?php get_footer(); ?>