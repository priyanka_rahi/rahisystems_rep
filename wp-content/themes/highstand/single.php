<?php
/**
 * (c) king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$highstand = highstand::globe();

get_header();

?>

	<?php highstand::path( 'blog_breadcrumb' ); ?>

	<div id="primary" class="site-content container-content content content_fullwidth less2">
		<div id="content" class="row row-content container">
			<div class="content_left blog-single-post">

			<?php

				while ( have_posts() ) : the_post();

					get_template_part( 'content' );

					if( !empty($highstand->cfg['showShareBox']) && $highstand->cfg['showShareBox'] == 1 ){

					$escaped_link = get_the_permalink();

					?>

					<div class="sharepost">
					    <h5 class="caps"><?php esc_html_e('Share this Article','highstand'); ?></h5>
					    <ul>
					    <?php if( $highstand->cfg['showShareFacebook'] == 1 ){ ?>
					      <li>
					      	<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( $escaped_link ); ?>">
					      		&nbsp;<i class="fa fa-facebook fa-lg"></i>&nbsp;
					      	</a>
					      </li>
					      <?php } ?>
					      <?php if( $highstand->cfg['showShareTwitter'] == 1 ){ ?>
					      <li>
					      	<a href="https://twitter.com/home?status=<?php echo esc_url( $escaped_link ); ?>">
					      		<i class="fa fa-twitter fa-lg"></i>
					      	</a>
					      </li>
					      <?php } ?>
					      <?php if( $highstand->cfg['showShareGoogle'] == 1 ){ ?>
					      <li>
					      	<a href="https://plus.google.com/share?url=<?php echo esc_url( $escaped_link ); ?>">
					      		<i class="fa fa-google-plus fa-lg"></i>	
					      	</a>
					      </li>
					      <?php } ?>
					      <?php if( $highstand->cfg['showShareLinkedin'] == 1 ){ ?>
					      <li>
					      	<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo esc_url( $escaped_link ); ?>">
					      		<i class="fa fa-linkedin fa-lg"></i>
					      	</a>
					      </li>
					      <?php } ?>
					      <?php if( $highstand->cfg['showSharePinterest'] == 1 ){ ?>
					      <li>
					      	<a href="https://pinterest.com/pin/create/button/?url=&amp;media=&amp;description=<?php echo esc_url( $escaped_link ); ?>">
					      		<i class="fa fa-pinterest fa-lg"></i>
					      	</a>
					      </li>
					      <?php } ?>
					    </ul>
					</div>

					<?php

					}

					if( !empty($highstand->cfg['navArticle']) && $highstand->cfg['navArticle'] == 1 ):

					?>
						<nav id="nav-single">
							<span class="nav-previous"><?php previous_post_link( '%link', wp_kses( __( '<span class="meta-nav">&larr;</span> Previous Article', 'highstand' ), array('span'=>array())) ); ?></span>
							<span class="nav-next"><?php next_post_link( '%link', wp_kses( __( 'Next Article<span class="meta-nav">&rarr;</span>', 'highstand' ), array('span'=>array())) ); ?></span>
						</nav><!-- #nav-single -->
					<?php

					endif;


					if( !empty($highstand->cfg['archiveAboutAuthor']) && $highstand->cfg['archiveAboutAuthor'] == 1 ){
					?>

					<!--About author-->
					<div class="clearfix"></div>
					<h5 class="caps"><?php esc_html_e('About the Author', 'highstand');?></h5>
					<div class="about_author">
				        <?php echo get_avatar(get_the_author_meta( 'ID' )); ?>
				        <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" target="_blank">
				        	<?php echo get_the_author(); ?>
				        </a>
				        <br>
						<?php the_author_meta( 'description' ); ?>
				    </div>

					<?php
					}


					if( !empty($highstand->cfg['archiveRelatedPosts']) && $highstand->cfg['archiveRelatedPosts'] == 1 ){
						get_template_part( 'post-related' );
					}

					if( is_single() ){
						comments_template( '', true );
					}

				endwhile; // end of the loop. ?>
			</div>
			
			<div class="right_sidebar">
				<?php get_sidebar( ); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
