<?php

class highstand_options_license extends highstand_options{	

	function __construct( $field = array(), $value ='', $parent ){
		
		parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
		$this->field = $field;
		$this->value = $value;
		//$this->render();
		
	}

	function render(){
		
		$highstand = highstand::globe('highstand');
		
		$current_valid = get_option( 'highstand_valid', true );
		if( !empty( $current_valid ) )
			$current_valid = $highstand->bsp( $current_valid );
		else $current_valid = '';
		
		$license = get_option( 'highstand_purchase_code' );
		if( empty( $license ) )
			$license = '';
		
?>
<table class="form-table blog-table-opt" style="border: none;margin: 0px;">
	<tr>
		<th scope="row">
			<label for="blog-layout"><?php esc_html_e( 'Purchase Code' , 'highstand'); ?></label>
		</th>
		<td class="king-upload-wrp" id="verify-purchase-wrp">
			<div id="verify-purchase-status"></div>
			<p>
				<input type="text" id="input-purchase-key" name="" autocomplete="off" value="<?php echo esc_attr( $license ); ?>" class="regular-text" style="height: 35px;" />
				<button class="button button-large button-primary" id="verify-purchase-key">
					<i class="fa fa-key"></i> <?php esc_html_e( 'Verify This Key' , 'highstand'); ?>
				</button>
			</p>
			<p>
				<a href="<?php echo esc_url( 'http://help.king-theme.com/faq/12-how-to-find-my-purchase-code-3f.html' ); ?>" target=_blank><?php esc_html_e( 'How to find the purchase code?' , 'highstand'); ?></a>
			</p>
			<div class="loading">
				<i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>
				<br />
				<?php esc_html_e( 'Please wait a moment...' , 'highstand'); ?>
			</div>
		</td>
	</tr>
	<tr>
		<th scope="row">
			<label for="blog-layout"><?php esc_html_e( 'Verification Status' , 'highstand'); ?></label>
		</th>
		<td id="verify-purchase-msg-wrp">
			<div class="msg-notice verify-stt <?php if( $current_valid !== $highstand->bsp( site_url() ) )echo ' active'; ?>">
				<i class="fa fa-warning"></i> <?php printf( esc_html__( 'The domain %s has yet to be verified.' , 'highstand'), '<u style="color:#555">'.site_url().'</u>' ); ?>
			</div>
			<div class="msg-success verify-stt <?php if( $current_valid === $highstand->bsp( site_url() ) )echo ' active'; ?>">
				<i class="fa fa-check"></i> <?php printf( esc_html__( 'Wooot! The domain %s has been verified.' , 'highstand'), '<u style="color:#555">'.site_url().'</u>' ); ?>
			</div>
			<br />
			<p>
				<?php esc_html_e( 'Have You verified the purchase key with another domain? Don\'t worry! You can reissues your purchase key for another domain', 'highstand'); ?>.<br />
				<i>(<?php esc_html_e( 'Maximum 3 domains per key, and can\'t reissues the domain which has been verified before', 'highstand'); ?>)</i>
			</p>
		</td>
	</tr>
</table>		
<?php	
	
			
	}
	
	function enqueue(){

		
	}//function
	
}//class