<?php

/*
*
*	(c) king-theme.com
*
*/

###Load core of theme###
function highstand_is_plugin_active( $plugin ){
	if ( in_array( $plugin , apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		return true;
	}
	else {
		return false;
	}
}

function highstand_incl_core( $file = '', $type = 'ro', $atts = array(), $content = '' ){

	$path = locate_template( $file );

	if( file_exists( $path ) && is_file( $path ) ){
		
		switch( $type ){
			case 'ro':
				require_once( $path );
				break;
			case 'r':
				require( $path );
				break;
			case 'i':
				include $path;
		}

	}else{
		echo 'Could not load theme file: '.$file;
	}

}

highstand_incl_core( trailingslashit('core').'highstand.define.php' );


//Register new path shortcode with mini
add_action('init', 'highstand_set_shortcode_template', 99 ); 
function highstand_set_shortcode_template(){
 
    global $kc;
	
	if( method_exists( $kc, 'set_template_path' ) ){
		$kc->set_template_path( get_template_directory().'/templates/kingcomposer/' );
	}    
    
}


if( !function_exists('randomId') ){
	
	function randomId( $length ) {
		$key = null;
		$keys = array_merge( range( 0, 9 ), range( 'a', 'z' ) );
		for( $i=0; $i < $length; $i++ ) {
			$key .= $keys[ array_rand( $keys ) ];
		}
		return $key;
	}
}
#
#	End Load
#

