<?php
/**
 * (c) king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$post = highstand::globe('post');
$highstand = highstand::globe();

$image = $highstand->get_featured_image( $post );
$escaped_link = get_permalink( $post );

$our_work_options = get_post_meta( $post->ID , '_highstand_post_meta_options' );

if ( isset( $our_work_options[0] ) ) {
	$our_work_options = $our_work_options[0];
}

if( isset( $highstand->cfg['our_works_field'] ) && is_array( $highstand->cfg['our_works_field'] ) ){
	$custom_fields = array();
	foreach($highstand->cfg['our_works_field'] as $k => $value){
		if($value != ''){
			$work_val = $value;
			$work_val = strtolower( $work_val );
			$work_val = trim( $work_val );
			$work_val = preg_replace('~[^a-z0-9]+~i', '-', $work_val);
			$work_id  = 'ow_extra_' . $work_val;
			$custom_fields[] = array(
				'id'	=> $work_id,
				'title'	=> $value
			);
		}
	}
}

get_header();

?>

<?php
	if ( $highstand->cfg['our_works_breadcrumb'] ) {
		highstand_incl_core( $highstand->cfg['our_works_breadcrumb'] );
	}
?>


<div id="primary" class="site-content container-content content content_fullwidth less2">
	<div id="content" class="row row-content container">
		<div class="portfolio_area">
			<div class="portfolio_area_left">
				<div class="animated fadeInLeft">
					<div id="portfolio-large-preview">
						<img src="<?php echo esc_url( $image ); ?>" alt="" />
					</div>
				</div>

				<?php if ( isset( $our_work_options['images_list'] ) && !empty( $our_work_options['images_list'] ) ): ?>
					<div class="portfolio_thumbnails">
						<?php foreach ($our_work_options['images_list'] as $key => $value): ?>
							<?php $url_thumb = wp_get_attachment_image_url( $key, 'thumbnail' ); ?>
							<a target="_blank" href="<?php echo esc_url( $value ); ?>">
								<img src="<?php echo esc_url( $url_thumb ); ?>">
							</a>
						<?php endforeach ?>
					</div>
				<?php endif ?>
			</div>
			<div class="portfolio_area_right animated eff-fadeInRight delay-200ms">
				<h3><?php echo esc_html($post->post_title); ?></h3>
				<p class="work-des">
					<?php echo do_shortcode( $post->post_content ); ?>
				</p>

				<ul class="small_social_links">
					<li>
						<a href="<?php echo esc_url( 'https://www.facebook.com/sharer/sharer.php?u='.$escaped_link ); ?>">
							<i class="fa fa-facebook">
							</i>
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url( 'https://twitter.com/home?status='.$escaped_link ); ?>">
							<i class="fa fa-twitter">
							</i>
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url( 'https://plus.google.com/share?url='.$escaped_link ); ?>">
							<i class="fa fa-google-plus">
							</i>
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url( 'https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source='.$escaped_link ); ?>">
							<i class="fa fa-linkedin">
							</i>
						</a>
					</li>
					<li>
						<a href="<?php echo esc_url( 'https://pinterest.com/pin/create/button/?url=&media=&description='.$escaped_link ); ?>">
							<i class="fa fa-pinterest">
							</i>
						</a>
					</li>
				</ul>
				<div class="project_details animated eff-fadeInUp delay-500ms">
					<h5>
						<?php esc_html_e('Project Details', 'highstand' ); ?>
					</h5>
					<span>
						<strong>
							<?php esc_html_e('Name', 'highstand' ); ?>
						</strong>
						<em>
							<?php the_title(); ?></em>
					</span>
					<?php if ( isset( $our_work_options['our_date'] ) && !empty( $our_work_options['our_date'] ) ): ?>
						<span>
							<strong><?php esc_html_e('Date', 'highstand' ); ?></strong>
							<em><?php echo esc_attr( $our_work_options['our_date'] ); ?></em>
						</span>
					<?php endif ?>
					<?php
					if(!isset($highstand->cfg['our_works_show_category']) || $highstand->cfg['our_works_show_category'] ==1){
					?>
					<span>
						<strong>
							<?php esc_html_e('Categories', 'highstand' ); ?>
						</strong>
						<em>
							<?php
								$terms = wp_get_post_terms($post->ID, 'our-works-category', array("fields" => "all"));
								if( !empty( $terms ) ){
									foreach( $terms as $term ){
										echo '<a href="'.esc_url( get_term_link( $term ) ).'">'.esc_html( $term->name ).'</a>';
									}
								}
							?>
						</em>
					</span>
					<?php }?>

					<?php if ( isset( $our_work_options['outhor'] ) && !empty( $our_work_options['outhor'] ) ): ?>
						<span>
							<strong><?php esc_html_e('Author', 'highstand' ); ?></strong>
							<em><?php echo esc_attr( $our_work_options['outhor'] ); ?></em>
						</span>
					<?php endif ?>
					<?php if ( isset( $custom_fields ) ): ?>
						<?php foreach ( $custom_fields as $key => $value ): ?>
							<?php if ( !empty( $our_work_options[$value['id']] ) ): ?>
								<span>
									<strong>
										<?php echo esc_html( $value['title'] ); ?>
									</strong>
									<em>
										<?php echo esc_html( $our_work_options[$value['id']] ) ?>
									</em>
								</span>
							<?php endif ?>
						<?php endforeach ?>
					<?php endif ?>
					<div class="clearfix margin_top5">
					</div>

					<?php
					if(isset($highstand->cfg['our_works_visit_link']) || $highstand->cfg['our_works_visit_link'] === true){
					?>
						<?php if ( !empty( $our_work_options['link'] ) ): ?>
							<a href="<?php echo esc_url( $our_work_options['link'] ); ?>" class="but_goback globalBgColor">
								<i class="fa fa-hand-o-right fa-lg">
								</i>
								<?php esc_html_e('Visit Site', 'highstand' ); ?>
							</a>
						<?php endif ?>
					<?php }?>
				</div>
			</div>
		</div>
		<!-- end section -->
		<div class="clearfix margin_top5"></div>
	</div>
</div>
<script type="text/javascript">
(function($){
	$(window).load(function() {
		$('.portfolio_thumbnails a').each(function(){
			var obj = this;
			var img = new Image();
			img.onload = function(){
				$(obj).html('').append( this ).on( 'click', function(e){
					var new_src = $(this).attr('href');
					$('#portfolio-large-preview img').animate({'opacity':0.1},150,function(){
						$('#portfolio-large-preview img').attr({ 'src' : new_src }).css({ 'opacity' : 0 }).animate({ 'opacity' : 1 });
					});
					e.preventDefault();
				});
			}
			img.src = $(this).attr('href');
		});
	});
})(jQuery);
</script>
<?php get_footer(); ?>