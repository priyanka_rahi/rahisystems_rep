<?php
/**
 #		(c) king-theme.com
 */
get_header(); ?>

<?php //highstand::path( 'breadcrumb' ); ?>

<div id="breadcrumb" class="page_title2" style="margin-bottom: 72px;">
	<div class="container">
		<h1><?php highstand_title(); ?></h1>
		<div class="pagenation">&nbsp;<?php $highstand->breadcrumb(); ?></div>
	</div>
</div>
<div class="clearfix"></div>

<div id="primary" class="site-content">
	<div id="content" class="container">

		<div class="error_pagenotfound">

	        <strong><?php esc_html_e('404', 'highstand'); ?></strong>
	        <br>
	    	<b><?php esc_html_e('Oops... Page Not Found!', 'highstand'); ?></b>

	        <em><?php esc_html_e('Sorry the Page Could not be Found here.', 'highstand'); ?></em>

	        <p><?php esc_html_e('Try using the button below to go to main page of the site', 'highstand'); ?></p>

	        <div class="clearfix margin_top3"></div>

	        <a href="<?php echo SITE_URI; ?>" class="but_medium1 but_404">
	        	<i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp; <?php esc_html_e('Go to Back', 'highstand'); ?>
	        </a>

	    </div>

	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>