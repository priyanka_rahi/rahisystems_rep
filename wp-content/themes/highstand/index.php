<?php
/**
 * (c) king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$highstand = highstand::globe();

get_header();

?>

	<?php highstand::path( 'blog_breadcrumb' ); ?>

	<div id="primary" class="site-content container-content content content_fullwidth less2">
		<div id="content" class="row row-content container">
			<div class="col-md-9 content_left">
				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', get_post_format() ); ?>
					<?php endwhile; ?>

					<?php $highstand->pagination(); ?>

				<?php else : ?>

					<article id="post-0" class="post no-results not-found">

					<?php if ( current_user_can( 'edit_posts' ) ) :
						// Show a different message to a logged-in user who can add posts.
					?>
						<header class="entry-header">
							<h1 class="entry-title"><?php esc_html_e( 'No posts to display', 'highstand' ); ?></h1>
						</header>

						<div class="entry-content">
							<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'highstand' ),array('a'=>array())), admin_url( 'post-new.php' ) ); ?></p>
						</div><!-- .entry-content -->

					<?php else :
						// Show the default message to everyone else.
					?>
						<header class="entry-header">
							<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'highstand' ); ?></h1>
						</header>

						<div class="entry-content">
							<p><?php esc_html_e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'highstand' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					<?php endif; // end current_user_can() check ?>

					</article><!-- #post-0 -->

				<?php endif; // end have_posts() check ?>
			</div>
			<div class="col-md-3 right_sidebar">
				<?php get_sidebar( ); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>