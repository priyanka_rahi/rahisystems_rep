<?php
/**
 * Template Name: Page Right Sidebar
 * (c) king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$post = highstand::globe('post');
$highstand = highstand::globe('highstand');

if( is_page() ){
	// $sidebar = get_post_meta( $post->ID,'_king_page_sidebar' , true );
	$page_options = get_post_meta( $post->ID , '_highstand_post_meta_options' );
	$sidebar = $page_options[0]['sidebar'];
} else if( is_home() ){
	if( get_option( 'page_for_posts', true ) ){
		$sidebar = get_post_meta( get_option( 'page_for_posts', true ),'_king_page_sidebar' , true );
	}
}
if( empty( $sidebar ) ){
	$sidebar = 'sidebar';
}

get_header();

?>

	<?php highstand::path( 'breadcrumb' ); ?>

	<div class="clearfix"></div>
	<div id="primary" class="site-content container-content content content_fullwidth less2">
		<div id="content" class="row row-content container">
			<div class="col-md-9 content_left">
				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', get_post_format() ); ?>
					<?php endwhile; ?>

					<?php $highstand->pagination(); ?>

				<?php else : ?>

					<article id="post-0" class="post no-results not-found">

					<?php if ( current_user_can( 'edit_posts' ) ) :
						// Show a different message to a logged-in user who can add posts.
					?>
						<header class="entry-header">
							<h1 class="entry-title"><?php esc_html_e( 'No posts to display', 'highstand' ); ?></h1>
						</header>

						<div class="entry-content">
							<p><?php printf( esc_html__( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'highstand' ), admin_url( 'post-new.php' ) ); ?></p>
						</div><!-- .entry-content -->

					<?php else :
						// Show the default message to everyone else.
					?>
						<header class="entry-header">
							<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'highstand' ); ?></h1>
						</header>

						<div class="entry-content">
							<p><?php esc_html_e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'highstand' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					<?php endif; // end current_user_can() check ?>

					</article><!-- #post-0 -->

				<?php endif; // end have_posts() check ?>
			</div>
			<div class="col-md-3 right_sidebar">
				<?php if ( is_active_sidebar( $sidebar ) ) : ?>
					<div id="sidebar" class="widget-area king-sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div><!-- #secondary -->
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php get_footer();
