<?php
/**
 * (c) www.king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$post = highstand::globe('post');
$highstand = highstand::globe('highstand');
$more = highstand::globe('more');

get_header();
//print_r($post);
?>

<div class="page_title2 sty2">
	<div class="container">
		
	    <h1>Login Form</h1>
	    <div class="pagenation">&nbsp;<a href="<?php echo site_url(); ?>">Home</a> <i>/</i> <a href="#">Features</a> <i>/</i> Login Form</div>
	     
	</div>
</div>

<div class="clearfix"></div>

<div id="primary" class="site-content">
	<div id="content" class="container">
		<div class="entry-content blog_postcontent">
			<div class="margin_top1"></div>
			<?php get_template_part( 'templates/login' ); ?>
			<div class="margin_top6"></div>
		</div>
	</div>
</div>



<?php get_footer(); ?>   