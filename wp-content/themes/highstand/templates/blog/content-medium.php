<?php

$post = highstand::globe('post');
$more = highstand::globe('more');
$highstand = highstand::globe();

$thumbnail_url = $highstand->get_featured_image( $post );
$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '831x369xc' );


$categories_list = get_the_category_list( esc_html__( ', ', 'highstand' ) );
$post_custom_field = get_post_meta($post->ID , '_'.THEME_OPTNAME.'_post_meta_options', true);

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( is_page()?'':'blog_post' ); ?>>
	<div class="blog_post">
	    <div class="blog_postcontent">
	    <div class="image_frame">
	    	<?php if(!empty($post_custom_field['feature_video'])): ?>
	    		<?php
	    			echo preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<ifr"."ame width=\"831\" height=\"369\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></ifr"."ame>", $post_custom_field['feature_video'] );
	    		?>
	    	<?php else: ?>
	    		<a href="#"><img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="<?php the_title(); ?>"></a>
	    	<?php endif; ?>
	    </div>
	    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	        <ul class="post_meta_links">
	        	<li><a href="#" class="date"><?php echo get_the_date('d F Y'); ?></a></li>

	            <li class="post_categoty"><i>in:</i> <?php echo highstand::esc_js( $categories_list ); ?></li>
	            <li class="post_comments"><i>note:</i> <a href="<?php echo esc_url( get_comment_link() ); ?>"><?php comments_number( 'no comments', 'one comment', '% comments' ); ?></a></li>
	        </ul>
	     <div class="clearfix"></div>
	     <div class="margin_top1"></div>
	    <p> <?php echo wp_trim_words( get_the_content(), 65, '' ); ?> <a href="<?php the_permalink(); ?>">read more...</a></p>
	    </div>
	</div>
</article>
<div class="clearfix divider_line1"></div>