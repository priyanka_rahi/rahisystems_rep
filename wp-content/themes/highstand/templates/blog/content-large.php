<?php
/**
 * (c) king-theme.com
 */

 	$highstand = highstand::globe();

	$thumbnail_url = $highstand->get_featured_image( $post );

	$categories_list = get_the_category_list( esc_html__( ', ', 'highstand' ) );
	$post_custom_field = get_post_meta($post->ID , '_'.THEME_OPTNAME.'_post_meta_options', true);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( is_page()?'':'blog_post' ); ?>>

	<div class="entry-content blog_postcontent blog-large-style">

		<div class="image_frame">
	    	<?php if(!empty($post_custom_field['feature_video'])): ?>
	    		<?php
	    			echo preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<ifr"."ame width=\"100%\" height=\"520\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></ifr"."ame>", $post_custom_field['feature_video'] );
	    		?>
	    	<?php else: ?>
	    		<a href="#"><img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="<?php the_title(); ?>"></a>
	    	<?php endif; ?>
	    </div>

		<?php if( !is_page() ): ?>

			<header class="entry-header animated ext-fadeInUp">

				<h3 class="entry-title">
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'highstand' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
						<?php the_title(); ?>
					</a>
					<?php //edit_post_link( esc_html__( 'Edit', 'highstand' ), '<span class="edit-link">', '</span>' ); ?>
				</h3>

				<?php if ( is_sticky() ) : ?>
					<h3 class="entry-format">
							<?php esc_html_e( 'Featured', 'highstand' ); ?>
					</h3>
				<?php endif; ?>

				<?php

				if ( 'post' == get_post_type() ){

					if ( isset($highstand->cfg['showMeta']) && $highstand->cfg['showMeta'] ==  1 ){
						highstand::posted_on( 'post_meta_links ' );
					}

				}


			echo '</header><!-- .entry-header -->';

		endif;
		/*End of header of single post*/

		if( ( get_option('rss_use_excerpt') == 1 || is_search() ) && !is_single() && !is_page() ){

			the_excerpt();
			echo '<a href="'.get_the_permalink().'">'.esc_html__('Read More &#187;','highstand').'</a>';

		}else{
			the_content( esc_html__( 'Read More &#187;', 'highstand' ) );
		}

		wp_link_pages( array( 'before' => '<div class="page-link"><span>' . esc_html__( 'Pages:', 'highstand' ) . '</span>', 'after' => '</div>' ) );

	?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
<?php

if( !is_page() ){
	echo '<div class="clearfix divider_line1"></div>';
}
?>
