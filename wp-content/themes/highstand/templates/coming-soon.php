<?php
/**
 * (c) www.devn.co
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$highstand = highstand::globe();
$post = highstand::globe('post');

$timedown = $highstand->cfg['cs_timedown'];
if( empty($highstand->cfg['cs_timedown'])){
	$timedown = date("F d, Y H:i:s",strtotime("+1 week"));
}
?>
<link href="<?php echo THEME_URI; ?>/assets/js/comingsoon/animations.min.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" media="screen" href="<?php echo THEME_URI; ?>/assets/js/comingsoon/coming.css" type="text/css" />

<script type="text/javascript" src="<?php echo THEME_URI; ?>/assets/js/comingsoon/jquery.bcat.bgswitcher.js"></script>

<div id="bg-body"></div>
<!--end -->
<div class="site_wrapper" style="background-color: initial;">
	<div class="comingsoon_page">
		<div class="container">
			<div class="topcontsoon">
				<?php if ( !empty( $highstand->cfg['cs_logo'] ) ): ?>
					<img height="45" src="<?php echo esc_url( $highstand->cfg['cs_logo'] ); ?>" alt="" />
				<?php else: ?>
					<img height="45" src="<?php echo THEME_URI; ?>/assets/images/logo.png" alt="" />
				<?php endif ?>
				<div class="clearfix">
				</div>
				<h5>
					<?php
						if ( !empty( $highstand->cfg['cs_text_after_logo'] ) ) {
							echo esc_html( $highstand->cfg['cs_text_after_logo'] );
						} else {
							_e('We\'re Launching Soon', 'highstand' );
						}
					?>
				</h5>
			</div>

			<div class="countdown_dashboard">
				<div class="flipTimer">

					<div class="days"></div>
					<div class="hours"></div>
					<div class="minutes"></div>
					<div class="seconds"></div>

					<div class="clearfix"></div>

					<div class="fttext ftt_days">DAYS</div>
					<div class="fttext ftt_hours">HRS</div>
					<div class="fttext ftt_min">MIN</div>
					<div class="fttext ftt_sec">SEC</div>
				</div>
			</div>
			<div class="clearfix"></div>


			<div class="socialiconssoon">

				<p class="white">
					<?php
						if ( !empty( $highstand->cfg['cs_description'] ) ) {
							echo wp_kses_post( $highstand->cfg['cs_description'] );
						} else {
							_e("Our website is under construction. We'll be here soon with our new awesome site. Get best experience with this one.", 'highstand' );
						}
					?>
				</p>

				<div class="clearfix margin_top3"></div>

				<form data-url="<?php echo admin_url( 'admin-ajax.php?t='. time() ); ?>" class="hs_subscribe hs_mailchimp" method="POST" action="" _lpchecked="1">
					<input class="enter_email_input required email newslesoon" name="highstand_email" value="<?php echo esc_html__( 'Enter email...', 'highstand' ); ?>" onfocus="if( this.value == '<?php echo esc_html__( 'Enter email...', 'highstand' ); ?>') { this.value = ''; }" onblur="if ( this.value == '') { this.value = '<?php echo esc_html__( 'Enter email...', 'highstand' ); ?>'; }" type="text">
					<input name="subscribe" value="<?php echo esc_html__( 'Submit', 'highstand' ); ?>" class="input_submit newslesubmit" type="submit">
					<div class="highstand_newsletter_status"></div>
				</form>

				<div class="clearfix"></div>
				<div class="margin_top3"></div>

				<?php highstand::socials( 'comming-socials', 15, false ); ?>


			</div><!-- end section -->


		</div>
	</div>
</div>

<!-- ######### JS FILES ######### -->
<script type="text/javascript" src="<?php echo THEME_URI; ?>/assets/js/comingsoon/jquery.flipTimer.js"></script>
<!-- animations -->
<script src="<?php echo THEME_URI; ?>/assets/js/comingsoon/animations.min.js" type="text/javascript"></script>

<?php
	$srcBgArray = array();
	for($i=1; $i<=5; $i++){
		$var_name = 'cs_slider'.$i;
		if(!empty($highstand->cfg[$var_name])){
			array_push($srcBgArray, $highstand->cfg[$var_name]);
		}
	}

	$str_arr = array();
	if($srcBgArray){
		foreach($srcBgArray as $src){
			$str_arr[] = str_replace(array('%SITE_URI%', '%HOME_URI%'), array(SITE_URI, SITE_URI), $src);
		}
	}


	if(empty($str_arr)){
		$str_arr = array(
		"http://gsrthemes.com/highstand/js/comingsoon/img-slider-1.jpg",
		"http://gsrthemes.com/highstand/js/comingsoon/img-slider-2.jpg",
		"http://gsrthemes.com/highstand/js/comingsoon/img-slider-3.jpg",
		);
	}
?>

<script type="text/javascript">
jQuery(document).ready(function() {
	//Callback works only with direction = "down"
	jQuery('.flipTimer').flipTimer({ direction: 'down', date: '<?php echo esc_js( $timedown ); ?>', callback: function() { alert('times up!'); } });
});


// var srcBgArray = ["http://gsrthemes.com/highstand/js/comingsoon/img-slider-1.jpg","http://gsrthemes.com/highstand/js/comingsoon/img-slider-2.jpg","http://gsrthemes.com/highstand/js/comingsoon/img-slider-3.jpg"];
var srcBgArray = [
<?php foreach($str_arr as $img) echo '"'.esc_html($img).'",';?>
];

jQuery(document).ready(function() {
	jQuery('#bg-body').bcatBGSwitcher({
		urls: srcBgArray,
		alt: 'Full screen background image',
		links: true,
		prevnext: true
	});
});
</script><!--end of bg-body script-->

<script>
function validateForm() {
	var x = document.forms["myForm"]["email"].value;
	var atpos = x.indexOf("@");
	var dotpos = x.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
		alert("Not a valid e-mail address");
		return false;
	}
}
</script>
