<?php
/**
    * Template Name: Page Full
	* (c) king-theme.com
*/
get_header(); ?>

	<?php highstand::path( 'breadcrumb' ); ?>

	<div id="container_full" class="site-content">
		<div id="content" class="">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
