<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: descriptions|textarea|<h5>The point of using Lorem Ipsum is that it has a more or less normal distribution of letters opposedthe versionsare here content here making it look like readable English. Many desktop publishing packages and web page editors now use Ipsum as their default model text and a search for lorem ipsum will uncover many defaultweb sites still in their infancy. Various versions have evolved over the years.</h5><i class="fa fa-paper-plane"></i> <i class="fa fa-coffee"></i> <i class="fa fa-cloud-upload"></i>
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

$highstand = highstand::globe();
$post = highstand::globe('post');

$page_options = get_post_meta( $post->ID , '_highstand_post_meta_options' );

if ( !empty( $page_options[0]['breadcrumb_bg'] ) ) {
	$breadcrumb_url =  str_replace( '%SITE_URI%', SITE_URI, $page_options[0]['breadcrumb_bg'] );
	$breadcrumb_bg = 'background-image: url('. esc_url( $breadcrumb_url ) .');';
} else {
	$breadcrumb_bg = '';
}

if ( isset( $post->ID ) ) {
	if ( get_post_type( $post->ID ) == 'our-works' ) {

		if ( isset( $highstand->cfg['our_works_breadcrumb_bg'] ) && !empty( $highstand->cfg['our_works_breadcrumb_bg'] ) ) {
			$breadcrumb_bg = 'background-image: url('. esc_url( $highstand->cfg['our_works_breadcrumb_bg'] ) .');';
		}

	}

	if( is_home() || is_single() || is_category() ){
		$blog_breadcrumb_bg = '';

		if(  !empty( $highstand->cfg['blog_breadcrumb_bg'] ) ){
			$blog_breadcrumb_bg = $highstand->cfg['blog_breadcrumb_bg'];

			$breadcrumb_url =  str_replace( '%SITE_URI%', SITE_URI, $blog_breadcrumb_bg );
			$breadcrumb_bg = 'background-image: url('. esc_url( $breadcrumb_url ) .');';							
		}
	}
}

?>

<?php if(!is_front_page()): ?>

	<div id="breadcrumb" class="page_title3" style="<?php echo esc_attr( $breadcrumb_bg ); ?>">
		<div class="container">
			<div class="title"><h1><?php highstand_title(); ?></h1></div>
			<?php if ( !empty( $atts['descriptions'] ) ): ?>
				<?php echo wp_kses_post( $atts['descriptions'] ); ?>
			<?php endif ?>
		</div>
	</div>

<?php endif; ?>