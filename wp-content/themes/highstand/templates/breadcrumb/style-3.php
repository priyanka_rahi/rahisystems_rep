<?php

$highstand = highstand::globe();
$post = highstand::globe('post');

$page_options = get_post_meta( $post->ID , '_highstand_post_meta_options' );

if ( !empty( $page_options[0]['breadcrumb_bg'] ) ) {
	$breadcrumb_url =  str_replace( '%SITE_URI%', SITE_URI, $page_options[0]['breadcrumb_bg'] );
	$breadcrumb_bg = 'background-image: url('. esc_url( $breadcrumb_url ) .');';
} else {
	$breadcrumb_bg = '';
}

if ( isset( $post->ID ) ) {
	if ( get_post_type( $post->ID ) == 'our-works' ) {

		if ( isset( $highstand->cfg['our_works_breadcrumb_bg'] ) && !empty( $highstand->cfg['our_works_breadcrumb_bg'] ) ) {
			$breadcrumb_bg = 'background-image: url('. esc_url( $highstand->cfg['our_works_breadcrumb_bg'] ) .');';
		}

	}

	if( is_home() || is_single() || is_category() ){
		$blog_breadcrumb_bg = '';

		if(  !empty( $highstand->cfg['blog_breadcrumb_bg'] ) ){
			$blog_breadcrumb_bg = $highstand->cfg['blog_breadcrumb_bg'];

			$breadcrumb_url =  str_replace( '%SITE_URI%', SITE_URI, $blog_breadcrumb_bg );
			$breadcrumb_bg = 'background-image: url('. esc_url( $breadcrumb_url ) .');';							
		}
	}
}

?>

<?php if(!is_front_page()): ?>

	<div id="breadcrumb" class="page_title4" style="<?php echo esc_attr( $breadcrumb_bg ); ?>">
		<div class="container">
			<div class="title"><h1><?php highstand_title(); ?></h1></div>
		</div>
	</div>

<?php endif; ?>