<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

$highstand = highstand::globe();
$post = highstand::globe('post');

if ( isset( $post->ID ) ) {

	$page_options = get_post_meta( $post->ID , '_highstand_post_meta_options' );
	if ( !empty( $page_options[0]['breadcrumb_bg'] ) ) {
		$breadcrumb_url =  str_replace( '%SITE_URI%', SITE_URI, $page_options[0]['breadcrumb_bg'] );
		$breadcrumb_bg = 'background-image: url('. esc_url( $breadcrumb_url ) .');';
	} else {
		$breadcrumb_bg = '';
	}

	if ( get_post_type( $post->ID ) == 'our-works' ) {

		if ( isset( $highstand->cfg['our_works_breadcrumb_bg'] ) && !empty( $highstand->cfg['our_works_breadcrumb_bg'] ) ) {
			$breadcrumb_bg = 'background-image: url('. esc_url( $highstand->cfg['our_works_breadcrumb_bg'] ) .');';
		}

	}

	if( is_home() || is_single() || is_category() ){
		$blog_breadcrumb_bg = '';

		if(  !empty( $highstand->cfg['blog_breadcrumb_bg'] ) ){
			$blog_breadcrumb_bg = $highstand->cfg['blog_breadcrumb_bg'];

			$breadcrumb_url =  str_replace( '%SITE_URI%', SITE_URI, $blog_breadcrumb_bg );
			$breadcrumb_bg = 'background-image: url('. esc_url( $breadcrumb_url ) .');';
		}
	}

?>

<?php if( !is_front_page() ): ?>

	<div id="breadcrumb" class="page_title2 sty2" style="<?php echo esc_attr( $breadcrumb_bg ); ?>">
		<div class="container">
			<div class="title"><h1><?php highstand_title(); ?></h1></div>
			<div class="pagenation">&nbsp;<?php $highstand->breadcrumb(); ?></div>
		</div>
	</div>

<?php endif; ?>

<?php } else { ?>

	<?php if( !is_front_page() ): ?>

		<div id="breadcrumb" class="page_title2 sty2">
			<div class="container">
				<div class="title"><h1><?php highstand_title(); ?></h1></div>
				<div class="pagenation">&nbsp;<?php $highstand->breadcrumb(); ?></div>
			</div>
		</div>

	<?php endif; ?>

<?php } ?>