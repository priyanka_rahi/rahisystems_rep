<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo.png, menu|menu, email|text|contact@king-theme.com, phone|text|+1 123-456-7890, show_icon|boolean|yes, show_top_menu|boolean|yes
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');
	wp_enqueue_style('highstand-menu-13');

	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';

	$menu = !empty($atts['menu']) ? $atts['menu'] : '';
	if(empty($menu) || $menu == 'Select Menu') $menu = 'main-menu';

	$showCart = empty($highstand->cfg['topInfoCart']) ? $highstand->cfg['topInfoCart'] : 'show';
	$default_logo = get_template_directory_uri().'/assets/images/logo4.png';
?>

<div class="top_nav">
	<div class="container">

	    <div class="left">

	        <?php if( !empty( $atts['email'] ) ): ?>
	        	<a href="mailto:<?php echo esc_attr( $atts['email'] ); ?>"><i class="fa fa-envelope"></i>&nbsp; <?php echo esc_html( $atts['email'] ); ?></a>
	       	<?php endif; ?>

	       	<?php if( !empty( $atts['phone'] ) ): ?>
	       		<span class="hs-phone"><i class="fa fa-phone-square"></i>&nbsp; <?php echo esc_html( $atts['phone'] ); ?></span>
	       	<?php endif; ?>

	    </div><!-- end left -->

	    <div class="right">

        <?php
	    	if( isset( $atts['show_icon'] ) && $atts['show_icon'] == 'yes'){
	    		$highstand->socials('topsocial');
	    	}
    	?>

	    <?php
			if ( isset( $atts['show_top_menu'] ) && $atts['show_top_menu'] == 'yes' ) {
				wp_nav_menu( array(
					'class' => 'hs-top-menu',
					'theme_location' => 'topmenu',
					'walker' => new king_Walker_Main_Nav_Menu()
				) );
			}
	    ?>

	    </div><!-- end right -->

	</div>
</div>

<header class="header header-13">

	<div class="container">

		<a class="sb-toggle-<?php echo esc_attr( $position ); ?>" href="javascript:;" data-connection="navbar-collapse-1"><i class="fa fa-bars"></i></a>

		<!-- Cart button for responsive -->
	    <?php if( !empty( $woocommerce ) ): ?>
	   	<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="minicart-reponsive minicart-reponsive-<?php echo ($position=='left')?'right':'left'; ?>">
	        <i class="et-basket et"></i>
	        <span class="cart-items"><?php echo WC()->cart->cart_contents_count; ?></span>
	    </a>
		<?php endif; ?>

		<!-- Logo -->
        <div class="logo">
		    <a href="<?php echo esc_url(home_url('/')); ?>" id="logo">
	    		<img src="<?php echo esc_url( isset($atts['logo'])?$atts['logo']:$default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
			</a>
	    </div>

		<!-- Navigation Menu -->
	    <div class="menu_main">
	      	<div class="navbar yamm navbar-default">
	      		<div id="navbar-collapse-1" class="navbar-collapse collapse">
	        		<nav <?php if( !empty( $woocommerce ) && $showCart == 'show' ) echo 'class="nav_has_cart"'; ?>>
	            		<?php $highstand->mainmenu( $menu ); ?>
	            	</nav>

	            	<?php
		           		//Start cart
				    	if( !empty( $woocommerce ) && $showCart == 'show' ){

					?>
				        <div  class="tpbut three minicart-li">
					        <a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="minicart-nav">
						        <i class="et-basket et"></i>
						        <span class="cart-items"><?php echo WC()->cart->cart_contents_count; ?></span>
						    </a>
					        <ul class="dropdown-menu">
								<li class="minicart-wrp">
								<?php
									if( function_exists( 'highstand_cart_func' ) ){
										echo '<div class="minicart-wrp">'.highstand_cart_func( array() ).'</div>';
									}
								?>
								</li>
							</ul>
				        </div>
				    <?php }
				    // End cart
				    ?>
	        	</div>
	      	</div>
	    </div>
	    <!-- end Navigation Menu -->

	</div>

</header>

