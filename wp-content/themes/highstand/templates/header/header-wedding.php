<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo.png, menu|menu
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');
	wp_enqueue_style('highstand-menu-wedding');

	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';

	$menu = !empty($atts['menu']) ? $atts['menu'] : '';

	if( empty($menu) || $menu == 'Select Menu' ) $menu = 'main-menu';
	$default_logo = get_template_directory_uri().'/assets/images/logo10.png';

?>

<header class="header header-wedding">
	<div class="container_full opstycky1">
		<div class="container">

			<a class="sb-toggle-<?php echo esc_attr( $position ); ?>" href="javascript:;" data-connection="navbar-collapse-1"><i class="fa fa-bars"></i></a>

			<!-- Logo -->
			<div class="logo">
				<a href="<?php echo esc_url(home_url('/')); ?>">
					<img src="<?php echo esc_url( isset($atts['logo'])?$atts['logo']:$default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
				</a>
			</div>

			<!-- Navigation Menu -->
			<div class="menu_main">
				<div class="navbar yamm navbar-default">
					<div id="navbar-collapse-1" class="navbar-collapse collapse pull-right menuopv1">
						<nav id="menu-onepage" class="nav-collapse">
							<?php $highstand->mainmenu( $menu ); ?>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>