<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo.png, menu|menu, donate_text|text|Donate Now, donate_link|text|#
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');
	wp_enqueue_style('highstand-menu-nonprofit');

	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';

	$menu = !empty($atts['menu']) ? $atts['menu'] : '';
	if(empty($menu) || $menu == 'Select Menu') $menu = 'main-menu';

	$default_logo = get_template_directory_uri().'/assets/images/logo11.png';
?>

<header class="header header-nonprofit">
	<div class="container">
		<a class="sb-toggle-<?php echo esc_attr( $position ); ?>" href="javascript:;" data-connection="navbar-collapse-1">
			<i class="fa fa-bars"></i>
		</a>

	    <!-- Logo -->
	    <div class="logo">
	    	<a href="<?php echo esc_url(home_url('/')); ?>" id="logo">
		    	<img src="<?php echo esc_url( isset( $atts['logo'] ) ? $atts['logo']:$default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
			</a>
		</div>

		<!-- Navigation Menu -->
	    <div class="menu_main">
	      	<div class="navbar yamm navbar-default">
	          	<div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">

	            	<nav>
	            		<?php $highstand->mainmenu( $menu ); ?>
	            	</nav>

	            	<?php if( isset( $atts['donate_text'] ) && !empty( $atts['donate_text'] ) ){ ?>
		          	<a href="<?php echo !empty( $atts['donate_link'] ) ? $atts['donate_text'] : '#'; ?>" class="buynowbut align-right"><?php echo esc_html( $atts['donate_text'] ); ?></a>
	            	<?php } ?>

	          	</div>
	      	</div>
	    </div>
	</div>
</header>
<div class="slidermar25"></div>