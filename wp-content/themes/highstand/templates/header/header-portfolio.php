<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo.png, menu|menu
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/


if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');
	wp_enqueue_style('highstand-menu-portfolio');

	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';

	$menu = !empty($atts['menu']) ? $atts['menu'] : '';
	if(empty($menu) || $menu == 'Select Menu') $menu = 'main-menu';

	$showCart = empty($highstand->cfg['topInfoCart']) ? $highstand->cfg['topInfoCart'] : 'show';
	$default_logo = get_template_directory_uri().'/assets/images/logo.png';
?>

<div class="container_full opstycky1 header-portfolio">
	<div class="container">
		<!-- Logo -->
		<div class="logoopv1">
			<a href="#home">
				<img src="<?php echo esc_url( isset($atts['logo'])?$atts['logo']:$default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
			</a>
		</div>

		<!-- Menu -->
		<div class="menuopv1">

			<a href="#" class="nav-toggle" aria-hidden="true"><span class="navtitle">Menu</span></a>

			<nav id="menu-onepage" class="nav-collapse nav-collapse-0 closed" aria-hidden="false" style="transition: max-height 284ms; position: relative;">
				<?php $highstand->mainmenu( $menu ); ?>
			</nav>

		</div><!-- end menu -->

	</div>
</div>