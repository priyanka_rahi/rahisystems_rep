<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo.png, menu|menu, email|text|contact@king-theme.com, phone|text|+1 123-456-7890, donate_text|text|Donate, donate_link|text|#, join_text|text|Join Volunteer, join_link|text|#
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');

	wp_enqueue_style('highstand-menu-political');

	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';

	$menu = !empty($atts['menu']) ? $atts['menu'] : '';
	if(empty($menu) || $menu == 'Select Menu') $menu = 'main-menu';

	$showCart = empty($highstand->cfg['topInfoCart']) ? $highstand->cfg['topInfoCart'] : 'show';

	$default_logo = get_template_directory_uri().'/assets/images/logo6.png';
?>

<div class="top_nav">
	<div class="container">

	    <div class="left">

	        <?php if(!empty( $atts['email'] )): ?>
	        	<a href="mailto:<?php echo esc_attr( $atts['email'] ); ?>"><i class="fa fa-envelope"></i>&nbsp; <?php echo esc_html( $atts['email'] ); ?></a>
	       	<?php endif; ?>

	       	<?php if(!empty( $atts['phone'] )): ?>
	       		<span class="h2_phone"><i class="fa fa-phone-square"></i>&nbsp; <?php echo esc_html( $atts['phone'] ); ?></span>
	       	<?php endif; ?>

	    </div><!-- end left -->

	    <div class="right">
		    <ul class="topsocial">
			    <?php
					if( isset( $atts['donate_text'] ) && !empty( $atts['donate_text'] ) ){
						echo '<li><a href="'.( !empty( $atts['donate_link'] )?esc_url($atts['donate_link']):'#' ).'">';
						echo esc_attr($atts['donate_text']);
						echo '</a></li>';
					}

					if( isset( $atts['join_text'] ) && !empty( $atts['join_text'] ) ){
						echo '<li><a href="'.( !empty( $atts['join_link'] )?esc_url($atts['join_link']):'#' ).'">';
						echo esc_attr($atts['join_text']);
						echo '</a></li>';
					}
				?>
	        </ul>
	    </div><!-- end right -->

	</div>
</div>

<header class="header header-political">

	<div class="container">

		<a class="sb-toggle-<?php echo esc_attr( $position ); ?>" href="javascript:;" data-connection="navbar-collapse-1">
			<i class="fa fa-bars"></i>
		</a>

		<!-- Cart button for responsive -->
		<?php if( !empty( $woocommerce ) ): ?>
		<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="minicart-reponsive minicart-reponsive-<?php echo ($position=='left')?'right':'left'; ?>">
			<i class="et-basket et"></i>
			<span class="cart-items"><?php echo WC()->cart->cart_contents_count; ?></span>
		</a>
		<?php endif; ?>

		 <!-- Logo -->
         <div class="logo">
		    <a href="<?php echo esc_url(home_url('/')); ?>" id="logo">
	    		<img src="<?php echo esc_url( isset($atts['logo'])?$atts['logo']:$default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
			</a>
	    </div>

		<!-- Navigation Menu -->
	    <div class="menu_main">
	      	<div class="navbar yamm navbar-default">
	      		<div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
	        		<nav>
	        			<?php

		        			$highstand->mainmenu( $menu );
		        			if( isset( $atts['donate_text'] ) && !empty( $atts['donate_text'] ) ){
								echo '<ul class="nav-button">';
								echo '<li><a href="'.( !empty( $atts['donate_link'] )?esc_url($atts['donate_link']):'#' ).'" class="buynowbut">';
								echo esc_attr($atts['donate_text']);
								echo '</a></li></ul>';
							}

						?>
	        		</nav>
	        	</div>
	      	</div>
	    </div>
	    <!-- end Navigation Menu -->

	</div>

</header>

<div class="clearfix slidermar23"></div>
