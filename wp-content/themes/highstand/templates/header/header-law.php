<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo.png, menu|menu, phone|text|(01) 123-456-7890, email|text|contact@king-theme.com, show_icon|boolean|yes
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');

	wp_enqueue_style('highstand-menu-law');

	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';

	$menu = !empty($atts['menu']) ? $atts['menu'] : '';
	if(empty($menu) || $menu == 'Select Menu') $menu = 'main-menu';

	$default_logo = get_template_directory_uri().'/assets/images/logo14.png';
?>

<div class="top_header">
	<div class="container">
		<div class="left">
			<a class="sb-toggle-<?php echo esc_attr( $position ); ?>" href="javascript:;" data-connection="navbar-collapse-1"><i class="fa fa-bars"></i></a>
			<!-- Logo -->
			<div class="logo">
				<a href="<?php echo esc_url(home_url('/')); ?>" id="logo">
					<img src="<?php echo esc_url( isset($atts['logo'])?$atts['logo']:$default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
				</a>
			</div>
		</div><!-- end left -->

		<div class="right">
			<?php
				if ( !empty( $atts['phone'] ) ) {
					echo esc_html( $atts['phone'] );
				}
			?>
			<?php if(!empty( $atts['email'] )): ?>
				<a href="mailto:<?php echo esc_attr( $atts['email'] ); ?>" class="email"><?php echo esc_html( $atts['email'] ); ?></a>
			<?php endif; ?>
			<?php
				if( isset( $atts['show_icon'] ) && $atts['show_icon'] == 'yes'){
					$highstand->socials('social');
				}
			?>
		</div><!-- end right -->
	</div>
</div>

<header class="header header-law">

	<div class="container">

		<!-- Navigation Menu -->
		<div class="menu_main_full">
			<div class="navbar yamm navbar-default">
				<div id="navbar-collapse-1" class="navbar-collapse collapse">
					<nav>
						<?php $highstand->mainmenu( $menu ); ?>
					</nav>
				</div>
			</div>
		</div>
		<!-- end Navigation Menu -->

	</div>

</header>

<div class="clearfix"></div>