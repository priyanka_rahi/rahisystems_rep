<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo.png, menu|menu, phone|text|Call: 123-456-7890, top_links|textarea|Enquire online&#124;#\nBecome an Instructor&#124;#\nSign in&#124;?action=login\nRegister&#124;?action=register
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');

	wp_enqueue_style('highstand-menu-education');

	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';

	$menu = !empty($atts['menu']) ? $atts['menu'] : '';
	if(empty($menu) || $menu == 'Select Menu') $menu = 'main-menu';

	$showCart = empty($highstand->cfg['topInfoCart']) ? $highstand->cfg['topInfoCart'] : 'show';
	$default_logo = get_template_directory_uri().'/assets/images/logo8.png';
?>
<div class="top_nav">

	<ul class="toplinks">
		<?php if ( !empty( $atts['phone'] ) ): ?>
			<li><strong><?php echo esc_html( $atts['phone'] ); ?></strong></li>
		<?php endif ?>
		<?php if ( !empty( $atts['top_links'] ) ): ?>
			<?php
				$top_arr = explode( "\n", $atts['top_links'] );
				if ( count( $top_arr ) ) {
					foreach ($top_arr as $top_val) {
						if ( !empty( $top_val ) ) {
							$top_str = explode( "|", $top_val );
							if ( isset( $top_str[1] ) ) {
								echo '<li><a href="'. esc_url( $top_str['1'] ) .'">' . $top_str[0] . '</a></li>';
							} else {
								echo '<li><strong>'. $top_str[0] .'</strong></li>';
							}
						}
					}
				}
			?>

		<?php endif ?>
	</ul>

</div>


<header class="header header-education">

	<div class="container_full_menu">

		<a class="sb-toggle-<?php echo esc_attr( $position ); ?>" href="javascript:;" data-connection="navbar-collapse-1"><i class="fa fa-bars"></i></a>

		<!-- Cart button for responsive -->
	    <?php if( !empty( $woocommerce ) ): ?>
	   	<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="minicart-reponsive minicart-reponsive-<?php echo ($position=='left')?'right':'left'; ?>">
	        <i class="et-basket et"></i>
	        <span class="cart-items"><?php echo WC()->cart->cart_contents_count; ?></span>
	    </a>
		<?php endif; ?>

		 <!-- Logo -->
         <div class="logo">
		    <a href="<?php echo esc_url(home_url('/')); ?>" id="logo">
	    		<img src="<?php echo esc_url( isset($atts['logo'])?$atts['logo']:$default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
			</a>
	    </div>

		<!-- Navigation Menu -->
	    <div class="menu_main">
	      	<div class="navbar yamm navbar-default">
	      		<div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
	        		<nav <?php if( !empty( $woocommerce ) && $showCart == 'show' ) echo 'class="nav_has_cart"'; ?>>
	        			<?php $highstand->mainmenu( $menu ); ?>
	        		</nav>

	        		<?php
		            	//Start cart
				    	if( !empty( $woocommerce ) && $showCart == 'show' ){

					?>
				        <div  class="tpbut three minicart-li">
					        <a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="minicart-nav">
						        <i class="et-basket et"></i>
						        <span class="cart-items"><?php echo WC()->cart->cart_contents_count; ?></span>
						    </a>
					        <ul class="dropdown-menu">
								<li class="minicart-wrp">
								<?php
									if( function_exists( 'highstand_cart_func' ) ){
										echo '<div class="minicart-wrp">'.highstand_cart_func( array() ).'</div>';
									}
								?>
								</li>
							</ul>
				        </div>
				    <?php }
				    // End cart
				    ?>
	        	</div>
	      	</div>
	    </div>
	    <!-- end Navigation Menu -->

	</div>

</header>

<div class="clearfix"></div>