<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo3.png, show_icon|boolean|yes, phone|text|(01) 123-456-7890, email|text|contact@king-theme.com
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe('highstand');
	wp_enqueue_style('highstand-menu-onepage');
	wp_enqueue_script('neo-pager');

	$default_logo = get_template_directory_uri().'/assets/images/logo3.png';
?>


<div id="page_1" class="topheader block1 o-page" data-hint-text="Home">
	<div class="container">

		<div class="left">
			<?php
				if( isset( $atts['show_icon'] ) && $atts['show_icon'] == 'yes'){
					$highstand->socials('social_links');
				}
			?>
	    </div>

		<div class="logo">
			<img src="<?php echo esc_url( isset($atts['logo'])? $atts['logo']: $default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
		</div>

		<div class="right">
			<?php if ( isset( $atts['phone'] ) ): ?>
				<i class="fa fa-mobile"></i>&nbsp; <?php echo esc_html( $atts['phone'] ); ?>
			<?php endif ?>
			<?php if(!empty( $atts['email'] )): ?>
				<a href="mailto:<?php echo esc_attr( $atts['email'] ); ?>" class="email"><i class="fa fa-envelope"></i> <?php echo esc_html( $atts['email'] ); ?></a>
			<?php endif; ?>
		</div>

	</div>
</div>