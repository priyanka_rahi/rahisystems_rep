<?php
/**
*
*	Author: King-Theme.com
*	Package: Templates system by King-Theme
*	Version: 1.0
*
*	Positions: logo|upload|%SITE_URL%/wp-content/themes/highstand/assets/images/logo9.png, menu|menu, main_slider|text|[masterslider alias="ms-demo-musicband"], show_icon|boolean|yes, facebook_link|text|#, twitter_link|text|#, google_plus_link|text|#, linkedin_link|text|, pinterest_link|text|
*
*	Register position_id and field_type to be add content from admin panel. ( text| textarea | upload | menu..etc.. )
*	All settings from backend will be return to varible $args
*	This file has been preloaded, so you can wp_enqueue_style to out in wp_head();
*
*/

	if ( ! defined( 'ABSPATH' ) )
		exit; // Exit if accessed directly

	$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');
	wp_enqueue_style('highstand-menu-music');

	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';

	$menu = !empty($atts['menu']) ? $atts['menu'] : '';

	if( empty($menu) || $menu == 'Select Menu' ) $menu = 'main-menu';

	$default_logo = get_template_directory_uri().'/assets/images/logo9.png';

?>

<?php if ( !empty( $atts['main_slider'] ) ): ?>

	<section id="home" data-anchor="home">
		<?php echo do_shortcode( $atts['main_slider'] ); ?>
	</section>

<?php endif ?>

<header class="header header-music" data-compact="780">
	<nav class="header header-music">
		<div class="container_full">

			<a class="sb-toggle-<?php echo esc_attr( $position ); ?>" href="javascript:;" data-connection="navbar-collapse-1"><i class="fa fa-bars"></i></a>

			<!-- Logo -->
			<div class="logo">
				<a href="<?php echo esc_url(home_url('/')); ?>">
					<img src="<?php echo esc_url( isset($atts['logo'])?$atts['logo']:$default_logo ); ?>" alt="<?php bloginfo('description'); ?>" />
				</a>
			</div>

			<!-- Navigation Menu -->
			<div class="menu_main">
				<div class="navbar yamm navbar-default">
					<div id="navbar-collapse-1" class="navbar-collapse collapse pull-right menuopv1">
						<div id="menu-onepage" class="nav-collapse">
							<?php $highstand->mainmenu( $menu ); ?>
						</div>

						<?php if ( $atts['show_icon'] = 'yes' ): ?>
							<ul>
								<?php if ( isset( $atts['facebook_link'] ) && !empty( $atts['facebook_link'] ) ): ?>
									<li><a href="<?php echo esc_url( $atts['facebook_link'] ); ?>" class="social"><i class="fa fa-facebook"></i></a></li>
								<?php endif ?>
								<?php if ( isset( $atts['twitter_link'] ) && !empty( $atts['twitter_link'] ) ): ?>
									<li><a href="<?php echo esc_url( $atts['twitter_link'] ); ?>" class="social"><i class="fa fa-twitter"></i></a></li>
								<?php endif ?>
								<?php if ( isset( $atts['google_plus_link'] ) && !empty( $atts['google_plus_link'] ) ): ?>
									<li><a href="<?php echo esc_url( $atts['google_plus_link'] ); ?>" class="social"><i class="fa fa-google-plus"></i></a></li>
								<?php endif ?>
								<?php if ( isset( $atts['linkedin_link'] ) && !empty( $atts['linkedin_link'] ) ): ?>
									<li><a href="<?php echo esc_url( $atts['linkedin_link'] ); ?>" class="social"><i class="fa fa-linkedin"></i></a></li>
								<?php endif ?>
								<?php if ( isset( $atts['pinterest_link'] ) && !empty( $atts['pinterest_link'] ) ): ?>
									<li><a href="<?php echo esc_url( $atts['pinterest_link'] ); ?>" class="social"><i class="fa fa-pinterest"></i></a></li>
								<?php endif ?>
							</ul>
						<?php endif ?>
					</div>
				</div>
			</div>

		</div>
	</nav>
</header>
