<?php
/**
 * (c) www.king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$post = highstand::globe('post');
$highstand = highstand::globe('highstand');
$more = highstand::globe('more');

get_header();

?>

<div id="breadcrumb" class="page_title2">
	<div class="container">
		<h1><?php highstand_title(); ?></h1>
		<div class="pagenation">&nbsp;<?php $highstand->breadcrumb(); ?></div>
	</div>
</div>

<div id="primary" class="site-content">
	<div id="content" class="container">
		<div class="entry-content blog_postcontent">
			<div class="margin_top1"></div>
			<?php locate_template( 'templates'.DS.'register.php', true ); ?>
			<div class="margin_top6"></div>
		</div>
	</div>
</div>



<?php get_footer(); ?>