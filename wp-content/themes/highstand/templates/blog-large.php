<?php
/*--------------------------------------
 #		(c) king-theme.com
 -----------------------------------*/

get_header(); ?>

	<?php highstand::path( 'blog_breadcrumb' ); ?>

	<div id="primary" class="content_fullwidth">
		<div id="content" class="container">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'templates/blog/content', 'large' ); ?>

			<?php endwhile; // end of the loop. ?>

			<?php $highstand->pagination(); ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>