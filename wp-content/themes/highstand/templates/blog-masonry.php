<?php
	/**
	*
	* @author king-theme.com
	*
	*/

	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	$post = highstand::globe('post');
	$more = highstand::globe('more');
	$highstand = highstand::globe();

	function highstand_masonry_assets() {
		$css_dir = THEME_URI.'/assets/css/';
		$js_dir = THEME_URI.'/assets/js/';

		wp_enqueue_style('highstand-cubeportfolio-min', $js_dir.'cubeportfolio/cubeportfolio.min.css', false, THEME_VERSION );
		wp_enqueue_script('highstand-cubeportfolio', highstand_child_theme_enqueue( $js_dir.'cubeportfolio/js/jquery.cubeportfolio.min.js' ), array( 'jquery' ), THEME_VERSION, true );
		wp_enqueue_script('highstand-cubeportfolio-main', highstand_child_theme_enqueue( $js_dir.'cubeportfolio/main.js' ), array( 'jquery' ), THEME_VERSION, true );
	}
	add_action('wp_print_styles', 'highstand_masonry_assets');

	get_header();

?>

<?php highstand::path( 'blog_breadcrumb' ); ?>

<div class="content_fullwidth">
	<div class="container">
		<div id="grid-masonry-container" class="cbp-l-grid-masonry-projects" data-cols="4" data-gap="30|10">
			<?php

				while ( have_posts() ) : the_post();
					
					$rand = rand(0,10);
					$cap = 'two';
					$heighClass = ' cbp-l-grid-masonry-height4';
					if( $rand <= 5 ){
						$height = 500;
						$heighClass = ' cbp-l-grid-masonry-height3';
						$cap = 'three';
					}else{
						$height = $rand*100;
					}

					$cats = get_the_category( $post->ID );
					$catsx = array();
					for( $i=0; $i<2; $i++ ){
						if( !empty($cats[$i]) ){
							array_push($catsx, $cats[$i]->name);
						}
					}

					$post_custom_field = get_post_meta($post->ID , '_'.THEME_OPTNAME.'_post_meta_options', true);
			 ?>
			<div class="cbp-item<?php echo esc_attr( $heighClass ); ?>">
			   <div class="cbp-caption">
					<div class="cbp-caption-defaultWrap <?php echo esc_attr( $cap ); ?>">
						 <a href="<?php echo get_permalink( $post->ID ); ?>">

						 	<?php if(!empty($post_custom_field['feature_video'])): ?>
					    		<?php 
					    			echo preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<ifr"."ame width=\"556\" height=\"247\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></ifr"."ame>", $post_custom_field['feature_video'] );
					    		?>
					    	<?php else: ?>

					    		<?php

									$img = $highstand->get_featured_image( $post, true );
									if( !empty( $img ) )
									{
										if( strpos( $img , 'youtube') !== false )
										{
											$img = THEME_URI.'/assets/images/default.jpg';
										}
										$img = highstand_createLinkImage( $img, '570x'.$height.'xc' );

										echo '<img alt="'.get_the_title().'" class="featured-image" src="'.$img.'" />';
									}

								?>

					    	<?php endif; ?>
							
						 </a>
					</div>					
			   </div>

			   <a href="<?php echo get_permalink( $post->ID ); ?>" class="cbp-l-grid-masonry-projects-title"><?php echo wp_trim_words( $post->post_title, 4 ); ?></a>
				<div class="cbp-l-grid-masonry-projects-desc"><?php echo implode( ' / ', $catsx ); ?></div>

			</div>
			<?php endwhile; ?>

		  </div><!-- #grid-container -->
		  	
		  	<div id="loadMore-container" class="cbp-l-loadMore-button hiddenf">
				<a href="#" class="cbp-l-loadMore-link" data-offset="0">LOAD MORE</a>
			</div>
	</div>
</div><!-- #primary -->

<?php get_footer(); ?>