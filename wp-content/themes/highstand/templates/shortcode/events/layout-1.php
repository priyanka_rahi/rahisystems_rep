<?php

$highstand = highstand::globe('highstand');

$events_posts = $atts['events_posts'];

$main_class = array( 'events-layout-1' );

if ( !empty( $atts['custom_class'] ) ) {
	$main_class[] = $atts['custom_class'];
}

if ( count($events_posts) > 0  ){
?>
	<div class="<?php echo esc_attr( implode( " ", $main_class ) ); ?>">
	<?php
		$i = 1;
		foreach( $events_posts as $event_post ):

			$options       = get_post_meta( $event_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
			$words         = !empty($words) ? $words : 30;
			$thumbnail_url = $highstand->get_featured_image( $event_post );
			$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '336x180xct' );

	?>

			<?php //if ( $i%2 == 1 ): ?>

				<div class="box">
					<div class="left"><img src="<?php echo esc_url( $thumbnail_url ); ?>" alt=""></div>
					<div class="right">
						<div class="date"><?php echo esc_attr( $options['events_date'] ); ?></div>
						<ul class="content post_con">
						
							<?php if ( !empty( $options['events_map_title'] ) ): ?>
								<li class="post_con"><a href="<?php echo esc_url( $options['events_link'] ); ?>"><i class="fa fa-map-marker"></i> <?php echo esc_attr( $options['events_map_title'] ); ?></a></li>
							<?php endif ?>
							<?php if ( !empty( $options['events_datetime'] ) ): ?>
								<li class="post_con"><a href="<?php echo esc_url( $options['events_link'] ); ?>"><i class="fa fa-clock-o "></i> <?php echo esc_attr( $options['events_datetime'] ); ?></a></li>
							<?php endif ?>
						</ul>
						<div class="clearfix margin_bottom1"></div>
						<div class="title"><h4 class="caps"><?php echo get_the_title($event_post->ID);  ?></h4></div>
						<p><?php echo wp_trim_words( $event_post->post_content, $words ); ?></p>
					</div>
				</div>

			<?php //else: ?>

				<!--
				<div class="box2">
					<div class="left">
						<div class="date"><?php echo esc_attr( $options['events_date'] ); ?></div>
						<ul class="content post_con">
							<li class="post_con"><h4 class="caps"><?php the_title( ); ?></h4></li>
							<?php if ( !empty( $options['events_map_title'] ) ): ?>
								<li class="post_con"><a href="<?php echo esc_url( $options['events_link'] ); ?>"><i class="fa fa-map-marker"></i> <?php echo esc_attr( $options['events_map_title'] ); ?></a></li>
							<?php endif ?>
							<?php if ( !empty( $options['events_datetime'] ) ): ?>
								<li class="post_con"><a href="<?php echo esc_url( $options['events_link'] ); ?>"><i class="fa fa-clock-o "></i> <?php echo esc_attr( $options['events_datetime'] ); ?></a></li>
							<?php endif ?>
						</ul>
						<div class="clearfix margin_bottom1"></div>
						<p><?php echo wp_trim_words( $event_post->post_content, $words ); ?></p>
					</div>
					<div class="right"><img src="<?php echo esc_url( $thumbnail_url ); ?>" alt=""></div>
				</div>
				-->
			<?php //endif ?>

	<?php
			$i++;
		endforeach;
	?>
	</div>
<?php

} else {
	echo '<h4>' . esc_html__( 'Events not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=events').'"><i class="fa fa-plus"></i> Add New Events</a>';
}
