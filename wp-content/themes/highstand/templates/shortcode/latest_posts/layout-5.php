<?php

$highstand = highstand::globe('highstand');

$class       = $custom_class = '';
$css_class   = array( 'last-post-layout-5');

extract( $atts );

array_push( $css_class, $custom_class );

if( isset( $class ) )
	array_push( $css_class, $class );

?>
<div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
	<?php
		$dem = 1;
		if( count( $posts ) ) {
			foreach( $posts as $item ) {
				$post_title = get_the_title( $item->ID );
				$img        = $highstand->get_featured_image( $item );
				$dem_loop	= $dem%3;

				switch ( $dem_loop ) {
					case '1':
						$class_align = 'left';
						$img_url     = highstand_createLinkImage( $img, '300x500xct' );
					break;
					case '2':
						$class_align = 'right';
						$img_url     = highstand_createLinkImage( $img, '293x250xct' );
					break;
					default:
						$class_align = '';
						$img_url     = highstand_createLinkImage( $img, '293x250xct' );
					break;
				}
	?>
				<?php if ( $dem%3==1 || $dem%3==2 ): ?>
					<div class="<?php echo esc_attr( $class_align ); ?>" >
				<?php endif ?>

						<?php switch ( $dem_loop ) {
							case '1':
						?>
								<div class="imgse"><img src="<?php echo esc_url( $img_url ); ?>" alt=""></div>
								<div class="arrow_box">
									<?php if ( $atts['show_date'] == 'show' ): ?>
										<em class="light"><?php echo get_the_date( 'M d, Y', $item->ID ); ?></em>
									<?php endif ?>
									<h5 class="medium"><?php echo esc_attr( $post_title ); ?></h5>
									<p><?php echo wp_trim_words( $item->post_content, 24 ); ?></p>
									<div class="clearfix"></div>
									<div class="margin_top3"></div>
									<a href="<?php the_permalink( $item->ID ); ?>" class="res_button2"><?php echo esc_html__( 'Read More', 'highstand' ); ?></a>
								</div>
						<?php
							break;
							case '2':
						?>
								<div class="box">
									<div class="imgse"><img src="<?php echo esc_url( $img_url ); ?>" alt=""></div>
									<div class="arrow_box">
										<?php if ( $atts['show_date'] == 'show' ): ?>
											<em class="light"><?php echo get_the_date( 'M d, Y', $item->ID ); ?></em>
										<?php endif ?>
										<h5 class="medium"><?php echo esc_attr( $post_title ); ?></h5>
										<div class="clearfix"></div>
										<div class="margin_top2"></div>
										<a href="<?php the_permalink( $item->ID ); ?>" class="res_button2"><?php echo esc_html__( 'Read More', 'highstand' ); ?></a>
									</div>
								</div>
						<?php
							break;
							default:
						?>
								<div class="box two">
									<div class="arrow_box">
										<?php if ( $atts['show_date'] == 'show' ): ?>
											<em class="light"><?php echo get_the_date( 'M d, Y', $item->ID ); ?></em>
										<?php endif ?>
										<h5 class="medium white"><?php echo esc_attr( $post_title ); ?></h5>
										<div class="clearfix"></div>
										<div class="margin_top2"></div>
										<a href="<?php the_permalink( $item->ID ); ?>" class="res_button3"><?php echo esc_html__( 'Read More', 'highstand' ); ?></a>
									</div>
									<div class="imgse"><img src="<?php echo esc_url( $img_url ); ?>" alt=""></div>
								</div>
						<?php
							break;
						} ?>

				<?php if ( $dem%3 ==0 || $dem%3 ==1 || $dem==count( $posts ) ): ?>
					</div>
				<?php endif ?>
	<?php
				$dem++;
			}
		} else {
			echo esc_html__( 'Pots not found.', 'highstand' );
		}
	?>
</div>