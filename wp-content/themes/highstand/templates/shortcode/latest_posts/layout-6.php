<?php

$highstand = highstand::globe('highstand');

$class       = $custom_class = '';
$css_class   = array( 'owl-carousel', 'last-post-layout-4', 'poli_feature_owl ');

extract( $atts );

array_push( $css_class, $custom_class );

if( isset( $class ) )
	array_push( $css_class, $class );

?>

 <div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
	<?php
		$dem = 1;
		if( count( $posts ) ) {
			foreach( $posts as $item ) {
				$post_title = get_the_title( $item->ID );
				$img        = $highstand->get_featured_image( $item );
				$img_url     = highstand_createLinkImage( $img, '370x247xct' );
	?>
	<div class="item">
		<img src="<?php echo esc_url( $img_url ); ?>" alt="" class="rimg"/>
		<div class="box">
			<h4 class="white caps"><?php echo esc_attr( $post_title ); ?></h4>
			<?php if ( $atts['show_date'] == 'show' ): ?>
			<a href="<?php the_permalink( $item->ID ); ?>" class="date">
				<?php echo get_the_date( 'M d, Y', $item->ID ); ?>
			</a>
			<?php endif ?>
		</div>
		<!-- end section  -->
	</div>
	<?php
			}
		} else {
			echo esc_html__( 'Pots not found.', 'highstand' );
		}
	?>
</div>
