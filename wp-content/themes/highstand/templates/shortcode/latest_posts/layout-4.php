<?php

$highstand = highstand::globe('highstand');

$class       = $custom_class = '';
$css_class   = array( 'last-post-layout-4');
$css_class[] = 'owl-carousel';
$css_class[] = 'nomg';

extract( $atts );

array_push( $css_class, $custom_class );

if( isset( $class ) )
	array_push( $css_class, $class );

?>
<div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
	<?php
		if( count( $posts ) ) {
			foreach( $posts as $item ) {
				$post_title = get_the_title( $item->ID );
				$img        = $highstand->get_featured_image( $item );
				$img_url    = highstand_createLinkImage( $img, '636x447xct' );
	?>
				<div class="item">
					<a href="<?php the_permalink( $item->ID ); ?>">
						<div class="imgbox1">
							<img src="<?php echo esc_url( $img_url ); ?>" class="rimg" alt=""/>
							<div class="con">
								<?php echo esc_attr( $post_title ); ?>
								<?php if ( $atts['show_date'] == 'show' ): ?>
									<p class="white"><?php echo get_the_date( 'd M, Y', $item->ID ); ?></p>
								<?php endif ?>
							</div>
						</div>
					</a>
				</div><!-- end item -->
	<?php
			}
		} else {
			echo esc_html__( 'Pots not found.', 'highstand' );
		}
	?>
</div>