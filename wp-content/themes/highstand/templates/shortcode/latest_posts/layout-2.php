<?php

$highstand = highstand::globe('highstand');

$class       = $custom_class = '';
$css_class   = array( 'last-post-layout-2');
$big_items   = array(4); //Item need to diplay bigger other ones
$noimg_items = array(2); //display items wihout image featured

extract( $atts );

array_push( $css_class, $custom_class );

if( !empty( $class ) )
	array_push( $css_class, $class );
	
?>
<div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
	<?php
		if( count($posts) ) {
			$i = 1;
			foreach( $posts as $item ) {
				$post_title = get_the_title( $item->ID );
				$img        = $highstand->get_featured_image( $item );
				$img_url    = highstand_createLinkImage( $img, '374x200xct' );
				$item_class = 'one_third_less';

				if( $i == 3 ) {
					$item_class .= ' last';
				}
	?>
			<div class="<?php echo esc_attr( $item_class ) ;?>">
				<?php if ( has_post_thumbnail( $item->ID ) ): ?>
					<img src="<?php echo esc_url( $img_url ); ?>" class="rimg" alt="">
				<?php endif ?>
				<div class="box">
					<div class="boxcon">
						<h5><a href="<?php echo get_permalink( $item->ID ) ;?>"><?php echo esc_attr( $post_title ); ?></a></h5>
						<p><?php echo wp_trim_words( $item->post_content, 25 ); ?></p>
					</div>
					<div class="conbtm">
						<ul class="links_small">
							<?php if ( $show_date == 'show' ): ?>
								<li class="content"><?php echo get_the_date( 'd F Y', $item->ID ); ?></li>
							<?php endif ?>
							<li class="icon"><a href="<?php echo get_comments_link( $item->ID ) ?>">
								<i class="fa fa-comments"></i> <?php echo get_comments_number( $item->ID ); ?>
							</a></li>
							<?php if (function_exists('getPostLikeLink')) { ?>
								<li class="icon likes">
									<?php echo getPostLikeLink( $item->ID ); ?>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>

			<?php if ( $i%3 == 0 && count($posts) > 3 ): ?>
				<div class="clearfix margin_bottom5"></div>
			<?php endif ?>
	<?php
				$i++;
			}
		} else {
			echo esc_html__( 'Pots not found.', 'highstand' );
		}
	?>
</div>