<?php

$highstand = highstand::globe('highstand');

$class       = $custom_class = '';
$css_class   = array( 'last-post-layout-7' );

extract( $atts );

if ( !empty( $custom_class ) ) {
	array_push( $css_class, $custom_class );
}

if( isset( $class ) && !empty( $class ) )
	array_push( $css_class, $class );

?>

 <div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
	<?php
		$dem = 1;
		if( count( $posts ) ) {
			foreach( $posts as $item ) {
				$post_title = get_the_title( $item->ID );
				$img        = $highstand->get_featured_image( $item );
				$kt_dem		= $dem%5;

				switch ( $kt_dem ) {
					case '1':
						$img_url = highstand_createLinkImage( $img, '570x415xct' );
						$class_box = 'one_half_less';
						$class_date = '';
					break;
					case '2':
						$img_url = highstand_createLinkImage( $img, '285x200xct' );
						$class_box = 'one_fourth_less';
						$class_date = 'box';
					break;
					case '3':
						$img_url = highstand_createLinkImage( $img, '285x200xct' );
						$class_box = 'one_fourth_less last';
						$class_date = 'box';
					break;
					case '4':
						$img_url = highstand_createLinkImage( $img, '285x200xct' );
						$class_box = 'one_fourth_less';
						$class_date = 'box';
					break;
					default:
						$img_url = highstand_createLinkImage( $img, '285x200xct' );
						$class_box = 'one_fourth_less last';
						$class_date = 'box';
					break;
				}
	?>

				<div class="<?php echo esc_attr( $class_box ); ?>">
					<a href="<?php the_permalink( $item->ID ); ?>">
						<img src="<?php echo esc_url( $img_url ); ?>" class="rimg" alt=""/>
						<?php if ( $atts['show_date']  == 'show' ): ?>
							<div class="datecont<?php echo esc_attr( $class_date ); ?>"><?php echo get_the_date( 'd', $item->ID ); ?> <br/><b class="white"><?php echo get_the_date( 'M, Y', $item->ID ); ?></b></div>
						<?php endif ?>
						<div class="bcont<?php echo esc_attr( $class_date ); ?>">
							<h3><?php echo esc_attr( $post_title ); ?></h3>
						</div>
					</a>
				</div>

				<?php if ( $dem%5 == 0 && count( $posts ) > $dem ): ?>
					<div class="clearfix margin_bottom5"></div>
				<?php endif ?>

	<?php
				$dem++;
			}
		} else {
			echo esc_html__( 'Pots not found.', 'highstand' );
		}
	?>
</div>