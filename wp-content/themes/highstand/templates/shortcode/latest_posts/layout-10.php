<?php

$highstand = highstand::globe('highstand');

$class       = $custom_class = '';
$css_class   = array( 'last-post-layout-10', 'owl-carousel', 'nomg', 'left' );

extract( $atts );

if ( !empty( $custom_class ) ) {
	array_push( $css_class, $custom_class );
}

if( isset( $class ) && !empty( $class ) )
	array_push( $css_class, $class );

if( count( $posts ) ) {
?>

	<div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
		<?php
			foreach( $posts as $item ) {

				$img     = $highstand->get_featured_image( $item );
				$img_url = highstand_createLinkImage( $img, '570x280xct' );

		?>

			<div class="slidesec">
				<img src="<?php echo esc_url( $img_url ); ?>" alt="" />
				<div class="cont">
					<?php if ( $atts['show_date']  == 'show' ): ?>
						<em><?php echo get_the_date( 'M d, Y', $item->ID ); ?></em>
					<?php endif ?>
					<h5><a href="<?php echo get_permalink( $item->ID ) ;?>"><?php echo get_the_title( $item->ID ); ?></a></h5>
					<p><?php echo wp_trim_words( $item->post_content, 11 ); ?></p>
				</div>
			</div>

		<?php } ?>
	</div>

<?php
} else {
	echo esc_html__( 'Pots not found.', 'highstand' );
}