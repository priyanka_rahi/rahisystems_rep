<?php

$highstand = highstand::globe('highstand');

$class       = $custom_class = '';
$css_class   = array( 'last-post-layout-9');
$css_class[] = 'owl-carousel';

extract( $atts );

array_push( $css_class, $custom_class );

if( isset( $class ) )
	array_push( $css_class, $class );

?>
<div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
	<?php
		if( count( $posts ) ) {
			foreach( $posts as $item ) {
				$post_title = get_the_title( $item->ID );
				$img        = $highstand->get_featured_image( $item );
				$img_url    = highstand_createLinkImage( $img, '573x320xct' );
				$year       = get_the_time('Y');
				$month      = get_the_time('m');
				$day        = get_the_time('d');
	?>
				<div class="item">
					<a href="<?php the_permalink( $item->ID ); ?>">
						<img src="<?php echo esc_url( $img_url ); ?>" alt="" class="rimg" />
					</a>
					<div class="content">
						<h5 ><?php echo esc_attr( $post_title ); ?></h5>
						<?php if ( $atts['show_date'] == 'show' ): ?>
							<a href="<?php echo get_day_link( $year, $month, $day ); ?>" class="befor"><?php echo get_the_date( 'M d, Y', $item->ID ); ?></a>
						<?php endif ?>
						<a href="<?php echo get_comments_link( $item->ID ); ?>" class="after"><?php comments_number( esc_html__('0 Comment','highstand'), esc_html__('1 Comment','highstand'), esc_html__('% Comments','highstand') ); ?></a>
					</div>
				</div>
	<?php
			}
		} else {
			echo esc_html__( 'Pots not found.', 'highstand' );
		}
	?>
</div>