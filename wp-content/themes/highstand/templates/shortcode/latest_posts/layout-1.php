<?php
	
$highstand = highstand::globe('highstand');
$highstand_sc_css = highstand::globe('highstand_sc_css');

$class       = $custom_class = '';
$css_class   = array( 'hs_latest_posts');

extract( $atts );

array_push( $css_class, $custom_class );

if( isset( $class ) )
	array_push( $css_class, $class );

?>
<div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
	<?php if( count($posts) ) { ?>
		<?php
			$i = 1;
			foreach( $posts as $item ) {

				$post_title = get_the_title( $item->ID );

				switch ( $i%5 ) {
					case '1':
						$class_col = 'one_third_less';
						$class_box = 'box01';
						$img_size  = '374x200xct';
					break;
					case '2':
						$class_col = 'one_third_less';
						$class_box = 'box02';
						$img_size  = '374x200xct';
					break;
					case '3':
						$class_col = 'one_third_less last';
						$class_box = 'box01';
						$img_size  = '374x200xct';
					break;
					case '4':
						$class_col = 'two_third_less';
						$class_box = 'box03';
						$img_size  = '772x331xct';
					break;
					default:
						$class_col = 'one_third_less last';
						$class_box = 'box01';
						$img_size  = '374x200xct';
					break;
				}

				$img        = $highstand->get_featured_image( $item );
				$img_url    = highstand_createLinkImage( $img, $img_size );
		?>

				<div class="<?php echo esc_attr( $class_col ) ;?>">
					<a href="<?php echo get_permalink( $item->ID ) ;?>" title="<?php echo esc_attr( $post_title ) ;?>">
						<div class="<?php echo esc_attr( $class_box ); ?>">

							<?php if ( $i%5 != 2 ): ?>
								<img src="<?php echo esc_attr( $img_url ) ;?>" class="rimg" alt="<?php echo esc_attr( $post_title ) ;?>" />
							<?php endif ?>

							<?php switch ($i%5) {
								case '2':
							?>
									<div>
										<h3><?php echo esc_attr( $post_title ) ;?></h3>
										<?php if( $show_date == 'show' ): ?>
											<em><?php echo get_the_date( 'F j, Y', $item->ID ) ;?></em>
										<?php endif;?>
									</div>
							<?php
								break;
								case '4':
							?>
									<div class="content">
										<h1 class="white"><?php echo esc_attr( $post_title ) ;?></h1>
										<?php if( $show_date == 'show' ): ?>
											<b class="white"><?php echo get_the_date( 'F j, Y', $item->ID ) ;?></b>
										<?php endif;?>
									</div>
							<?php
								break;
								default:
							?>
									<h4><?php echo esc_attr( $post_title ) ;?></h4>
									<?php if( $show_date == 'show' ): ?>
										<b><?php echo get_the_date( 'F j, Y', $item->ID ) ;?></b>
									<?php endif;?>
							<?php
								break;
							} ?>

						</div>
					</a>
				</div>

				<?php if( $i%5 == 3 || ( $i%5 == 0 && count( $posts ) > 5 ) ) :?>
					<div class="clearfix margin_bottom2"></div>
				<?php endif; ?>
	<?php
				$i++;
			}
		} else {
			echo esc_html__( 'Pots not found.', 'highstand' );
		}
	?>
</div>
<?php

