<?php

$highstand = highstand::globe('highstand');

$class       = $custom_class = '';
$css_class   = array( 'last-post-layout-8' );

extract( $atts );

if ( !empty( $custom_class ) ) {
	array_push( $css_class, $custom_class );
}

if( isset( $class ) && !empty( $class ) )
	array_push( $css_class, $class );

?>

 <div class="<?php echo esc_attr( implode( ' ', $css_class ) ) ;?>">
	<?php

		$i = 1;
		if( count( $posts ) ) {

			foreach( $posts as $item ) {

				$img        = $highstand->get_featured_image( $item );
				$img_url = highstand_createLinkImage( $img, '135x86xct' );

	?>

		<div class="isboxs<?php if( $i == count( $posts ) )echo ' last'; ?>">
			<span aria-hidden="true"><img src="<?php echo esc_url( $img_url ); ?>" alt=""></span>
	        <?php if ( $atts['show_date']  == 'show' ): ?>
					<b><?php echo get_the_date( 'M d, Y', $item->ID ); ?></b>
			<?php endif ?>
	        <p><?php echo wp_trim_words( $item->post_content, 15 ); ?></p>
	    </div>

	<?php
			$i++;

			}
		} else {
			echo esc_html__( 'Pots not found.', 'highstand' );
		}
	?>
</div>