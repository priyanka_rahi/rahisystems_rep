<?php

$post = highstand::globe('post');

extract($atts);


if( count($posts) ) {
	echo '<div class="latest-post-layout-3">';
	$dem = 1;
	foreach( $posts as $item ) {
		$post_title = get_the_title( $item->ID );

		if ( has_post_format( array( 'chat', 'status' ) ) )
			$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'highstand' );
		else
			$format_prefix = '%2$s';

		$date = sprintf( '%2$s',
			esc_attr( get_the_date( 'c' ) ),
			esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
		);
?>

		<?php if ( $atts['show_date'] != 'hide' ): ?>
			<b><a href="<?php echo esc_url( get_permalink( $item->ID ) ); ?>"><?php echo get_the_date( 'd F, Y', $item->ID ); ?></a></b>
		<?php endif ?>
		<p><?php echo wp_trim_words( strip_tags($item->post_content), 10, ' ...' ); ?></p>
		<?php if ( $dem < count( $posts ) ): ?>
			<div class="clearfix margin_bottom2"></div>
		<?php endif ?>

<?php
		$dem++;
	}
	echo '</div>';
}else{
	echo esc_html__( 'Pots not found.', 'highstand' );
}