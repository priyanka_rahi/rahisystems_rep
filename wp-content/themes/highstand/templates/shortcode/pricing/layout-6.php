<?php

$highstand_sc_css = highstand::globe('highstand_sc_css');
$prcs = $content;

extract( $atts );

if( count( $prcs  ) ) {

	$attribute_class = array();
	$custom_class = 'pricing-box-'.randomstring(10);
	$attribute_class[] = $custom_class;
	$attribute_class[] = 'pricing-layout-6';
	if( !empty( $atts['class'] ) ){
		$attribute_class[] = $atts['class'];
	}

	?>

		<div class="<?php echo esc_attr( implode( ' ', $attribute_class ) ); ?>">

			<?php
				$i = 0;
				foreach( $prcs as $prc ) {

					$i++;
					$pricing = get_post_meta( $prc->ID , '_highstand_post_meta_options' );

					$_pricing = array(
						'price' => '100',
						'per' => 'month',
						'attr' => "Option 1\nOption 2",
						'currency' => '$',
						'textsubmit' => 'Select Plan',
						'linksubmit' => '#'
					);
					
					if( !empty( $pricing ) ){
						$pricing  = array_merge( $_pricing, (array)$pricing[0] );
					} else {
						$pricing = $_pricing;
					}

					$col_class = array( 'pricing-tables' );
					switch ( $atts['amount'] ) {
						case '1':
							$col_class[] = 'one_full';
						break;
						case '2':
							$col_class[] = 'one_half';
						break;
						case '3':
							$col_class[] = 'one_third';
						break;
						case '5':
							$col_class[] = 'one_fifth';
						break;
						default:
							$col_class[] = 'one_fourth';
						break;
					}

					if( $i == $atts['amount'] ){
						$col_class[] = 'last';
					}

					if (  isset( $pricing['best_seller'] ) && $pricing['best_seller'] == 'yes' ) {
						$col_class[] = 'highlight';
					}
			?>

					<div class="<?php echo esc_attr( implode( ' ', $col_class ) ); ?>">

						<div class="title"><?php echo esc_html( $prc->post_title ); ?></div>
						<div class="price"><?php echo esc_html( $pricing['currency'] ) ?><?php echo esc_html( $pricing['price'] ) ?> <i><?php if ( isset( $pricing['per'] ) && !empty( $pricing['per'] ) ) { echo '/'; } ?><?php echo esc_html( $pricing['per'] ); ?></i></div>
						<div class="cont-list">
							<ul>
								<?php
									if ( isset( $pricing['before_attr'] ) && !empty( $pricing['before_attr'] ) ) {
										if ( $atts['icon_show'] == 1 ) {
											echo '<li>'. $atts['icon'] .' '. $pricing['before_attr'] .' </li>';
										} else {
											echo '<li>'. $pricing['before_attr'] .' </li>';
										}
									}

									if ( isset( $pricing['attr'] ) && !empty( $pricing['attr'] ) ) {
										$pros = explode( "\n", $pricing['attr'] );
										if( count( $pros ) ){
											foreach( $pros as $pro ) {
												if ( $atts['icon_show'] == 1 ) {
													echo '<li>'. $atts['icon'] .' '. $pro .' </li>';
												} else {
													echo '<li>'. $pro .' </li>';
												}
											}
										}
									}
								?>
							</ul>
						</div>
						<?php if ( $pricing['textsubmit'] ): ?>
							<div class="ordernow"><a href="<?php echo esc_url( $pricing['linksubmit'] ); ?>" class="but_small3"><span><i class="fa fa-shopping-cart"></i></span> <?php echo esc_html( $pricing['textsubmit'] ); ?></a></div>
						<?php endif ?>
						<?php if ( isset( $pricing['textmore'] ) && !empty( $pricing['textmore'] ) ): ?>
							<div class="clearfix margin_bottom2"></div>
							<a href="<?php echo esc_url( $pricing['linkmore'] ); ?>" class="style4_more"><?php echo esc_html( $pricing['textmore'] ); ?></a>
						<?php endif ?>

					</div>

			<?php } ?>
			<?php
				if ( !empty( $atts['color'] ) ) {
					/**** Custom Style ***/
					$custom_css_output = array(
						'.'. $custom_class .'.pricing-layout-6 li.price.act h1' => array(
							'color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-6 li.title.act' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-6 .pri_but_small1.act' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-6 .pri_but_small1.gray:hover' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-6 li.hecont.act strong' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-6 .pri_but_small1.act:hover' => array(
							'background-color' => '#272727'
						)
					);
					$custom_style = Su_Tools::get_css($custom_css_output);
					/**** end Style ***/
			?>
					<style type="text/css"><?php echo highstand::esc_js( $custom_style ); ?></style>
			<?php
				}
			?>

		</div>

	<?php

} else {
	echo 'No pricing table, <a href="'.admin_url('post-new.php?post_type=pricing-tables').'" target="_blank">Add Pricing</a>';
}