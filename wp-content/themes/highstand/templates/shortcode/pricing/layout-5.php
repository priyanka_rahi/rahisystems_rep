<?php

$highstand_sc_css = highstand::globe('highstand_sc_css');
$prcs = $content;

extract( $atts );

if( count( $prcs  ) ) {

	$attribute_class = array();
	$custom_class = 'pricing-box-'.randomstring(10);
	$attribute_class[] = $custom_class;
	$attribute_class[] = 'pricing-layout-5';
	if( !empty( $atts['class'] ) ){
		$attribute_class[] = $atts['class'];
	}

	?>

		<div class="<?php echo esc_attr( implode( ' ', $attribute_class ) ); ?>">

			<?php
				$i = 0;
				foreach( $prcs as $prc ) {

					$i++;
					$pricing = get_post_meta( $prc->ID , '_highstand_post_meta_options' );

					$_pricing = array(
						'price' => '100',
						'per' => 'month',
						'attr' => "Option 1\nOption 2",
						'currency' => '$',
						'textsubmit' => 'Select Plan',
						'linksubmit' => '#'
					);
					
					if( !empty( $pricing ) ){
						$pricing  = array_merge( $_pricing, (array)$pricing[0] );
					} else {
						$pricing = $_pricing;
					}

					$col_class = array( 'pacdetails', 'normal' );
					switch ( $atts['amount'] ) {
						case '1':
							$col_class[] = 'one_full';
						break;
						case '2':
							$col_class[] = 'one_half';
						break;
						case '3':
							$col_class[] = 'one_third';
						break;
						case '5':
							$col_class[] = 'one_fifth';
						break;
						default:
							$col_class[] = 'one_fourth';
						break;
					}

					if( $i == $atts['amount'] ){
						$col_class[] = 'last';
					}

					if (  isset( $pricing['best_seller'] ) && $pricing['best_seller'] == 'yes' ) {
						$class_highlight = ' act';
						$button_color	 = '';
					} else {
						$class_highlight = '';
						$button_color	 = ' gray';
					}
			?>

					<div class="<?php echo esc_attr( implode( ' ', $col_class ) ); ?>">
						<div class="pricing-table-5">
							<ul>

								<li class="title<?php echo esc_attr( $class_highlight ) ?>"><h3 class="white"><?php echo esc_html( $prc->post_title ); ?></h3></li>

								<li class="price<?php echo esc_attr( $class_highlight ) ?>">
									<h1><?php echo esc_html( $pricing['currency'] ) ?><?php echo esc_html( $pricing['price'] ) ?> <em><?php if ( isset( $pricing['per'] ) && !empty( $pricing['per'] ) ) { echo '/'; } ?><?php echo esc_html( $pricing['per'] ); ?></em></h1>
								</li>

								<?php
									if ( isset( $pricing['before_attr'] ) && !empty( $pricing['before_attr'] ) ) {
										echo '<li class="hecont'. esc_attr( $class_highlight ) .'">'. $pricing['before_attr'] .' </li>';
									}
								?>

								<li></li>

								<?php
									if ( isset( $pricing['attr'] ) && !empty( $pricing['attr'] ) ) {
										$pros = explode( "\n", $pricing['attr'] );
										if( count( $pros ) ){
											foreach( $pros as $pro ) {
												if ( $atts['icon_show'] == 1 ) {
													echo '<li>'. $atts['icon'] .' '. $pro .' </li>';
												} else {
													echo '<li>'. $pro .' </li>';
												}
											}
										}
									}
								?>

								<li></li>

								<li>
									<?php if ( $pricing['textsubmit'] ): ?>
										<a href="<?php echo esc_url( $pricing['linksubmit'] ); ?>" class="pri_but_small1<?php echo esc_attr( $button_color ) . esc_attr( $class_highlight ); ?>"><?php echo esc_html( $pricing['textsubmit'] ); ?></a>
									<?php endif ?>
									<?php if ( isset( $pricing['textmore'] ) && !empty( $pricing['textmore'] ) ): ?>
										<a href="<?php echo esc_url( $pricing['linkmore'] ); ?>" class="pri_but_small1<?php echo esc_attr( $button_color ) . esc_attr( $class_highlight ); ?>"><?php echo esc_html( $pricing['textmore'] ); ?></a>
									<?php endif ?>

								<li></li>
								<li></li>

							</ul>
						</div>
					</div>

			<?php } ?>
			<?php
				if ( !empty( $atts['color'] ) ) {
					/**** Custom Style ***/
					$custom_css_output = array(
						'.'. $custom_class .'.pricing-layout-5 .pricing-table-5 li.price.act h1' => array(
							'color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-5 .pricing-table-5 li.title.act' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-5 .pricing-table-5 .pri_but_small1.act' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-5 .pricing-table-5 .pri_but_small1.gray:hover' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-5 .pricing-table-5 li.hecont.act strong' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-5 .pricing-table-5 .pri_but_small1.act:hover' => array(
							'background-color' => '#272727'
						)
					);
					$custom_style = Su_Tools::get_css($custom_css_output);
					/**** end Style ***/
			?>
					<style type="text/css"><?php echo highstand::esc_js( $custom_style ); ?></style>
			<?php
				}
			?>

		</div>

	<?php

} else {
	echo 'No pricing table, <a href="'.admin_url('post-new.php?post_type=pricing-tables').'" target="_blank">Add Pricing</a>';
}