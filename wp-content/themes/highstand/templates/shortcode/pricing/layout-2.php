<?php

$highstand_sc_css = highstand::globe('highstand_sc_css');
$prcs = $content;

extract( $atts );

if( count( $prcs  ) ) {

	$attribute_class = array();
	$custom_class = 'pricing-box-'.randomstring(10);
	$attribute_class[] = $custom_class;
	$attribute_class[] = 'pricing-layout-2';
	if( !empty( $atts['class'] ) ){
		$attribute_class[] = $atts['class'];
	}

	?>

		<div class="<?php echo esc_attr( implode( ' ', $attribute_class ) ); ?>">

			<?php
				$i = 0;
				foreach( $prcs as $prc ) {

					$i++;
					$pricing = get_post_meta( $prc->ID , '_highstand_post_meta_options' );

					$_pricing = array(
						'price' => '100',
						'per' => 'month',
						'attr' => "Option 1\nOption 2",
						'currency' => '$',
						'textsubmit' => 'Select Plan',
						'linksubmit' => '#'
					);
					
					if( !empty( $pricing ) ){
						$pricing  = array_merge( $_pricing, (array)$pricing[0] );
					} else {
						$pricing = $_pricing;
					}

					$col_class = array();
					switch ( $atts['amount'] ) {
						case '1':
							$col_class[] = 'one_full';
						break;
						case '2':
							$col_class[] = 'one_half';
						break;
						case '3':
							$col_class[] = 'one_third';
						break;
						case '5':
							$col_class[] = 'one_fifth';
						break;
						default:
							$col_class[] = 'one_fourth';
						break;
					}

					if( $i == $atts['amount'] ){
						$col_class[] = 'last';
					}

					if ( isset( $pricing['best_seller'] ) && $pricing['best_seller'] == 'yes' ) {
						$col_class[] = 'highlight';
					}

					$col_class[] = 'pricing-box-2';
			?>

					<div class="<?php echo esc_attr( implode( ' ', $col_class ) ); ?>">

						<div class="title">
							<h4><?php echo esc_html( $prc->post_title ); ?></h4>
							<h2><?php echo esc_html( $pricing['currency'].$pricing['price'] ); ?> <?php if ( !empty( $pricing['per'] ) ) { ?><b>/<?php echo esc_html( $pricing['per'] ); ?></b><?php } ?></h2>
						</div>

						<ul>
							<?php
								if ( isset( $pricing['before_attr'] ) && !empty( $pricing['before_attr'] ) ) {
									if ( $atts['icon_show'] == 1 ) {
										echo '<li>'. $atts['icon'] .' '. $pricing['before_attr'] .' </li>';
									} else {
										echo '<li>'. $pricing['before_attr'] .' </li>';
									}
								}

								if ( isset( $pricing['attr'] ) && !empty( $pricing['attr'] ) ) {
									$pros = explode( "\n", $pricing['attr'] );
									if( count( $pros ) ){
										foreach( $pros as $pro ) {
											if ( $atts['icon_show'] == 1 ) {
												echo '<li>'. $atts['icon'] .' '. $pro .' </li>';
											} else {
												echo '<li>'. $pro .' </li>';
											}
										}
									}
								}
							?>
						</ul>

						<?php if ( isset( $pricing['textsubmit'] ) && !empty( $pricing['textsubmit'] ) ): ?>
							<div class="clearfix margin_bottom2"></div>
							<a href="<?php echo esc_url( $pricing['linksubmit'] ); ?>" class="button_2"><?php echo esc_html( $pricing['textsubmit'] ); ?></a>
						<?php endif ?>

					</div>

			<?php } ?>
			<?php
				if ( !empty( $atts['color'] ) ) {
					/**** Custom Style ***/
					$custom_css_output = array(
						'.'. $custom_class .'.pricing-layout-2 .highlight .title h4' => array(
							'color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-2 .highlight .title h2' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-2 a.button_2' => array(
							'background-color' => $atts['color']
						),
						'.'. $custom_class .'.pricing-layout-2 a.button_2:hover' => array(
							'background-color' => '#272727'
						),
						'.'. $custom_class .'.pricing-layout-2 .highlight' => array(
							'border-bottom' => '5px solid '. $atts['color']
						)
					);
					$custom_style = Su_Tools::get_css($custom_css_output);
					/**** end Style ***/
			?>
					<style type="text/css"><?php echo highstand::esc_js( $custom_style ); ?></style>
			<?php
				}
			?>

		</div>

	<?php

} else {
	echo 'No pricing table, <a href="'.admin_url('post-new.php?post_type=pricing-tables').'" target="_blank">Add Pricing</a>';
}