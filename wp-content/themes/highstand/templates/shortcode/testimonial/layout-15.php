<?php

$highstand = highstand::globe();

$main_class = array( 'testimonial-layout-15', 'ow-testimonials', 'owl-carousel', 'nomg', 'lightbults' );

if ( !empty( $atts['custom_class'] ) ) {
	$main_class[] = $atts['custom_class'];
}


extract($atts);

if ( count( $testimonials ) > 0 ){

?>

	<div class="<?php echo implode( " ", $main_class ); ?>">
		<?php
			foreach ($testimonials as $testi_post):

				$options = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
				$words   = !empty( $words ) ? $words : 30;
		?>

				<div class="item">
					<h3 class="fonb caps"><strong>@<?php echo get_the_title( $testi_post->ID ); ?></strong></h3>
					<div class="clearfix margin_bottom2"></div>
					<p class="fonm"><?php echo wp_trim_words( $testi_post->post_content, $words ); ?></p>
					<span class="line"></span>
				</div>

		<?php endforeach; ?>
	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}