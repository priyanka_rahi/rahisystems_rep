<?php

$highstand = highstand::globe();

$custom_class = '';

$custom_class = '';

extract($atts);

if ( count($testimonials) >0 ){

?>

	<div class="ow-testimonials testimonial-layout-2 <?php echo esc_attr( $custom_class ); ?>">
		<?php
			foreach ($testimonials as $testi_post):
				$options = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
		?>

				<div class="slidesec">

					<div class="left">
						<?php @the_post_thumbnail(); ?>
						<h5 class="nocaps"><?php echo get_the_title( $testi_post->ID ); ?><br> <em><?php echo esc_html( $options['website'] ); ?></em></h5>
					</div>

					<div class="right">
						<div class="arrow_box"><?php echo wp_trim_words( $testi_post->post_content, $words ); ?></div>
					</div>

				</div>

		<?php endforeach; ?>
	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}