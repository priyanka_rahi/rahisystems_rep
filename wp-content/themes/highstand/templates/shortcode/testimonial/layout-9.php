<?php

$highstand = highstand::globe();

$custom_class = '';

extract($atts);

if ( count($testimonials) >0 ){

?>

	<div class="testimonials-layout-9 <?php echo esc_attr( $custom_class ); ?>">
		<?php
			$dem = 1;
			foreach ($testimonials as $testi_post):
				$options = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);

				$class_col = array( 'one_third' );
				if ( $dem%3 == 0 ) {
					$class_col[] = 'last';
				}

				$box_class = array( 'contentbox' );
				if ( $dem%2 == 0 ) {
					$box_class[] = 'highlight';
				}
		?>

				<div class="<?php echo esc_attr( implode( " ", $class_col ) ); ?>">
					<div class="<?php echo esc_attr( implode( " ", $box_class ) ); ?>">
						<h5 class="caps">
							<?php if ( isset( $options['sub_title'] ) ): ?>
								<?php echo esc_html( $options['sub_title'] ); ?></h5>
							<?php endif ?>
						<p>" <?php echo wp_trim_words( $testi_post->post_content, $words ); ?> "</p>

						<div class="lbt">
							<strong>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</strong>
						</div><!-- end -->

						<b><?php echo get_the_title( $testi_post->ID ); ?><em><?php echo esc_html( $options['website'] ); ?></em></b>

					</div>
				</div>
				<!-- end section -->

		<?php
				$dem++;
			endforeach;
		?>
	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}