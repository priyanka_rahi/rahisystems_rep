<?php

$highstand = highstand::globe();

$custom_class = '';

extract($atts);

if ( count($testimonials) >0 ){

?>

	<div class="peoplesays testimonials-layout-7 <?php echo esc_attr( $custom_class ); ?>">
		<?php
			$dem = 1;
			foreach ($testimonials as $testi_post):
				$options = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
				$words	= !empty($words) ? $words : 30;
				$thumbnail_url = $highstand->get_featured_image( $testi_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '43x43xct' );
				if ( $dem%3 == 0 ) {
					$class_col = ' last';
				} else {
					$class_col = '';
				}
		?>

				<div class="one_third <?php echo esc_attr( $class_col ); ?>">
					<div class="box"><?php echo wp_trim_words( $testi_post->post_content, $words ); ?></div>
					<div class="who">
						<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="">
						<strong><?php echo get_the_title( $testi_post->ID ); ?></strong> <?php echo esc_html( $options['website'] ); ?>
					</div>
				</div>
				<!-- end section -->
				<?php if ( $dem%3 == 0 && $dem < count( $testimonials ) ): ?>
					<div class="margin_top5"></div>
				<?php endif ?>

		<?php
				$dem++;
			endforeach;
		?>
	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}