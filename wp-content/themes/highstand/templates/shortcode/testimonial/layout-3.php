<?php

$highstand = highstand::globe();

$custom_class = '';

extract($atts);

if ( count($testimonials) >0 ){
	$dem = 1;
?>
	<div class="ow-testimonials owl-carousel testimonial-layout-3 <?php echo esc_attr( $custom_class ); ?>">
		<?php foreach ( $testimonials as $testi_post ): ?>
			<?php if ( $dem%2 == 1 ): ?>
				<?php $class_item = 'one_half'; ?>
				<div class="peosays">
			<?php else: ?>
				<?php $class_item = 'one_half last'; ?>
			<?php endif ?>

				<div class="<?php echo esc_attr( $class_item ); ?>">
					<div class="box">
						<?php echo wp_trim_words( $testi_post->post_content, $words ); ?>
						<br /><br />
						<strong>- <?php echo get_the_title( $testi_post->ID ); ?></strong>
					</div>
				</div><!-- end section -->

			<?php if ( $dem%2 == 0 || $dem == count( $testimonials ) ): ?>
				</div>
			<?php endif ?>
			<?php $dem++; ?>
		<?php endforeach ?>
	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}