<?php

$highstand = highstand::globe();

$main_class = array( 'testimonial-layout-13', 'owl-carousel', 'nomg' );

if ( !empty( $custom_class ) ) {
	$main_class[] = $custom_class;
}


extract($atts);

if ( count($testimonials) > 0 ){

?>

	<div class="<?php echo implode( " ", $main_class ); ?>">
		<?php
			foreach ($testimonials as $testi_post):

				$options       = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
				$words         = !empty( $words ) ? $words : 30;
				$thumbnail_url = $highstand->get_featured_image( $testi_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '44x44xct' );
		?>

				<div class="item">
					<?php if ( !empty( $options['sub_title'] ) ): ?>
						<h5 class="roboto"><?php echo esc_html( $options['sub_title'] ); ?></h5>
					<?php endif ?>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<p><?php echo wp_trim_words( $testi_post->post_content, $words ); ?></p>
					<div class="who">
						<div class="box">
							<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" />
							<strong><?php echo get_the_title( $testi_post->ID ); ?> <br /><em><?php echo esc_html( $options['website'] ); ?></em></strong>
						</div>
					</div>
				</div>

		<?php endforeach; ?>
	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}