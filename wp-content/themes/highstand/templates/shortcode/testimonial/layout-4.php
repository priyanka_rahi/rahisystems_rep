<?php

$highstand = highstand::globe();

$custom_class = '';

extract($atts);

if ( count($testimonials) >0 ){

?>

<div class="animated eff-fadeIn delay-500ms">
	<div class="ow-testimonials owl-demo20 owl-carousel testimonial-layout-4 <?php echo esc_attr( $custom_class ); ?>">
    	<?php
		foreach ($testimonials as $testi_post):

            $options       = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
            $words         = !empty($words) ? $words : 30;
            $thumbnail_url = $highstand->get_featured_image( $testi_post );
            $thumbnail_url = highstand_createLinkImage( $thumbnail_url, '80x60xct' );
		?>

            <div class="item">

            	<div class="slidesec">
                 	<?php echo '<img src="'. esc_url( $thumbnail_url ) .'" />'; ?>
                    <h6><?php echo get_the_title( $testi_post->ID ); ?> <em><?php echo esc_html( $options['website'] ); ?></em></h6>
                    <div class="clearfix margin_bottom2"></div>
                    <p><?php echo wp_trim_words( $testi_post->post_content, $words ); ?></p>
    			</div>

            </div>

        <?php endforeach; ?>
	</div>
</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}