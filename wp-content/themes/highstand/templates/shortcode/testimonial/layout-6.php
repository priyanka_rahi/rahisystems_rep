<?php

$highstand = highstand::globe();

$custom_class = '';

extract($atts);

if ( count($testimonials) >0 ){

?>

<div class="animated eff-fadeIn delay-500ms">
	<div class="ow-testimonials owl-demo20 owl-carousel testimonial-layout-6 <?php echo esc_attr( $custom_class ); ?>">
		<?php
			foreach ($testimonials as $testi_post):

				$options       = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
				$words         = !empty($words) ? $words : 30;
				$thumbnail_url = $highstand->get_featured_image( $testi_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '60x60xct' );
		?>

				<div class="item">

					<div class="slidesec">
						<p class="fontmed less7">&ldquo; <?php echo wp_trim_words( $testi_post->post_content, $words ); ?> &rdquo;</p><br>
						<div class="who_pic">
							<?php echo '<img src="'. esc_url( $thumbnail_url ) .'" />'; ?>
							<div class="name"> <?php echo get_the_title( $testi_post->ID ); ?> <em><?php echo esc_html( $options['website'] ); ?></em> </div>
						</div>
					</div>

				</div><!-- end slide -->

		<?php endforeach; ?>
	</div>
</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}