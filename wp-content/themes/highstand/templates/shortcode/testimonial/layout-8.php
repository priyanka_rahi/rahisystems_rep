<?php

$highstand = highstand::globe();

$custom_class = '';

extract($atts);

if ( count($testimonials) >0 ){

?>

	<div class="testimonial-layout-8 owl-carousel lightbults <?php echo esc_attr( $custom_class ); ?>">
		<?php
			foreach ($testimonials as $testi_post):

				$options = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
				$words	= !empty($words) ? $words : 30;
				$thumbnail_url = $highstand->get_featured_image( $testi_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '90x90xct' );
		?>

				<div class="item">
					<div class="sectionbox">
						<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="">
						<h5>&ldquo; <?php echo wp_trim_words( $testi_post->post_content, $words ); ?> &rdquo;</h5>
						<h6><?php echo get_the_title( $testi_post->ID ); ?><b><?php echo esc_html( $options['website'] ); ?></b></h6>
					</div>
				</div><!-- end item -->

		<?php endforeach; ?>
	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}