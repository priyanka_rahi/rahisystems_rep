<?php

$highstand = highstand::globe();

$custom_class = '';

extract($atts);

if ( count($testimonials) >0 ){

?>

	<div class="testimonials-layout-10 peoplesays <?php echo esc_attr( $custom_class ); ?>">
		<?php
			$dem = 1;
			foreach ($testimonials as $testi_post):
				$options       = get_post_meta( $testi_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
				$words         = !empty($words) ? $words : 30;
				$thumbnail_url = $highstand->get_featured_image( $testi_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '58x58xct' );

				$class_col = array( 'one_third' );
				if ( $dem%3 == 0 ) {
					$class_col[] = 'last';
				}
		?>

				<div class="<?php echo esc_attr( implode( " ", $class_col ) ); ?>">
					<div class="arrow_box">&quot; <?php echo wp_trim_words( $testi_post->post_content, $words ); ?> &quot; </div>
					<div class="who"> <?php echo '<img src="'. esc_url( $thumbnail_url ) .'" />'; ?></div>
					<div class="content">
						<?php echo get_the_title( $testi_post->ID ); ?>
						<p> - <?php echo esc_html( $options['website'] ); ?> -</p>
					</div>
				</div>

		<?php
				$dem++;
			endforeach;
		?>
	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Testimonials not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=testimonials').'"><i class="fa fa-plus"></i> Add New Testimonial</a>';
}