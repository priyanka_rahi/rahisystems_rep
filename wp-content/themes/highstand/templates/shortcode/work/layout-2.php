<?php

$highstand = highstand::globe();

extract( $atts );

$showlink = !empty($show_link) ? $show_link : 'yes';
$gap = !empty($gap) ? $gap : 0;

if ( count($works) >0 ) {

?>

	<div id="js-grid-juicy-projects-<?php echo highstand_random_string( 10 ); ?>" class="cbp js-grid-juicy-projects" data-cols="<?php echo isset($column)?$column:'3'; ?>" data-gap="<?php echo esc_attr( $gap ); ?>">

		<?php foreach ( $works as $item ): ?>

			<?php
				$catsStack = array();

				$image = $highstand->get_featured_image( $item );

				$cats = wp_get_post_terms( $item->ID, 'our-works-category' );
				$cateClass = '';

				if( count( $cats ) ){
					foreach( $cats as $cat ){
						$cat_name = strtolower( str_replace(array(' ','&amp;','&'),array('-','',''),$cat->name) );
						$cat_args = array( $cat_name, $cat->count );
						if( !in_array( $cat_args, $catsStack ) ){
							array_push( $catsStack , $cat_args );
						}
						$cateClass .= $cat_name.' ';
					}
				}

				$work_cf = get_post_meta( $item->ID , '_highstand_post_meta_options', TRUE );

				$link = !empty($work_cf['link']) ? $work_cf['link'] : get_permalink( $item->ID );
			?>

			<div class="cbp-item">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="<?php echo esc_url($image); ?>" alt="" />
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="cbp-l-caption-alignCenter">
							<div class="cbp-l-caption-body">

								<?php if($showlink == 'yes'): ?>
									<a href="<?php echo esc_url( $link ); ?>" class="cbp-singlePage cbp-l-caption-buttonLeft" rel="nofollow"><?php esc_html_e( 'more info', 'highstand' ); ?></a>
								<?php endif; ?>

								<a href="<?php echo esc_url($image); ?>" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?php echo get_the_title( $item->ID ); ?><br>by <?php echo esc_html($work_cf['outhor']); ?>"><?php esc_html_e( 'view larger', 'highstand' ); ?></a>
							</div>
						</div>
					</div>
				</div>
			</div><!-- end item -->

		<?php endforeach; ?>

	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Works not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-works').'"><i class="fa fa-plus"></i> Add New Work</a>';
}