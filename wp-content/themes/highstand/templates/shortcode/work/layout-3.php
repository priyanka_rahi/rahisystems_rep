<?php

$highstand = highstand::globe();

extract( $atts );

$showlink = !empty($show_link) ? $show_link : 'yes';
$gap = !empty($gap) ? $gap : 0;

if ( count($works) >0 ) {

	echo '<div class="owl-carousel owl-demo20 center sttwo owl-theme our-works-layout-3 '. esc_attr( $class ) .'">';

	foreach($works as $item) {

		$image = $highstand->get_featured_image( $item );

		$work_cf = get_post_meta( $item->ID , '_highstand_post_meta_options', TRUE );

		$link = !empty($work_cf['link']) ? $work_cf['link'] : get_permalink( $item->ID );

		if($showlink == 'yes'){
			echo '<div class="item">
			    <a href="'. esc_url( $link ) .'">
				    <div class="box">
				    	<img class="rimg" src="'. esc_url($image) .'" alt="">
				    	<p>'. wp_trim_words( $item->post_content, 15 ) .'</p>
				    </div>
				</a>
		    </div>';
		}else{
			echo '<div class="item">			    
			    <div class="box">
			    	<img class="rimg" src="'. esc_url($image) .'" alt="">
			    	<p>'. wp_trim_words( $item->post_content, 15 ) .'</p>
			    </div>				
		    </div>';
		}
		

	}


	echo '</div>';

}else{
	echo '<h4>' . esc_html__( 'Works not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-works').'"><i class="fa fa-plus"></i> Add New Work</a>';
}