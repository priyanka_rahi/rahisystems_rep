<?php

$highstand = highstand::globe();

extract( $atts );

$showlink = !empty($show_link) ? $show_link : 'yes';
$gap = !empty($gap) ? $gap : 0;

if ( count($works) >0 ) {

?>

	<?php if ( $atts['filter'] == 'Yes' ): ?>

		<?php 

		$tax_term = trim( $atts['tax_term'] );
		if ( !empty( $tax_term ) ): ?>

			<div id="js-filters-juicy-projects" class="cbp-l-filters-alignCenter">
				<div data-filter="*" class="cbp-filter-item-active cbp-filter-item"><?php echo esc_html__( 'ALL', 'highstand' ); ?></div> /

				<?php
					$cat_str = $atts['tax_term'];
					$cat_arr = explode( ",", $cat_str );
					for ( $i=0; $i < count( $cat_arr ); $i++ ) {
						$cat_exp = explode( "-", $cat_arr[$i] );
						$cat_dis = implode( " ", $cat_exp );
						if ( $i == ( count( $cat_arr ) - 1 ) ) {
							$str_demiter = '';
						} else {
							$str_demiter = '/';
						}
				?>
						<div data-filter=".<?php echo esc_attr( $cat_arr[$i] ); ?>" class="cbp-filter-item"><?php echo strtoupper( $cat_dis ); ?></div> <?php echo esc_html( $str_demiter ); ?>
				<?php
					}
				?>

			</div>

		<?php endif ?>

	<?php endif ?>

	<div id="js-grid-juicy-projects-<?php echo highstand_random_string( 10 ); ?>" class="cbp js-grid-juicy-projects projects-layout-6" data-cols="<?php echo isset($column)?$column:'3'; ?>" data-gap="<?php echo esc_attr( $gap ); ?>">

		<?php 
		$i = 1;
		foreach ( $works as $item ): ?>

			<?php
				$i++;

				$catsStack = array();

				$image = $highstand->get_featured_image( $item );

				$cats = wp_get_post_terms( $item->ID, 'our-works-category' );
				$cateClass = '';

				if( count( $cats ) ){
					foreach( $cats as $cat ){
						$cat_name = strtolower( str_replace(array(' ','&amp;','&'),array('-','',''),$cat->name) );
						$cat_args = array( $cat_name, $cat->count );
						if( !in_array( $cat_args, $catsStack ) ){
							array_push( $catsStack , $cat_args );
						}
						$cateClass .= $cat_name.' ';
					}
				}

				$class_item = array();
				$class_item[] = 'cbp-item';
				$class_item[] = 'animated eff-fadeInLeft delay-'. ($i+2) .'00ms';

				if ( $atts['filter'] == 'Yes' ) {
					$taxonomy = 'our-works-category';
					$post_id  = $item->ID;

					$post_terms = wp_get_object_terms( $post_id, $taxonomy, array( 'fields' => 'ids' ) );

					if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {
						$term_ids = implode( ',' , $post_terms );

						$args = array(
							'orderby'  => 'name',
							'order'    => 'ASC',
							'include'  => $term_ids,
							'taxonomy' => $taxonomy
						);

						$categories = get_categories( $args );

						foreach ( $categories as $category ) {
							$class_item[] = $category->slug;
						}
					}
				}

				$thumbnail_url = $highstand->get_featured_image( $item );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '680x600xct' );

				$work_cf = get_post_meta( $item->ID , '_highstand_post_meta_options', TRUE );

				$link = !empty($work_cf['link']) ? $work_cf['link'] : get_permalink( $item->ID );
			?>

			<div class="<?php echo implode( " ", $class_item ); ?>">
				<div class="cbp-caption">
					<div class="cbp-caption-defaultWrap">
						<img src="<?php echo esc_url($thumbnail_url); ?>" alt="" />
					</div>
					<div class="cbp-caption-activeWrap">
						<div class="cbp-l-caption-alignCenter">
							<div class="cbp-l-caption-body">
								<?php if($showlink == 'yes'): ?>
									<a href="<?php echo esc_url( $link ); ?>" class="cbp-l-caption-buttonLeft" rel="nofollow"><?php esc_html_e( 'more info', 'highstand' ); ?></a>
								<?php endif ?>

								<?php if ( isset( $work_cf['video_link'] ) && !empty( $work_cf['video_link'] ) ): ?>
									<a href="<?php echo esc_url( $work_cf['video_link'] ) ?>" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?php echo get_the_title( $item->ID ); ?><br>by <?php echo esc_html($work_cf['outhor']); ?>"><?php echo esc_html__( 'view video', 'highstand' ); ?></a>
								<?php else: ?>
									<a href="<?php echo esc_url($image); ?>" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?php echo get_the_title( $item->ID ); ?><br>by <?php echo esc_html($work_cf['outhor']); ?>"><?php esc_html_e( 'view larger', 'highstand' ); ?></a>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			</div><!-- end item -->

		<?php endforeach; ?>

	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Works not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-works').'"><i class="fa fa-plus"></i> Add New Work</a>';
}