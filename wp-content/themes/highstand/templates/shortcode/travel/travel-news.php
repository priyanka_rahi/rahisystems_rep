<?php

$highstand = highstand::globe();

extract( $atts );

$args = array(
	'post_type' => 'travel_news',
	'posts_per_page' => $number_show
);

$travel_news_query = new WP_Query( $args );

$i=0;

?>

<div class="travel_news_wrapper">
<?php
	if ( $travel_news_query->have_posts() ) {
		while ( $travel_news_query->have_posts() ) {
			$travel_news_query->the_post();

			$thumbnail_url = $highstand->get_featured_image( $travel_news_query->post );
			$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '560x300xc' );

			$i++;
			?>

			<div class="one_half <?php if($i%2 == 0) echo 'last'; ?>">
		    	<a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" class="rimg"></a>
		    	<div class="date"><strong>18</strong> March</div> <span><strong>72</strong> Likes</span>

		    	<div class="conh2"><?php the_title(); ?><i class="light"><?php echo wp_trim_words( get_the_content(), 40, '...' ); ?></i>
		    	</div>
		    </div>

			<?php
		}
	} else {
		// no posts found
	}
	/* Restore original Post Data */
	wp_reset_postdata();
?>

</div>