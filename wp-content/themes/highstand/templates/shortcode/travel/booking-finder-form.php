<?php

extract( $atts );

//print_r( $atts );

?>

<div class="hs_booking_finder prosearch">
    <div class="container">
    	        
        <ul class="travel_bf_tabs tabs7">
            <li class="active"><a href="#" data-type="Flights" >Flights</a></li>
            <li><a href="#" data-type="Hotels">Hotels</a></li>
            <li><a href="#" data-type="Holidays">Holidays</a></li>
            <li><a href="#" data-type="Flight+Hotel">Flight+Hotel</a></li>
            <li><a href="#" data-type="Buses">Buses</a></li>
		</ul>
    	
        <div class="tabs-content-travel tabs-content7 fullw">
        
            <div id="" class="tabs-panel7">
             	
            	<strong>Book Domestic &amp; International Flight Tickets</strong>
                
            	<?php 
            		$c_shortcode = '[contact-form-7 id="' . $atts['c_id'] . '" title="Contact Form"]'; 
            		echo do_shortcode( $c_shortcode );
            	?>                
			</div><!-- end tab 1 -->
                       
        </div>
        
        
    </div>
    </div>