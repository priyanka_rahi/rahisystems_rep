<?php

$highstand = highstand::globe();

extract($atts);

$total_items_show = !empty($atts['items']) ? $atts['items'] : 4;

$team_posts = $atts['team_posts'];

$classes = 'our-team-layout-2 '. $atts['custom_class'];

if ( count($team_posts) > 0  ){
?>
	<div class="<?php echo esc_attr( $classes ); ?>">
		<?php
			$i = 1;

			foreach($team_posts as $team_post):
				$our_team_options = get_post_meta( $team_post->ID , '_highstand_post_meta_options' );
				if ( isset( $our_team_options[0] ) ) {
					$our_team_options = $our_team_options[0];
				}
				$words         = !empty($words) ? $words : 30;

				$thumbnail_url = $highstand->get_featured_image( $team_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '146x146xct' );

				if ( count( $team_posts ) < 4 ) {
					switch ( count( $team_posts ) ) {
						case '1':
							$class_col = array( 'one_full' );
						break;
						case '2':
							$class_col = array( 'one_half' );
						break;
						case '3':
							$class_col = array( 'one_third' );
						break;
						default:
						break;
					}
					if ( $i == count( $team_posts ) ) {
						$class_col[] = 'last';
					}
				} else {
					$class_col = array( 'one_fourth' );
				}
				if ( $i%4 == 0 ) {
					$class_col[] = 'last';
				}

				$profile_link = !empty( $our_team_options['website'] ) ? $our_team_options['website'] :  get_permalink( $team_post->ID );

		?>
				<div class="<?php echo esc_attr( implode( " ", $class_col ) ); ?>">
					<?php if ( has_post_thumbnail( $team_post->ID ) ): ?>
						<div class="uimg">
							<?php if ( $atts['link_view'] == 'no' ): ?>
								<a href="<?php echo esc_url( $profile_link ); ?>">
									<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" class="rimg">
								</a>
							<?php else: ?>
								<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" class="rimg">
							<?php endif ?>
						</div>
					<?php endif ?>

					<h4>
						<?php echo get_the_title( $team_post->ID ); ?>
						<?php if ( !empty( $our_team_options['position'] ) ): ?>
							<br/> <em>- <?php echo esc_html( $our_team_options['position'] ); ?> -</em>
						<?php endif ?>
					</h4>

					<p><?php echo wp_trim_words( strip_tags($team_post->post_content), $words, ' ...' ); ?></p>

					<ul class="people_soci">
						<?php if ( !empty( $our_team_options['facebook'] ) ): ?>
							<li><a target="_blank" href="<?php echo (!empty( $our_team_options['facebook'] )) ? esc_url($our_team_options['facebook']) : '#'; ?>"><i class="fa fa-facebook"></i></a></li>
						<?php endif ?>
						<?php if ( !empty( $our_team_options['twitter'] ) ): ?>
							<li><a target="_blank" href="<?php echo (!empty( $our_team_options['twitter'] )) ? esc_url($our_team_options['twitter']) : '#'; ?>"><i class="fa fa-twitter"></i></a></li>
						<?php endif ?>
						<?php if ( !empty( $our_team_options['gplus'] ) ): ?>
							<li><a target="_blank" href="<?php echo (!empty( $our_team_options['gplus'] )) ? esc_url($our_team_options['gplus']) : '#'; ?>"><i class="fa fa-google-plus"></i></a></li>
						<?php endif ?>
						<?php if ( !empty( $our_team_options['youtube'] ) ): ?>
							<li><a target="_blank" href="<?php echo (!empty( $our_team_options['youtube'] )) ? esc_url($our_team_options['youtube']) : '#'; ?>"><i class="fa fa-youtube"></i></a></li>
						<?php endif ?>
						<?php if ( !empty( $our_team_options['linkedin'] ) ): ?>
							<li><a target="_blank" href="<?php echo (!empty( $our_team_options['linkedin'] )) ? esc_url($our_team_options['linkedin']) : '#'; ?>"><i class="fa fa-linkedin"></i></a></li>
						<?php endif ?>
						<?php if ( !empty( $our_team_options['envelope'] ) ): ?>
							<li><a target="_blank" href="<?php echo esc_url( $our_team_options['envelope'] ); ?>"><i class="fa fa-envelope"></i></a></li>
						<?php endif ?>
					</ul>
				</div>
		<?php
				if($i % 4 == 0 && $total_items_show > 4) echo '<div class="clearfix margin_bottom5"></div>';
				$i++;
			endforeach;
		?>
	</div>
<?php

} else {
	echo '<h4>' . esc_html__( 'Teams not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-team').'"><i class="fa fa-plus"></i> Add New Staff</a>';
}