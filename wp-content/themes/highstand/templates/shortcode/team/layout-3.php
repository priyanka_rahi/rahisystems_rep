<?php

$highstand = highstand::globe();

$team_posts = $atts['team_posts'];

$classes = 'our-team-layout-3 owl-carousel '. $atts['custom_class'];

if ( count($team_posts) > 0  ){
?>
	<div class="<?php echo esc_attr( $classes ); ?>">
<?php
		foreach($team_posts as $team_post):
			$our_team_options = get_post_meta( $team_post->ID , '_highstand_post_meta_options' );
			if ( isset( $our_team_options[0] ) ) {
				$our_team_options = $our_team_options[0];
			}

			$thumbnail_url = $highstand->get_featured_image( $team_post, 'full' );
			$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '274x250xct' );

			$profile_link = !empty( $our_team_options['website'] ) ? $our_team_options['website'] :  get_permalink( $team_post->ID );
?>

			<div class="item">
				<?php if ( $atts['link_view'] == 'no' ): ?>
					<a href="<?php echo esc_url( $profile_link ); ?>">
				<?php endif ?>
						<div class="box">
							<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" class="rimg">
							<h5 class="caps">
								<strong><?php echo get_the_title( $team_post->ID ); ?></strong>
								<?php if ( !empty( $our_team_options['position'] ) ): ?>
									<br><b><?php echo esc_html( $our_team_options['position'] ); ?></b>
								<?php endif ?>
							</h5>
						</div>
				<?php if ( $atts['link_view'] == 'no' ): ?>
					</a>
				<?php endif ?>
			</div>
<?php
		endforeach;
?>
	</div>
<?php

} else {
	echo '<h4>' . esc_html__( 'Teams not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-team').'"><i class="fa fa-plus"></i> Add New Staff</a>';
}