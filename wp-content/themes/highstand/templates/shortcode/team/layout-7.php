<?php

$highstand = highstand::globe();

$team_posts = $atts['team_posts'];

$classes = array( 'our-team-layout-7', 'owl-carousel' );
if ( !empty( $atts['custom_class'] ) ) {
	$classes[] = $atts['custom_class'];
}

if ( count($team_posts) > 0  ){
?>
	<div class="<?php echo esc_attr( implode( " ", $classes ) ); ?>">
		<?php
			foreach( $team_posts as $team_post ):

				$team_post_title  = get_the_title( $team_post->ID );
				$words            = !empty($words) ? $words : 30;
				$our_team_options = get_post_meta( $team_post->ID , '_highstand_post_meta_options' );
				if ( isset( $our_team_options[0] ) ) {
					$our_team_options = $our_team_options[0];
				}

				$thumbnail_url = $highstand->get_featured_image( $team_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '275x400xct' );

				$profile_link = !empty( $our_team_options['website'] ) ? $our_team_options['website'] :  get_permalink( $team_post->ID );
		?>

				<div class="item">
					<div class="box">
						<div class="box-cnt"><img src="<?php echo esc_url( $thumbnail_url ); ?>" alt=""/></div>
						<div class="box-details">
							<h5><?php echo esc_html( $team_post_title ); ?> <em><?php echo esc_html( $our_team_options['position'] ); ?></em></h5>
							<p><?php echo wp_trim_words( $team_post->post_content, $words ); ?></p>
						</div>
					</div>
				</div>

		<?php endforeach; ?>
	</div>
<?php

} else {
	echo '<h4>' . esc_html__( 'Teams not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-team').'"><i class="fa fa-plus"></i> Add New Staff</a>';
}