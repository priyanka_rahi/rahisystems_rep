<?php

$highstand = highstand::globe();

$team_posts = $atts['team_posts'];

$wrap_class = array( 'our-team-layout-6' );
if ( !empty( $atts['custom_class'] ) ) {
	$wrap_class[] = $atts['custom_class'];
}

if ( count( $team_posts ) > 0  ){
?>
	<div class="<?php echo esc_attr( implode( " ", $wrap_class ) ); ?>">

		<?php
			$dem = 1;
			foreach( $team_posts as $team_post ):

				$our_team_options = get_post_meta( $team_post->ID , '_highstand_post_meta_options' );
				if ( isset( $our_team_options[0] ) ) {
					$our_team_options = $our_team_options[0];
				}
				$words         = !empty($words) ? $words : 30;
				$thumbnail_url = $highstand->get_featured_image( $team_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '257x450xct' );

				if ( count( $team_posts ) < 4 ) {
					switch ( count( $team_posts ) ) {
						case '1':
							$class_col = array( 'one_full' );
						break;
						case '2':
							$class_col = array( 'one_half' );
						break;
						case '3':
							$class_col = array( 'one_third' );
						break;
						default:
						break;
					}
					if ( $dem == count( $team_posts ) ) {
						$class_col[] = 'last';
					}
				} else {
					$class_col = array( 'one_fourth' );
				}
				if ( $dem%4 == 0 ) {
					$class_col[] = 'last';
				}
				$icon_num = 0;
				if ( !empty( $our_team_options['facebook'] ) ) { $icon_num++; }
				if ( !empty( $our_team_options['twitter'] ) ) { $icon_num++; }
				if ( !empty( $our_team_options['envelope'] ) ) { $icon_num++; }
				if ( !empty( $our_team_options['gplus'] ) ) { $icon_num++; }
				if ( !empty( $our_team_options['youtube'] ) ) { $icon_num++; }
				if ( !empty( $our_team_options['linkedin'] ) ) { $icon_num++; }

				$profile_link = !empty( $our_team_options['website'] ) ? $our_team_options['website'] :  get_permalink( $team_post->ID );
		?>

				<div class="<?php echo esc_attr( implode( " ", $class_col ) ); ?>">
					<div class="box">
						<div class="box-cnt">
							<img src="<?php echo esc_url( $thumbnail_url ); ?>" />
						</div>

						<div class="box-details">
							<h5><?php echo get_the_title( $team_post->ID ); ?><em><?php echo esc_html( $our_team_options['position'] ); ?></em></h5>
							<div class="hline"></div>
							<p><?php echo wp_trim_words( $team_post->post_content, $words ); ?></p>
							<?php if ( $atts['link_view'] == 'no' ): ?>
								<a href="<?php echo esc_url( $profile_link ); ?>"><?php echo esc_html__( 'Read more', 'highstand' ); ?></a>
							<?php endif ?>
						</div>
					</div>
				</div>

				<?php if ( $dem%4 == 0 && $dem < count( $team_posts ) ): ?>
					<div class="margin_bottom5"></div>
				<?php endif ?>

		<?php
				$dem++;
			endforeach;
		?>

	</div>
<?php

} else {
	echo '<h4>' . esc_html__( 'Teams not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-team').'"><i class="fa fa-plus"></i> Add New Staff</a>';
}