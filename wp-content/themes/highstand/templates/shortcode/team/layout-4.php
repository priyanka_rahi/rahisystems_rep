<?php

$highstand = highstand::globe();

$team_posts = $atts['team_posts'];

$wrap_class = array( 'our-team-layout-4' );
if ( !empty( $atts['custom_class'] ) ) {
	$wrap_class[] = $atts['custom_class'];
}

if ( count($team_posts) > 0  ){
?>
	<div class="<?php echo esc_attr( implode( " ", $wrap_class ) ); ?>">
		<ul>

			<?php
				foreach($team_posts as $team_post):
					$our_team_options = get_post_meta( $team_post->ID , '_highstand_post_meta_options' );
					if ( isset( $our_team_options[0] ) ) {
						$our_team_options = $our_team_options[0];
					}

					$thumbnail_url = $highstand->get_featured_image( $team_post );
					$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '230x250xct' );

					$profile_link = !empty( $our_team_options['website'] ) ? $our_team_options['website'] :  get_permalink( $team_post->ID );
			?>

					<li>
						<?php if ( $atts['link_view'] == 'no' ): ?>
							<a href="<?php echo esc_url( $profile_link ); ?>">
								<?php echo '<img src="'. esc_url( $thumbnail_url ) .'" />'; ?>
								<span><?php echo get_the_title( $team_post->ID ); ?></span>
							</a>
						<?php else: ?>
							<?php echo '<img src="'. esc_url( $thumbnail_url ) .'" />'; ?>
							<span><?php echo get_the_title( $team_post->ID ); ?></span>
						<?php endif ?>
					</li>

			<?php endforeach; ?>

		</ul>
	</div>
<?php

} else {
	echo '<h4>' . esc_html__( 'Teams not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-team').'"><i class="fa fa-plus"></i> Add New Staff</a>';
}