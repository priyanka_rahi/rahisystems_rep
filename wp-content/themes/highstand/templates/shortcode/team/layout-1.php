<?php

$highstand = highstand::globe();

$team_posts = $atts['team_posts'];

$classes = 'our-team-layout-1 owl-our-team owl-carousel '. $atts['custom_class'];

if ( count($team_posts) > 0  ){
?>
	<div class="<?php echo esc_attr( $classes ); ?>">
<?php
	foreach( $team_posts as $team_post ):
		$our_team_options = get_post_meta( $team_post->ID , '_highstand_post_meta_options' );
		if ( isset( $our_team_options[0] ) ) {
			$our_team_options = $our_team_options[0];
		}

		$thumbnail_url = $highstand->get_featured_image( $team_post );
		$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '340x400xtc' );

		$profile_link = !empty( $our_team_options['website'] ) ? $our_team_options['website'] :  get_permalink( $team_post->ID );
?>

		<div class="item">
			<div class="box">
				<div class="boximg">
					<?php if ( $atts['link_view'] == 'no' ): ?>
						<a href="<?php echo esc_url( $profile_link ); ?>">
					<?php endif ?>
							<?php if ( has_post_thumbnail( $team_post->ID ) ): ?>
								<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" class="rimg">
							<?php endif ?>
							<div class="content">
								<?php echo get_the_title( $team_post->ID ); ?>
								<?php if ( !empty( $our_team_options['position'] ) ): ?>
									<br/> <b>- <?php echo esc_html( $our_team_options['position'] ); ?> -</b>
								<?php endif ?>
							</div>
					<?php if ( $atts['link_view'] == 'no' ): ?>
						</a>
					<?php endif ?>
				</div>
			</div>
		</div>
<?php
	endforeach;
?>
	</div>
<?php

} else {
	echo '<h4>' . esc_html__( 'Teams not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=our-team').'"><i class="fa fa-plus"></i> Add New Staff</a>';
}