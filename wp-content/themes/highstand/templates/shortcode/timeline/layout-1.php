<?php

$highstand = highstand::globe();

$main_class = array( 'time-line-layout-1', 'cd-timeline', 'cd-container' );

if ( !empty( $atts['custom_class'] ) ) {
	$main_class[] = $atts['custom_class'];
}

extract( $atts );

if ( count($timeline) > 0 ){

?>

	<div class="<?php echo esc_attr( implode( " ", $main_class ) ); ?>">
		<?php
			wp_enqueue_style( 'highstand-timeline' );
			foreach ( $timeline as $timeline_post ):

				$options       = get_post_meta( $timeline_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
				$words         = !empty( $words ) ? $words : 20;
				$thumbnail_url = $highstand->get_featured_image( $timeline_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '400x250xct' );
		?>

				<div class="cd-timeline-block animated eff-fadeInUp delay-500ms">
					<div class="cd-timeline-img cd-picture"><span><?php echo esc_html( $options['timeline'] ); ?></span></div>
					<div class="cd-timeline-content">
					<p class="text">
						<strong><?php echo get_the_title( $timeline_post->ID ); ?></strong>
						<?php echo wp_trim_words( $timeline_post->post_content, $words ); ?>
					</p>
					<span class="cd-date"><img src="<?php echo esc_url( $thumbnail_url ); ?>" class="rimg" alt=""/></span> </div>
				</div>

		<?php endforeach; ?>

	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Time Line not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=time-line').'"><i class="fa fa-plus"></i> Add New Time Line</a>';
}