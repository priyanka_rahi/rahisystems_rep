<?php

$highstand = highstand::globe();

$main_class = array( 'time-line-layout-2' );

if ( !empty( $atts['custom_class'] ) ) {
	$main_class[] = $atts['custom_class'];
}

extract( $atts );

if ( count($timeline) > 0 ){

?>

	<div class="<?php echo esc_attr( implode( " ", $main_class ) ); ?>">
		<?php
			wp_enqueue_style( 'highstand-timeline' );

			$dem = 1;
			foreach ( $timeline as $timeline_post ):

				$options       = get_post_meta( $timeline_post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
				$words         = !empty( $words ) ? $words : 20;
				$thumbnail_url = $highstand->get_featured_image( $timeline_post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '50x50xct' );
		?>

				<div class="cd-timeline-block-2">
					<div class="cd-timeline-img-2 cd-picture-2"> <img src="<?php echo esc_url( $thumbnail_url ); ?>" alt=""> </div>
					<div class="cd-timeline-content-2">
						<h2><?php echo get_the_title( $timeline_post->ID ); ?></h2>
						<p class="text-2"><?php echo wp_trim_words( $timeline_post->post_content, $words ); ?></p>
						<a href="<?php the_permalink(); ?>" class="cd-read-more-2"><?php echo esc_html__( 'Read more', 'highstand' ); ?></a>
						<span class="cd-date-2">
							<?php if ( $dem%2 == 1 ): ?>
								<strong><?php echo esc_html( $options['timeline'] ); ?></strong>
							<?php else: ?>
								<b><?php echo esc_html( $options['timeline'] ); ?></b>
							<?php endif ?>
						</span>
					</div>
				</div>

		<?php
				$dem++;
			endforeach;
		?>

	</div>

<?php

}else {
	echo '<h4>' . esc_html__( 'Time Line not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php?post_type=time-line').'"><i class="fa fa-plus"></i> Add New Time Line</a>';
}