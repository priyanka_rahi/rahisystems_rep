<?php

$highstand = highstand::globe();

extract( $atts );

if( isset( $se_options ) ){

	echo '<div class="owl-carousel services-owl-carousel services-layout-1 '. esc_attr( $class ) .'">';

		$dem = 1;
		foreach( $se_options as $option ){
			$s_title = $option->title;

			$s_desc  = base64_decode( $option->desc );

			$link    = ( '||' === $option->link ) ? '' : $option->link;
			$link    = kc_parse_link($link);
			if ( strlen( $link['url'] ) > 0 ) {
				$a_href   = $link['url'];
				$a_title  = $link['title'];
				$a_target = strlen( $link['target'] ) > 0 ? $link['target'] : '_self';
			}
			if( !isset( $a_href ) ) {
				$a_href = "";
			}

			$class_box = array( 'box' );
			if ( isset( $option->active ) && $option->active == 'yes' ) {
				$class_box[] = 'active';
			}
?>

			<div class="item">
				<div class="<?php echo esc_attr( implode( " ", $class_box ) ); ?>">
					<div class="box-cnt">
						<?php if ( $option->image ): ?>
							<?php
								$image_url_full = wp_get_attachment_image_src( $option->image, 'full' );
								$image_url      = $image_url_full[0];
								$thumbnail_url  = highstand_createLinkImage( $image_url, '375x350xct' );
							?>
							<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" />
						<?php endif ?>
					</div>
					<div class="box-details">
						<h4><?php echo esc_html( $s_title ); ?></h4>
						<div class="clearfix"></div>
						<p><?php echo esc_html( $s_desc ); ?></p>
						<div class="clearfix margin_bottom1"></div>
						<?php if ( !empty( $a_href ) ): ?>
							<a href="<?php echo esc_url( $a_href ); ?>"><?php esc_html_e( 'Read more', 'highstand' ); ?></a>
						<?php endif ?>
					</div>
				</div>
			</div><!-- end item -->

<?php
			$dem++;
		}

	echo '</div>';

}