<?php

extract( $atts );

?>

<div class="prosearch">
	<div class="container">

		<h1><?php echo esc_html( $title ); ?></h1>
		<h2><?php echo esc_html( $sub_title ); ?></h2>

		<div class="clearfix margin_bottom4"></div>

		<ul class="tabs-realestate">
			<?php
			if(highstand_real_estate::get_cats()){
				foreach ( highstand_real_estate::get_cats() as $cat ) {
					echo '<li class=""><a href="#'. $cat->slug .'" target="_self" data-cat="'. $cat->slug .'">'. $cat->name .'</a></li>';
				}
			}else{
				$cat_url = get_site_url(). '/wp-admin/edit-tags.php?taxonomy=property-category&post_type=property';
				echo '<li class="active"><a href="'. esc_url( $cat_url ) .'" target="_self">Add category</a></li>';
			}
			?>
		</ul>

		<div class="tabs-content-realestate fullw">

			<div id="example-realestate-tab-1" class="tabs-panel-realestate">

				<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<input class="search_input" name="search" id="thisid" value="<?php echo esc_attr( $placeholder ); ?>" onfocus="if(this.value == '<?php echo esc_attr( $placeholder ); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php echo esc_attr( $placeholder ); ?>';}" type="text">


					<select name="property_type">
						<option value="Property Type">Property Type</option>
						<?php
							foreach ( highstand_real_estate::get_types() as $type) {
								echo '<option value="'. $type->term_id .'">'. $type->name .'</option>';
							}
						?>
					</select>

					<select name="budget">
						<option value="Budget">Budget</option>
						<option value="100000">$100,000</option>
						<option value="200000">$200,000</option>
						<option value="300000">$300,000</option>
						<option value="400000">$400,000</option>
						<option value="500000">$500,000</option>
						<option value="750000">$750,000</option>
						<option value="1000000">$1,000,000</option>
						<option value="2000000">$2,000,000</option>
						<option value="3000000">$3,000,000</option>
						<option value="4000000">$4,000,000</option>
						<option value="5000000">$5,000,000</option>
						<option value="10000000">$10,000,000</option>
					</select>

					<a href="#" class="but1 search_property"><i class="fa fa-search"></i> Search</a>
					<a href="#" class="but2 real_view_maps"><i class="fa fa-map-marker"></i> Map</a>

					<div class="clearfix margin_bottom2"></div>

					<a href="#" class="but3"><i class="fa fa-caret-right"></i> Advanced Search</a> <a href="#" class="but3"><i class="fa fa-caret-right"></i> Download Search APP</a>


					<input class="property_cat" type="hidden" name="property_cat" value="sale" />

				</form>

			</div><!-- end tab 1 -->

		</div>

	</div>
</div>