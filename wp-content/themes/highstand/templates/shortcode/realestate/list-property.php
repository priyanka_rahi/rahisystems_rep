<?php

$highstand = highstand::globe();

extract( $atts );

$args = array(
	'post_type' => 'property',
	'posts_per_page' => $number_show
);

$properties = new WP_Query( $args );

?>


<?php if(!empty( $wrapper_custom_class ) ) echo '<div class="'. esc_attr( $wrapper_custom_class ) .'">'; ?>

<div class="owl-carousel list-property-owl-carousel <?php echo esc_attr( $custom_class ); ?>">

<?php
	if ( $properties->have_posts() ) {
		while ( $properties->have_posts() ) {
			$properties->the_post();

			$thumbnail_url = $highstand->get_featured_image( $properties->post );
			$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '275x250xct' );

			$property_options = get_post_meta( $properties->post->ID , '_highstand_post_meta_options', true );
			?>
			<div class="item">
				<img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" class="rimg" />
				<span class="pro-title"><?php echo !empty($property_options['city'])?$property_options['city']:'City'; ?></span>

				<div class="box">
					<div class="box list">
						<div class="title">
							<h5><?php the_title(); ?></h5>
							<p><?php esc_html_e( 'by:', 'highstand' ); ?> <?php echo !empty($property_options['by_author'])?$property_options['by_author']:'King-Theme'; ?></p>
						</div>
						<div class="clearfix"></div>

						<?php echo !empty($property_options['address'])?wpautop($property_options['address']):'<p>Your address</p>'; ?>

						<p>
							<strong class="bigtfont"><?php echo !empty($property_options['unit'])?$property_options['unit']:'$'; ?><?php echo !empty($property_options['price'])?$property_options['price']:'0'; ?></strong> <?php esc_html_e( 'onwards', 'highstand' ); ?>
						</p>

						<div class="clearfix margin_top2"></div>

						<a href="<?php echo esc_url( get_permalink( $properties->post->ID ) ); ?>" class="real_button1"><?php esc_html_e( 'see details', 'highstand' ); ?></a>
					</div>
				</div>
			</div>
			<?php
		}
	} else {
		// no posts found
	}
	/* Restore original Post Data */
	wp_reset_postdata();
?>
</div>
<?php if(!empty( $wrapper_custom_class )) echo '</div>'; ?>