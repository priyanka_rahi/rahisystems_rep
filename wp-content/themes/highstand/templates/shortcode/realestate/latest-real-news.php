<?php

$highstand = highstand::globe();

extract( $atts );

$args = array(
	'post_type' => 'real_news',
	'posts_per_page' => $number_show
);

$real_news_query = new WP_Query( $args );

$i=0;
?>

<div class="real_news_wrapper <?php echo esc_attr( $custom_class ); ?>">
	<?php
		if ( $real_news_query->have_posts() ) {
			while ( $real_news_query->have_posts() ) {
				$real_news_query->the_post();

				$thumbnail_url = $highstand->get_featured_image( $real_news_query->post );
				$thumbnail_url = highstand_createLinkImage( $thumbnail_url, '375x250xct' );

				$i++;
				?>
				<div class="one_third_less <?php if($i%3 == 0) echo 'last'; ?>">
					<?php if(!empty($thumbnail_url)): ?>
						<img src="<?php echo esc_url( $thumbnail_url ); ?>" class="rimg" alt="<?php the_title(); ?>">
					<?php endif; ?>

					<div class="box">
						<h5><?php the_title(); ?></h5>
						<div class="clearfix margin_top2"></div>

						<p><?php echo wp_trim_words( get_the_content(), 40, '...' ); ?></p>

						<div class="clearfix margin_top2"></div>
						<a href="<?php the_permalink(); ?>" class="real_button1 nml"><?php esc_html_e( 'read more', 'highstand' ); ?></a>
					</div>
				</div>
				<?php
			}
		} else {
			// no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();
	?>
</div>