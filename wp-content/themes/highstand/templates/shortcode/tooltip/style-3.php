<?php
	
$highstand = highstand::globe();
$highstand_sc_css = highstand::globe('highstand_sc_css');

if( !isset( $highstand_sc_css ) )
	$highstand_sc_css = array();
	
$atts = $highstand->bag['atts'];
extract( $atts );


$position = trim($atts['position']);

$text_tooltip = '<span>'.$atts['text_tooltip'].'</span>';

echo '<a class="hs_tooltip style3" href="#" data-tooltip="true" data-position="'. esc_attr($position).'"> <i class="fa fa-'. esc_attr($atts['icon']) . ' fati17"></i>' . $text_tooltip .' </a>';

if(!in_array(__FILE__, (array)$highstand_sc_css)){	
	echo '<style>
	.hs_tooltip.style3{
		position: relative;
		display: inline-block;
	}
	.hs_tooltip.style3 span{
		background: #454545 none repeat scroll 0 0;
		border: 4px solid #454545;
		border-radius: 5px;
		bottom: 40px;
		box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.1);
		color: #fff;
		font-size: 14px;
		font-style: italic;
		font-weight: 400;
		height: auto;
		left: 50%;
		line-height: 21px;
		margin-left: 0;
		opacity: 0;
		padding: 10px;
		pointer-events: none;
		position: absolute;
		text-align: center;
		text-indent: 0;
		transition: all 0.3s ease-in-out 0s;
		width: 100px;
		z-index: 9999;
	}
	</style>';
	array_push($highstand_sc_css, __FILE__);
}


if($position == 'top'){
	echo '<style>
	.hs_tooltip.style3.top span:before, .hs_tooltip.style3.top span:after{
		border-left: 10px solid transparent;
		border-right: 10px solid transparent;
		border-top: 10px solid rgba(0, 0, 0, 0.1);
		bottom: -15px;
		content: "";
		height: 0;
		left: 50%;
		margin-left: -9px;
		position: absolute;
		width: 0;
	}
	.hs_tooltip.style3.top span::after {
		border-top: 10px solid #454545;
		bottom: -14px;
		margin-left: -10px;
	}
	.hs_tooltip.style3.top:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'right'){
	echo '<style>
	.hs_tooltip.style3.right span:before{
		border-color: transparent;
		border-left-width: 0;
		border-right-color: #454545;
		top: 50%;
		margin-top: -11px;
		content: " ";
		left: -24px;
		border-style: solid;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
		border-width: 10px;
	}
	.hs_tooltip.style3.right:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'bottom'){
	echo '<style>
	.hs_tooltip.style3.bottom span:before{
		border-color: transparent;
		border-width: 10px;
		border-style: solid;
		border-bottom-color: #454545;
		border-top-width: 0;
		content: " ";
		margin-left: -10px;
		top: -14px;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
		left: 50%;
	}
	.hs_tooltip.style3.bottom:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'left'){
	echo '<style>
	.hs_tooltip.style3.left span:after{
		border-color: transparent;
		border-left-color: #454545;
		border-right-width: 0;
		border-width: 10px;
		top: 50%;
		margin-top: -11px;
		content: " ";
		right: -24px;	
		border-style: solid;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
	}
	.hs_tooltip.style3.left:hover span{
		opacity: 1;
	}
	</style>';
}
?>