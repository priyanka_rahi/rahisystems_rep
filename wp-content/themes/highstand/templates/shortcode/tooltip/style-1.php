<?php
	
$highstand = highstand::globe();
$highstand_sc_css = highstand::globe('highstand_sc_css');

if( !isset( $highstand_sc_css ) )
	$highstand_sc_css = array();
	
$atts = $highstand->bag['atts'];
extract( $atts );

$img_icon = wp_get_attachment_image_src( $atts['img_icon'], 'full' );
$position = $atts['position'];

echo '<a class="hs_tooltip style1" href="#" data-tooltip="true" data-position="'. esc_attr($position).'">';
	
	if( isset( $img_icon[0] ) ) 
		echo '<img alt="" src="'. esc_attr($img_icon[0]) .'">';
	else if( !empty( $icon ) ) 
		echo '<i class="'. esc_attr($icon) .'"></i>';
	
echo '<h6>'. esc_html($textlink) .'</h6>
	<span>'.$text_tooltip.'</span>
</a>';

if(!in_array(__FILE__, (array)$highstand_sc_css)){
	echo '<style>
	.hs_tooltip.style1{
		background-color: #f3f3f3;
		border-radius: 100%;
		display: block;
		height: 120px;
		margin: 0 2px;
		outline: medium none;
		position: relative;
		text-align: center;
		width: 120px;
	}
	.hs_tooltip.style1 h6{
		margin-top: 10px;
	}
	.hs_tooltip.style1 span{
		background: #f9f9f9 none repeat scroll 0 0;
		border: 4px solid #fff;
		border-radius: 5px;
		bottom: 0;
		box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
		color: #719dab;
		font-family: "Alegreya SC",Georgia,serif;
		font-size: 14px;
		font-style: italic;
		font-weight: 400;
		height: auto;
		left: 50%;
		line-height: 21px;
		margin-left: 0;
		opacity: 0;
		padding: 10px;
		pointer-events: none;
		position: absolute;
		text-align: center;
		text-indent: 0;
		text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
		transition: all 0.3s ease-in-out 0s;
		width: 300px;
		z-index: 99;
	}
	</style>';
	array_push($highstand_sc_css, __FILE__);
}

if($position == 'top'){
	echo '<style>
	.hs_tooltip.style1.top span:before, .hs_tooltip.style1.top span:after{
		border-left: 10px solid transparent;
		border-right: 10px solid transparent;
		border-top: 10px solid rgba(0, 0, 0, 0.1);
		bottom: -15px;
		content: "";
		height: 0;
		left: 50%;
		margin-left: -9px;
		position: absolute;
		width: 0;
	}
	.hs_tooltip.style1.top span::after {
		border-top: 10px solid #fff;
		bottom: -14px;
		margin-left: -10px;
	}
	.hs_tooltip.style1.top:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'right'){
	echo '<style>
	.hs_tooltip.style1.right span:before{
		border-color: transparent;
		border-left-width: 0;
		border-right-color: #fff;
		top: 50%;
		margin-top: -11px;
		content: " ";
		left: -24px;
		border-style: solid;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
		border-width: 10px;
	}
	.hs_tooltip.style1.right:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'bottom'){
	echo '<style>
	.hs_tooltip.style1.bottom span:before{
		border-color: transparent;
		border-width: 10px;
		border-style: solid;
		border-bottom-color: #fff;
		border-top-width: 0;
		content: " ";
		margin-left: -10px;
		top: -14px;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
		left: 50%;
	}
	.hs_tooltip.style1.bottom:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'left'){
	echo '<style>
	.hs_tooltip.style1.left span:after{
		border-color: transparent;
		border-left-color: #fff;
		border-right-width: 0;
		border-width: 10px;
		top: 50%;
		margin-top: -11px;
		content: " ";
		right: -24px;	
		border-style: solid;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
	}
	.hs_tooltip.style1.left:hover span{
		opacity: 1;
	}
	</style>';
}
?>