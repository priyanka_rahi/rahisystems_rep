<?php

$highstand = highstand::globe();
$highstand_sc_css = highstand::globe('highstand_sc_css');

if( !isset( $highstand_sc_css ) )
	$highstand_sc_css = array();
	
$atts = $highstand->bag['atts'];
extract( $atts );

$position = $atts['position'];

$text_tooltip = '<span>'.$atts['text_tooltip'].'</span>';

echo '<a class="hs_tooltip style2 but_large1" href="#" data-tooltip="true" data-position="'. esc_attr($position).'"> '. esc_html($atts['textlink']) . $text_tooltip .' </a>';

if(!in_array(__FILE__, (array)$highstand_sc_css)){	
	echo '<style>
	.hs_tooltip.style2{
		position: relative;
		display: inline-block;
	}
	.hs_tooltip.style2 span{
		background: #f9f9f9 none repeat scroll 0 0;
		border: 4px solid #fff;
		border-radius: 5px;
		bottom: 0;
		box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
		color: #719dab;
		font-family: "Alegreya SC",Georgia,serif;
		font-size: 14px;
		font-style: italic;
		font-weight: 400;
		height: auto;
		left: 50%;
		line-height: 21px;
		margin-left: 0;
		opacity: 0;
		padding: 10px;
		pointer-events: none;
		position: absolute;
		text-align: center;
		text-indent: 0;
		text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
		transition: all 0.3s ease-in-out 0s;
		width: 300px;
		white-space: pre-wrap;
		z-index: 99;
	}
	</style>';
	array_push($highstand_sc_css, __FILE__);
}


if($position == 'top'){
	echo '<style>
	.hs_tooltip.style2.top span:before, .hs_tooltip.style2.top span:after{
		border-left: 10px solid transparent;
		border-right: 10px solid transparent;
		border-top: 10px solid rgba(0, 0, 0, 0.1);
		bottom: -15px;
		content: "";
		height: 0;
		left: 50%;
		margin-left: -9px;
		position: absolute;
		width: 0;
	}
	.hs_tooltip.style2.top span::after {
		border-top: 10px solid #fff;
		bottom: -14px;
		margin-left: -10px;
	}
	.hs_tooltip.style2.top:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'right'){
	echo '<style>
	.hs_tooltip.style2.right span:before{
		border-color: transparent;
		border-left-width: 0;
		border-right-color: #fff;
		top: 50%;
		margin-top: -11px;
		content: " ";
		left: -24px;
		border-style: solid;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
		border-width: 10px;
	}
	.hs_tooltip.style2.right:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'bottom'){
	echo '<style>
	.hs_tooltip.style2.bottom span:before{
		border-color: transparent;
		border-width: 10px;
		border-style: solid;
		border-bottom-color: #fff;
		border-top-width: 0;
		content: " ";
		margin-left: -10px;
		top: -14px;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
		left: 50%;
	}
	.hs_tooltip.style2.bottom:hover span{
		opacity: 1;
	}
	</style>';
}

if($position == 'left'){
	echo '<style>
	.hs_tooltip.style2.left span:after{
		border-color: transparent;
		border-left-color: #fff;
		border-right-width: 0;
		border-width: 10px;
		top: 50%;
		margin-top: -11px;
		content: " ";
		right: -24px;	
		border-style: solid;
		display: block;
		height: 0;
		position: absolute;
		width: 0;
	}
	.hs_tooltip.style2.left:hover span{
		opacity: 1;
	}
	</style>';
}
?>
