<?php

extract( $atts );

$images = explode( ',', $images );

$image_size = 'full';

foreach($images as $image_id){
	$attachment_data[] = wp_get_attachment_image_src( $image_id, $image_size );
	$attachment_data_full[] = wp_get_attachment_image_src( $image_id, 'full' );
}

?>


<div class="image_fadein_slider">
	<div class="image_fadein" data-rotate="<?php echo esc_attr( $rotate ); ?>">
	<?php
		
		if( !isset( $attachment_data[0] ) || empty( $attachment_data[0] ) ){
			
			echo '<h3>Images Gallery: No images found</h3>';
			
		}else{
		
			foreach($attachment_data as $i => $image){
				echo '<img src="'. esc_attr($image[0]) .'" />';
			}
				
		}
	?>
	</div>
</div>
