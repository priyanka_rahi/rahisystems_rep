<?php
/**
 * (c) king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$highstand = highstand::globe();

get_header();

?>

	<?php highstand::path( 'blog_breadcrumb' ); ?>

	<div id="primary" class="site-content container-content content content_fullwidth less2">
		<div id="content" class="row row-content container blog-2-columns">
			<div class="content_left">
				<?php

					while ( have_posts() ) : the_post();
					
						get_template_part( 'templates/blog/content', 'medium' );
						
					endwhile;
					
				?>
				<?php $highstand->pagination(); ?>
			</div>

			<div class="right_sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
				
<?php get_footer(); ?>		



		