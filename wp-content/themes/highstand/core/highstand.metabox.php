<?php
/**
 * (C) King-Theme.Com
 * Calls the class on the post edit screen.
 * Meta box version 1.0
 */

function highstand_call_metabox() {
    new highstand_metabox();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'highstand_call_metabox' );
    add_action( 'load-post-new.php', 'highstand_call_metabox' );
}

/**
 * The highstand_metabox Class.
 */
class highstand_metabox {

	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		//$this->_instance = $instance;

		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save' ) );
		add_action('admin_enqueue_scripts', array($this, 'enqueue_style'));
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_boxes( $post_type ) {

		$meta_box_list = array(
			array(
				'id'            => 'page_layout_metabox_options',
				'title'         => THEME_NAME.esc_html__( ' Theme - Page layout Settings', 'highstand' ),
				'callback'      => array( $this, 'page_layout_meta_box' ),
				'screen'        => array('page'),
				'context'       => 'advanced',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'page_metabox_options',
				'title'         => THEME_NAME.esc_html__( ' Theme - Page Settings', 'highstand' ),
				'callback'      => array( $this, 'page_meta_box' ),
				'screen'        => array('page'),
				'context'       => 'advanced',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'post_metabox_options',
				'title'         => esc_html__( 'Post - Options', 'highstand' ),
				'callback'      => array( $this, 'post_meta_box' ),
				'screen'        => array('post'),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'megamenu_metabox_options',
				'title'         => esc_html__( 'Mega menu - Options', 'highstand' ),
				'callback'      => array( $this, 'megamenu_meta_box' ),
				'screen'        => array('mega_menu'),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'testimonial_metabox_options',
				'title'         => esc_html__( 'Testimonial - Options', 'highstand' ),
				'callback'      => array( $this, 'testimonial_meta_box' ),
				'screen'        => array('testimonials'),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'our_team_metabox_options',
				'title'         => esc_html__( 'Staff Profiles - Options', 'highstand' ),
				'callback'      => array( $this, 'our_team_meta_box' ),
				'screen'        => array('our-team'),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'our_work_metabox_options',
				'title'         => esc_html__( 'Project\'s - Options', 'highstand' ),
				'callback'      => array( $this, 'our_work_meta_box' ),
				'screen'        => array('our-works'),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'pricing_metabox_options',
				'title'         => esc_html__( 'Pricing Tables Fields - Options', 'highstand' ),
				'callback'      => array( $this, 'pricing_meta_box' ),
				'screen'        => array('pricing_tables'),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'time_line_metabox_options',
				'title'         => esc_html__( 'Time Line Fields - Options', 'highstand' ),
				'callback'      => array( $this, 'time_line_meta_box' ),
				'screen'        => array('time-line'),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => array()
			),
			array(
				'id'            => 'events_metabox_options',
				'title'         => esc_html__( 'Events Fields - Options', 'highstand' ),
				'callback'      => array( $this, 'events_meta_box' ),
				'screen'        => array('events'),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => array()
			)
		);

		$meta_box_list = apply_filters( 'highstand_reg_meta_box_list', $meta_box_list );

		foreach($meta_box_list as $meta_box){
			if ( in_array( $post_type, $meta_box['screen'] )) {
				add_meta_box(
					$meta_box['id']
					,$meta_box['title']
					,$meta_box['callback']
					,$post_type
					,$meta_box['context']
					,$meta_box['priority']
					,$meta_box['callback_args']
				);
			}
		}
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
		global $post;
		if( !empty( $_POST['highstand'] ) ){
			if( !add_post_meta( $post->ID , '_'.THEME_OPTNAME.'_post_meta_options' , $_POST['highstand'], true ) ){
				update_post_meta( $post->ID , '_'.THEME_OPTNAME.'_post_meta_options' , $_POST['highstand'] );
			}
		}

	}



	public function page_layout_meta_box( $post ){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id' => 'body_bg',
				'type' => 'upload',
				'title' => esc_html__('Upload body background', 'highstand'),
				'sub_desc' => esc_html__('This will be display as background in boxed mode', 'highstand'),
				'desc' => esc_html__('Upload background image for website boxed mode.', 'highstand'),
				'std' => ''
			),
			array(
				'id' => 'page_layout',
				'type' => 'button_set',
				'title' => esc_html__('Select Layout', 'highstand'),
				'sub_desc' => esc_html__('Wide or boxed mode', 'highstand'),
				'desc' => '',
				'options' => array('wide' => 'WIDE','boxed' => 'BOXED', 'onepage' => 'ONE PAGE'),
				'std' => 'wide'
			),
			array(
				'id' => 'page_boxed_width',
				'type' => 'text',
				'title' => esc_html__('Boxed width', 'highstand'),
				'sub_desc' => esc_html__('Page box width', 'highstand'),
				'desc' => esc_html__('Enter width of boxed mode. (Unit px)', 'highstand')
			),
			array(
				'id' => 'boxed_margin_top',
				'type' => 'text',
				'title' => esc_html__('Boxed margin top', 'highstand'), 
				'sub_desc' => esc_html__('The boxed content margin top', 'highstand'),
				'desc' => esc_html__('Enter margin top of boxed mode. (Unit px)', 'highstand')
			)
		);

		$this->display($fields);
	}

	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function post_meta_box( $post ) {
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id' => 'feature_video',
				'type' => 'text',
				'title' => esc_html__('Enter feature video url', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => esc_html__('Enter feature video url, for example: https://www.youtube.com/watch?v=YRb-xF0RW-k', 'highstand')
			)
		);

		$this->display($fields);
	}

	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function page_meta_box( $post ) {
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );
			
		$listHeaders = array();
		if ( $handle = opendir( THEME_PATH.DS.'templates'.DS.'header' ) ){
			while ( false !== ( $entry = readdir($handle) ) ) {
				if( $entry != '.' && $entry != '..' && strpos($entry, '.php') !== false  ){
					$title  = ucwords( str_replace( '-', ' ', basename( $entry, '.php' ) ) );
					$listHeaders[ 'templates'.DS.'header'.DS.$entry ] = array('title' => $title, 'img' => THEME_URI.'/templates/header/thumbnails/'.basename( $entry, '.php' ).'.jpg');
				}
			}
		}
			
		$listBreadcrumbs = array();
		if ( $handle = opendir( THEME_PATH.DS.'templates'.DS.'breadcrumb' ) ){
			while ( false !== ( $entry = readdir($handle) ) ) {
				if( $entry != '.' && $entry != '..' && strpos($entry, '.php') !== false  ){
					$title  = ucwords( str_replace( '-', ' ', basename( $entry, '.php' ) ) );
					$listBreadcrumbs[ 'templates'.DS.'breadcrumb'.DS.$entry ] = array('title' => $title, 'img' => THEME_URI.'/templates/breadcrumb/thumbnails/'.basename( $entry, '.php' ).'.jpg');
				}
			}
		}

		$list_footers_style = array();
		$list_footers_style['global'] = 'From global setting';
		$list_footers_style['empty'] = 'Empty';
		$posts = get_posts( array('post_type' => 'highstand_footer', 'posts_per_page' => -1, 'order' => 'ASC') );
		foreach ($posts as $post) {
			$list_footers_style[$post->post_name] = $post->post_title;
		}

		$sidebars = array( '' => '--Select Sidebar--' );

		if( !empty( $highstand->cfg['sidebars'] ) ){
			foreach( $highstand->cfg['sidebars'] as $sb ){
				$sidebars[ sanitize_title_with_dashes( $sb ) ] = esc_html( $sb );
			}
		}

		$fields = array(
			array(
				'id'       => 'header',
				'type'     => 'profile_template',
				'title'    => esc_html__('Select Header', 'highstand'),
				'sub_desc' => esc_html__('Overlap: The header will cover up anything beneath it.', 'highstand'),
				'options'  => $listHeaders,
				'std'      => ''
			),
			array(
				'id'       => 'breadcrumb',
				'type'     => 'profile_template',
				'title'    => esc_html__('Display Breadcrumb', 'highstand'),
				'options'  => $listBreadcrumbs,
				'std'      => '',
				'sub_desc' => esc_html__( 'Set for show or dont show breadcrumb for this page.', 'highstand' )
			),
			array(
				'id'       => 'breadcrumb_bg',
				'type'     => 'upload',
				'title'    => esc_html__('Upload Breadcrumb Background Image', 'highstand'),
				'std'      => '',
				'sub_desc' => esc_html__( 'Upload your Breadcrumb background image for this page.', 'highstand' )
			),
			array(
				'id'       => 'sidebar',
				'type'     => 'select',
				'title'    => esc_html__('Select Sidebar', 'highstand'),
				'options'  => $sidebars,
				'std'      => '',
				'sub_desc' => esc_html__( 'Select template from Page Attributes at right side', 'highstand' ),
				'desc'     => '<br /><br />'.esc_html__( 'Select a dynamic sidebar what you created in theme-panel to display under page layout.', 'highstand' )
			),
			array(
				'id'       => 'footer_style',
				'type'     => 'footer_styles',
				'title'    => esc_html__('Select Footer Styles', 'highstand'),
				'sub_desc' => wp_kses( __('<br />Select footer for all pages, You can also go to each page to select specific.', 'highstand'), array( 'br'=>array() )),
				'options'  => $list_footers_style,
				'std'      => 'default'
			),
			array(
				'id'       => 'description',
				'type'     => 'textarea',
				'title'    => esc_html__('Description', 'highstand'),
				'std'      => '',
				'sub_desc' => esc_html__( 'The description will show in content of meta tag for SEO + Sharing purpose', 'highstand' ),
			),
		);

		echo '<textarea name="highstand[vc_cache]" id="highstand_vc_cache" style="display:none">'.esc_html( get_post_meta( $post->ID, '_highstand_page_vc_cache', true) ).'</textarea>';

		$this->display($fields);
	}


	public function megamenu_meta_box( $post ){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id' => 'menu_width',
				'type' => 'text',
				'title' => esc_html__('Width for mega menu', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => esc_html__('Enter custom width for mega menu (Unit: px). For example: 800px', 'highstand')
			)
		);

		$this->display($fields);
	}

	public function testimonial_meta_box( $post ){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );
		
		$fields = array(
			array(
				'id' => 'website',
				'type' => 'text',
				'title' => esc_html__('Position', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			),
			array(
				'id' => 'sub_title',
				'type' => 'text',
				'title' => esc_html__('Sub Title', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			)
		);

		$this->display($fields);
	}

	public function our_team_meta_box(){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id' => 'position',
				'type' => 'text',
				'title' => esc_html__('Position', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			),
			array(
				'id' => 'website',
				'type' => 'text',
				'title' => esc_html__('Member profile link', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			),		
			array(
				'id' => 'facebook',
				'type' => 'text',
				'title' => esc_html__('Facebook', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			),
			array(
				'id' => 'twitter',
				'type' => 'text',
				'title' => esc_html__('Twitter', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			),
			array(
				'id' => 'gplus',
				'type' => 'text',
				'title' => esc_html__('Google+', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			),
			array(
				'id' => 'youtube',
				'type' => 'text',
				'title' => esc_html__('Youtube', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			),
			array(
				'id' => 'linkedin',
				'type' => 'text',
				'title' => esc_html__('Linkin', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			),
			array(
				'id' => 'envelope',
				'type' => 'text',
				'title' => esc_html__('E-mail', 'highstand'),
				'std'	=> '',
				'sub_desc' => '',
				'desc' => ''
			)
		);

		$this->display($fields);
	}


	public function our_work_meta_box(){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id'       => 'link',
				'type'     => 'text',
				'title'    => esc_html__('Link', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'outhor',
				'type'     => 'text',
				'title'    => esc_html__('Create by', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'our_date',
				'type'     => 'text',
				'title'    => esc_html__('Date Create', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'images_list',
				'type'     => 'images',
				'title'    => esc_html__('Images', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'video_link',
				'type'     => 'text',
				'title'    => esc_html__('Video Link', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => 'Please insert video link from youtube.com or vimeo.com'
			)
		);

		if( isset( $highstand->cfg['our_works_field'] ) && is_array( $highstand->cfg['our_works_field'] ) ){
			foreach($highstand->cfg['our_works_field'] as $k => $value){
				if($value != ''){
					$work_val = $value;
					$work_val = strtolower( $work_val );
					$work_val = trim( $work_val );
					$work_val = preg_replace('~[^a-z0-9]+~i', '-', $work_val);
					$work_id  = 'ow_extra_' . $work_val;
					$fields[] = array(
						'id'		=> $work_id,
						'type'		=> 'text',
						'title'		=> $value,
						'std'		=> '',
						'sub_desc'	=> '',
						'desc'		=> ''
					);
				}
			}
		}

		$this->display($fields);
	}

	public function pricing_meta_box(){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id'       => 'price',
				'type'     => 'text',
				'title'    => esc_html__('Price', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'per',
				'type'     => 'text',
				'title'    => esc_html__('Per', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => 'For example: per month, year'
			),
			array(
				'id'       => 'regularly_price',
				'type'     => 'text',
				'title'    => esc_html__('Regularly Price', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'currency',
				'type'     => 'text',
				'title'    => esc_html__('Currency', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => esc_html__('A system of money in general use in a particular country.', 'highstand')
			),
			array(
				'id'       => 'best_seller',
				'type'     => 'radio',
				'title'    => esc_html__('Best Seller', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => '',
				'options'  => array(
					'yes' => 'Yes',
					'no'  => 'No'
				)
			),
			array(
				'id'       => 'attr',
				'type'     => 'textarea',
				'title'    => esc_html__('Attributes', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'textsubmit',
				'type'     => 'text',
				'title'    => esc_html__('Text Button Submit', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'linksubmit',
				'type'     => 'text',
				'title'    => esc_html__('Link Submit', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'textmore',
				'type'     => 'text',
				'title'    => esc_html__('Text Button Read More', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'linkmore',
				'type'     => 'text',
				'title'    => esc_html__('Link Read More', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			),
			array(
				'id'       => 'before_attr',
				'type'     => 'textarea',
				'title'    => esc_html__('Before Attributes List', 'highstand'),
				'std'      => '',
				'sub_desc' => '',
				'desc'     => ''
			)
		);

		$this->display($fields);
	}

	public function time_line_meta_box(){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id'       => 'timeline',
				'type'     => 'text',
				'title'    => esc_html__('Date Time', 'highstand'),
				'sub_desc' => esc_html__('Date time', 'highstand'),
				'desc'     => esc_html__('Enter date time', 'highstand')
			)
		);

		$this->display($fields);
	}

	public function events_meta_box(){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id'       => 'events_link',
				'type'     => 'text',
				'title'    => esc_html__('Link', 'highstand'),
				'sub_desc' => esc_html__('Link', 'highstand'),
				'desc'     => esc_html__('Enter link to events', 'highstand')
			),
			array(
				'id'       => 'events_date',
				'type'     => 'text',
				'title'    => esc_html__('Date', 'highstand'),
				'sub_desc' => esc_html__('Date', 'highstand'),
				'desc'     => esc_html__('Enter date events', 'highstand')
			),
			array(
				'id'       => 'events_datetime',
				'type'     => 'text',
				'title'    => esc_html__('Time Events', 'highstand'),
				'sub_desc' => esc_html__('time Events', 'highstand'),
				'desc'     => esc_html__('Enter time events', 'highstand')
			),
			array(
				'id'       => 'events_map_title',
				'type'     => 'text',
				'title'    => esc_html__('Location Title', 'highstand'),
				'sub_desc' => esc_html__('Location Title', 'highstand'),
				'desc'     => esc_html__('Enter location address', 'highstand')
			),
			array(
				'id'       => 'events_map_embed',
				'type'     => 'textarea',
				'title'    => esc_html__('Map Embed', 'highstand'),
				'sub_desc' => esc_html__('Map Embed', 'highstand'),
				'desc'     => esc_html__('Enter map embed', 'highstand')
			)
		);

		$this->display($fields);
	}


	public function display($fields){
		$html = $this->_before();
		$html .= $this->ren_fields($fields);
		$html .= $this->_after();

		print( $html );
	}

	public function _before(){
		return '<div class="nhp-opts-group-tab single-page-settings" style="display:block;padding:0px;">
			<table class="form-table" style="border:none;">
				<tbody>';
	}
	
	public function _after(){
		return '</tbody>
			</table>
		</div>';
	}
	
	public function ren_fields($fields){
		global $post;
		$post_type = $post->post_type;
		
		$html = '';
		
		foreach( $fields as $key => $field ){
			
			$field_data = get_post_meta( $post->ID,'_'.THEME_OPTNAME.'_post_meta_options' , true );
			if($field_data){
				if(!empty($field_data[$field['id']])){
					$field['std'] = $field_data[$field['id']];
				}else{
					$field['std'] = '';
				}
				
			}else{
				$field['std'] = '';
			}			
			
			if( empty( $field['std'] ) ){
				if( $field['id'] == 'header' ){
					$field['std'] = 'default';
				}
				if( $field['id'] == 'footer' ){
					$field['std'] = 'default';
				}
				if(  $field['id'] == 'breadcrumb' ){
					$field['std'] = 'global';
				}
			}
			
			locate_template( 'options'.DS.'fields'.DS.$field['type'].'/field_'.$field['type'].'.php', true );
			
			$field_class = THEME_OPTNAME.'_options_'.$field['type'];
			
			if( class_exists( $field_class ) ){
				
				$render = '';
				$obj = new stdClass();
				$obj->extra_tabs = '';
				$obj->sections = '';
				$obj->args = '';
				$render = new $field_class($field, $field['std'], $obj );
				
				$html .= '<tr><th scope="row">'.esc_html($field['title']).'<span class="description">';
				$html .= esc_html($field['sub_desc']).'</span></th>';
				$html .= '<td>';
				
				ob_start();
				$render->render();
				$html .= ob_get_clean();
				
				if( method_exists( $render, 'enqueue' ) ){
					$render->enqueue();
				}	
				
				$html .= '</td></tr>';
			}
		}
		
		return $html;
	}
	
	public function enqueue_style(){
		wp_enqueue_style('highstand-metabox-admin', THEME_URI.'/core/assets/css/metabox-admin.css', false, time() );
	}
}