<?php
/**
*
* (C) King-Theme.com
*
*/

/********************************************************/
/*                        Actions                       */
/********************************************************/

	// Constants
	
	$theme = wp_get_theme();
	if( !empty( $theme['Template'] ) ){
		$theme = wp_get_theme($theme['Template']);
	}
	define('THEME_NAME', $theme['Name'] );
	define('THEME_SLUG', $theme['Template'] );
	define('THEME_VERSION', $theme['Version'] );
	
	defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

	define('HOME_URL', home_url() );
	define('SITE_URI', site_url() );
	define('SITE_URL', site_url() );
	define('THEME_URI', get_template_directory_uri() );
	define('THEME_PATH', get_template_directory() );
	define('THEME_CPATH', get_template_directory().DS.'core'.DS );
	define('THEME_SPATH', get_stylesheet_directory() );
	define('THEME_OPTNAME', 'highstand');
	
	if( !class_exists( 'highstand' ) ){
		highstand_incl_core( 'core'.DS.'highstand.class.php' );
	}
	
	### Start Run FrameWork ###
	$loader = new highstand();
	highstand::set_globe( $loader );
	$loader->init();
	### End FrameWork ###
