<?php

/**
*
*	Theme functions
*	(c) king-theme.com
*
*/
$highstand = highstand::globe();
/*----------------------------------------------------------*/
#	Theme Setup
/*----------------------------------------------------------*/
function highstand_themeSetup() {

	load_theme_textdomain( 'highstand', get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// This theme supports a variety of post formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ,'title','editor','author','thumbnail','excerpt','custom-fields','page-attributes') );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'topmenu' => esc_html__( 'Top menu', 'highstand' ),
		'primary' => esc_html__( 'Primary Menu', 'highstand' ),
		'onepage' => esc_html__( 'One Page Menu', 'highstand' ),
		'footer'  => esc_html__( 'Footer Menu', 'highstand' )
	));
	
	/*
	 * This theme supports custom background color and image,
	 * and here we also set up the default background color.
	 */
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );
	
	add_theme_support( "custom-header", array() ); 

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );

	add_theme_support( "title-tag" );

}
add_action( 'after_setup_theme', 'highstand_themeSetup' );


/**
 * Child Theme 
 **/
function highstand_child_theme_enqueue( $url ){
	
	$highstand = highstand::globe('highstand');

	if( $highstand->template != $highstand->stylesheet ){
		$path = str_replace( THEME_URI, ABSPATH.'wp-content'.DS.'themes'.DS.$highstand->stylesheet, $url );
		$path = str_replace( array( '\\', '\/' ), array(DS, DS), $path );

		if( file_exists( $path ) ){
			return str_replace( DS, '/', str_replace( ABSPATH , SITE_URI.'/', $path ) );
		}else{
			return $url;
		}
		
	}else{
		
		return $url;
		
	}
}

/**
 * Color mode
 **/
if( !empty( $_GET['mode'] ) && !empty( $_GET['color'] ) ){
	if( $_GET['mode'] == 'css-color-style' ){
		$color = urldecode( $_GET['color'] );
		$rgb = $highstand->hex2rgb( $color );
		$darkercolor = $highstand->hex2rgb( $color, 30 );
		$brightercolor = $highstand->hex2rgb( $color, -30 );
		$color_rgb = $highstand->hex2rgb( $color );
		$file = highstand_child_theme_enqueue( THEME_PATH.DS.'assets'.DS.'css'.DS.'colors'.DS.'color-primary.css' );
		$file = str_replace( SITE_URI.'/', ABSPATH, str_replace( '/', DS, $file ) );
		if (file_exists($file)) {
			$handle = $highstand->ext['fo']( $file, 'r' );
			$css_data = $highstand->ext['fr']( $handle, filesize( $file ) );
			header("Content-type: text/css", true);
			echo str_replace( array( '{color}', '{darkercolor}', '{brightercolor}', '{color_rgb}' ), array( $color, 'rgb('.$darkercolor.')', 'rgb('.$brightercolor.')', $color_rgb ), $css_data );
		}	
		exit;
	}
}


/*-----------------------------------------------------------------------------------*/
# Menu sidebar on mobile
/*-----------------------------------------------------------------------------------*/
function highstand_responsive_sidebar_menu_set_post(){
	$highstand = highstand::globe('highstand');
	
	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';
		
	echo '<script type="text/javascript">var highstand_set_pos_sidebar_menu = "'. $position .'";</script>';
}
add_action('wp_head', 'highstand_responsive_sidebar_menu_set_post');



function highstand_responsive_sidebar_menu(){
	$highstand = highstand::globe('highstand');
	
	$position = !empty($highstand->cfg['sidebar_menu_pos']) ? $highstand->cfg['sidebar_menu_pos'] : 'left';
	
	echo '<div class="sb-slidebar sb-'. $position .'"></div>';
}
add_action('wp_footer', 'highstand_responsive_sidebar_menu');


/*-----------------------------------------------------------------------------------*/
# Comment template
/*-----------------------------------------------------------------------------------*/

function highstand_comment( $comment, $args, $depth ) {

	$GLOBALS['comment'] = $comment;
	
	switch ( $comment->comment_type ) :
		case 'pingback' : break;
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php esc_html_e( 'Pingback:', 'highstand' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( esc_html__( 'Edit', 'highstand' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class('comment_wrap'); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			
			<?php
				$avatar_size = 68;
				if ( '0' != $comment->comment_parent )
					$avatar_size = 39;

				echo '<div class="gravatar">'.get_avatar( $comment, $avatar_size ).'</div>';
						
			?>			
			<div class="comment_content">
				<div class="comment_meta">
					<div class="comment_author">
						<?php
							/* translators: 1: comment author, 2: date and time */
							printf( esc_html__( '%1$s - %2$s ', 'highstand' ),
								sprintf( '%s', get_comment_author_link() ),
								sprintf( '<i>%1$s</i>',
									sprintf( esc_html__( '%1$s at %2$s', 'highstand' ), get_comment_date(), get_comment_time() )
								)
							);
						?>
	
						<?php edit_comment_link( esc_html__( 'Edit', 'highstand' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .comment-author .vcard -->
	
					<?php if ( $comment->comment_approved == '0' ) : ?>
						<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'highstand' ); ?></em>
						<br />
					<?php endif; ?>
	
				</div>

				<div class="comment_text">
					<?php comment_text(); ?>
					<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'highstand' ).'<span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?> 
				</div>
				
			</div>
		</article><!-- #comment-## -->

	<?php
	break;
	endswitch;
}

/*-----------------------------------------------------------------------------------*/
# Display title with options format
/*-----------------------------------------------------------------------------------*/

add_filter('wp_title', 'highstand_filter_title');
function highstand_filter_title( $title ){
	
	global $highstand, $paged, $page;

	$title = trim( str_replace( array( '&raquo;', get_bloginfo( 'name' ), '|' ),array( '', '', ''), $title ) );
	
	if( $highstand->cfg['titleSeparate'] == '' )$highstand->cfg['titleSeparate'] = '&raquo;';
	
	ob_start();
	
	if( is_home() || is_front_page() )
	{
		if( !empty( $highstand->cfg['homeTitle'] ) )
		{
			echo esc_html( str_replace( array('%Site Title%', '%Tagline%' ), array( get_bloginfo( 'name' ), get_bloginfo( 'description', 'display' ) ), $highstand->cfg['homeTitle'] ) );
		}else{
			$site_description = get_bloginfo( 'description', 'display' );
			if( $highstand->cfg['homeTitleFm'] == 1 )
			{
				bloginfo( 'name' );
				if ( $site_description )
					echo ' '.$highstand->cfg['titleSeparate']." $site_description";	
				
			}else if( $highstand->cfg['homeTitleFm'] == 2 ){
				if ( $site_description )
					echo esc_html( $highstand->cfg['titleSeparate'] )." $site_description";
				bloginfo( 'name' );
			}else{
				bloginfo( 'name' );
			}
		}	
	
	}else if( is_page() || is_single() )
	{
		
		if( $highstand->cfg['postTitleFm'] == 1 )
		{

			echo esc_html( $title.' '.$highstand->cfg['titleSeparate'].' ' );
			bloginfo( 'name' );
			
		}else if( $highstand->cfg['postTitleFm'] == 2 ){
			bloginfo( 'name' );
			echo esc_html( ' '.$highstand->cfg['titleSeparate'].' '.$title );
		}else{
			echo esc_html( $title );
		}
		
	}else{
		if( $highstand->cfg['archivesTitleFm'] == 1 )
		{
			echo esc_html( $title.' '.$highstand->cfg['titleSeparate'].' ' );
			bloginfo( 'name' );
			
		}else if( $highstand->cfg['archivesTitleFm'] == 2 ){
			bloginfo( 'name' );
			echo esc_html( ' '.$highstand->cfg['titleSeparate'].' '.$title );
		}else{
			echo esc_html( $title );
		}
	}
	if ( $paged >= 2 || $page >= 2 )
		echo esc_html( ' '.$highstand->cfg['titleSeparate'].' ' . 'Page '. max( $paged, $page ) );
		
	$out = ob_get_contents();
	ob_end_clean();
	
	return $out;	
}
	
/*-----------------------------------------------------------------------------------*/
# Set meta tags on header for SEO onpage
/*-----------------------------------------------------------------------------------*/
function highstand_meta(){

	global $highstand, $post;
	?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="generator" content="king-theme" />
<?php if( isset($highstand->cfg['responsive']) && $highstand->cfg['responsive'] == 1 ){ ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta name="apple-mobile-web-app-capable" content="yes" />
<?php }
	
	//show meta tags on frontend
	if( isset( $highstand->cfg['metatag']) && $highstand->cfg['metatag'] == 1 ):
		if( is_home() || is_front_page() ){ ?>
<meta name="description" content="<?php echo esc_attr( $highstand->cfg['homeMetaDescription'] ); ?>" />
<meta name="keywords" content="<?php echo esc_attr( $highstand->cfg['homeMetaKeywords'] ); ?>" />
<?php }else{ ?>
<meta name="description" content="<?php echo esc_attr( $highstand->cfg['otherMetaDescription'] ); ?>" />
<meta name="keywords" content="<?php echo esc_attr( $highstand->cfg['otherMetaKeywords'] ); ?>" />
		<?php }
	endif;
	
	if( isset($highstand->cfg['ogmeta']) && $highstand->cfg['ogmeta'] == 1 && ( is_page() || is_single() ) ){
		
?> <meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo get_permalink( $post->ID ); ?>" />
<meta property="og:title" content="<?php echo esc_attr( $post->post_title ); ?>" />
<meta property="og:description" content="<?php
			
			if( is_front_page() || is_home() ){ 
				echo esc_attr( bloginfo( 'description' ) ); 
			}else {
				
				if( !empty( $post->ID ) ){
					
					$pagedes = get_post_meta( $post->ID, 'highstand', true );
					
					if( isset( $pagedes ) && isset( $pagedes['description'] ) && !empty( $pagedes['description'] ) ){
						echo esc_attr( $pagedes['description'] );
					}else if( !empty( $post->post_excerpt ) ){
						echo esc_attr( wp_trim_words( $post->post_excerpt, 50 ) );
					}else if( strpos( $post->post_content, '[kc_row') === false ){
						echo esc_attr( wp_trim_words( $post->post_content, 50 ) );
					}else{
						echo esc_attr( $post->post_title );
					}
				}
				
			} 
			
		echo '" />'."\n";
		
		$meta_image = $highstand->get_featured_image( $post );
		if( !empty( $highstand->cfg['logo'] ) && strpos( $meta_image, 'default.jpg') ){
			$meta_image = $highstand->get_featured_image( $post );
		}
	
		echo '<meta property="og:image" content="'.esc_url( $meta_image ).'" />'."\n";
	
	}// End If 
	
	//show meta tags on frontend
	if( isset($highstand->cfg['metatag']) && $highstand->cfg['metatag'] == 1 ):
		if( !empty( $highstand->cfg['authorMetaKeywords'] ) ){
			echo '<meta name="author" content="'.esc_attr( $highstand->cfg['authorMetaKeywords'] ).'" />';
		}

		if( !empty( $highstand->cfg['contactMetaKeywords'] ) ){
			echo '<meta name="contact" content="'.esc_attr( $highstand->cfg['contactMetaKeywords'] ).'" />';
		}	
	endif;
	
	echo '<link rel="pingback" href="'.get_bloginfo( 'pingback_url' ).'" />'."\n";

	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {	
		if( !empty( $highstand->cfg['favicon'] ) ){
			echo '<link rel="shortcut icon" href="'. esc_url( $highstand->cfg['favicon'] ).'" type="image/x-icon" />'."\n";
		}
	}else{
		wp_site_icon();
	}
	
}

/*-----------------------------------------------------------------------------------*/
# Filter content at blog posts
/*-----------------------------------------------------------------------------------*/
function highstand_the_content_filter( $content ) {
  
  if( is_home() ){
	  
	  $content = preg_replace('/<ifr'.'ame.+src=[\'"]([^\'"]+)[\'"].*iframe>/i', '', $content );
	  
  }
  
  return $content;
}

add_filter( 'the_content', 'highstand_the_content_filter' );


/**
 * Blog link
 **/
function highstand_blog_link() {
  
  if( get_option( 'show_on_front', true ) ){
	  
	  $_id = get_option( 'page_for_posts', true );
	  if( !empty( $_id ) ){
		  echo get_permalink( $_id );
		  return;
	  }
  }
  
  echo SITE_URI;
  
}

/**
 * Create image link
 **/
function highstand_createLinkImage( $source, $attr ){

	$highstand = highstand::globe('highstand');
	
	$attr = explode( 'x', $attr );
	$arg = array();
	if( !empty( $attr[2] ) ){
		$arg['w'] = $attr[0];
		$arg['h'] = $attr[1];
		$arg['a'] = $attr[2];
		if( $attr[2] != 'c' ){
			$attr = '-'.implode('x',$attr);
		}else{
			$attr = '-'.$attr[0].'x'.$attr[1];
		}
	}else if( !empty( $attr[0] ) && !empty( $attr[1] ) ){
		$arg['w'] = $attr[0];
		$arg['h'] = $attr[1];
		$attr = '-'.$attr[0].'x'.$attr[1];
	}else{
		return $source;
	}
	
	$source = strrev( $source );
	$st = strpos( $source, '.');
	
	if( $st === false ){
		return strrev( $source ).$attr;
	}else{
		
		$file = str_replace( array( SITE_URI.'/', '\\', '/' ), array( ABSPATH, DS, DS ), strrev( $source ) );
		
		$_return = strrev( substr( $source, 0, $st+1 ).strrev($attr).substr( $source, $st+1 ) );
		$__return = str_replace( array( SITE_URI.'/', '\\', '/' ), array( ABSPATH, DS, DS ), $_return );

		if( file_exists( $file ) && !file_exists( $__return ) ){
			ob_start();
			$highstand->processImage( $file, $arg, $__return );
			ob_end_clean();
		}
		
		return $_return;
		
	}
}


/**
 * Is Shop Function
 **/
if( !function_exists( 'is_shop' ) ){
	function is_shop(){
		return false;
	}
}

function highstand_random_string( $length = 10 ){
	$str = "";
	$allow_characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$_max_length = count($allow_characters) - 1;

	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $_max_length);
		$str .= $allow_characters[$rand];
	}

	return $str;
}

/*
* Function return styles array from string font param of VC
*/
function highstand_get_styles($font_container_data) {
	$styles = array();
	if ( ! empty( $font_container_data ) && isset( $font_container_data['values'] ) ) {
		foreach ( $font_container_data['values'] as $key => $value ) {
			if ( $key !== 'tag' && strlen( $value ) > 0 ) {
				if ( preg_match( '/description/', $key ) ) {
					continue;
				}
				if ( $key === 'font_size' || $key === 'line_height' ) {
					$value = preg_replace( '/\s+/', '', $value );
				}
				if ( $key === 'font_size' ) {
					$pattern = '/^(\d*(?:\.\d+)?)\s*(px|\%|in|cm|mm|em|rem|ex|pt|pc|vw|vh|vmin|vmax)?$/';
					// allowed metrics: http://www.w3schools.com/cssref/css_units.asp
					$regexr = preg_match( $pattern, $value, $matches );
					$value = isset( $matches[1] ) ? (float) $matches[1] : (float) $value;
					$unit = isset( $matches[2] ) ? $matches[2] : 'px';
					$value = $value . $unit;
				}
				if ( strlen( $value ) > 0 ) {
					$styles[] = str_replace( '_', '-', $key ) . ': ' . $value;
				}
			}
		}
	}
	return $styles;
}


/**
 * Get site logo
 **/ 
function highstand_get_logo(){
	global $highstand, $post;
	
	if(is_page() || is_single()){
		$post_options = get_post_meta($post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);
		$post_options = str_replace( '%SITE_URI%', SITE_URI, $post_options ); 
		
		if(!empty($post_options['logo'])){
			return $post_options['logo'];
		}else if(!empty($highstand->cfg['logo'])){
			return $highstand->cfg['logo'];
		}else{
			return get_template_directory_uri().'/assets/images/logo.png';
		}
	}else{
		if(!empty($highstand->cfg['logo'])){
			return $highstand->cfg['logo'];
		}else{
			return get_template_directory_uri().'/assets/images/logo.png';
		}
	}
}

function highstand_title($display = true){
	 global $wp_locale, $page, $paged;
 
    $m = get_query_var('m');
    $year = get_query_var('year');
    $monthnum = get_query_var('monthnum');
    $day = get_query_var('day');
    $search = get_query_var('s');
    $title = '';
 
    $t_sep = ' > '; // Temporary separator, for accurate flipping, if necessary
 
    // If there is a post
    if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
        $title = single_post_title( '', false );
    }
 
    // If there's a post type archive
    if ( is_post_type_archive() ) {
        $post_type = get_query_var( 'post_type' );
        if ( is_array( $post_type ) )
            $post_type = reset( $post_type );
        $post_type_object = get_post_type_object( $post_type );
        if ( ! $post_type_object->has_archive )
            $title = post_type_archive_title( '', false );
    }
 
    // If there's a category or tag
    if ( is_category() || is_tag() ) {
        $title = single_term_title( '', false );
    }
 
    // If there's a taxonomy
    if ( is_tax() ) {
        $term = get_queried_object();
        if ( $term ) {
            $tax = get_taxonomy( $term->taxonomy );
            $title = single_term_title( $tax->labels->name . $t_sep, false );
        }
    }
 
    // If there's an author
    if ( is_author() && ! is_post_type_archive() ) {
        $author = get_queried_object();
        if ( $author )
            $title = $author->display_name;
    }
 
    // Post type archives with has_archive should override terms.
    if ( is_post_type_archive() && $post_type_object->has_archive )
        $title = post_type_archive_title( '', false );
 
    // If there's a month
    if ( is_archive() && !empty($m) ) {
        $my_year = substr($m, 0, 4);
        $my_month = $wp_locale->get_month(substr($m, 4, 2));
        $my_day = intval(substr($m, 6, 2));
        $title = $my_year . ( $my_month ? $t_sep . $my_month : '' ) . ( $my_day ? $t_sep . $my_day : '' );
    }
 
    // If there's a year
    if ( is_archive() && !empty($year) ) {
        $title = $year;
        if ( !empty($monthnum) )
            $title .= $t_sep . $wp_locale->get_month($monthnum);
        if ( !empty($day) )
            $title .= $t_sep . zeroise($day, 2);
    }
 
    // If it's a search
    if ( is_search() ) {
        /* translators: 1: separator, 2: search phrase */
        $title = sprintf(__('Search Results %1$s %2$s', 'highstand'), $t_sep, strip_tags($search));
    }
 
    // If it's a 404 page
    if ( is_404() ) {
        $title = esc_html__('Page not found', 'highstand');
    }
 
    
    // Send it out
    if ( $display )
        echo highstand::esc_js( $title );
    else
        return $title;
}

if(!function_exists('randomstring')):
function randomstring($length) {
	$key = null;
    $keys = array_merge(range(0,9), range('a', 'z'));
    for($i=0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }
    return $key;
}
endif;


function highstand_global_shortcode_css($content){
	$highstand_sc_css = highstand::globe('highstand_sc_css');
	
	$highstand_sc_css = array();
		
	return $content;
}
add_filter('the_content', 'highstand_global_shortcode_css');


function highstand_get_css( $value = array() ) {
	$css = ''; 		
	$prefix = '.';
	
	if ( ! empty( $value ) && is_array( $value ) ) {
		foreach($value as $class => $style){
			$pos = strpos($class, '#');
			if($pos!== false && $pos == 0){
				$prefix = '';
			}
			$css .= $prefix.$class.'{';
			foreach ( $style as $key => $value ) {
				if ( ! empty( $value ) && $key != "media" ) {
					if ( $key == "background-image" ) {
						$css .= $key . ":url('" . $value . "');";
					} else {
						$css .= $key . ":" . $value . ";";
					}
				}
			}
			$css .= '}'."\n";
		}
	}

	return $css;
}
/*
 * @param array $array1
 * @param array $array2
 * @return array
 * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
 * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
 */
function array_merge_recursive_distinct(array &$array1, array &$array2)
{
    $merged = $array1;
    foreach ($array2 as $key => &$value)
    {
        if (is_array($value) && isset($merged[$key]) && is_array($merged[$key]))
        {
            $merged[$key] = array_merge_recursive_distinct($merged[$key], $value);
        }
        else
        {
            $merged[$key] = $value;
        }
    }
    return $merged;
}


if(!function_exists('body_style')):
function body_style( $style = null ){
	global $post;

	if(is_page() || is_single()){
		$body_style = '';
		$post_data = get_post_meta( $post->ID , '_'.THEME_OPTNAME.'_post_meta_options', TRUE );

		if( !empty($post_data['body_bg'] ) ){
			$body_style .= 'background: url('. str_replace( '%SITE_URI%', SITE_URI, $post_data['body_bg'] ) .') fixed center top #000;';
		}

		$body_style .= $style;
		echo 'style="'. esc_attr($body_style) .'"';
	}
}
endif;


function highstand_hex2rgb( $hexColor ){
	
  $shorthand = (strlen($hexColor) == 4);

  list($r, $g, $b) = $shorthand? sscanf($hexColor, "#%1s%1s%1s") : sscanf($hexColor, "#%2s%2s%2s");

  return hexdec($shorthand? "$r$r" : $r).', '.hexdec($shorthand? "$g$g" : $g).','.hexdec($shorthand? "$b$b" : $b);
  
}

function highstand_post_meta_options($post_id = null){
	global $post;

	$post_data = '';

	if(is_page() || is_single()){
		if($post_id == null){
			$post_id = $post->ID;
		}

		$post_data = get_post_meta( $post->ID , '_'.THEME_OPTNAME.'_post_meta_options', TRUE );
	}

	return $post_data;
}


function highstand_disable_search_in_menu(){
	$highstand = highstand::globe('highstand');

	if(isset($highstand->cfg['searchNav']) && $highstand->cfg['searchNav'] == 'hide'){
		echo '<script type="text/javascript">jQuery("li.nav-search").remove();</script>';
	}

}
add_action( 'wp_footer', 'highstand_disable_search_in_menu' );


function highstand_fix_breadcrumb_same_color(){
	if( !is_admin() ){
		global $post;

		if(is_page()){
			$options = get_post_meta( $post->ID, '_'.THEME_OPTNAME.'_post_meta_options', TRUE);

			if(isset($options['breadcrumb']['_file_'])){
				$breadcrumb = $options['breadcrumb']['_file_'];
			}

			if( isset($breadcrumb) && 'templates/breadcrumb/style-4.php' == $breadcrumb ){
				add_filter( 'body_class', 'highstand_breadcrumb_same_color' );
			}
		}
	}

}
add_action( 'get_header', 'highstand_fix_breadcrumb_same_color' );

function highstand_breadcrumb_same_color( $classes ) {
	$classes[] = 'breadcrumb_same_color';
	return $classes;
}


function highstand_move_comment_field_to_bottom( $fields ) {

	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;

}
add_filter( 'comment_form_fields', 'highstand_move_comment_field_to_bottom' );