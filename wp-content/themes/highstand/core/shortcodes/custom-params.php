<?php
/**
 * Register new param type for KingComposer
 */
 
$highstand = highstand::globe();


add_action('init', 'highstand_param_type_init', 99 );
 
function highstand_param_type_init(){
 
    global $kc;
	
	if( isset( $kc ) && method_exists($kc, 'add_param_type') ){
		$kc->add_param_type( 'select_layout', 'highstand_select_layout_param_type' );
	}
    
}

/*--------------------------------------------------
# Select layout
---------------------------------------------------*/

function highstand_select_layout_param_type(){
?>
	<select class="kc-param" name="{{data.name}}">
		<#
		if( data.options ){
			for( var n in data.options ){
				if( typeof data.options[n] == 'array' ||  typeof data.options[n] == 'object' ){
				#><optgroup label="{{n}}"><#
					for( var m in data.options[n] ){
						#><option<#
							if( m == data.value ){ #> selected<# }
							#> value="{{m}}">{{data.options[n][m]}}</option><#
					}
				#></optgroup><#

				}else{

		#><option<#
					if( n == data.value ){ #> selected<# }
				#> value="{{n}}">{{data.options[n]}}</option><#
				}
			}
		} #>
	</select>
	<input type="hidden" name="tp_shortcode_url" value="<?php echo get_template_directory_uri(); ?>/templates/shortcode/" />
	<div class="shortcode_layout_preview"></div>
	<#
		data.callback = function( wrp, $ ){
			wrp.find('select').on('change', function() {
			  	var layout = $(this).val(),
			  		shortcode_url = $(this).next("input").val();

			  	layout = layout.substr(0, layout.lastIndexOf(".")) + ".jpg";

			  	var slp = $(this).next('input').next('.shortcode_layout_preview');
			  	slp.css('top', $(this).offset().top);
			  	slp.html('<img src="'+ shortcode_url + layout +'" />').fadeIn('fast');
			  	
			}).change();

			wrp.find('select').on('blur', function(){
				//$('.shortcode_layout_preview').fadeOut('fast');
			});
		}

	#>
<?php
}