<?php

class highstand_tiny_shortcode{

	public function __construct(){
		add_action( 'init', array( &$this, 'register') );

		add_action('wp_ajax_load_tinyshortcodes', array( $this, 'ajax_load' ));
	}

	public function register(){
		$highstand = highstand::globe('highstand');

		foreach( $this->shortcodes_list() as $name => $data ){
			$highstand->ext['asc']( $name, array( __CLASS__, $name ) );
		}
	}

	public function shortcodes_list(){

		$list_shortcodes = array(
			'hs_social' => array(
				'icon' => 'fa fa-share-square',
				'data' => '[hs_social class="" count=5 eff="true"]'
			),
			'hs_img'	=> array(
				'icon' => 'fa fa-picture-o',
				'data' => '[hs_img w="" h="" src="your_url" alt=""]'
			),
			'hs_phone'	=> array(
				'icon' => 'fa fa-phone-square',
				'data' => '[hs_phone number="+1-541-754-3010"]'
			),
			'hs_email'	=> array(
				'icon' => 'fa fa-envelope-o',
				'data' => '[hs_email email="contact@king-theme.com"]'
			),
			'hs_comming_soon'	=> array(
				'data' => '[hs_comming_soon]'
			),
			'hs_link'	=> array(
				'icon' => 'fa fa-link',
				'data' => '[hs_link class="" icon="fa-heart" href="http://king-theme.com" title="View" echo="true"]'
			),
			'hs_rss_link'	=> array(
				'icon' => 'fa fa-rss-square',
				'data' => '[hs_rss_link]'
			),
		);

		return $list_shortcodes;
	}


	/*
	 * Load for custom header in theme panel
	 */
	public function ajax_load(){
		$html = '';
		$list_shortcodes = $this->shortcodes_list();

		foreach ($list_shortcodes as $key => $value) {
			$html .= '<a class="button button-small '. esc_attr($value['icon']) .'" href="javascript:;" data-shortcode="'. esc_attr($value['data']) .'">['. $key .']</a>';
		}

		$output = array(
			'message' => 'ok',
			'html'	=> $html
		);

		wp_send_json($output);
	}


	public static function hs_rss_link($atts, $content = null){
		$atts = shortcode_atts( array(
			'class' => ''
		), $atts );

		extract( $atts );

		$rss_link = get_bloginfo( 'rss_url' );

		return '
			<span class="' . esc_attr( $atts['class'] ) . '">
				<i class="fa fa-rss"></i> <a href="' . esc_attr( $rss_link ) . '">' . esc_html__( 'RSS', 'highstand' ) . '</a>
			</span>
		';
	}


	public static function hs_email($atts, $content = null){
		$atts = shortcode_atts( array(
			'class' => '',
			'email' => ''
		), $atts );

		extract( $atts );

		return '
			<span class="' . esc_attr( $class ) . '">
				<i class="fa fa-envelope"></i> <a href="mailto:' . esc_attr( $email ) . '">' . esc_html( $email ) . '</a>
			</span>
		';
	}


	public static function hs_comming_soon($atts, $content = null){
		$atts = shortcode_atts( array(), $atts );

		extract( $atts );

		$output = '';
		ob_start();

			highstand::template('coming-soon');

		$result = ob_get_contents();
		ob_end_clean();
		$output .= $result;

		return $output;
	}
	/*
	* return social list [hs_social class="yourclass" eff="true" count="5"]
	* @param int $count Max social icons display
	* @param string $class Class for list
	* @param boolean $eff Enable effect showing icon
	*/
	public static function hs_social($atts , $content = null){
		$highstand = highstand::globe('highstand');
		// Attributes
		extract( shortcode_atts(
			array(
				'class' => '',
				'count' => 5,
				'eff' => true,
			), $atts )
		);

		return $highstand->socials($class, $count, $eff, false);
	}

	/*
	* Display an image on position [hs_img width="100" height="100" class="yourclass"]http://yourdomain.com/yourimage.png[/hs_img]
	* @param int $width Width of image
	* @param int $height Height of image
	* @param string $class Extra class for image
	*/
	public static function hs_img($atts , $content = null){
		// Attributes
		$atts = shortcode_atts(array(
				'class' => '',
				'w' => '',
				'h' => '',
				'src' => '',
				'alt' => ''
			), $atts );

		extract( $atts );

		if ( !empty($src) ){
			if( is_numeric($src) ){
				$link_img = wp_get_attachment_image_src( $atts['id'], 'full' );
				$src = $link_img[0];
			}
		} else {
			return esc_html__('Image not found', 'highstand');
		}

		$image_attribute = array();

		if(!empty($w)){
			$image_attribute[] = 'width="'. esc_attr($w) .'"';
		}

		if(!empty($h)){
			$image_attribute[] = 'height="'. esc_attr($h) .'"';
		}

		if(!empty($alt)){
			$image_attribute[] = 'height="'. esc_attr($alt) .'"';
		}

		return '
			<figure class="' . esc_attr( $class ) . '">
				<img '. implode(' ', $image_attribute) .'  src="' . esc_url( $src ) . '" alt="">
			</figure>
		';
	}

	/*
	* Display phone number [hs_phone class="yourclass"]0123456789[/hs_phone]
	* @param string $class Extra class for image
	*/
	public static function hs_phone($atts , $content = null){
		$atts = shortcode_atts( array(
			'class' => '',
			'number' => ''
		), $atts );

		extract( $atts );

		$highstand = highstand::globe('highstand');

		$number = preg_replace('/[^0-9]/', '', $atts['number']);

		if( !isset($number) )
			$number = $highstand->cfg['topInfoPhone'];

		return '
			<span class="' . esc_attr( $atts['class'] ) . '">
				<i class="fa fa-phone"></i> <a href="tel:' . esc_attr( $number ) . '">' . esc_attr( $atts['number'] ) . '</a>
			</span>
		';
	}

	/*
	* Display a link [hs_link href="http://yourdomain.com"]Your Text[/hs_link]
	* @param string $class Extra class for image
	* @param string $icon Icon for link. Struct 'fa-home'
	* @param string $href Link url for anchor
	*/
	public static function hs_link($atts , $content = null){
		// Attributes
		extract( shortcode_atts(
			array(
				'class' => '',
				'icon' => '',
				'url' => '',
				'title' => '',
				'echo' => ''
			), $atts )
		);

		$link_url = '';
		
		if ( !empty( $url ) ) {
			if ( !filter_var( $url, FILTER_VALIDATE_URL ) === false ) {
				$link_url = $url;
			} elseif ( is_numeric( $url ) ) {
				$link_url = get_permalink( $url );
			} elseif ( ( strpos( $url, " " ) !== false ) || ( preg_match( '/[A-Z]/', $url ) ) ) {
				$post_id = get_page_by_title( $url, 'OBJECT', 'post' );
				if ( isset( $post_id->ID ) && $post_id->ID > 0 ) {
					$link_url = get_permalink( $post_id->ID );
				} else {
					$link_url = get_permalink( get_page_by_title( $url ) );
				}
			} else {
				$post_id = get_page_by_path( $url, 'OBJECT', 'post' );
				if ( isset( $post_id->ID ) && $post_id->ID > 0 ) {
					$link_url = get_permalink( $post_id->ID );
				} else {
					$link_url = get_permalink( get_page_by_path( $url ) );
				}
			}

			if ( empty( $link_url ) ) {
				return esc_html__( 'link not found', 'highstand' );
			}
		}

		if ( $echo == 'false' ) {
			return $link_url;
		} else {
			if ( empty( $title ) ) {
				$title = esc_html__( 'View Link', 'highstand' );
			}

			$output = '';

			$output .= '<span class="' . esc_attr( $class ) . '">';

				if ( !empty( $icon ) ) {
					$output .= '<i class="fa '. $icon .'"></i>';
				}

				$output .= '<a href="'. esc_url( $link_url ) .'">'. esc_html( $title ) .'</a>';

			$output .= '</span>';

			return $output;
		}
	}

}

new highstand_tiny_shortcode();
