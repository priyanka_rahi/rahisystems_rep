<?php

class highstand_shortcodes {

	static $youTubePlayerReady = false;
	static $king_carousel_atts = array();
	static $elements;

	public static function register() {

		$highstand = highstand::globe('highstand');

		highstand_incl_core( 'core'.DS.'shortcodes'.DS.'assets.php', 'ro', array() );
		highstand_incl_core( 'core'.DS.'shortcodes'.DS.'tools.php', 'ro', array() );
		//highstand_incl_core( 'core'.DS.'shortcodes'.DS.'elements.php', 'ro', array() );

		//self::$elements = new highstand_elements();

		foreach( array(
			'hs_image_fadein',
			'hs_posts',
			'hs_events',
			'hs_team',
			'hs_work',
			'hs_testimonials',
			'hs_piechart',
			'hs_pricing',
			'hs_progress',
			'hs_divider',
			'hs_box_alert',
			'hs_button',
			'hs_tooltip',
			'hs_counter',
			'hs_flip_clients',
			'hs_flip_content',
			'hs_latest_posts',
			'hs_services',
			'hs_contact_form7',
			'hs_search_menu',
			'hs_subscribe',
			'hs_timeline',
			'hs_template_image'
		) as $name ){
			$highstand->ext['asc']( $name, array( __CLASS__, $name ) );
		}
	}


	/*
	* return Post list
	*/
	public static function get_posts($atts){
		$atts  = shortcode_atts( array(
			'id'	                      => '',
			'amount'	                  => 4,
			'category'	                  => '',
			'tax_term'	                  => '',
			'post_type'	                  => '',
			'taxonomy'	                  => '',
			'items'	                      => 4,
			'order'	                      => 'desc',
			'offset'	                  => 0,
			'post_parent'                 => false,
			'ignore_sticky_posts'         => false,
			'post_status'                 => 'publish',
		), $atts);

		//some shortcodes use amount/items param for limit items.
		if(intval($atts['amount']) !=4){
			$atts['items']        = intval($atts['amount']);
		}
		//assign category for work shortcode
		if($atts['tax_term'] !=''){
			$atts['category']     = $atts['tax_term'];
		}

		//prepare arguments for get_posts function
		$args = array(
			'posts_per_page'   => intval($atts['items']),
			'orderby'          => 'menu_order post_date date title',
			'order'            => $atts['order'],
			'post_type'        => sanitize_text_field($atts['post_type']),
			'post_status'      => $atts['post_status'],
			'offset' 		   => intval($atts['offset']),
			'suppress_filters' => true,
		);

		//get posts from list IDs
		if ( !empty( $atts['id'] )) {
			$posts_in = array_map( 'intval', explode( ',', $atts['id'] ) );
			$args['post__in'] = $posts_in;
		}
		if( $atts['ignore_sticky_posts'] === 'yes' ){
			$args['ignore_sticky_posts'] = true;
		}



		//category filter
		if( !empty( $atts['category'] ) ){
			$args['tax_query'] = array(
				array(
					'taxonomy' => $atts['taxonomy'],
					'field'    => 'slug',
					'terms'    => explode( ',', $atts['category'] )
				)
			);
		}

		//return data with list of posts object
		return get_posts( $args );
	}


	public static function hs_image_fadein( $atts = null, $content = null ){
		$highstand = highstand::globe('highstand');

		$atts = shortcode_atts( array(
			'title' => '',
			'images' => '',
			'rotate' => '3000',
			'wrap_class' => ''
		), $atts, 'hs_image_fadein' );

		$_return = '';
		
		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.'image_fadein'.DS.'image_fadein.php', 'i', $atts, $content );
			$_return = ob_get_contents();

		ob_end_clean();

		return $_return;

	}

	public static function hs_button( $atts = null, $content = null ) {
		$highstand = highstand::globe('highstand');

		$atts = shortcode_atts( array(
			'textbutton' => 'Text button',
			'link' => '',
			'button_hover_background_color' => '',
			'button_text_color' => '',
			'button_hover_text_color' => '',
			'button_hover_border_color' => '',
			'but_style' => 'but_style1',
			'button_size' => '',
			'show_icon' => '',
			'icon' => '',
			'icon_color' => '',
			'icon_position' => '',
			'show_tooltip' => '',
			'text_tooltip' => '',
			'class' => '',
			'css' => '',
		), $atts, 'button' );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.'button'.DS.'button.php', 'i', $atts, $content );
			$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}


	public static function hs_tooltip( $atts = null, $content = null ){
		$highstand = highstand::globe('highstand');

		$atts = shortcode_atts( array(
			'style' => 'tooltip/style-1.php',
			'img_icon' => '',
			'icon' => '',
			'textlink' => '',
			'text_tooltip' => '',
			'position' => 'top',
			'link' => '',
			'class' => '',
		), $atts, 'tooltip' );

		$highstand->bag = array(
			'atts' => $atts
		);

		$_return = '';

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.$atts['style'], 'i', $atts, $content );
			$_return = ob_get_contents();

		$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}


	public static function hs_counter( $atts = null, $content = null ){
		$highstand = highstand::globe('highstand');

		$atts = shortcode_atts( array(
			'title' => '',
			'style' => 'counter/style-1.php',
			'number' => '',
			'color' => '',
			'icon' => '',
			'class' => '',
		), $atts, 'counter' );

		$highstand->bag = array(
			'atts' => $atts
		);

		$_return = '';

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.$atts['style'], 'i', $atts, $content );
			$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}


	public static function hs_posts( $atts = null, $content = null ) {

		$highstand = highstand::globe('highstand');

		$atts = shortcode_atts( array(
			'items' => 20,
			'category' => '',
			'per_row' => 4,
			'class' => '',
			'cl_class' => '',
			'words' => 20,
			'img_size' => '245x245',
			'format' => '',
			'post_type' => 'post',
			'taxonomy' => 'category',
			'order' => 'DESC' ), $atts, 'faq' );

		$atts['format'] = strip_tags( $atts['format'] );

		$posts = self::get_posts( $atts );

		if( !count( $posts ) ){
			return '<h4>' . esc_html__( 'Article not found', 'highstand' ) . '</h4> <a href="'.admin_url('post-new.php').'"><i class="fa fa-plus"></i> Add New Article</a>';
		}

		$eff = rand(0,10);
		if( $eff <= 2 ){$eff = 'eff-fadeInUp';}else if( $eff > 2 && $eff <=4 ){$eff = 'eff-fadeInRight';}
		else if( $eff > 4 && $eff <=8 ){$eff = 'eff-fadeInLeft';}else{$eff = 'eff-flipInY';}

		$columns = array( '1' => 'one_full', '2' => 'one_half', '3' => 'one_third', '4' => 'one_fourth', '5' => 'one_fifth' );

		$_return = '';

		if( !empty( $atts['class'] ) ){
			$_return = '<div class="'.esc_attr( $atts['class'] ).'">';
		}
		$i = 1;
		foreach( $posts as $post ){

			$title = esc_html( $post->post_title );
			$des = esc_html( wp_trim_words( $post->post_content, $atts['words'] ) );

			$img_size = explode( 'x', $atts['img_size'] );
			$_w = intval( !empty( $img_size[0] ) ? $img_size[0] : 245 );
			$_h = intval( !empty( $img_size[1] ) ? $img_size[1] : 245 );
			$_a = !empty( $img_size[2] ) ? esc_attr($img_size[2] ) : 'c';

			$categories_list = get_the_category_list( esc_html__( ', ', 'highstand' ), '', $post->ID );
			$categories = explode( ',', $categories_list );
			if( count( $categories ) == 1 ){
				$categories = $categories_list;
			}else if( count( $categories ) > 1 ){
				$categories_list = '';
				foreach( $categories as $categorie ){
					if( strpos( $categorie, 'Uncategorized' ) === false ){
						$categories_list .= $categorie.', ';
					}
				}
				$categories_list .= '.';
				$categories = str_replace( ', .', '', $categories_list );
			}else{
				$categories = $categories_list;
			}

			$img = highstand_createLinkImage(highstand::get_featured_image($post ), $_w.'x'.$_h.'x'.$_a );

			$link = get_permalink( $post->ID );

			$_return .= '<div class="animated delay-'.$i.'00ms '.$eff;
			if( $i % $atts['per_row'] == 0 )$_return .= ' last ';
			if( empty( $atts['cl_class'] ) ){
				$_return .= ' '.esc_attr( $columns[ $atts['per_row'] ] );
			}else{
				$_return .= ' '.$atts['cl_class'];
			}
			$_return .= '">';

			$_return .= str_replace(
							array('{title}', '{img}', '{des}', '{link}', '{category}'),
							array( $title, $img, $des, $link, $categories ), $atts['format'] );

			$_return .= '</div>';

			$i++;

		}

		if( !empty( $atts['class'] ) ){
			$_return .= '</div>';
		}

		return do_shortcode( $_return );

	}


	public static function hs_events( $atts = null, $content = null ) {
		$highstand = highstand::globe('highstand');
		$error = null;

		$atts = shortcode_atts( array(
				'template'            => 'events/layout-1.php',
				'id'                  => false,
				'items'     	 	  => '12',
				'post_type'           => 'events',
				'taxonomy'            => 'events-category',
				'words'       		  => 30,
				'category'            => false,
				'order'               => 'DESC',
				'orderby'             => 'menu_order post_date date title',
				'post_parent'         => false,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 'no',
				'custom_class'        => ''
			), $atts, 'events' );

		$events_posts = self::get_posts( $atts );

		$atts = array_merge( $atts, array( 'events_posts' => $events_posts ) );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.$atts['template'], 'i', $atts, $content );
			$output = ob_get_contents();

		ob_end_clean();

		return $output;
	}


	public static function hs_team( $atts = null, $content = null ) {
		$highstand = highstand::globe('highstand');
		$error = null;

		$atts = shortcode_atts( array(
				'template'            => '',
				'id'                  => false,
				'items'     	 	  => get_option( 'posts_per_page' ),
				'post_type'           => 'our-team',
				'taxonomy'            => 'our-team-category',
				'words'       		  => 30,
				'category'            => false,
				'order'               => 'DESC',
				'orderby'             => 'menu_order post_date date title',
				'post_parent'         => false,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 'no',
				'link_view'           => 'no',
				'custom_class'        => ''
			), $atts, 'team' );

		$team_posts = self::get_posts( $atts );

		$atts = array_merge( $atts, array( 'team_posts' => $team_posts ) );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.$atts['template'], 'i', $atts, $content );
			$output = ob_get_contents();

		ob_end_clean();

		if( function_exists( 'kc_js_callback' ) )
			kc_js_callback('highstand_ourteam_init');

		return $output;
	}

	public static function hs_work( $atts = null, $content = null ) {
		$highstand = highstand::globe('highstand');
		// Prepare error var
		$error = null;
		// Parse attributes

		$atts = shortcode_atts( array(
				'template'               => '',
				'id'                     => false,
				'class'                  => '',
				'items'     		     => get_option( 'posts_per_page' ),
				'gap'     		     	 => '0',
				'post_type'              => 'our-works',
				'taxonomy'               => 'our-works-category',
				'column'           	     => '2',
				'tax_term'               => false,
				'order'                  => 'desc',
				'filter'			     => 'No',
				'margin'			     => 'Yes',
				'ignore_sticky_posts'    => 'no',
				'show_link'			 	 => 'yes'
			), $atts, 'work' );

		//get posts
		$posts = self::get_posts($atts);

		$atts = array_merge( $atts, array('works' => $posts) );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.$atts['template'], 'i', $atts, $content );
			$output = ob_get_contents();

		ob_end_clean();
		
		if( function_exists( 'kc_js_callback' ) )
			kc_js_callback('highstand_ourworks_init');
		
		return $output;
	}


	public static function hs_testimonials( $atts = null, $content = null ) {
		
		$highstand = highstand::globe('highstand');

		$error = null;

		$atts = shortcode_atts( array(
				'template'            => '',
				'id'                  => false,
				'layout'     		  => 'slide',
				'items'        		  => get_option( 'posts_per_page' ),
				'post_type'           => 'testimonials',
				'taxonomy'            => 'testimonials-category',
				'words'          	  => 100,
				'category'            => false,
				'order'               => 'DESC',
				'orderby'             => 'menu_order post_date date title',
				'post_parent'         => false,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 'no',
				'custom_class' 		  => ''
			), $atts, 'testimonials' );

		$original_atts = $atts;

		$author = '';
		$id = $atts['id'];
		$ignore_sticky_posts = ( bool ) ( $atts['ignore_sticky_posts'] === 'yes' ) ? true : false;
		$meta_key = '';
		$offset = '';
		$order = sanitize_key( $atts['order'] );
		$orderby = sanitize_key( $atts['orderby'] );
		$post_parent = $atts['post_parent'];
		$post_status = $atts['post_status'];
		$post_type = sanitize_text_field( $atts['post_type'] );
		$posts_per_page = intval( $atts['items'] );
		$tag = '';
		$tax_operator = '';
		$tax_term = sanitize_text_field( $atts['category'] );
		$taxonomy = sanitize_key( $atts['taxonomy'] );

		$words = sanitize_key( $atts['words'] );
		$items = sanitize_key( $atts['items'] );
		$layout = sanitize_key( $atts['layout'] );

		$args = array(
			'category_name'  => '',
			'order'          => $order,
			'orderby'        => $orderby,
			'post_type'      => explode( ',', $post_type ),
			'posts_per_page' => $posts_per_page
		);

		if ( $ignore_sticky_posts ) $args['ignore_sticky_posts'] = true;

		if ( !empty( $meta_key ) ) $args['meta_key'] = $meta_key;

		if ( $id ) {
			$posts_in = array_map( 'intval', explode( ',', $id ) );
			$args['post__in'] = $posts_in;
		}


		$post_status = explode( ', ', $post_status );
		$validated = array();
		$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
		foreach ( $post_status as $unvalidated ) {
			if ( in_array( $unvalidated, $available ) ) $validated[] = $unvalidated;
		}
		if ( !empty( $validated ) ) $args['post_status'] = $validated;

		if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {

			$tax_term = explode( ',', $tax_term );
			// Validate operator
			if ( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ) $tax_operator = 'IN';
			$tax_args = array( 'tax_query' => array( array(
						'taxonomy' => $taxonomy,
						'field' => ( is_numeric( $tax_term[0] ) ) ? 'id' : 'slug',
						'terms' => $tax_term,
						'operator' => $tax_operator ) ) );
			// Check for multiple taxonomy queries
			$count = 2;
			$more_tax_queries = false;
			while ( isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) &&
				isset( $original_atts['tax_' . $count . '_term'] ) &&
				!empty( $original_atts['tax_' . $count . '_term'] ) ) {
				// Sanitize values
				$more_tax_queries = true;
				$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
				$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
				$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts[
				'tax_' . $count . '_operator'] : 'IN';
				$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
				$tax_args['tax_query'][] = array( 'taxonomy' => $taxonomy,
					'field' => 'slug',
					'terms' => $terms,
					'operator' => $tax_operator );
				$count++;
			}
			if ( $more_tax_queries ):
				$tax_relation = 'AND';
			if ( isset( $original_atts['tax_relation'] ) &&
				in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) )
			) $tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
			endif;
			$args = array_merge( $args, $tax_args );
		}

		//$posts = new WP_Query( $args );
		$testimonials = self::get_posts($atts);
		$atts = array_merge( $atts, array('testimonials' => $testimonials) );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.$atts['template'], 'i', $atts, $content );
			$output = ob_get_contents();

		ob_end_clean();

		wp_reset_postdata();
		
		if( function_exists( 'kc_js_callback' ) )
			kc_js_callback('highstand_testimonials_init');
		
		return $output;
	}


	public static function hs_timeline( $atts = null, $content = null ) {
		$highstand = highstand::globe('highstand');

		$error = null;

		$atts = shortcode_atts( array(
				'template'            => 'timeline/layout-1.php',
				'id'                  => false,
				'layout'     		  => 'slide',
				'items'        		  => get_option( 'posts_per_page' ),
				'post_type'           => 'time-line',
				'taxonomy'            => 'time-line-category',
				'words'          	  => 100,
				'category'            => false,
				'order'               => 'DESC',
				'orderby'             => 'menu_order post_date date title',
				'post_parent'         => false,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 'no',
				'custom_class' 		  => ''
			), $atts, 'time-line' );

		$original_atts = $atts;

		$author = '';
		$id = $atts['id'];
		$ignore_sticky_posts = ( bool ) ( $atts['ignore_sticky_posts'] === 'yes' ) ? true : false;
		$meta_key = '';
		$offset = '';
		$order = sanitize_key( $atts['order'] );
		$orderby = sanitize_key( $atts['orderby'] );
		$post_parent = $atts['post_parent'];
		$post_status = $atts['post_status'];
		$post_type = sanitize_text_field( $atts['post_type'] );
		$posts_per_page = intval( $atts['items'] );
		$tag = '';
		$tax_operator = '';
		$tax_term = sanitize_text_field( $atts['category'] );
		$taxonomy = sanitize_key( $atts['taxonomy'] );

		$words = sanitize_key( $atts['words'] );
		$items = sanitize_key( $atts['items'] );

		$args = array(
			'category_name'  => '',
			'order'          => $order,
			'orderby'        => $orderby,
			'post_type'      => explode( ',', $post_type ),
			'posts_per_page' => $posts_per_page
		);

		if ( $ignore_sticky_posts ) $args['ignore_sticky_posts'] = true;

		if ( !empty( $meta_key ) ) $args['meta_key'] = $meta_key;

		if ( $id ) {
			$posts_in = array_map( 'intval', explode( ',', $id ) );
			$args['post__in'] = $posts_in;
		}


		$post_status = explode( ', ', $post_status );
		$validated = array();
		$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
		foreach ( $post_status as $unvalidated ) {
			if ( in_array( $unvalidated, $available ) ) $validated[] = $unvalidated;
		}
		if ( !empty( $validated ) ) $args['post_status'] = $validated;

		if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {

			$tax_term = explode( ',', $tax_term );
			// Validate operator
			if ( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ) $tax_operator = 'IN';
			$tax_args = array( 'tax_query' => array( array(
						'taxonomy' => $taxonomy,
						'field' => ( is_numeric( $tax_term[0] ) ) ? 'id' : 'slug',
						'terms' => $tax_term,
						'operator' => $tax_operator ) ) );
			// Check for multiple taxonomy queries
			$count = 2;
			$more_tax_queries = false;
			while ( isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) &&
				isset( $original_atts['tax_' . $count . '_term'] ) &&
				!empty( $original_atts['tax_' . $count . '_term'] ) ) {
				// Sanitize values
				$more_tax_queries = true;
				$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
				$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
				$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts[
				'tax_' . $count . '_operator'] : 'IN';
				$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
				$tax_args['tax_query'][] = array( 'taxonomy' => $taxonomy,
					'field' => 'slug',
					'terms' => $terms,
					'operator' => $tax_operator );
				$count++;
			}
			if ( $more_tax_queries ):
				$tax_relation = 'AND';
			if ( isset( $original_atts['tax_relation'] ) &&
				in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) )
			) $tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
			endif;
			$args = array_merge( $args, $tax_args );
		}

		//$posts = new WP_Query( $args );
		$timeline = self::get_posts($atts);
		$atts = array_merge( $atts, array('timeline' => $timeline) );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.$atts['template'], 'i', $atts, $content );
			$output = ob_get_contents();

		ob_end_clean();

		wp_reset_postdata();

		return $output;
	}


	public static function hs_piechart( $atts = null, $content = null ) {


		$atts = shortcode_atts( array(
				'size'   => 7,
				'style' => 'piechart1',
				'percent'  => '75',
				'color' => '#333',
				'text'  => '',
				'class'  => '',
				'rand'	=> rand(354345,2353465),
				'fx'	=> array(15,16,18,22,27,30,35,40,50)
			), $atts, 'piechart' );

		if( $atts['style'] == 'piechart3' ){
			$atts['color'] = '#fff';
		}

		$_action = $atts['percent'].'|'.$atts['fx'][ $atts['size'] ].'px|'.(($atts['size']+2)*10).'|'.$atts['color'].'|'.($atts['size']+2);


		ob_start();

		$atts['class'] .= ' s'.$atts['size'].' '.$atts['style'];

		echo '<div class="'.$atts['class'].' piechart" data-option="'.str_replace( array( '"', "'" ), array( '', '' ), $_action ).'">';
		echo '<canvas class="loader'.$atts['rand'].'"></canvas>';
		if( $atts['text'] != '' )echo ' <br /> '.$atts['text'];
		echo '</div>';

		$_return = ob_get_contents();
		ob_end_clean();

		su_query_asset( 'js', 'classyloader' );

		return $_return;

	}

	public static function hs_pricing( $atts = null, $content = null, $tag) {

		global $highstand, $wpdb, $highstand_shortcodes_data;

		$atts = shortcode_atts( array(
				'amount'	=> 4,
				'category'	=> '',
				'active'	=> 4,
				'icon_show'	=> 1,
				'icon'		=> '',
				'order'		=> 'ASC',
				'template'	=> 'pricing/layout-1.php',
				'currency'	=> '$',
				'color'		=> '',
				'class'		=> '',
				'post_type' => 'pricing_tables',
				'taxonomy' => 'pricing_tables-category',
			), $atts, 'pricing' );

		$prcs = self::get_posts($atts );
		$highstand_shortcodes_data[$tag]['atts'] = $atts;

		$output = '';

		if( $atts['icon'] != '' ){
			$atts['icon'] = '<i class="fa '.str_replace( 'icon: ', '', $atts['icon'] ).'"></i> ';
		}

		$content = $prcs;

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.$atts['template'], 'i', $atts, $content );
			$output = ob_get_contents();

		ob_end_clean();

		return $output;

	}


	public static function hs_progress( $atts = null, $content = null ) {


		$atts = shortcode_atts( array(
				'style'   => 1,
				'percent'  => '75',
				'color' => '',
				'text'  => 'Website Design',
				'class'  => '',
			), $atts, 'piechart' );

		ob_start();

		$colour = '';

		if( $atts['color'] != '' ){
			if( $atts['style'] != 4 ){
				$colour = 'border-bottom: 10px solid '.$atts['color'].'';
			}else{
				$colour = 'background: '.$atts['color'].'';
			}
		}
		
		?>
		<h5><?php echo esc_html( $atts['text'] ); ?></h5>
		<div class="ui-progress-bar ui-progress-bar<?php echo esc_attr( $atts['style'] ); ?> king-progress-bar ui-container <?php echo esc_attr( $atts['class'] ); ?>">
			<div class="ui-progress ui-progress<?php echo esc_attr( $atts['style'] ); ?>"  style="<?php echo esc_attr( $colour ); ?>;">
				<span class="ui-label">
					<b class="value"><?php echo esc_html( $atts['percent'] ); ?>%</b>
				</span>
			</div>
		</div>
		<br />

		<?php

		$_return = ob_get_contents();
		ob_end_clean();

		su_query_asset( 'js', 'progress-bar' );
		su_query_asset( 'css', 'progress-bar' );

		return $_return;

	}


	public static function hs_divider( $atts = null, $content = null ) {

		$atts = shortcode_atts( array(
				'style'   => 1,
				'icon'	 => '',
				'class'    => ''
			), $atts, 'dediver' );

		if( $atts['icon'] != '' ){
			$atts['style'] = $atts['style'].' divider-icon';
		}

		$_return = '<div class="divider_line'.esc_attr($atts['style']).' '.esc_attr($atts['class']).'">';
		switch( $atts['style'] ){

			case 3:
				if( $atts['icon'] == '' )$_return .= '<i class="fa fa-paper-plane"></i>';
				else $_return .= '<i class="fa '.esc_attr($atts['icon']).'"></i>';
			break;
			case 4:
				if( $atts['icon'] == '' )$_return .= '<i class="fa fa-heart"></i>';
				else $_return .= '<i class="fa '.esc_attr($atts['icon']).'"></i>';
			break;
			case 5:
				if( $atts['icon'] == '' )$_return .= '<i class="fa fa-trophy"></i>';
				else $_return .= '<i class="fa '.esc_attr($atts['icon']).'"></i>';
			break;

		}
		$_return .= '</div>';

		return $_return;

	}


	public static function hs_box_alert( $atts = null, $content = null ) {
	
		$hs = highstand::globe();
		
		$atts = shortcode_atts( array(
			'title'       => '',
			'icon'        => '',
			'text_color'  => '',
			'bg_color'    => '',
			'boder_color' => '',
			'show_button' => '',
			'class'       => ''
		), $atts, 'box_alert' );

		$custom_style = '';
		if ( !empty( $atts['text_color'] ) ) {
			$custom_style .= 'color: ' . $atts['text_color'] . ';';
		}
		if ( !empty( $atts['bg_color'] ) ) {
			$custom_style .= 'background-color: ' . $atts['bg_color'] . ';';
		}
		if ( !empty( $atts['boder_color'] ) ) {
			$custom_style .= 'border-color: ' . $atts['boder_color'] . ';';
		}

		$_return = '';
		$_return .= '<div class="message-boxes '. $atts['class'] .'" style="'. $custom_style .'">';
			$_return .= '<div class="message-box-wrap">';
				if ( !empty( $atts['icon'] ) ) {
					$_return .= '<i class="fa '. $atts['icon'] .' fa-lg"></i>';
				}
				if ( $atts['show_button'] == 'show' ) {
					$_return .= '<button class="close-but">close</button>';
				}
				$_return .= base64_decode( $atts['title'] );
			$_return .= '</div>';
		$_return .= '</div>';

		return $_return;

	}


	public static function hs_flip_clients( $atts = null, $content = null ) {

		$atts = shortcode_atts( array(
			'img'   => '',
			'title'	 => '',
			'link'	 => '#',
			'des'	 => '',
			'class'    => ''
		), $atts, 'flip_clients' );
		ob_start();
		?>

		<div class="one_fifth <?php echo esc_attr($atts['class']); ?>">
			<div class="flips4 <?php echo esc_attr($atts['class']); ?>">
				<div class="flips4_front flipscont4">
					<?php echo wp_get_attachment_image( $atts['img'], 'full' ); ?>
				</div>
				<div class="flips4_back flipscont4">
					<h5>
						<strong>
							<a href="<?php echo esc_url( $atts['link'] ); ?>"><?php echo esc_html( $atts['title'] ); ?></a>
						</strong>
					</h5>
					<p><?php echo esc_html( wp_trim_words($atts['des'], 10) ); ?></p>
				</div>
			</div>
		</div>

		<?php
		$_return = ob_get_contents();
		ob_end_clean();

		return $_return;

	}


	public static function hs_flip_content( $atts = null, $content = null){

		$atts = shortcode_atts( array(
			'category'   => '',
			'number_show'	 => 4,
			'icon_button'	 => 'paper-plane',
			'text_button'	 => 'Read More',
			'type'	 => 'horizontal',
			'background_color'	 => '#86c724',
			'class'    => ''
		), $atts, 'flip_content' );

		ob_start();

		highstand_incl_core( 'templates'.DS.'shortcode'.DS.'flip_content'.DS.$atts['type'].'.php', 'i', $atts, $content );
		$output = ob_get_contents();

		ob_end_clean();

		return $output;
	}



	public static function hs_cf7( $atts = null, $content = null ) {

		global $wpdb;

		$atts = shortcode_atts( array(
				'title' => 'Contact Form',
				'slug'       => '',
			), $atts, 'cf7' );

		$form = $wpdb->get_results("SELECT `ID` FROM `".$wpdb->posts."` WHERE `post_type` = 'wpcf7_contact_form' AND `post_name` = '".esc_attr(sanitize_title($atts['slug']))."' LIMIT 1");

		if( !empty( $form ) ){
			return do_shortcode('[contact-form-7 id="'.$form[0]->ID.'" title="'.esc_attr($atts['title']).'"]');
		}else{
			return '[contact-form-7 not found slug ('.esc_attr($atts['slug']).') ]';
		}
	}

	public static function hs_latest_posts( $atts = null, $content = null ) {
		//init param values
		$atts = shortcode_atts( array(
			'template'				=> '',
			'items'					=> 5,
			'post_type'				=> '',
			'taxonomy'				=> 'category',
			'column'				=> '2',
			'tax_term'				=> false,
			'order'					=> 'desc',
			'filter'				=> 'No',
			'show_date'				=> 'show',
			'ignore_sticky_posts'	=> 'no',
			'custom_class'			=> '',
			'main_color'			=> '#2ecc71',
			'class'					=> ''
		), $atts, 'latest_posts' );

		//get posts
		$posts = self::get_posts( $atts );

		//assign posts to global variable on shortcode
		$atts['posts'] = $posts;

		//If can not find the tempate file, => show message
		if( empty( $atts[ 'template' ] ) )
		{
			return esc_html__( 'Latest Posts: template is empty.', 'highstand' );
		}

		ob_start();

			highstand_incl_core( 'templates' . DS . 'shortcode' . DS . $atts[ 'template' ], 'i', $atts, $content );

			$_return = ob_get_contents();

		ob_end_clean();
		
		if( function_exists( 'kc_js_callback' ) )
			kc_js_callback('highstand_lastpost_init');
		
		return $_return;
	}

	public static function hs_services( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
			'se_options' => '',
			'class'      => ''
		), $atts, 'hs_services' );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.'services'.DS.'layout-1.php', 'i', $atts, $content );

			$_return = ob_get_contents();

		ob_end_clean();


		return $_return;
	}


	public static function hs_contact_form7( $atts = null, $content = null ) {

		$atts = shortcode_atts( array(
			'c_id'  => '',
			'class' => ''
		), $atts, 'hs_contact_form7' );

		if ( !empty( $atts['c_id'] ) ) {
			$c_shortcode = '[contact-form-7 id="' . $atts['c_id'] . '" title="Contact Form"]';

			$_return = '<div class="contact_' . esc_attr( rand(100,999) ) . (empty( $atts['class'] ) ? "" : " ".$atts['class']) .'">';
				$_return .= do_shortcode( $c_shortcode );
			$_return .= '</div>';
		} else {
			$_return = esc_html__( 'Create and select contact form.', 'highstand' );
		}

		return $_return;

	}


	public static function hs_search_menu( $atts = null, $content = null ){

		$_return = '';

		ob_start();

		?>

		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<label>
				<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'highstand' ); ?></span>
				<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search ...', 'placeholder', 'highstand' ); ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'highstand' ); ?>" />
			</label>
			<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'highstand' ); ?>" />
		</form>


		<?php
		
			$_return = ob_get_contents();

		ob_end_clean();
		
		return $_return;
	}

	public static function hs_subscribe( $atts = null, $content = null){
		$highstand = highstand::globe('highstand');

		$atts = shortcode_atts( array(
			'input_text'         => 'Please enter your email...',
			'input_submit'       => '',
			'input_submit_class' => '',
			'class'              => ''
		), $atts, 'hs_subscribe' );

		

		$_return = $form_action = '';

		ob_start();

		if ( !empty( $atts['input_submit'] ) ) {

			$text_submit = $atts['input_submit'];

		} else {

			$text_submit = esc_html__( 'Subscribe', 'highstand' );

		}

		if( !empty($atts['class']) ) echo '<div class="'. esc_attr( $atts['class'] ) .'">';

		?>

		<form data-url="<?php echo admin_url( 'admin-ajax.php?t='. time() ); ?>" class="hs_subscribe hs_mailchimp" method="POST" action="" _lpchecked="1">
			<input class="enter_email_input required email" name="highstand_email" value="<?php echo esc_attr( $atts['input_text'] ); ?>" onfocus="if( this.value == '<?php echo esc_attr( $atts['input_text'] ); ?>') { this.value = ''; }" onblur="if ( this.value == '') { this.value = '<?php echo esc_attr( $atts['input_text'] ); ?>'; }" type="text">
			<input name="subscribe" value="<?php echo esc_attr( $text_submit ); ?>" class="input_submit <?php echo esc_attr( $atts['input_submit_class'] ); ?>" type="submit">
			<div class="highstand_newsletter_status"></div>
		</form>

		<?php
		if( !empty( $atts['class'] ) ) 
			echo '</div>';

		$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}


	public static function hs_template_image($atts , $content = null){
		$atts = shortcode_atts(array(
			'class' => '',				
			'file' => '',
			'alt' => ''
		), $atts );

		extract( $atts );

		$src = '';

		if(!empty($file)){
			$src = get_template_directory_uri().'/assets/images/'.$file;
		}
		
		return '<img src="'. esc_url($src) .'" alt="' .esc_attr( $alt ). '" />';
	}

}

add_action( 'init', array( 'highstand_shortcodes', 'register') );
