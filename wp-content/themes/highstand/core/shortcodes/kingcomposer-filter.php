<?php


if(!function_exists('is_plugin_active')){
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}


if ( is_plugin_active( 'kingcomposer/kingcomposer.php' ) ) {

	add_filter( 'shortcode_kc_post_type_list', 'highstand_post_type_list_filter' );
	function highstand_post_type_list_filter(  $atts = array() ){

		global $kc_front;
		$css = '';

		$atts = kc_remove_empty_code( $atts );
		extract( $atts );

		return $atts;
	}


	add_filter( 'shortcode_hs_work', 'highstand_work_filter' );
	function highstand_work_filter( $atts ){

		$atts = kc_remove_empty_code( $atts );
		extract( $atts );

		$css_dir = THEME_URI.'/assets/css/';
		$js_dir = THEME_URI.'/assets/js/';

		wp_enqueue_style('cubeportfolio-min', $js_dir.'cubeportfolio/cubeportfolio.min.css', false, THEME_VERSION );

		if ( $atts['filter'] == 'Yes' ) {
			wp_enqueue_script('cubeportfolio', highstand_child_theme_enqueue( $js_dir.'cubeportfolio/js/jquery.cubeportfolio.min2.js' ), array( 'jquery' ), THEME_VERSION, true );
		} else {
			wp_enqueue_script('cubeportfolio', highstand_child_theme_enqueue( $js_dir.'cubeportfolio/js/jquery.cubeportfolio.min.js' ), array( 'jquery' ), THEME_VERSION, true );
		}

		wp_enqueue_script('cubeportfolio-main', highstand_child_theme_enqueue( $js_dir.'cubeportfolio/main.js' ), array( 'jquery' ), THEME_VERSION, true );


		return $atts;
	}


	add_filter( 'shortcode_hs_team', 'highstand_shortcode_use_owl_filter' );
	add_filter( 'shortcode_hs_work', 'highstand_shortcode_use_owl_filter' );
	add_filter( 'shortcode_hs_testimonials', 'highstand_shortcode_use_owl_filter' );
	add_filter( 'shortcode_hs_latest_posts', 'highstand_shortcode_use_owl_filter' );
	function highstand_shortcode_use_owl_filter( $atts ){

		wp_enqueue_script( 'owl-carousel' );
		wp_enqueue_style( 'owl-theme' );
		wp_enqueue_style( 'owl-carousel' );

		return $atts;
	}

	/*
	 * Latest posts shortcode custom css
	 */
	function highstand_latest_posts_filter( $atts = array() ){

			global $kc_front;
            $main_color = '';
            $atts       = kc_remove_empty_code( $atts );

			extract( $atts );

			if( empty($main_color) )
				return $atts;

            $box_hover_color        = ( !empty($box_hover_color ) )? $box_hover_color : '#FFFFFF';
            $custom_class           = 'latest_posts_' . highstand_random_string( 10 );
            $atts[ 'custom_class' ] = $custom_class;

			//custom CSS
            $custom_style           = "
			.$custom_class .box02{
				background-color: $main_color;
			}
			.$custom_class a{
				color: $main_color;
			}

			.$custom_class .box01:hover , .$custom_class .box03:hover, .$custom_class .box05:hover {
				-webkit-box-shadow: 0px 0px 0px 10px $main_color;
				-moz-box-shadow: 0px 0px 0px 10px $main_color;
				box-shadow: 0px 0px 0px 10px $main_color;
			}

			.$custom_class .box02:hover {
				-webkit-box-shadow: 0px 0px 0px 10px rgba(39,39,39,1);
				-moz-box-shadow: 0px 0px 0px 10px rgba(39,39,39,1);
				box-shadow: 0px 0px 0px 10px rgba(39,39,39,1);
			}

			.$custom_class .box04:hover {
				-webkit-box-shadow: 0px 0px 0px 10px rgba(39,39,39,1);
				-moz-box-shadow: 0px 0px 0px 10px rgba(39,39,39,1);
				box-shadow: 0px 0px 0px 10px rgba(39,39,39,1);
			}
			";

			$kc_front->add_header_css($custom_style);

			return $atts;

		}

		add_filter( 'shortcode_hs_latest_posts', 'highstand_latest_posts_filter' );



}
