<?php

/*
*	Register extend component for King Composer
*	king-theme.com
*/

if(!function_exists('is_plugin_active')){
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}


if ( is_plugin_active( 'kingcomposer/kingcomposer.php' ) ) {
	
	// For alter default shortcodes map from mini
	highstand_incl_core( 'core'.DS.'shortcodes'.DS.'kingcomposer-map-alter.php' );

	//Add new maps for highstand shortcodes
	if(!function_exists('highstand_extend_kingcomposer')){

		add_action( 'init', 'highstand_extend_kingcomposer' );
		function highstand_extend_kingcomposer(){

			global $highstand, $kc;


			$kc_map = array(

				'mega_menu' => array(
					'name' => esc_html__( 'Mega Menu', 'highstand' ),
					'title' => 'Mega menu',
					'icon' => 'fa fa-bars',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Output a menu from WordPress menu.', 'highstand' ),
					'params' => array(
						array(
							'type' => 'text',
							'label' => esc_html__( 'Title', 'highstand' ),
							'name' => 'title',
							'admin_label' => true,
							'description' => esc_html__( '', 'highstand')
						),
						array(
							'type' => 'select',
							'label' => esc_html__( 'Select menu', 'highstand' ),
							'name' => 'menu',
							'options' => Su_Tools::get_list_menu(),
							'admin_label' => true,
							'description' => esc_html__( '', 'highstand' )
						),
						array(
							'type' => 'text',
							'label' => esc_html__( 'Custom class', 'highstand' ),
							'name' => 'class',
							'description' => esc_html__( '', 'highstand')
						),
					)
				),

				'hs_image_fadein' => array(
					'name' => esc_html__('Image FadeIn Slider', 'highstand'),
					'description' => esc_html__('', 'highstand'),
					'category' => THEME_NAME.' Theme',
					'icon' => 'fa fa-sliders',
					'params' => array(

						array(
							'type'			=> 'text',
							'label'			=> esc_html__( 'Title', 'highstand' ),
							'name'			=> 'title',
							'description'	=> esc_html__( 'The title of the Image Gallery. Leave blank if no title is needed.', 'highstand' ),
							'admin_label'	=> true
						),
						array(
							'type'			=> 'attach_images',
							'label'			=> esc_html__( 'Images', 'highstand' ),
							'name'			=> 'images',
							'description'	=> esc_html__( 'Upload multiple image to the carousel with the SHIFT key holding.', 'highstand' ),
							'admin_label'	=> true
						),
						array(
							'type'			=> 'text',
							'label'			=> esc_html__( 'Rotate time', 'highstand' ),
							'name'			=> 'rotate',
							'description'	=> esc_html__( 'Rotate time each slide. For example 5000', 'highstand' ),
							'admin_label'	=> true,
							'value' 		=> '3000'
						),
						array(
							'type'			=> 'text',
							'label'			=> esc_html__( 'Wrapper class name', 'highstand' ),
							'name'			=> 'wrap_class',
							'description'	=> esc_html__( 'Custom class for wrapper of the shortcode widget.', 'highstand' ),
						),


					)
				),

				'hs_events' => array(
					'name'          => esc_html__( 'Events', 'highstand' ),
					'title'         => 'Events Settings',
					'icon'          => 'fa fa-graduation-cap',
					'category'      => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description'   => esc_html__( 'Output Events list from events post type.', 'highstand' ),
					'params'        => array(
						array(
							'type'        => 'multiple',
							'label'       => esc_html__( 'Select Categories ( hold ctrl or shift to select multiple )', 'highstand' ),
							'name'        => 'category',
							'options'     => Su_Tools::get_terms( 'events-category', 'slug' ),
							'admin_label' => true,
							'description' => esc_html__( 'Select category which you chosen for Events', 'highstand' )
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Amount', 'highstand' ),
							'name'        => 'items',
							'value'       => 12,
							'admin_label' => true,
							'description' => esc_html__( 'Enter number of Events that you want to display. To edit Events, go to ', 'highstand' ).'/wp-admin/edit.php?post_type=events'
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Words Limit', 'highstand' ),
							'name'        => 'words',
							'value'       => 30,
							'description' => esc_html__( 'Limit words you want show as short description', 'highstand' )
						),
						array(
							'type'    => 'select',
							'label'   => esc_html__( 'Order By', 'highstand' ),
							'name'    => 'order',
							'options' => array(
								'DESC' => 'Descending',
								'ASC'  => 'Ascending'
							)
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Custom class', 'highstand' ),
							'name'        => 'custom_class',
							'description' => esc_html__( 'Enter extra custom class', 'highstand' )
						)
					)
				),


				'hs_team' => array(
					'name' => esc_html__( 'Our Team', 'highstand' ),
					'title' => 'Our Team Settings',
					'icon' => 'fa fa-group',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Output our team template', 'highstand' ),
					'params' => array(
						array(
							'type'        => 'multiple',
							'label'       => esc_html__( 'Select Category ( hold ctrl or shift to select multiple )', 'highstand' ),
							'name'        => 'category',
							'options'     => Su_Tools::get_terms( 'our-team-category', 'slug' ),
							'height'      => '150px',
							'description' => esc_html__( 'Select category to display team', 'highstand' )
						),
						array(
							'type'        => 'select_layout',
							'label'       => esc_html__( 'Select Template', 'highstand' ),
							'name'        => 'template',
							'admin_label' => true,
							'options'     => Su_Tools::get_templates( 'team' )
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Amount', 'highstand' ),
							'name'        => 'items',
							'value'       => 12,
							'description' => esc_html__( 'Enter number of people to show', 'highstand' )
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Words Limit', 'highstand' ),
							'name'        => 'words',
							'value'       => 30,
							'description' => esc_html__( 'Limit words you want show as short description', 'highstand' )
						),
						array(
							'type'    => 'select',
							'label'   => esc_html__( 'Order By', 'highstand' ),
							'name'    => 'order',
							'options' => array(
								'DESC' => 'Descending',
								'ASC'  => 'Ascending'
							)
						),
						array(
							'type'    => 'select',
							'label'   => esc_html__( 'Disabled Link View Detail', 'highstand' ),
							'name'    => 'link_view',
							'options' => array(
								'no'  => 'No, Thanks!',
								'yes' => 'Yes, Please!'
							)
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Custom class', 'highstand' ),
							'name'        => 'custom_class',
							'description' => esc_html__( 'Enter extra custom class', 'highstand' )
						),
					)
				),

				'hs_work' => array(
					'name' => esc_html__( 'Our Work (Portfolio)', 'highstand' ),
					'title' => 'Our Work Settings',
					'icon' => 'fa fa-send-o',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Our work for portfolio template.', 'highstand' ),
					'params' => array(
						array(
							'type' => 'multiple',
							'label' => esc_html__( 'Select Categories ( hold ctrl or shift to select multiple )', 'highstand' ),
							'name' => 'tax_term',
							'options' => Su_Tools::get_terms( 'our-works-category', 'slug' ),
							'height' => '120px',
							'admin_label' => true,
							'description' => esc_html__( 'Select category which you chosen for Team items', 'highstand' )
						),
						array(
							'type' => 'select_layout',
							'label' => esc_html__( 'Select Template', 'highstand' ),
							'name' => 'template',
							'admin_label' => true,
							'options' => Su_Tools::get_templates( 'work' ),
							'description' => esc_html__( 'This preview for 4 cols if you select other col, it may be dificult.', 'highstand' )
						),
						array(
							'type' => 'select',
							'label' => esc_html__( 'Show Filter', 'highstand' ),
							'name' => 'filter',
							'options' => array(
								'No'	=> 'No',
								'Yes'	=> 'Yes',
							),
							'description' => esc_html__( 'Does not apply to layout 3.', 'highstand' )
						),
						array(
							'type' => 'select',
							'label' => esc_html__( 'Items on Row', 'highstand' ),
							'name' => 'column',
							'options' => array(
								'2' => 'two',
								'3' => 'three',
								'4' => 'four',
								//'5' => 'five',
							),
							'description' => esc_html__( 'Choose number of items display on a row (Does not apply to layout 3.)', 'highstand' )
						),
						array(
							'type' => 'text',
							'label' => esc_html__( 'Items Limit', 'highstand' ),
							'name' => 'items',
							'value' => get_option( 'posts_per_page' ),
							'description' => esc_html__( 'Specify number of team that you want to show. Enter -1 to get all team', 'highstand' )
						),
						array(
							'type' => 'text',
							'label' => esc_html__( 'Gap', 'highstand' ),
							'name' => 'gap',
							'value' => '0',
							'description' => esc_html__( 'Gap space Horizontal and Vertical, For example: 10 or 10|20', 'highstand' )
						),
						array(
							'type' => 'select',
							'label' => esc_html__( 'Order By', 'highstand' ),
							'name' => 'order',
							'options' => array(
								'DESC' => esc_html__( 'Descending', 'highstand' ),
								'ASC' => esc_html__( 'Ascending', 'highstand' )
							),
							'description' => ' &nbsp; '
						),
						array(
							'type' => 'select',
							'label' => esc_html__( 'Show link', 'highstand' ),
							'name' => 'show_link',
							'options' => array(
								'yes' => 'Yes',
								'no' => 'No',
							),
							'description' => esc_html__( 'Show or hide portfolio link', 'highstand' )
						),
						array(
							'type' => 'text',
							'label' => esc_html__( 'Class Css', 'highstand' ),
							'name' => 'class',
							'value' => '',
						),
					)
				),

				'hs_testimonials' => array(
					'name' => esc_html__( 'Testimonials', 'highstand' ),
					'title' => 'Testimonials Settings',
					'icon' => 'fa fa-group',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Out testimonians post type.', 'highstand' ),
					'params' => array(
						array(
							'type' => 'multiple',
							'label' => esc_html__( 'Select Categories ( hold ctrl or shift to select multiple )', 'highstand' ),
							'name' => 'category',
							'options' => Su_Tools::get_terms( 'testimonials-category', 'slug' ),
							'height' => '120px',
							'admin_label' => true,
							'description' => esc_html__( 'Select category which you chosen for Team items', 'highstand' )
						),
						array(
							'type' => 'select_layout',
							'label' => esc_html__( 'Select Template', 'highstand' ),
							'name' => 'template',
							'admin_label' => true,
							'options' => Su_Tools::get_templates( 'testimonial' )
						),
						array(
							'type' => 'text',
							'label' => esc_html__( 'Items Limit', 'highstand' ),
							'name' => 'items',
							'value' => get_option( 'posts_per_page' ),
							'description' => esc_html__( 'Specify number of team that you want to show. Enter -1 to get all', 'highstand' )
						),
						array(
							'type' => 'text',
							'label' => esc_html__( 'Limit Words', 'highstand' ),
							'name' => 'words',
							'value' => 20,
							'description' => esc_html__( 'Limit words you want show as short description', 'highstand' )
						),
						array(
							'type' => 'select',
							'label' => esc_html__( 'Order By', 'highstand' ),
							'name' => 'order',
							'options' => array(
								'DESC' => esc_html__( 'Descending', 'highstand' ),
								'ASC' => esc_html__( 'Ascending', 'highstand' )
							),
							'description' => ' &nbsp; '
						),
						array(
							'type' => 'text',
							'label' => esc_html__( 'Custom class', 'highstand' ),
							'name' => 'custom_class',
							'description' => esc_html__( 'Enter extra custom class', 'highstand' )
						),
					)
				),

				'hs_pricing' => array(
					'name' => esc_html__( 'Pricing Table', 'highstand' ),
					'title' => 'Pricing Table Settings',
					'icon' => 'fa fa-table',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Display Pricing Plan Table', 'highstand' ),
					'params' => array(
						array(
							'type'        => 'select',
							'label'       => esc_html__( 'Select Categories ( hold ctrl or shift to select multiple )', 'highstand' ),
							'name'        => 'category',
							'options'     => Su_Tools::get_terms( 'pricing_tables-category', 'slug', 'pricing_tables', '---Select Category---' ),
							'admin_label' => true,
							'description' => esc_html__( 'Select category which you chosen for Pricing Table', 'highstand' )
						),
						array(
							'type' => 'select',
							'name' => 'amount',
							'options' => array(
								'1' => '1',
								'2' => '2',
								'3' => '3',
								'4' => '4',
								'5' => '5'
							),
							'value' => 4,
							'label' => esc_html__( 'Amount', 'highstand' ),
							'description' => esc_html__( 'Number of columns', 'highstand' )
						),
						array(
							'type'  => 'select',
							'name'  => 'order',
							'value' => 'ASC',
							'label' => esc_html__( 'Order', 'highstand' ),
							'options' => array(
								'ASC'  => esc_html__( 'Ascending', 'highstand' ),
								'DESC' => esc_html__( 'Descending', 'highstand' ),
							)
						),
						array(
							'type' => 'select_layout',
							'label' => esc_html__( 'Select Template', 'highstand' ),
							'name' => 'template',
							'admin_label' => true,
							'options' => Su_Tools::get_templates( 'pricing' )
						),
						array(
							'type'    => 'select',
							'name'    => 'icon_show',
							'value'   => '1',
							'label'   => esc_html__( 'Display Icon', 'highstand' ),
							'options' => array(
								'1'  => esc_html__( 'Yes, Please!', 'highstand' ),
								'0' => esc_html__( 'No, Thanks!', 'highstand' ),
							)
						),
						array(
							'name'        => 'icon',
							'type'        => 'icon_picker',
							'label'       => esc_html__( 'Icon', 'highstand' ),
							'description' => esc_html__( 'the icon display on per row', 'highstand' ),
							'relation'    => array(
								'parent' => 'icon_show',
								'show_when' => '1'
							)
						),
						array(
							'name' => 'color',
							'type' 	=> 'color_picker',
							'label' => esc_html__( 'Highlight Color', 'highstand' ),
							'admin_label' => true,
							'description' => esc_html__( 'Global color for pricing box', 'highstand' )
						),
						array(
							'name' => 'class',
							'type' 	=> 'text',
							'label' => esc_html__( 'Class', 'highstand' ),
							'description' => esc_html__( 'Extra CSS class', 'highstand' )
						)
					)
				),

				'hs_tooltip' => array(
					'name' => esc_html__('Popover & Tooltip', 'highstand'),
					'title' => 'Popover & Tooltip Settings',
					'icon' => 'fa fa-twitch',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Tooltip for a link button', 'highstand' ),
					'params' => array(
						array(
							'type' => 'select',
							'label' => esc_html__( 'Style', 'highstand' ),
							'description' => esc_html__( 'Select button display style.', 'highstand' ),
							'name' => 'style',
							'options' => array(
								'tooltip/style-1.php' => esc_html__( 'Style 1 (Image icon)', 'highstand' ) ,
								'tooltip/style-2.php' => esc_html__( 'Style 2 (button format)', 'highstand' ),
								'tooltip/style-3.php' => esc_html__( 'Style 3 (only icon)', 'highstand' )
							),
						),
						array(
							'type' => 'attach_image',
							'name' => 'img_icon',
							'label' => esc_html__( 'Upload image icon', 'highstand' ),
							'description' => esc_html__( 'Upload image icon', 'highstand' ),
							'relation' => array(
								'parent' => 'style',
								'show_when' => array('tooltip/style-1.php')
							)
						),
						array(
							'type' => 'icon_picker',
							'name' => 'icon',
							'label' => esc_html__( 'Icon', 'highstand' ),
							'admin_label' => true,
							'description' => esc_html__( 'Select icon for button', 'highstand' )
						),
						array(
							'type' => 'text',
							'name' => 'textlink',
							'label' => esc_html__( 'Text title', 'highstand' ),
							'admin_label' => true,
							'description' => esc_html__( '', 'highstand' )
						),
						array(
							'type' => 'textarea',
							'name' => 'text_tooltip',
							'label' => esc_html__( 'Text tooltip', 'highstand' ),
							'description' => esc_html__( 'Text for tooltip', 'highstand' )
						),
						array(
							'type' => 'select',
							'name' => 'position',
							'label' => esc_html__( 'Position', 'highstand' ),
							'description' => '',
							'options' => array(
								'top' => esc_html__('Top', 'highstand'),
								'right' => esc_html__('Right', 'highstand'),
								'bottom' => esc_html__('Bottom', 'highstand'),
								'left' => esc_html__('Left', 'highstand'),
							)
						),
						array(
							'type' => 'link',
							'label' => esc_html__( 'URL (Link)', 'highstand' ),
							'name' => 'link',
							'description' => esc_html__( 'Add link to button.', 'highstand' ),
						),
						array(
							'type' => 'text',
							'name' => 'class',
							'label' => esc_html__( 'Class', 'highstand' ),
							'description' => esc_html__( 'Extra CSS class', 'highstand' )
						)
					)
				),

				'hs_divider' => array(
					'name' => esc_html__( 'Divider', 'highstand' ),
					'title' => 'Divider Settings',
					'icon' => 'fa fa-exchange',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'List of horizontal divider line', 'highstand' ),
					'params' => array(
						array(
							'type' => 'select',
							'name' => 'style',
							'options' => array(
								'1' => 'Style 1',
								'2' => 'Style 2',
								'3' => 'Style 3',
								'4' => 'Style 4',
								'5' => 'Style 5',
								'6' => 'Style 6',
								'7' => 'Style 7',
								'8' => 'Style 8',
								'9' => 'Style 9',
								'10' => 'Style 10',
								'11' => 'Style 11',
								'12' => 'Style 12',
								'13' => 'Style 13',
								' ' => 'Divider Line',
							),
							'admin_label' => true,
							'label' => esc_html__( 'Style', 'highstand' ),
							'description' => esc_html__( 'Style of divider', 'highstand' )
						),
						array(
							'type' => 'icon_picker',
							'name' => 'icon',
							'label' => esc_html__( 'Icon', 'highstand' ),
							'description' => esc_html__( 'Select icon on divider', 'highstand' )
						),
						array(
							'type' => 'text',
							'name' => 'class',
							'label' => esc_html__( 'Class', 'highstand' ),
							'description' => esc_html__( 'Extra CSS class', 'highstand' )
						)
					)
				),


				'hs_box_alert' => array(
					'name' => esc_html__( 'Boxes Alert', 'highstand' ),
					'title' => 'Boxes Alert Settings',
					'icon' => 'fa fa-exclamation-triangle',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Display box alert for message', 'highstand' ),
					'params' => array(
						array(
							'type'        => 'textarea',
							'name'        => 'title',
							'label'       => esc_html__( 'Text in Boxes', 'highstand' ),
							'description' => esc_html__( 'Insert text display in boxes', 'highstand' )
						),
						array(
							'type'        => 'icon_picker',
							'name'        => 'icon',
							'label'       => esc_html__( 'Icon', 'highstand' ),
							'description' => esc_html__( 'Select icon on divider', 'highstand' )
						),
						array(
							'type'        => 'color_picker',
							'name'        => 'text_color',
							'label'       => esc_html__( 'Color', 'highstand' ),
							'admin_label' => true,
							'description' => esc_html__( 'Global color for text boxes.', 'highstand' )
						),
						array(
							'type'        => 'color_picker',
							'name'        => 'bg_color',
							'label'       => esc_html__( 'Background Color', 'highstand' ),
							'admin_label' => true,
							'description' => esc_html__( 'Global color for background boxes.', 'highstand' )
						),
						array(
							'type'        => 'color_picker',
							'name'        => 'boder_color',
							'label'       => esc_html__( 'Border Color', 'highstand' ),
							'admin_label' => true,
							'description' => esc_html__( 'Global color for border boxes.', 'highstand' )
						),
						array(
							'type'        => 'radio',
							'name'        => 'show_button',
							'label'       => esc_html__( 'Show Button', 'highstand' ),
							'description' => esc_html__('Show/Hide button click hidden boxes.', 'highstand'),
							'value'       => 'hide',
							'options'     => array(
								'hide' => esc_html__( 'No, Thanks!', 'highstand' ),
								'show' => esc_html__( 'Yes, Please!', 'highstand' )
							)
						),
						array(
							'type'        => 'text',
							'name'        => 'class',
							'label'       => esc_html__( 'Class', 'highstand' ),
							'description' => esc_html__( 'Extra CSS class', 'highstand' )
						)
					)
				),


				'hs_latest_posts' => array(
					'name' => esc_html__( 'Latest Posts', 'highstand' ),
					'title' => 'Latest Posts Settings',
					'icon' => 'fa fa-newspaper-o',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'List of latest post with more layouts.', 'highstand' ),
					'params' => array(
						array(
							'type' => 'multiple',
							'label' => esc_html__( 'Select Categories', 'highstand' ),
							'name' => 'tax_term',
							'options' => Su_Tools::get_terms( 'category', 'slug' ),
							'height' => '120px',
							'admin_label' => true,
							'description' => esc_html__( 'Select category which you chosen for posts ( hold ctrl or shift to select multiple )', 'highstand' )
						),
						array(
							'type' => 'select_layout',
							'label' => esc_html__( 'Select Template', 'highstand' ),
							'name' => 'template',
							'admin_label' => true,
							'options' => Su_Tools::get_templates( 'latest_posts' )
						),
						array(
							'name' => 'items',
							'label' => esc_html__( 'Items Limit', 'highstand' ),
							'type' => 'number_slider',
							'value' => '5',
							'options' => array(
								'min' => 1,
								'max' => 10,
								'unit' => '',
								'show_input' => false
							),
							'description' => esc_html__('Specify number of post that you want to show. Enter -1 to get all team', 'highstand'),
						),
						array(
							'name' => 'show_date',
							'label' => esc_html__( 'Show Date', 'highstand' ),
							'type' => 'radio',
							'value' => 'show',
							'description' => esc_html__('Show/Hide date of post.', 'highstand'),
							'options' => array(
								'show' => esc_html__( 'Show', 'highstand' ),
								'hide' => esc_html__( 'Hide', 'highstand' ),
							),
						),
						array(
							'type' => 'select',
							'label' => esc_html__( 'Order By', 'highstand' ),
							'name' => 'order',
							'options' => array(
								'DESC' => esc_html__( 'Descending', 'highstand' ),
								'ASC' => esc_html__( 'Ascending', 'highstand' )
							),
							'description' => ' &nbsp; '
						),
						array(
							'name' => 'main_color',
							'type' 	=> 'color_picker',
							'label' => esc_html__( 'Main Color', 'highstand' ),
							'admin_label' => true,
							'description' => esc_html__( 'Global color for border and hover color of boxes', 'highstand' )
						),
						array(
							'type'  => 'text',
							'label' => esc_html__( 'Extra Class', 'highstand' ),
							'name'  => 'class',
							'value' => '',
						)
					)
				),

				'hs_services' => array(
					'name' => esc_html__( 'Services Featured', 'highstand' ),
					'title' => 'Services Featured Settings',
					'icon' => 'fa fa-cogs',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'List featured service owl carousel', 'highstand' ),
					'params' => array(
						array(
							'type'        => 'group',
							'label'       => esc_html__( 'Options', 'highstand' ),
							'name'        => 'se_options',
							'description' => '',
							'value'       => $highstand->ext['be']( json_encode( array(
								"1" => array(
									"image"  => "",
									"title"  => "Hosting Template",
									"desc"   => "",
									"link"   => "#",
									"active" => "no"
								)
							))),
							'params' => array(
								array(
									'name'        => 'title',
									'label'       => 'Title Name',
									'type'        => 'text',
									'admin_label' => true
								),
								array(
									'name'        => 'image',
									'label'       => 'Image',
									'type'        => 'attach_image',
									'description' => esc_html__( 'Upload single image from WP Media ( return id of attachment )', 'highstand' ),
									'admin_label' => false
								),
								array(
									'name'        => 'desc',
									'label'       => 'Descriptions',
									'type'        => 'textarea',
									'description' => esc_html__( 'Insert you desciption', 'highstand' )
								),
								array(
									'name'        => 'link',
									'label'       => 'Link Button',
									'type'        => 'link',
									'description' => esc_html__( 'Insert link for button. If blank then button will hidden', 'highstand' )
								),
								array(
									'type'    => 'select',
									'label'   => esc_html__( 'Active', 'highstand' ),
									'name'    => 'active',
									'options' => array(
										'no'  => esc_html__( 'No, Thanks!', 'highstand' ),
										'yes' => esc_html__( 'Yes, Please!', 'highstand' )
									)
								)
							)
						),
						array(
							'type'  => 'text',
							'label' => esc_html__( 'Extra Class', 'highstand' ),
							'name'  => 'class',
							'value' => '',
						)
					)
				),

				'hs_subscribe' => array(
					'name' => esc_html__( 'Subscribe form', 'highstand' ),
					'title' => 'Newsletter Subscribe form',
					'icon' => 'fa fa-envelope-o',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Display Subscribe form', 'highstand' ),
					'params' => array(
						array(
							'type' => 'text',
							'label' => esc_html__( 'Title', 'highstand' ),
							'name' => 'title',
							'admin_label' => true,
							'description' => esc_html__( '', 'highstand')
						),
						array(
							'type'  => 'text',
							'label' => esc_html__( 'Input text', 'highstand' ),
							'name'  => 'input_text',
							'description' => esc_html__( 'Input text, text placeholder', 'highstand')
						),
						array(
							'type'  => 'text',
							'label' => esc_html__( 'Text Button Sumit', 'highstand' ),
							'name'  => 'input_submit',
							'description' => esc_html__( 'Submit text. It blank, we will use Subscribe', 'highstand')
						),
						array(
							'type'  => 'text',
							'label' => esc_html__( 'Custom Button class', 'highstand' ),
							'name'  => 'input_submit_class',
							'description' => esc_html__( 'Add custom class for submit button', 'highstand')
						),
						array(
							'type'  => 'text',
							'label' => esc_html__( 'Extra Class', 'highstand' ),
							'name'  => 'class',
							'value' => '',
						)
					)
				),

				'hs_timeline' => array(
					'name'          => esc_html__( 'Time Line', 'highstand' ),
					'title'         => 'Time Line Settings',
					'icon'          => 'fa fa-clock-o',
					'category'      => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description'   => esc_html__( 'Out timeline post type.', 'highstand' ),
					'params'        => array(
						array(
							'type'        => 'multiple',
							'label'       => esc_html__( 'Select Categories ( hold ctrl or shift to select multiple )', 'highstand' ),
							'name'        => 'category',
							'options'     => Su_Tools::get_terms( 'time-line-category', 'slug' ),
							'height'      => '120px',
							'admin_label' => true,
							'description' => esc_html__( 'Select category which you chosen for Team items', 'highstand' )
						),
						array(
							'type'        => 'select_layout',
							'label'       => esc_html__( 'Select Template', 'highstand' ),
							'name'        => 'template',
							'admin_label' => true,
							'options'     => Su_Tools::get_templates( 'timeline' )
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Items Limit', 'highstand' ),
							'name'        => 'items',
							'value'       => get_option( 'posts_per_page' ),
							'description' => esc_html__( 'Specify number of team that you want to show. Enter -1 to get all', 'highstand' )
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Limit Words', 'highstand' ),
							'name'        => 'words',
							'value'       => 20,
							'description' => esc_html__( 'Limit words you want show as short description', 'highstand' )
						),
						array(
							'type'    => 'select',
							'label'   => esc_html__( 'Order By', 'highstand' ),
							'name'    => 'order',
							'options' => array(
								'DESC' => esc_html__( 'Descending', 'highstand' ),
								'ASC'  => esc_html__( 'Ascending', 'highstand' )
							)
						),
						array(
							'type'        => 'text',
							'label'       => esc_html__( 'Custom class', 'highstand' ),
							'name'        => 'custom_class',
							'description' => esc_html__( 'Enter extra custom class', 'highstand' )
						),
					)
				),


				#-----------#
			); //End map array

			// Contact form 7 plugin
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); // Require plugin.php to use is_plugin_active() below
			if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
				global $wpdb;
				$cf7 = $wpdb->get_results(
					"
						SELECT ID, post_title
						FROM $wpdb->posts
						WHERE post_type = 'wpcf7_contact_form'
					"
				);
				$contact_forms = array();
				if ( $cf7 ) {
					$contact_forms[] = esc_html__( 'Select Contact Form', 'highstand' );
					foreach ( $cf7 as $cform ) {
						$contact_forms[$cform->ID] = $cform->post_title;
					}
				} else {
					$contact_forms[0] = esc_html__( 'No contact forms found', 'highstand' );
				}

				$kc_map['hs_contact_form7'] = array(
					'name' => esc_html__( 'Contact Form 7', 'highstand' ),
					'title' => 'Contact Form 7',
					'icon' => 'fa fa-arrows-h',
					'category' => THEME_NAME.' Theme',
					'wrapper_class' => 'clearfix',
					'description' => esc_html__( 'Display contact form 7', 'highstand' ),
					'params' => array(
						array(
							'name'        => 'c_id',
							'type'        => 'select',
							'label'       => esc_html__( 'Select Contact Form', 'highstand' ),
							'admin_label' => true,
							'options'     => $contact_forms,
							'description' => esc_html__( 'Choose previously created contact form from the drop down list.', 'highstand' )
						),
						array(
							'type'  => 'text',
							'label' => esc_html__( 'Extra Class', 'highstand' ),
							'name'  => 'class',
							'value' => '',
						)
					)
				);

			}

			if(method_exists($kc, 'add_map')){
				$kc->add_map( $kc_map );
			}

		}
	}

}