<?php
add_action('init', 'highstand_alter_text_block_shortcode', 99 );

function highstand_alter_text_block_shortcode(){
	return;
	global $kc;
	if( isset( $kc ) ){
		$kc->add_map_param(
	        'kc_column_text',
			array(
				'type' => 'select',
				'name' => 'effect',
				'options' => array(
					'' => '--Select Effect--',
					' animated eff-fadeIn delay-200ms' =>'Fade In',
					' animated eff-fadeInUp delay-200ms' =>'From bottom up',
					' animated eff-fadeInUp delay-300ms' =>'From bottom up 300ms',
					' animated eff-fadeInDown delay-200ms' =>'From top down',
					' animated eff-fadeInLeft delay-200ms' =>'From left',
					' animated eff-fadeInRight delay-200ms' =>'From right',
					' animated eff-zoomIn delay-200ms' =>'Zoom In',
					' animated eff-bounceIn delay-200ms' =>'Bounce In',
					' animated eff-bounceInUp delay-200ms' =>'Bounce In Up',
					' animated eff-bounceInDown delay-200ms' =>'Bounce In Down',
					' animated eff-bounceInOut delay-200ms' =>'Bounce In Out',
					' animated eff-flipInX delay-200ms' =>'Flip In X',
					' animated eff-flipInY delay-200ms' =>'Flip In Y',
				),
				'label' => esc_html__( 'Effect', 'highstand' ),
				'description' => esc_html__( 'The effect showing when scroll over', 'highstand' )
			), 2
		);
	}
}

