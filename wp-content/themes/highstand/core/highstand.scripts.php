<?php

add_action( 'wp_enqueue_scripts', 'highstand_enqueue_content', 9999 );
add_action( 'wp_enqueue_scripts', 'highstand_enqueue_content_last', 99999 );
add_action('admin_enqueue_scripts', 'highstand_enqueue_admin');
add_action( 'admin_head', 'highstand_admin_head', 99999 );

function highstand_enqueue_content() {

	$highstand = highstand::globe('highstand');

	$css_dir = THEME_URI.'/assets/css/';
	$js_dir = THEME_URI.'/assets/js/';

	wp_enqueue_style( THEME_SLUG.'-reset', highstand_child_theme_enqueue( $css_dir.'reset.css' ), false, THEME_VERSION );
	wp_enqueue_style( 'bootstrap', highstand_child_theme_enqueue( $css_dir.'main_menu/bootstrap.min.css' ), false, THEME_VERSION );

	if( is_home() || is_single() ){
		wp_enqueue_style( THEME_SLUG.'-blog-reset', highstand_child_theme_enqueue( $css_dir.'blog-reset.css' ), false, THEME_VERSION );
	}

	wp_enqueue_style( THEME_SLUG.'-stylesheet', highstand_child_theme_enqueue( THEME_URI.'/style.css' ), false, THEME_VERSION );
	wp_enqueue_style( 'kc-icons', THEME_URI.'/core/assets/css/icons.css', false, THEME_VERSION );

	if( !empty( $highstand->cfg['effects'] ) ){
		if( $highstand->cfg['effects'] == 1 ){
			wp_enqueue_style( THEME_SLUG.'-effects', THEME_URI.'/core/assets/css/animate.css', false, THEME_VERSION );
		}
	}

	wp_enqueue_style( THEME_SLUG.'-highstand', highstand_child_theme_enqueue( $css_dir.'highstand.css'  ), false, THEME_VERSION );
	wp_enqueue_style( THEME_SLUG.'-highstand-mslider', highstand_child_theme_enqueue( $css_dir.'ms-slider.css'  ), false, THEME_VERSION );
	wp_enqueue_style( THEME_SLUG.'-external', $css_dir.'external.css', false, THEME_VERSION );

	wp_enqueue_style( THEME_SLUG.'-shortcodes', $css_dir.'shortcodes.css', false, THEME_VERSION );


	wp_register_style('highstand-menu', highstand_child_theme_enqueue( $css_dir.'main_menu/menu.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-default', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-default.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-1', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-1.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-2', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-2.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-3', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-3.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-4', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-4.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-5', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-5.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-6', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-6.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-7', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-7.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-8', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-8.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-9', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-9.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-10', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-10.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-12', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-12.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-13', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-13.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-14', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-14.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-demo', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-demo.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-onepage', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-onepage.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-construction', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-construction.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-political', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-political.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-restaurant', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-restaurant.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-realestate', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-realestate.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-education', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-education.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-wedding', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-wedding.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-nonprofit', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-nonprofit.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-medical', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-medical.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-travel', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-travel.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-law', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-law.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-hosting', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-hosting.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-portfolio', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-portfolio.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-shopping', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-shopping.css' ), false, THEME_VERSION );
	wp_register_style('highstand-menu-music', highstand_child_theme_enqueue( $css_dir.'main_menu/menu-music.css' ), false, THEME_VERSION );

	wp_enqueue_style('highstand-slidebars', highstand_child_theme_enqueue( $css_dir.'slidebars.css' ), false, THEME_VERSION );

	wp_register_style('highstand-timeline', highstand_child_theme_enqueue( $css_dir.'timeline.css' ), false, THEME_VERSION );

	wp_enqueue_script('highstand-slidebars', highstand_child_theme_enqueue( $js_dir.'slidebars.js' ), array( 'jquery' ), THEME_VERSION, true );
	wp_register_script('neo-pager', highstand_child_theme_enqueue( $js_dir.'neo.pager.js' ), array( 'jquery' ), THEME_VERSION, true );
	wp_register_script('highstand-custom', highstand_child_theme_enqueue( $js_dir.'custom.js' ), array( 'jquery' ), THEME_VERSION, true );
	wp_enqueue_script('highstand-custom');
	wp_register_script('highstand-user', highstand_child_theme_enqueue( $js_dir.'highstand.user.js' ), array( 'jquery' ), THEME_VERSION, true );
	wp_enqueue_script('highstand-user');

	wp_register_script('highstand-viewportchecker', highstand_child_theme_enqueue( $js_dir.'viewportchecker.js' ), array( 'jquery' ), THEME_VERSION, true );
	wp_enqueue_script('highstand-viewportchecker');

	// Register scripts cube portfolio
	wp_register_style('cubeportfolio', $js_dir.'cubeportfolio/cubeportfolio.min.css', false, THEME_VERSION );
	wp_register_script('cubeportfolio', highstand_child_theme_enqueue( $js_dir.'cubeportfolio/js/jquery.cubeportfolio.min.js' ), array( 'jquery' ), THEME_VERSION, true );
	wp_register_script('cubeportfolio-main', highstand_child_theme_enqueue( $js_dir.'cubeportfolio/main.js' ), array( 'jquery' ), THEME_VERSION, true );

	wp_register_script('loopslider', highstand_child_theme_enqueue( $js_dir.'jquery.loopslider.js' ), array( 'jquery' ), THEME_VERSION, true );
	wp_register_script('highstand-universal-custom', highstand_child_theme_enqueue( $js_dir.'universal/custom.js' ), array( 'jquery' ), THEME_VERSION, true );
	wp_enqueue_script('highstand-universal-custom');

	if ( is_singular() ){
		wp_enqueue_script( "comment-reply" );
	}

	/* Register google fonts */
	$protocol = is_ssl() ? 'https' : 'http';

	wp_enqueue_style( 'highstand-google-fonts', "$protocol:".highstand_google_fonts_url() );

	ob_start();
		$header = $highstand->path( 'header' );
		if( $header == true ){
			$highstand->path['header'] = ob_get_contents();
		}
	ob_end_clean();

}

function highstand_enqueue_content_last(){

	$highstand = highstand::globe('highstand');
	$css_dir = THEME_URI.'/assets/css/';

	wp_enqueue_style( THEME_SLUG.'-responsive', $css_dir.'responsive.css', false, THEME_VERSION );
		
	echo "<script type=\"text/javascript\">if(!document.getElementById('rs-plugin-settings-inline-css')){document.write(\"<style id='rs-plugin-settings-inline-css' type='text/css'></style>\")}</script>";

}

function highstand_enqueue_admin() {

	$highstand = highstand::globe('highstand');

	$screen = get_current_screen();

	$css_dir = THEME_URI.'/assets/css/';

	//if( $highstand->page == strtolower( THEME_NAME ).'-panel' ||  $highstand->page == 'page' || $highstand->page == 'post' ){
	wp_enqueue_style( THEME_SLUG.'-admin', THEME_URI.'/core/assets/css/highstand-admin.css', false, THEME_VERSION );
	if( $screen->base == 'post' ){
		wp_enqueue_style( THEME_SLUG.'-aristo', THEME_URI.'/options/css/jquery-ui-aristo/aristo.css', false, THEME_VERSION );
	}	
	wp_enqueue_style( THEME_SLUG.'-simple-line-icons.', THEME_URI.'/core/assets/css/icons.css', false, THEME_VERSION );
	//}
	if( $highstand->page == strtolower( THEME_NAME ).'-importer' ){
		add_thickbox();
	}
	if( $highstand->page == 'page' ){
		//wp_enqueue_style( THEME_SLUG.'-simple-line-icons.', THEME_URI.'/core/assets/css/icons.css', false, THEME_VERSION );
		/* wp_register_script('highstand-admin', THEME_URI.'/core/assets/js/highstand-admin.js', false, THEME_VERSION, true );
		wp_register_script('highstand-bs64', THEME_URI.'/core/assets/js/base'.'64.js', false, THEME_VERSION, true );
		wp_enqueue_script('highstand-admin');
		wp_enqueue_script('highstand-bs64'); */
	}

}

function highstand_admin_head() {

	$highstand = highstand::globe('highstand');

	echo '<script type="text/javascript">var site_uri = "'.SITE_URI.'";var SITE_URI = "'.SITE_URI.'";var HOME_URL = "'.HOME_URL.'";var theme_uri = "'.THEME_URI.'";var theme_name = "'.THEME_NAME.'";</script>';

	echo '<script type="text/javascript">jQuery(document).ready(function(){jQuery("#sc_select").change(function() {send_to_editor(jQuery("#sc_select :selected").val());return false;});});</script><style type="text/css">.vc_license-activation-notice,.ls-plugins-screen-notice,.rs-update-notice-wrap{display: none;}</style>';

}

function highstand_google_fonts_url() {

    $font_url = '';

    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'highstand' ) ) {
        $font_url = '//fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic|Raleway:400,100,200,300,500,600,700,800,900|Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Dancing+Script:400,700|Droid+Serif:400,400italic,700,700italic|Lobster|Oswald:400,100,200,300,500,600,700,800,900|Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic|Righteous:400,100,200,300,500,600,700,800,900|Righteous:400,100,200,300,500,600,700,800,900|fredoka+one:400,100,200,300,500,600,700,800,900';
    }

    return $font_url;

}
