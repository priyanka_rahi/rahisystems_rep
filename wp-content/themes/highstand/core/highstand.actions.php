<?php
/*
*	This is private registration with WP
* 	(c) king-theme.com
*
*/


$highstand = highstand::globe();

add_action( "wp_head", 'highstand_meta', 0 );
add_action( "get_header", 'highstand_set_header' );
add_action( "wp_head", 'highstand_custom_header', 99999 );
add_action( "wp_footer", 'highstand_custom_footer' );

	

function highstand_set_header( $name ){

	$highstand = highstand::globe('highstand');
	// This uses for call template via get_header( 'name_of_template' );
	if( !empty( $name ) ){
		$file = ( strpos( $name, '.php' ) === false ) ? $name.'.php' : $name;
		if( file_exists( THEME_PATH.DS.'templates/header/'.$file ) ){
			$highstand->cfg[ 'header' ] = array( '_file_' => $file );
			$highstand->cfg[ 'header_autoLoaded' ] = 1;
		}
	}

}

/*-----------------------------------------------------------------------------------*/
# Setup custom header from theme panel
/*-----------------------------------------------------------------------------------*/

function highstand_custom_header(){

	echo '<script type="text/javascript">var site_uri = "'.SITE_URI.'";var SITE_URI = "'.SITE_URI.'";var theme_uri = "'.THEME_URI.'";</script>';

	$options_css = get_option( strtolower( THEME_NAME ).'_options_css', true );
	if( !empty( $options_css ) ){
		echo '<style type="text/css">';
		echo str_replace( array( '%SITE_URI%', '<style', '</style>' ), array( SITE_URI, '&lt;', '' ), $options_css );
		if( is_admin_bar_showing() ){
			echo '.header{margin-top:32px;}';
		}
		echo '</style>';
	}

}

/*-----------------------------------------------------------------------------------*/
# setup footer from theme panel
/*-----------------------------------------------------------------------------------*/


function highstand_custom_footer( ){

	$highstand = highstand::globe('highstand');

	echo '<a href="#" class="scrollup" id="scrollup" style="display: none;">Scroll</a>'."\n";

	if( !empty( $highstand->cfg['GAID'] ) ){
		/*
		*
		* Add google analytics in footer
		*
		*/
		echo "<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', '".esc_attr($highstand->cfg['GAID'])."', 'auto');ga('send', 'pageview');</script>";

	}
	if(is_array($highstand->carousel) && count($highstand->carousel) >0){
		echo '<script type="text/javascript">
		jQuery(document).ready(function() {
		';
		foreach($highstand->carousel as $car_js){
			echo "\n".$car_js."\n";
		}
		echo '
		});
		</script>';
	}
}

function highstand_post_save_regexp($m){

	return str_replace('"',"'",$m[0]);

}

add_action("after_switch_theme", "highstand_activeTheme", 1000 ,  1);
/*----------------------------------------------------------*/
#	Active theme -> import some data
/*----------------------------------------------------------*/
function highstand_activeTheme( $oldname, $oldtheme=false ) {

 	$highstand = highstand::globe('highstand');
	#Check to import base of settings
	#Check to import base of settings
	$opname = strtolower( THEME_NAME) .'_import';
	$king_opimp  = get_option( $opname, true );

	if($king_opimp == 1){
		get_template_part( 'core/import' );
	}

	if( $highstand->template == $highstand->stylesheet ){

		?>
		<style type="text/css">
			body{display:none;}
		</style>
		<script type="text/javascript">
			/*Redirect to install required plugins after active theme*/
			window.location = '<?php echo esc_url( 'admin.php?page='.strtolower( THEME_NAME ).'-importer' ); ?>';
		</script>

		<?php

	}
}

/*-----------------------------------------------------------------------------------*/
# 	Register Menus in NAV-ADMIN
/*-----------------------------------------------------------------------------------*/


add_action('admin_menu', 'highstand_settings_menu');

function highstand_settings_menu() {
	
	$hs = highstand::globe();
	
	$capability = THEME_SLUG.'_access';
	$roles = array( 'administrator', 'admin', 'editor' );

	foreach ( $roles as $role ) {
		if( ! $role = get_role( $role ) ) 
			continue;
		$role->add_cap( $capability  );
	}
	
	$icon = THEME_URI.'/assets/images/icon_50x50.png';
	
	$hs->ext['amp'](
		THEME_NAME.' Theme Panel',
		THEME_NAME.' Theme',
		$capability,
		THEME_SLUG.'-panel',
		THEME_SLUG.'_theme_panel',
		$icon
	);

	$hs->ext['rsp']( THEME_SLUG.'-panel', THEME_SLUG.'-panel' );

	$hs->ext['asmp'](
		THEME_SLUG.'-panel',
		THEME_NAME.' Theme Panel',
		'Theme Panel',
		$capability,
		THEME_SLUG.'-panel',
		THEME_SLUG.'_theme_panel'
	);
	
	$hs->ext['asmp'](
		THEME_SLUG.'-panel',
		THEME_NAME.' Demos Panel',
		'Sample Demos',
		$capability,
		THEME_SLUG.'-importer',
		THEME_SLUG.'_theme_import'
	);
	
	$hs->ext['asmp'](
		THEME_SLUG.'-panel',
		THEME_NAME.' Footers',
		'Manage Footers',
		$capability,
		THEME_SLUG.'-footers-manage',
		THEME_SLUG.'_manage_footer'
	);
	
}

function highstand_theme_panel(){

	global $highstand, $highstand_options;

	$highstand->assets(array(
		array('js' => THEME_URI.'/core/assets/jscolor/jscolor')
	));

	$highstand_options->_options_page_html();

}

function highstand_theme_import() {

	$highstand = highstand::globe('highstand');

	$highstand->assets(array(
		array('css' => THEME_URI.'/core/assets/css/bootstrap.min'),
		array('css' => THEME_URI.'/options/css/theme-pages')
	));
	highstand_incl_core( 'core'.DS.'sample.php' );

}

function highstand_manage_footer() {
	echo '<script>window.location="'.admin_url('/edit.php?post_type=highstand_footer').'";</script>';
}

/*Add post type*/
add_action( 'init', 'highstand_init' );
function highstand_init() {

	$highstand = highstand::globe('highstand');
	
    if( is_admin() ){
   		$highstand->sysInOut();
   	}else{
   		if( !empty( $highstand->cfg['admin_bar'] ) ){
   			if( $highstand->cfg['admin_bar'] != 'show' ){
		   		show_admin_bar(false);
		   	}
   		}
   	}
   	
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
	//init sections 
	global $kc;
	if( isset( $kc ) ){
		$kc->locate_profile_sections( THEME_CPATH.'sample/data/sections/home-demo.kc' );
		$kc->locate_profile_sections( THEME_CPATH.'sample/data/sections/home-demo-6-14.kc' );
		$kc->locate_profile_sections( THEME_CPATH.'sample/data/sections/more-demos.kc' );
	}
}

/*Add Custom Sidebar*/
function highstand_widgets_init() {

	$highstand = highstand::globe('highstand');


	$sidebars = array(

		'sidebar' => array(
			__( 'Main Sidebar', 'highstand' ),
			__( 'Appears on posts and pages at left-side or right-side except the optional Front Page template.', 'highstand' )
		),

		'sidebar-woo' => array(
			__( 'Archive Products Sidebar', 'highstand' ),
			__( 'Appears on Archive Products.', 'highstand' )
		),
		'sidebar-woo-single' => array(
			__( 'Single Product Sidebar', 'highstand' ),
			__( 'Appears on Single Product detail page', 'highstand' )
		),

		'footer_1' => array(
			__( 'Footer Column 1', 'highstand' ),
			__( 'Appears on column 1 at Footer', 'highstand' )
		),

		'footer_2' => array(
			__( 'Footer Column 2', 'highstand' ),
			__( 'Appears on column 2 at Footer', 'highstand' )
		),

		'footer_3' => array(
			__( 'Footer Column 3', 'highstand' ),
			__( 'Appears on column 3 at Footer', 'highstand' )
		),

		'footer_4' => array(
			__( 'Footer Column 4', 'highstand' ),
			__( 'Appears on column 4 at Footer', 'highstand' )
		),

	);

	$footer1 = 1;

	// if(isset($highstand->cfg['footer_widgets'])){
	// 	foreach($highstand->cfg['footer_widgets'] as $key => $val){

	// 		for ($i=1; $i <= 4; $i++) { 
	// 			$sidebars['footer_v'.$key.'_'.$i] = array(
	// 				__( 'Footer v'. $key .' Column '.$i, 'highstand' ),
	// 				__( 'Appears on v'. $key .' column '. $i .' at Footer', 'highstand' )
	// 			);
	// 		}
	// 	}
	// }
	

	if( !empty( $highstand->cfg['sidebars'] ) ){
		foreach( $highstand->cfg['sidebars'] as $sb ){
			$sidebars[ sanitize_title_with_dashes( $sb ) ] = array(
				esc_html( $sb ),
				__( 'Dynamic Sidebar - Manage via theme-panel', 'highstand' )
			);
		}
	}

	foreach( $sidebars as $k => $v ){

		if(in_array($k, array('footer_1', 'footer_2', 'footer_3', 'footer_4'))){
			$h = 'h4';
		}else{
			$h = 'h3';
		}

		register_sidebar( array(
			'name' => $v[0],
			'id' => $k,
			'description' => $v[1],
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<'. $h .' class="widget-title"><span>',
			'after_title' => '</span></'. $h .'>',
		));

	}

}
add_action( 'widgets_init', 'highstand_widgets_init' );


add_filter( 'image_size_names_choose', 'highstand_custom_sizes' );
function highstand_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'large-small' => esc_html__('Large Small', 'highstand'),
    ) );
}

add_filter( 'wp_nav_menu_items','highstand_mainnav_last_item', 10, 2 );
function highstand_mainnav_last_item( $items, $args ) {
	if( $args->theme_location == 'primary' || $args->theme_location == 'onepage' ){

		$highstand = highstand::globe();
	$woocommerce = highstand::globe('woocommerce');

		if( empty( $highstand->cfg['searchNav'] ) ){
			$highstand->cfg['searchNav'] = 'show';
		}
		/*
		*	Display Search Box
		*/
		if( $highstand->cfg['searchNav'] == 'show' ){
			$items .= '<li class="dropdown yamm ext-nav search-nav">'.
						  '<a href="#"><i class="icon icon-magnifier"></i></a>'.
						  '<ul class="dropdown-menu">'.
						  '<li>'.get_search_form( false ).'</li>'.
						  '</ul>'.
					  '</li>';
		}

	}
	return $items;
}

/*-----------------------------------------------------------------------------------*/
# Load layout from system before theme loads
/*-----------------------------------------------------------------------------------*/

function highstand_load_layout( $file ){

	global $highstand, $post;

	if( is_home() ){

		$cfg = ''; $_file = '';

		if( !empty( $highstand->cfg['blog_layout'] ) ){
			$cfg = $highstand->cfg['blog_layout'];
		}

		if( file_exists( THEME_PATH.DS.'templates'.DS.'blog-'.$cfg.'.php' ) ){
			$_file =  'templates'.DS.'blog-'.$cfg.'.php';
		}

		if( get_option('show_on_front',true) == 'page' && $_file === '' ){
			$id = get_option('page_for_posts',true);
			if( !empty( $id ) ){
				$get_page_tem = get_page_template_slug( $id );
			    if( !empty( $get_page_tem ) ){
					$_file = $get_page_tem;
				}
			}
		}

		if( !empty( $_GET['layout'] ) ){
			if( file_exists( THEME_PATH.DS.'templates'.DS.'blog-'.$_GET['layout'].'.php' ) ){
				$_file = 'templates'.DS.'blog-'.$_GET['layout'].'.php';
			}
		}

		if( !empty( $_file ) ){
			return locate_template( $_file );
		}
	}

	if( $highstand->vars( 'action', 'login' ) ){
		return locate_template( 'templates'.DS.'highstand.login.php' );
	}
	if( $highstand->vars( 'action', 'register' ) ){
		return locate_template( 'templates'.DS.'highstand.register.php' );
	}
	if( $highstand->vars( 'action', 'forgot' ) ){
		return locate_template( 'templates'.DS.'highstand.forgot.php' );
	}

	$highstand->tp_mode( basename( $file, '.php' ) );

	return $file;

}
add_action( "template_include", 'highstand_load_layout', 99 );

function highstand_exclude_category( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
    	$highstand = highstand::globe('highstand');
    	if( !empty( $highstand->cfg['timeline_categories'] ) ){
	    	if( $highstand->cfg['timeline_categories'][0] != 'default' ){
		    	 $query->set( 'cat', implode( ',', $highstand->cfg['timeline_categories'] ) );
	    	}
    	}
    }
}
add_action( 'pre_get_posts', 'highstand_exclude_category' );

function highstand_admin_notice() {
	if ( get_option('permalink_structure', true) === false ) {
    ?>
    <div class="updated">
        <p>
	        <?php sprintf( wp_kses( __('You have not yet enabled permalink, the 404 page and some functions will not work. To enable, please <a href="%s">Click here</a> and choose "Post name"', 'highstand' ), array('a'=>array()) ), SITE_URI.'/wp-admin/options-permalink.php' ); ?>
        </p>
    </div>
    <?php
    }
}
add_action( 'admin_notices', 'highstand_admin_notice' );


/*
* Defind ajax for newsletter actions
*/
if( !function_exists( 'highstand_newsletter' ) ){
	
	add_action( 'wp_ajax_highstand_newsletter', 'highstand_newsletter' );
	add_action( 'wp_ajax_nopriv_highstand_newsletter', 'highstand_newsletter' );

	function highstand_newsletter () { 
		$highstand = highstand::globe('highstand');

		if( !empty( $_POST[ 'highstand_newsletter' ] ) ) 
		{
			
			if( $_POST[ 'highstand_newsletter' ] == 'subcribe' ){

				$email    = $_POST[ 'highstand_email' ];
				$hasError = false;
				$status   = array();
				
				if ( trim( $email ) === '' ) {
					$status = array( 
						'error',
						__( 'Error: Please enter your email', 'highstand' )
					);
					$hasError = true;
				}

				if( !$hasError && !filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {

					$status = array( 
						'error',
						__( 'Error: Your email is invalid', 'highstand' )
					);
					$hasError = true;
				}

				if( !$hasError ){

					//check which method in use
					if( isset( $highstand->cfg['newsletter_method'] ) && $highstand->cfg['newsletter_method'] == 'mc' ){

						locate_template( 'core' . DS . 'inc' . DS . 'MCAPI.class.php', true);

												
						$api_key =  $highstand->cfg['mc_api'];	// grab an API Key from http://admin.mailchimp.com/account/api/			
						$list_id = $highstand->cfg['mc_list_id'];
						$mc_api  = new MCAPI($api_key);
						//If one of config is empty => return error

						if( empty( $api_key ) || empty( $list_id ) ){

							$status = array( 
								'error',
								__('Error: Can not signup into list. Please contact administrator to solve issues.', 'highstand' )
							);
							$hasError = true;
						}
						else
						{
							if( $mc_api->listSubscribe( $list_id, $email, '') === true && empty( $status) ) {

								$status    = array( 
									'success',
									__('Success! Check your email to confirm sign up.', 'highstand' )
								);

							}else{
								
								$status = array(
									'error',
									//sprintf( wp_kses( __( 'Error: %s', 'highstand' ), array() ), $mc_api->errorMessage )
__('Error: This email is already subscribed.', 'highstand' )
								);

							}
						}
						
					}
					else /* Subcribe email to post type subcribe */
					{
						if ( !post_type_exists( 'subcribers' ) ){
							$status = array( 
								'error',
								__('Error: Can not signup into list. Please contact administrator to solve issues.', 'highstand' )
							);
							highstand_return_ajax( $status);
						}

						if ( !get_page_by_title( $email, 'OBJECT', 'subcribers') )
						{
		
							$subcribe_data = array(
								'post_title'   => wp_strip_all_tags( $email ),
								'post_content' => '',
								'post_type'    => 'subcribers',
								'post_status'  => 'pending'
							);
							
							$subcribe_id = wp_insert_post( $subcribe_data );

							if ( is_wp_error( $subcribe_id ) ) {

								$errors = $id->get_error_messages();

								foreach ( $errors as $error ) {
									$error_msg .= "{$error}\n";
								}

							}else{

								$status    = array( 
									'success',
									__('Success! Your email is subcribed.', 'highstand' )
								);

							}
		
						}else{

							$status    = array( 
								'error',
								__('Error: This email already is subcribed', 'highstand' )
							);
						}
					}
					
				}

				highstand_return_ajax( $status);
			}
		}
	}
}

if( !function_exists( 'highstand_return_ajax' ) ){

	function highstand_return_ajax( $status){

		@ob_clean();

		echo '{"status":"' . $status[0] . '","messages":"' . $status[1] . '"}';

		wp_die();

	}
}
