<?php

/**
* Travel Estate
*/
class highstand_travel extends highstand_metabox
{

	public function __construct(){
		$this->init();
	}

	public function init(){
		$highstand = highstand::globe('highstand');

		add_action( 'init', array($this, 'reg_map'), 99 );

		$highstand->ext['asc']( 'hs_travel_book_finder', array( __CLASS__, 'hs_travel_book_finder' ) );
		$highstand->ext['asc']( 'hs_travel_news', array( __CLASS__, 'hs_travel_news' ) );
	}

	public function reg_map(){
		
		global $kc, $wpdb;

		$cf7 = $wpdb->get_results(
			"
				SELECT ID, post_title
				FROM $wpdb->posts
				WHERE post_type = 'wpcf7_contact_form'
			"
		);

		$contact_forms = array();
		if ( $cf7 ) {
			$contact_forms[] = esc_html__( 'Select Contact Form', 'highstand' );
			foreach ( $cf7 as $cform ) {
				$contact_forms[$cform->ID] = $cform->post_title;
			}
		} else {
			$contact_forms[0] = esc_html__( 'No contact forms found', 'highstand' );
		}

		if ( isset( $kc ) ) {

			$kc->add_map(
				array(
					'hs_travel_book_finder' => array(
						'name' => 'Travel booking finder',
						'description' => esc_html__('Display a booking finder form.', 'highstand'),
						'icon' => 'fa-plane',
						'category' => THEME_NAME.' Theme',
						'params' => array(
							array(
								'name' => 'title',
								'label' => 'Title',
								'type' => 'text',
								'admin_label' => true,
							),
							array(
								'name'        => 'c_id',
								'type'        => 'select',
								'label'       => esc_html__( 'Select Contact Form', 'highstand' ),
								'admin_label' => true,
								'options'     => $contact_forms,
								'description' => esc_html__( 'Choose previously created contact form from the drop down list.', 'highstand' )
							),
							array(
								'name' => 'custom_class',
								'label' => 'Extra class',
								'type' => 'text',
								'description' => esc_html__('Extra custom class for list wrapper', 'highstand')
							)
						)
					),

					'hs_travel_news' => array(
						'name' => 'Latest Travel News',
						'description' => esc_html__('Display Latest Travel News', 'highstand'),
						'icon' => 'fa-tree',
						'category' => THEME_NAME.' Theme',
						'params' => array(
							array(
								'name'        => 'title',
								'label'       => 'Title',
								'type'        => 'text',
								'admin_label' => true,
							),
							array(
								'name'        => 'number_show',
								'label'       => 'Number of post travel',
								'type'        => 'text',
								'admin_label' => true,
								'description' => esc_html__('Number of post travel will show in this list', 'highstand'),
								'value'       => '12'
							),
							array(
								'name'        => 'custom_class',
								'label'       => 'Extra class',
								'type'        => 'text',
								'description' => esc_html__('Extra custom class for list wrapper', 'highstand')
							)
						)
					),
				)
			);

		}
	}


	public static function hs_travel_book_finder( $atts, $content = null ){

		$atts = shortcode_atts(array(
			'title' => '',
			'c_id' => '',
			'custom_class' => ''
		), $atts );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.'travel'.DS.'booking-finder-form.php', 'i', $atts, $content );

			$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}

	public static function hs_travel_news( $atts, $content = null ){
		$atts = shortcode_atts(array(
			'title' => '',
			'number_show' => '2',
			'custom_class' => ''
		), $atts );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.'travel'.DS.'travel-news.php', 'i', $atts, $content );

			$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}

}

new highstand_travel();