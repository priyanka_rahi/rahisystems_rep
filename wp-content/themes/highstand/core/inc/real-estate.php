<?php

/**
* Real Estate
*/
class highstand_real_estate extends highstand_metabox
{

	public function __construct(){
		$this->init();
	}

	public function init(){
		$highstand = highstand::globe('highstand');

		add_filter( 'highstand_reg_meta_box_list', array( $this, 'reg_meta_box' ) );

		add_action( 'init', array($this, 'reg_map'), 99 );

		add_action( 'save_post', array( $this, 'save' ) );


		$highstand->ext['asc']( 'hs_realestate_search', array( __CLASS__, 'hs_realestate_search' ) );
		$highstand->ext['asc']( 'hs_list_property', array( __CLASS__, 'hs_list_property' ) );
		$highstand->ext['asc']( 'hs_latest_real_news', array( __CLASS__, 'hs_latest_real_news' ) );
	}

	public function reg_map(){
		
		global $kc;

		if ( isset( $kc ) ) {
			$kc->add_map(
				array(
					'hs_realestate_search' => array(
						'name' => esc_html__( 'Find property form', 'highstand' ),
						'title' => 'Find your property form',
						'icon' => 'fa fa-search',
						'category' => THEME_NAME.' Theme',
						'wrapper_class' => 'clearfix',
						'description' => esc_html__( 'Display find your property form', 'highstand' ),
						'params' => array(
							array(
								'type' => 'text',
								'label' => esc_html__( 'Title', 'highstand' ),
								'name' => 'title',
								'admin_label' => true,
								'description' => esc_html__( '', 'highstand')
							),
							array(
								'type' => 'text',
								'label' => esc_html__( 'Sub Title', 'highstand' ),
								'name' => 'sub_title',
								'admin_label' => true,
								'description' => esc_html__( '', 'highstand')
							),
							array(
								'type' => 'text',
								'label' => esc_html__( 'Text placeholder', 'highstand' ),
								'name' => 'placeholder',
								'admin_label' => true,
								'description' => esc_html__( '', 'highstand'),
								'value' => esc_html__('Enter city, Location or Project', 'highstand')
							),
							array(
								'type'  => 'text',
								'label' => esc_html__( 'Extra Class', 'highstand' ),
								'name'  => 'class',
								'value' => '',
							)
						)
					),

					'hs_list_property' => array(
						'name' => 'List properties',
						'description' => esc_html__('Display List properties', 'highstand'),
						'icon' => 'fa-building',
						'category' => THEME_NAME.' Theme',
						'params' => array(
							array(
								'name' => 'title',
								'label' => 'Title',
								'type' => 'text',
								'admin_label' => true,
							),
							array(
								'name' => 'number_show',
								'label' => 'Number of properties',
								'type' => 'text',
								'admin_label' => true,
								'description' => esc_html__('Number of properties will show in this list', 'highstand'),
								'value' => '12'
							),
							array(
								'name' => 'custom_class',
								'label' => 'Extra class',
								'type' => 'text',
								'description' => esc_html__('Extra custom class for list wrapper', 'highstand')
							),
							array(
								'name' => 'wrapper_custom_class',
								'label' => 'Wrapper Custom class',
								'type' => 'text',
								'description' => esc_html__('Wrapper custom class for list wrapper', 'highstand')
							)
						)
					),

					'hs_latest_real_news' => array(
						'name' => 'Latest Real News',
						'description' => esc_html__('Display Latest Real News', 'highstand'),
						'icon' => 'fa-newspaper-o',
						'category' => THEME_NAME.' Theme',
						'params' => array(
							array(
								'name' => 'title',
								'label' => 'Title',
								'type' => 'text',
								'admin_label' => true,
							),
							array(
								'name' => 'number_show',
								'label' => 'Number of properties',
								'type' => 'text',
								'admin_label' => true,
								'description' => esc_html__('Number of properties will show in this list', 'highstand'),
								'value' => '12'
							),
							array(
								'name' => 'custom_class',
								'label' => 'Extra class',
								'type' => 'text',
								'description' => esc_html__('Extra custom class for list wrapper', 'highstand')
							)
						)
					),
				)
			);
		}
	}

	public function reg_meta_box( $meta_box_list ){
		$meta_box = array(
				'id' => 'property_metabox_options',
				'title' => esc_html__( 'Property Tables Fields - Options', 'highstand' ),
				'callback' => array( $this, 'property_meta_box' ),
				'screen' => array('property'),
				'context' => 'normal',
				'priority' => 'high',
				'callback_args' => array()
			);

		array_push( $meta_box_list, $meta_box );

		return $meta_box_list;
	}

	public function property_meta_box( $post ){
		global $highstand, $highstand_options;

		locate_template( 'options'.DS.'options.php', true );

		$fields = array(
			array(
				'id' => 'price',
				'type' => 'text',
				'title' => esc_html__('Price', 'highstand'),
				'sub_desc' => esc_html__('Property price', 'highstand'),
				'desc' => esc_html__('Enter Property price', 'highstand')
			),
			array(
				'id' => 'unit',
				'type' => 'text',
				'title' => esc_html__('Unit', 'highstand'),
				'sub_desc' => esc_html__('Property price unit', 'highstand'),
				'desc' => esc_html__('Enter Property price unit', 'highstand')
			),
			array(
				'id' => 'by_author',
				'type' => 'text',
				'title' => esc_html__('By', 'highstand'),
				'sub_desc' => esc_html__('Property by author', 'highstand'),
				'desc' => esc_html__('Enter Property by author', 'highstand')
			),
			array(
				'id' => 'address',
				'type' => 'textarea',
				'title' => esc_html__('Adress', 'highstand'),
				'sub_desc' => esc_html__('', 'highstand'),
				'desc' => esc_html__('', 'highstand')
			),
			array(
				'id' => 'city',
				'type' => 'text',
				'title' => esc_html__('City', 'highstand'),
				'sub_desc' => esc_html__('Property City', 'highstand'),
				'desc' => esc_html__('Enter Property City', 'highstand')
			),
		);

		$this->display($fields);
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
		global $post;

		$screen = get_current_screen();

		if(isset($screen) && $screen->post_type == 'property'){
			if( !empty( $_POST['highstand'] ) ){
				if(is_array($_POST['highstand'])){
					foreach ($_POST['highstand'] as $key => $value) {
						update_post_meta( $post_id, '_property_'.$key, $value );
					}
				}

			}
		}

	}

	public static function get_types(){

		$types_term = get_terms( 'property_type', array(
			'orderby'    => 'term_id',
			'order' => 'ASC',
			'hide_empty' => 0,
		) );

		return $types_term;
	}


	public static function get_cats(){

		$cats_term = get_terms( 'property-category', array(
			'orderby'    => 'term_id',
			'order' => 'ASC',
			'hide_empty' => 0,
		) );

		return $cats_term;
	}


	public static function hs_realestate_search( $atts , $content = null ){
		$atts = shortcode_atts(array(
			'title' => 'FIND YOUR PROPERTY',
			'sub_title' => 'AWESOME REAL ESTATE DEALS',
			'placeholder' => 'Enter city, Location or Project',
			'class' => ''
		), $atts );


		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.'realestate'.DS.'realestate-search-form.php', 'i', $atts, $content );

			$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}


	public static function hs_list_property( $atts , $content = null ){
		$atts = shortcode_atts(array(
			'title' => '',
			'number_show' => '12',
			'custom_class' => '',
			'wrapper_custom_class' => ''
		), $atts );


		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.'realestate'.DS.'list-property.php', 'i', $atts, $content );

			$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}


	public static function hs_latest_real_news( $atts , $content = null ){
		$atts = shortcode_atts(array(
			'title' => '',
			'number_show' => '3',
			'custom_class' => ''
		), $atts );

		ob_start();

			highstand_incl_core( 'templates'.DS.'shortcode'.DS.'realestate'.DS.'latest-real-news.php', 'i', $atts, $content );

			$_return = ob_get_contents();

		ob_end_clean();

		return $_return;
	}

}

new highstand_real_estate();