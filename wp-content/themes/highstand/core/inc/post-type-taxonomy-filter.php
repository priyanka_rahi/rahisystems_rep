<?php
/*
 Description: Adds a taxonomy filter in the admin list page for a custom post type.
 Written by King Theme
*/

class highstand_taxonomy_filter{

	public function __construct(){		
		add_action('restrict_manage_posts', array($this, 'restrict_post_type_by_taxonomy'));	
	}

		
	function restrict_post_type_by_taxonomy() {
		global $typenow, $wp_query;
		
		$post_type_list = array(
			'pricing_tables',
			'testimonials',
			'our-works',
			'our-team'
		);

		foreach ($post_type_list as $post_type) {
			if(empty($wp_query->query[$post_type.'-category'])){
				$selected = 0;
			}else{
				$selected = $wp_query->query[$post_type.'-category'];
			}

			if ($typenow == $post_type) {
				$taxonomy = $post_type.'-category';
				$posttype_category_taxonomy = get_taxonomy($taxonomy);
				
				wp_dropdown_categories(array(
					'show_option_all' =>  esc_html__("Show All ", 'highstand') . $posttype_category_taxonomy->label,
					'taxonomy'        =>  $taxonomy,
					'name'            =>  $post_type.'-category',
					'orderby'         =>  'name',
					'selected'        =>  $selected,
					'hierarchical'    =>  true,
					'depth'           =>  3,
					'show_count'      =>  true,
					'hide_empty'      =>  true, 
					'value_field'	  => 'slug',
				));
			}
		}
	}
	
}

new highstand_taxonomy_filter();