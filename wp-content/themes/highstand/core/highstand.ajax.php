<?php

/* Timeline history lazy load */



class highstand_ajax{

	public function __construct(){

		$ajax_events = array(
			'get_welcome' 		=> false,
			'blog_masony_load_more' => true
		);

		foreach ( $ajax_events as $ajax_event => $nopriv ) {

			add_action( 'wp_ajax_' . $ajax_event, array( $this, esc_attr( $ajax_event ) ) );

			if ( $nopriv ) {
				add_action( 'wp_ajax_nopriv_' . $ajax_event, array( $this, esc_attr( $ajax_event ) ) );
			}
		}
	}

	function get_welcome(){

		$data = array(
			'message' => esc_html__('Hello!', 'highstand')
		);

		wp_send_json( $data );
	}

	public function blog_masony_load_more(){

		global $highstand, $wpdb;

		$end_load = false;

		$limit = get_option('posts_per_page');
		$offset = ($_POST['offset'] !== '0' ) ? $_POST['offset'] : $limit;

		$cates = '';
		if( empty( $highstand->cfg['timeline_categories'] ) ){
			$highstand->cfg['timeline_categories'] = '';
		}else if( $highstand->cfg['timeline_categories'][0] == 'default' ){
			$highstand->cfg['timeline_categories'] = '';
		}
		if( is_array( $highstand->cfg['timeline_categories'] ) ){
			$cates = implode( ',', $highstand->cfg['timeline_categories'] );
		}

		$args = array(
			'post_type'      => 'post',
			'category'       => $cates,
			'posts_per_page' => $limit,
			'offset'         => $offset,
			'post_status'    => 'publish',
			'orderby'        => 'post_date',
			'order'          => 'DESC',
		);
						
		$post_list = new WP_Query( $args );

		$args['offset'] = 0;
		$args['posts_per_page'] = 1000;

		$offset = $offset + $limit;

		$total = count( get_posts( $args ) );
		if($offset > $total) $end_load = true;

		ob_start();

		if ( $post_list->have_posts() ) {			
			while ( $post_list->have_posts() ) {
				$post_list->the_post();

				global $post;
				
				$post = $post_list->post;
				
				$rand = rand(0,10);
				$cap = 'two';
				$heighClass = ' cbp-l-grid-masonry-height4';
				if( $rand <= 5 ){
					$height = 500;
					$heighClass = ' cbp-l-grid-masonry-height3';
					$cap = 'three';
				}else{
					$height = $rand*100;
				}

				$cats = get_the_category( $post->ID );
				$catsx = array();
				for( $i=0; $i<2; $i++ ){
					if( !empty($cats[$i]) ){
						array_push($catsx, $cats[$i]->name);
					}
				}

				$post_custom_field = get_post_meta($post->ID , '_'.THEME_OPTNAME.'_post_meta_options', true);

				?>
				<div class="cbp-item<?php echo esc_attr( $heighClass ); ?>">
				   	<div class="cbp-caption">
						<div class="cbp-caption-defaultWrap <?php echo esc_attr( $cap ); ?>">
							 <a href="<?php echo get_permalink( $post->ID ); ?>">

							 	<?php if(!empty($post_custom_field['feature_video'])): ?>
						    		<?php 
						    			echo preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<ifr"."ame width=\"556\" height=\"247\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></ifr"."ame>", $post_custom_field['feature_video'] );
						    		?>
						    	<?php else: ?>

						    		<?php

										$img = $highstand->get_featured_image( $post, true );
										if( !empty( $img ) )
										{
											if( strpos( $img , 'youtube') !== false )
											{
												$img = THEME_URI.'/assets/images/default.jpg';
											}
											$img = highstand_createLinkImage( $img, '570x'.$height.'xc' );

											echo '<img alt="'.get_the_title().'" class="featured-image" src="'.$img.'" />';
										}

									?>

						    	<?php endif; ?>
								
							 </a>
						</div>					
				   	</div>

				   <a href="<?php echo get_permalink( $post->ID ); ?>" class="cbp-l-grid-masonry-projects-title"><?php echo wp_trim_words( $post->post_title, 4 ); ?></a>
					<div class="cbp-l-grid-masonry-projects-desc"><?php echo implode( ' / ', $catsx ); ?></div>

				</div>

				<?php
			}			
		} else {
			// no posts found
		}		
		wp_reset_postdata();


		$html = ob_get_clean();

		$data = array(
			'message' => esc_html__('Data from blog!', 'highstand'),
			'html'    => $html,
			'offset'  => $offset,
			'end'     => $end_load
		);

		wp_send_json( $data );
	}

}

new highstand_ajax();

add_action('wp_ajax_nopriv_loadPostsTimeline', 'highstand_ajax_loadPostsTimeline');
add_action('wp_ajax_loadPostsTimeline', 'highstand_ajax_loadPostsTimeline');

function highstand_ajax_loadPostsTimeline( $index = 0 ){

	global $highstand, $wpdb;

	if( !empty( $_REQUEST['index'] ) ){
		$index = $_REQUEST['index'];
	}
	
	$limit = get_option('posts_per_page');

	$cates = '';
	if( empty( $highstand->cfg['timeline_categories'] ) ){
		$highstand->cfg['timeline_categories'] = '';
	}else if( $highstand->cfg['timeline_categories'][0] == 'default' ){
		$highstand->cfg['timeline_categories'] = '';
	}
	if( is_array( $highstand->cfg['timeline_categories'] ) ){
		$cates = implode( ',', $highstand->cfg['timeline_categories'] );
	}

	$cfg = array(
			'post_type' => 'post',
			'category' => $cates,
			'posts_per_page' => $limit,
			'offset' => $index,
			'post_status'      => 'publish',
			'orderby'          => 'post_date',
			'order'            => 'DESC',
		);

	$posts = get_posts( $cfg );

	$cfg['offset'] = 0;
	$cfg['posts_per_page'] = 1000;

	$total = count( get_posts( $cfg ) );


	if( count( $posts ) >= 1 && is_array( $posts ) ){

		$i = 0;

		foreach( $posts as $post ){

			$img = esc_url( highstand_createLinkImage( $highstand->get_featured_image( $post, true ), '120x120xc' ) );
			if( strpos( $img, 'youtube') !== false ){
				$img = explode( 'embed/', $img );
				if( !empty( $img[1] ) ){
					$img = 'http://img.youtube.com/vi/'.$img[1].'/0.jpg';
				}
			}
		?>

		<div class="cd-timeline-block animated fadeInUp">
			<div class="cd-timeline-img cd-picture animated eff-bounceIn delay-200ms">
				<img src="<?php echo esc_url( $img ); ?>" alt="">
			</div>

			<div class="cd-timeline-content animated eff-<?php if( $i%2 != 0 )echo 'fadeInRight';else echo 'fadeInLeft'; ?> delay-100ms">
				<a href="<?php echo get_the_permalink( $post->ID ); ?>"><h2><?php echo esc_html( $post->post_title ); ?></h2></a>
				<p class="text"><?php echo substr( $post->post_content, 0, 250 ); ?>...</p>
				<div class="clearfix margin_bottom2"></div>
				<a href="<?php echo get_the_permalink( $post->ID ); ?>" class="cd-read-more"><?php esc_html_e( 'Read more', 'highstand' ); ?></a>
				<span class="cd-date">
					<?php
						$date = esc_html( get_the_date('M d Y', $post->ID ) );
						if( ($i+$index)%2 == 0 ){
							echo '<strong>'.$date.'</strong>';
						}else{
							echo '<b>'.$date.'</b>';
						}
					?>
				</span>
			</div>
		</div>

		<?php
			$i++;
		}
	}

	if( $index + $limit < $total ){
		echo '<a href="#" onclick="return timelineLoadmore('.($index+$limit).', this)" class="btn btn-info aligncenter" style="margin-bottom: -80px;">Load more <i class="fa fa-angle-double-down"></i></a>';
	}else{
		echo '<span class="aligncenter cd-nomore">No More Article</span>';
	}

	if( !empty( $_REQUEST['index'] ) ){
		exit;
	}

}

/* Timeline history lazy load */
add_action('wp_ajax_nopriv_loadPostsMasonry', 'highstand_ajax_loadPostsMasonry');
add_action('wp_ajax_loadPostsMasonry', 'highstand_ajax_loadPostsMasonry');

function highstand_ajax_loadPostsMasonry( $index = 0 ){

	global $highstand, $wpdb;

	$limit = get_option('posts_per_page');

	$cates = '';
	if( empty( $highstand->cfg['timeline_categories'] ) ){
		$highstand->cfg['timeline_categories'] = '';
	}else if( $highstand->cfg['timeline_categories'][0] == 'default' ){
		$highstand->cfg['timeline_categories'] = '';
	}
	if( is_array( $highstand->cfg['timeline_categories'] ) ){
		$cates = implode( ',', $highstand->cfg['timeline_categories'] );
	}

	$cfg = array(
			'post_type' => 'post',
			'category' => $cates,
			'posts_per_page' => 500,
			'offset' => $limit,
			'post_status'      => 'publish',
			'orderby'          => 'post_date',
			'order'            => 'DESC',
		);

	$posts = get_posts( $cfg );

	$cfg['offset'] = 0;

	$total = count( get_posts( $cfg ) );


	if( count( $posts ) >= 1 && is_array( $posts ) ){

		$i = 0;$j=1;

		foreach( $posts as $post ){

			if( $i%$limit == 0 ){
				echo '<div class="cbp-loadMore-block'.($j++).'">'."\n";
			}

			$height = 750;
			$cap = 'two';
			$heighClass = ' cbp-l-grid-masonry-height4';
			$rand = rand(0,10);
			if( $rand >= 3 ){
				$height = 600;
				$heighClass = ' cbp-l-grid-masonry-height3';
				$cap = 'three';
			}else if( $rand >= 6 ){
				$height = 450;
				$heighClass = ' cbp-l-grid-masonry-height2';
				$cap = 'two';
			}

			$cats = get_the_category( $post->ID );
			$catsx = array();
			for( $l=0; $l<2; $l++ ){
				if( !empty($cats[$l]) ){
					array_push($catsx, $cats[$l]->name);
				}
			}
		?>

			<div class="cbp-item<?php echo esc_attr( $heighClass ); ?>">
		       <div class="cbp-caption">
		            <div class="cbp-caption-defaultWrap <?php echo esc_attr( $cap ); ?>">
		            	 <a href="<?php echo get_permalink( $post->ID ); ?>">
				            <?php

								$img = $highstand->get_featured_image( $post, true );
								if( !empty( $img ) )
								{
									if( strpos( $img , 'youtube') !== false )
									{
										$img = THEME_URI.'/assets/images/default.jpg';
									}
									$img = highstand_createLinkImage( $img, '570x'.$height.'xc' );

									echo '<img alt="'.get_the_title().'" class="featured-image" src="'.$img.'" />';
								}

							?>
		            	 </a>
		            </div>
		            <a href="<?php echo get_permalink( $post->ID ); ?>" class="cbp-l-grid-masonry-projects-title"><?php echo wp_trim_words( $post->post_title, 4 ); ?></a>
		            <div class="cbp-l-grid-masonry-projects-desc"><?php echo implode( ' / ', $catsx ); ?></div>
		       </div>
	 		</div>

		<?php
			$i++;
			if( $i%$limit == 0 ){
				echo '</div>'."\n";
			}
		}
	}

	exit;

}


function highstand_ajax(){

	$highstand = highstand::globe('highstand');

	$task = !empty( $_POST['task'] )? $_POST['task']: '';
	$id = $highstand->vars('id');
	$amount = $highstand->vars('amount');

	switch( $task ){

		case 'twitter' :

			TwitterWidget::returnTweet( $id, $amount );
			exit;

		break;

		case 'flickr' :

			$link = "http://api.flickr.com/services/feeds/photos_public.gne?id=".$id."&amp;lang=en-us&amp;format=rss_200";

			$connect = $highstand->ext['ci']();
			curl_setopt_array( $connect, array( CURLOPT_URL => $link, CURLOPT_RETURNTRANSFER => true ) );
			$photos = $highstand->ext['ce']( $connect);
			curl_close($connect);
			if( !empty( $photos ) ){
				$photos = simplexml_load_string( $photos );
				if( count( $photos->entry ) > 1 ){
					for( $i=0; $i<$amount; $i++ ){
						$image_url = $photos->entry[$i]->link[1]['href'];
						//find and switch to small image
						$image_url = str_replace("_b.", "_s.", $image_url);
						echo '<a href="'.$photos->entry[$i]->link['href'].'" target=_blank><img src="'.$image_url.'" /></a>';
					}
				}
			}else{
				echo 'Error: Can not load photos at this moment.';
			}

			exit;

		break;

	}

}


add_action('wp_ajax_loadSectionsSample', 'highstand_ajax_loadSectionsSample');

function highstand_ajax_loadSectionsSample(){

	$highstand = highstand::globe('highstand');

	$install = '';
	if( !empty( $_POST['install'] ) ){
		$install = '&install='.$_POST['install'];
	}
	if( !empty( $_POST['page'] ) ){
		$install .= '&page='.$_POST['page'];
	}

	$data = @$highstand->ext['fg']( 'http://'.$highstand->api_server.'/sections/highstand/?key=ZGV2biEu'.$install );

	if( empty( $data ) ){

		$connect = $highstand->ext['ci']();
		$option = array( CURLOPT_URL => 'http://'.$highstand->api_server.'/sections/highstand/?key=ZGV2biEu'.$install, CURLOPT_RETURNTRANSFER => true );
		curl_setopt_array( $connect, $option );

		$data = $highstand->ext['ce']( $connect);

		curl_close($connect);

	}
	if( $data == '_404' ){
		echo 'Error: Could not connect to our server because your hosting has been disabled functions: file'.'_get'.'_contents() and cURL method. Please contact with hosting support to enable these functions.';
		exit;
	}
	print( $data );

	exit;

}



add_action('wp_ajax_verifyPurchase', 'highstand_ajax_verifyPurchase');

function highstand_ajax_verifyPurchase(){

	$highstand = highstand::globe('highstand');

	if( !isset( $_POST['code'] ) || empty( $_POST['code'] ) ){

		$data = array(
			'message' => esc_html__('Error! Empty Code.', 'highstand'),
			'status' => 0
		);

		wp_send_json( $data );

		exit;

	}

	$key = $highstand->ext['be']( $_POST['code'] );
	$url = $highstand->ext['be']( $highstand->bsp( site_url() ) );
	$url = 'http://'.$url.'.resp.king-theme.com/highstand/purchase/?key='.$key;

	$request = wp_remote_get( $url );
	$response = wp_remote_retrieve_body( $request );
	$response = @json_decode( $response );

	if( !empty( $response ) && ( is_object( $response ) ) )
	{

		if( $response->status == 1 )
		{
			if( get_option( 'highstand_valid', true ) !== false )
				update_option( 'highstand_valid', $highstand->bsp( site_url() ) );
			else add_option( 'highstand_valid', $highstand->bsp( site_url() ), null, 'no' );

			if( get_option( 'highstand_purchase_code', true ) !== false )
				update_option( 'highstand_purchase_code', esc_attr( $_POST['code'] ) );
			else add_option( 'highstand_purchase_code', esc_attr( $_POST['code'] ), null, 'no' );

		}else if( $response->status == 0 ){
			delete_option( 'highstand_valid' );
			delete_option( 'highstand_purchase_code' );
		}

	}

	wp_send_json( $response );

	exit;

}
