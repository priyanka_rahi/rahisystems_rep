
function hs_cube_init(){

(function($, window, document, undefined) {
    'use strict';

    // init cubeportfolio

    $('.js-grid-full-width').each(function(){
	    
	    if( $(this).data('loaded') !== true )
	    	$(this).data({'loaded':true});
	    else return;
	    
        var _cols = $(this).data('cols'),
            _gap = $(this).data('gap').toString(),
            _gap_sp, _gapHorizontal, _gapVertical;

        _gap_sp = _gap.split("|");
        //console.log(_gap_sp[0]);
        if(_gap_sp.length > 1){
            _gapHorizontal = parseInt(_gap_sp[0]);
            _gapVertical = parseInt(_gap_sp[1]);
        }else{
            _gapHorizontal = parseInt(_gap_sp[0]);
            _gapVertical = parseInt(_gap_sp[0]);
        }

        $(this).cubeportfolio({
            filters: '#js-filters-full-width',
            loadMore: '#js-loadMore-full-width',
            loadMoreAction: 'auto',
            layoutMode: 'mosaic',
            sortToPreventGaps: true,
            defaultFilter: '*',
            animationType: 'fadeOutTop',
            gapHorizontal: _gapHorizontal,
            gapVertical: _gapVertical,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 999,
                cols: _cols
            }, {
                width: 767,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'fadeIn',
            displayTypeSpeed: 100,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',
        });
    });



    // init cubeportfolio
    $('.js-grid-juicy-projects').each(function(){
        
        if( $(this).data('loaded') !== true )
	    	$(this).data({'loaded':true});
	    else return;
        
        var _cols = $(this).data('cols'),
            _gap = $(this).data('gap').toString(),
            _gap_sp, _gapHorizontal, _gapVertical;

        _gap_sp = _gap.split("|");
        //console.log(_gap_sp[0]);
        if(_gap_sp.length > 1){
            _gapHorizontal = parseInt(_gap_sp[0]);
            _gapVertical = parseInt(_gap_sp[1]);
        }else{
            _gapHorizontal = parseInt(_gap_sp[0]);
            _gapVertical = parseInt(_gap_sp[0]);
        }

        $(this).cubeportfolio({
            filters: '#js-filters-juicy-projects',
            loadMore: '#js-loadMore-juicy-projects',
            loadMoreAction: 'click',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: 'quicksand',
            gapHorizontal: _gapHorizontal,
            gapVertical: _gapVertical,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: _cols
            }, {
                width: 960,
                cols: _cols
            }, {
                width: 800,
                cols: _cols
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'overlayBottomReveal',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

            // singlePage popup
            singlePageDelegate: '.cbp-singlePage',
            singlePageDeeplinking: true,
            singlePageStickyNavigation: true,
            singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
            singlePageCallback: function(url, element) {
                // to update singlePage content use the following method: this.updateSinglePage(yourContent)
                var t = this;

                $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'html',
                        timeout: 10000
                    })
                    .done(function(result) {
                        t.updateSinglePage(result);
                    })
                    .fail(function() {
                        t.updateSinglePage('AJAX Error! Please refresh the page!');
                    });
            },
        });
    });


    // Layout 4
    $('.js-grid-juicy-projects-layout-4').each(function(){
	    
	    if( $(this).data('loaded') !== true )
	    	$(this).data({'loaded':true});
	    else return;
	    
        var _cols = $(this).data('cols');

        $(this).cubeportfolio({
            filters: '#js-filters-juicy-projects-layout-4',
            loadMore: '#js-loadMore-juicy-projects-layout-4',
            loadMoreAction: 'click',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: 'quicksand',
            gapHorizontal: 0,
            gapVertical: 0,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: _cols
            }, {
                width: 960,
                cols: _cols
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

            // singlePage popup
            singlePageDelegate: '.cbp-singlePage',
            singlePageDeeplinking: true,
            singlePageStickyNavigation: true,
            singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
            singlePageCallback: function(url, element) {
                // to update singlePage content use the following method: this.updateSinglePage(yourContent)
                var t = this;

                $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'html',
                        timeout: 10000
                    })
                    .done(function(result) {
                        t.updateSinglePage(result);
                    })
                    .fail(function() {
                        t.updateSinglePage('AJAX Error! Please refresh the page!');
                    });
            },
        });
    });


    function highstand_blog_masonry(){
        $('.cbp-l-grid-masonry-projects').each(function(){
            var _cols = $(this).data('cols'),
                _gap = $(this).data('gap').toString(),
                _gap_sp, _gapHorizontal, _gapVertical;

            _gap_sp = _gap.split("|");
            //console.log(_gap_sp[0]);
            if(_gap_sp.length > 1){
                _gapHorizontal = parseInt(_gap_sp[0]);
                _gapVertical = parseInt(_gap_sp[1]);
            }else{
                _gapHorizontal = parseInt(_gap_sp[0]);
                _gapVertical = parseInt(_gap_sp[0]);
            }

            $(this).cubeportfolio({
                filters: '#js-filters-grid-masonry-projects',
                loadMore: '#js-loadMore-full-width',
                loadMoreAction: 'auto',
                layoutMode: 'mosaic',
                sortToPreventGaps: true,
                defaultFilter: '*',
                animationType: 'fadeOutTop',
                gapHorizontal: _gapHorizontal,
                gapVertical: _gapVertical,
                gridAdjustment: 'responsive',
                mediaQueries: [{
                    width: 999,
                    cols: _cols
                }, {
                    width: 767,
                    cols: 3
                }, {
                    width: 480,
                    cols: 2
                }, {
                    width: 320,
                    cols: 1
                }],
                caption: 'zoom',
                displayType: 'fadeIn',
                displayTypeSpeed: 100,

                // lightbox
                lightboxDelegate: '.cbp-lightbox',
                lightboxGallery: true,
                lightboxTitleSrc: 'data-title',
                lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',
            });
        });
    }
    highstand_blog_masonry();
    
    /*********************************
        add listener for load more
    *********************************/

    $('.cbp-l-loadMore-link').off('click').on('click', function(e){
        e.preventDefault();

        var _this = $( this ) ;
        
        if (_this.hasClass('cbp-l-loadMore-button-stop')) {
            return;
        }

        _this.text('LOADING...');

        $.ajax({
            url: ajax_var.url,
            method: 'POST',
            dataType: 'json',
            data: {
                action: 'blog_masony_load_more',
                offset: _this.attr('data-offset')
            },
            success: function( response ){
                _this.text('LOAD MORE');

                _this.attr( 'data-offset', response.offset );

                if( response.end === true ){
                    _this.text('NO MORE WORKS');
                    _this.addClass('cbp-l-loadMore-button-stop');
                }
                
                $('#grid-masonry-container').find('.cbp-wrapper').append( response.html );
                $("#grid-masonry-container").cubeportfolio('destroy');
                
                highstand_blog_masonry();
            }
        })
    });
    

})(jQuery, window, document);

};


hs_cube_init();

