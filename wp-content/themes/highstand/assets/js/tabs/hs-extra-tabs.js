/**
 * (c) King-Theme.Com
 **/

;(function ( $, window, document, undefined ) {

    var pluginName = "min_extra_tabs",
        defaults = {
            effect: 'scale'
        };

       // $('<style>body { background-color: red; color: white; }</style>').appendTo('head');

    function Plugin( element, options ) {
        this.element = element;
        this.$elem = $(this.element);
        this.options = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {

        init: function() {
			var $elem = this.$elem;
            var links = this.$elem.find('a');
            var firstchild = this.$elem.find('li:first-child').find('a');
            var lastchild = this.$elem.find('li:last-child').after('<span class="extra_tabs_clear"></span>');

            if (this.options.effect == 'scale') {
				tab_content = this.$elem.find('div.tab_panel').not(':first').not(':nth-child(1)').addClass('hidescale');
            } else if (this.options.effect == 'slideLeft') {
                tab_content = this.$elem.find('div.tab_panel').not(':first').not(':nth-child(1)').addClass('hideleft');
            } else if (this.options.effect == 'scaleUp') {
                tab_content = this.$elem.find('div.tab_panel').not(':first').not(':nth-child(1)').addClass('hidescaleup');
            } else if (this.options.effect == 'flip') {
                tab_content = this.$elem.find('div.tab_panel').not(':first').not(':nth-child(1)').addClass('hideflip');
            }

            var $panel_container = this.$elem.find('.tabs_container');
            var first_panel_height = $panel_container.find('div.tab_panel:first').innerHeight();

            var alldivs = $elem.find('.tab_panel');

            alldivs.css({'position': 'absolute','top':'0px'});

            if(first_panel_height < 488) first_panel_height = 488;

            $panel_container.height(first_panel_height);

            firstchild.addClass('extra_tabs_active');

			
            links.on('click', {myOptions: this.options}, function(e) {
                e.preventDefault();

                var $options = e.data.myOptions,
					effect = $options.effect,
					$_this = $(this), 
					thislink = $_this.attr('href'),
					before_border_width = $_this.innerHeight() / 2;

                $panel_container.addClass('transition');

                links.removeClass('extra_tabs_active');
                $_this.addClass('extra_tabs_active');
                panel_height = $elem.find('div'+thislink).innerHeight();


				if($('#fixed_tabs_arrow').length > 0){
					$('#fixed_tabs_arrow').html('.extra_tabs_active:before { border-top-width: ' + before_border_width + 'px; border-bottom-width: ' + before_border_width + 'px; }');
				}else{
					$( '<style id="fixed_tabs_arrow" type="text/css">.extra_tabs_active:before { border-top-width: ' + before_border_width + 'px; border-bottom-width: ' + before_border_width + 'px; }</style>' ).appendTo( "head" );
				}
				
				
                if (effect == 'scale') {
                    alldivs.removeClass('showscale').addClass('make_transist').addClass('hidescale');
                    $elem.find('div'+thislink).addClass('make_transist').addClass('showscale');
                } else if (effect == 'slideLeft') {
                    alldivs.removeClass('showleft').addClass('make_transist').addClass('hideleft');
                    $elem.find('div'+thislink).addClass('make_transist').addClass('showleft');
                } else if (effect == 'scaleUp') {
                    alldivs.removeClass('showscaleup').addClass('make_transist').addClass('hidescaleup');
                    $elem.find('div'+thislink).addClass('make_transist').addClass('showscaleup');
                } else if (effect == 'flip') {
                    alldivs.removeClass('showflip').addClass('make_transist').addClass('hideflip');
                    $elem.find('div'+thislink).addClass('make_transist').addClass('showflip');
                }


                $panel_container.height(panel_height);

                
            });        
            
        },

        yourOtherFunction: function(el, options) {
            // some logic
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            new Plugin( this, options );
        });
    };

})( jQuery, window, document );


jQuery(document).ready(function($) {
    
    $('.hs_extra_tab').min_extra_tabs({
    	effect: 'scaleUp'
    });


    $('.hs_extra_tab_hor').each(function(){
        var _this = $(this);
        var tab_active = $(this).find('.hor_tabs').data('tab_active');

        $(this).find('.hor_tabs>li').eq( tab_active - 1).addClass('active');
        $(this).find('.tab__content>div').eq( tab_active - 1).addClass('active');
    

        var clickedTab = _this.find(".hor_tabs > .active");
        var tabWrapper = _this.find(".tab__content");
        var activeTab = tabWrapper.find(".active");
        var activeTabHeight = activeTab.outerHeight();
        
        // Show tab on page load
        activeTab.show();
        
        // Set height of wrapper on page load
        tabWrapper.height(activeTabHeight);
        
        _this.find(".hor_tabs > li").on("click", function() {
            
            // Remove class from active tab
            _this.find(".hor_tabs > li").removeClass("active");
            
            // Add class active to clicked tab
            $(this).addClass("active");
            
            // Update clickedTab variable
            clickedTab = _this.find(".hor_tabs .active");
            
            // fade out active tab
            activeTab.fadeOut(100, function() {
                
                // Remove active class all tabs
                _this.find(".tab__content > div").removeClass("active");
                
                // Get index of clicked tab
                var clickedTabIndex = clickedTab.index();

                // Add class active to corresponding tab
                _this.find(".tab__content > div").eq(clickedTabIndex).addClass("active");
                
                // update new active tab
                activeTab = _this.find(".tab__content > .active");
                
                // Update variable
                activeTabHeight = activeTab.outerHeight();
                
                // Animate height of wrapper to new tab height
                tabWrapper.stop().delay(30).animate({
                    height: activeTabHeight
                }, 300, function() {
                    
                    // Fade in active tab
                    activeTab.delay(30).fadeIn(100);
                    
                });
            });
        });


    });


});