<?php


/*
 *
 * Thanks for Leemason-NHP
 * Copyright (c) Options by Leemason-NHP 
 *
 * 
 * Require the framework class before doing anything else, so we can use the defined urls and dirs
 * Also if running on windows you may have url problems, which can be fixed by defining the framework url first
 *
 */
//define('highstand_options_URL', HOME_URL('path the options folder'));
if(!class_exists('highstand_options')){
	locate_template( 'options'.DS.'options.php', true );
}

/*
 * 
 * Custom function for filtering the sections array given by theme, good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 *
 * NOTE: the defined constansts for urls, and dir will NOT be available at this point in a child theme, so you must use
 * get_template_directory_uri() if you want to use any of the built in icons
 *
 */
function add_another_section($sections){
	
	//$sections = array();
	$sections[] = array(
				'title' => esc_html__('A Section added by hook', 'highstand'),
				'desc' => wp_kses( __('<p class="description">This is a section created by adding a filter to the sections array, great to allow child themes, to add/remove sections from the options.</p>', 'highstand'), array('p'=>array())),
				//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
				//You dont have to though, leave it blank for default.
				'icon' => trailingslashit(get_template_directory_uri()).'options/img/glyphicons/glyphicons_062_attach.png',
				//Lets leave this as a blank section, no options just some intro text set above.
				'fields' => array()
				);
	
	return $sections;
	
}//function

/*
 * 
 * Custom function for filtering the args array given by theme, good for child themes to override or add to the args array.
 *
 */
function change_framework_args($args){
	
	//$args['dev_mode'] = false;
	
	return $args;
	
}//function

/*
 * This is the meat of creating the optons page
 *
 * Override some of the default values, uncomment the args and change the values
 * - no $args are required, but there there to be over ridden if needed.
 *
 *
 */

function setup_framework_options(){
	
	$highstand = highstand::globe('highstand');
	
	$args = array();
	
	$args['dev_mode'] = false;

	$args['google_api_key'] = 'AIzaSyDAnjptHMLaO8exTHk7i8jYPLzygAE09Hg';

	$args['intro_text'] = wp_kses( __('<p>This is the HTML which can be displayed before the form, it isnt required, but more info is always better. Anything goes in terms of markup here, any HTML.</p>', 'highstand'), array('p'=>array()));

	$args['share_icons']['twitter'] = array(
											'link' => 'http://twitter.com/devnCo',
											'title' => 'Folow me on Twitter'
											);

	$args['show_import_export'] = false;

	$args['opt_name'] = THEME_OPTNAME;

	$args['page_position'] = 1001;
	$args['allow_sub_menu'] = false;


	$import_file = ABSPATH.'wp-content'.DS.'themes'.DS.THEME_SLUG.DS.'core'.DS.'sample'.DS.'data.xml';
	$import_html = '';
	if ( file_exists($import_file) ){

		$import_html = '<h2></h2><br /><div class="nhp-opts-section-desc"><p class="description"><a style="font-style: normal;" href="admin.php?page=king-sample-data" class="btn btn_green">One-Click Importer Sample Data</a>  &nbsp; Just click and your website will look exactly our demo (posts, pages, menus, categories, tags, layouts, images, sliders, post-type) </p> <br /></div><hr style="background: #ccc;border: none;height: 1px;"/><br />';

	}

	$sections = array();

	$patterns = array();
	for( $i=1; $i<13; $i++ ){
		$patterns['pattern'.$i.'.png'] = array('title' => 'Background '.$i, 'img' => THEME_URI.'/assets/images/elements/pattern'.$i.'.png');
	}
	for( $i=13; $i<17; $i++ ){
		$patterns['pattern'.$i.'.jpg'] = array('title' => 'Background '.$i, 'img' => THEME_URI.'/assets/images/elements/pattern'.$i.'-small.jpg');
	}

	$listHeaders = array();
	if ( $handle = opendir( THEME_PATH.DS.'templates'.DS.'header' ) ){
		while ( false !== ( $entry = readdir($handle) ) ) {
			if( $entry != '.' && $entry != '..' && strpos($entry, '.php') !== false  ){
				$title  = ucwords( str_replace( '-', ' ', basename( $entry, '.php' ) ) );
				$listHeaders[ 'templates'.DS.'header'.DS.$entry ] = array('title' => $title, 'img' => THEME_URI.'/templates/header/thumbnails/'.basename( $entry, '.php' ).'.jpg');
			}
		}
	}

	$listBreadcrumbs = array();
	if ( $handle = opendir( THEME_PATH.DS.'templates'.DS.'breadcrumb' ) ){
		while ( false !== ( $entry = readdir($handle) ) ) {
			if( $entry != '.' && $entry != '..' && strpos($entry, '.php') !== false  ){
				$title  = ucwords( str_replace( '-', ' ', basename( $entry, '.php' ) ) );
				$listBreadcrumbs[ 'templates'.DS.'breadcrumb'.DS.$entry ] = array('title' => $title, 'img' => THEME_URI.'/templates/breadcrumb/thumbnails/'.basename( $entry, '.php' ).'.jpg');
			}
		}
	}

	$sidebars = array( '' => '--Select Sidebar--' );

	if( !empty( $highstand->cfg['sidebars'] ) ){
		foreach( $highstand->cfg['sidebars'] as $sb ){
			$sidebars[ sanitize_title_with_dashes( $sb ) ] = esc_html( $sb );
		}
	}

$sections[] = array(
	'id' => 'general-settings',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_023_cogwheels.png',
	'title' => esc_html__('General Settings', 'highstand'),
	'desc' => wp_kses( __('<p class="description">general configuration options for theme</p>', 'highstand'), array('p'=>array())),
	'fields' => array(

		array(
			'id' => 'logo_height',
			'type' => 'text',
			'title' => esc_html__('Logo Max Height', 'highstand'), 
			'sub_desc' => esc_html__('Limit logo\'s size. Eg: 60', 'highstand'),
			'std' => '45',
			'desc' => 'px',
			'css' => '<?php if($value!="")echo "html body .logo img{max-height: ".$value."px;}"; ?>',
		),		
		array(
			'id' => 'logo_top',
			'type' => 'text',
			'title' => esc_html__('Logo Top Spacing', 'highstand'), 
			'sub_desc' => esc_html__('The spacing from the logo to the edge of the page. Eg: 5', 'highstand'),
			'std' => '5',
			'desc' => 'px',
			'css' => '<?php if($value!="")echo "html body .logo{margin-top: ".$value."px;}"; ?>',
		),			
		array(
			'id' => 'favicon',
			'type' => 'upload',
			'title' => esc_html__('Upload Favicon', 'highstand'), 
			'std' => THEME_URI.'/favico.png',
			'sub_desc' => esc_html__('This will be display at title of browser', 'highstand'),
			'desc' => esc_html__('Upload new or from media library to use as your favicon.', 'highstand')
		),				
		array(
			'id' => 'layout',
			'type' => 'button_set',
			'title' => esc_html__('Select Layout', 'highstand'), 
			'desc' => '',
			'options' => array('wide' => 'WIDE','boxed' => 'BOXED'),
			'std' => 'wide'
		),
		array(
			'id' => 'responsive',
			'type' => 'button_set',
			'title' => esc_html__('Responsive Support', 'highstand'), 
			'desc' => esc_html__('Help display well on all screen size (smartphone, tablet, laptop, desktop...)', 'highstand'),
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'std' => '1'
		),		
		array(
			'id' => 'effects',
			'type' => 'button_set',
			'title' => esc_html__('Effects Lazy Load', 'highstand'), 
			'desc' => esc_html__('Sections\' effect displaying when scoll over.', 'highstand'),
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'std' => '1'
		),		
		array(
			'id' => 'admin_bar',
			'type' => 'button_set',
			'title' => esc_html__('Admin Bar', 'highstand'), 
			'desc' => esc_html__('The admin bar on top at Front-End when you logged in.', 'highstand'),
			'options' => array('hide' => 'Hide','show' => 'Show'),
			'std' => 'hide'
		),		
		array(
			'id' => 'breadcrumb',
			'type' => 'profile_template',
			'title' => esc_html__('Show Breadcrumb', 'highstand'), 
			'desc' => esc_html__('The Breadcrumb on every page', 'highstand'),
			'options' => $listBreadcrumbs,
			'std' => 'default.php'
		),
		array(
			'id' => 'breadeli',
			'type' => 'text',
			'title' => esc_html__('Breadcrumb Delimiter', 'highstand'), 
			'desc' => esc_html__('The symbol in beetwen your Breadcrumbs.', 'highstand'),
			'std' => '/'
		),
		array(
			'id' => 'breadcrumb_bg',
			'type' => 'upload',
			'title' => esc_html__('Breadcrumb Background Image', 'highstand'), 
			'desc' => esc_html__('Upload your background image for Breadcrumb', 'highstand'),
			'std' => '',
			'css' => '<?php if($value!="")echo "#breadcrumb.page_title1{background-image:url(".$value.");}"; ?>}'
		),
		array(
			'id' => 'api_server',
			'type' => 'button_set',
			'title' => esc_html__('Select API Server', 'highstand'), 
			'desc' => esc_html__('Select API in case you have problems importing sample data or install sections', 'highstand'),
			'options' => array('api.devn.co' => 'API Server 1','api2.devn.co' => 'API Server 2'),
			'std' => 'api.devn.co'
		),
	)	
);

$sections[] = array(
	'id' => 'header-settings',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_263_bank.png',
	'title' => esc_html__('Header Settings', 'highstand'),
	'desc' => wp_kses( __('<p class="description">Select header & footer layouts, Add custom meta tags, hrefs and scripts to header.</p>', 'highstand'), array('p'=>array())),
	'parent' => 'general-settings',
	'fields' => array(
		array(
			'id' => 'sidebar_menu_pos',
			'type' => 'button_set',
			'title' => esc_html__('Display sidebar menu mobile on', 'highstand'),
			'options' => array( 'left' => 'Left side', 'right' => 'Right side' ),
			'std' => 'left'
		),
		array(
			'id' => 'header',
			'type' => 'profile_template',
			'title' => esc_html__('Select Header', 'highstand'),
			'sub_desc' => '<br /><br />'.wp_kses( __('Overlap: The header will cover up anything beneath it. <br /> <br />Select header for all pages, You can also go to each page to select specific. This path has located /templates/header/{-file-}', 'highstand'), array( 'br'=>array() )),
			'options' => $listHeaders,
			'std' => 'default.php'
		),
		array(
			'id' => 'topInfoCart',
			'type' => 'button_set',
			'title' => esc_html__('Minicart', 'highstand'),
			'desc' => esc_html__('Display minicart in right side of top navigation (Only when Woocommerce plugin has been activated)', 'highstand'),
			'options' => array( 'show' => 'Show', 'hide' => 'Hide' ),
			'std' => 'show'
		),
		array(
			'id' => 'searchNav',
			'type' => 'button_set',
			'title' => esc_html__('Search box in Menu', 'highstand'),
			'desc' => esc_html__('Display search in right side of main menu', 'highstand'),
			'options' => array( 'show' => 'Show', 'hide' => 'Hide' ),
			'std' => 'show'
		),

	)
);


$list_footers_style = array();
$list_footers_style['empty'] = 'Empty';
$posts = get_posts( array('post_type' => 'highstand_footer', 'posts_per_page' => -1, 'order' => 'ASC') );
foreach ($posts as $post) {
	$list_footers_style[$post->post_name] = $post->post_title;
}

$sections[] = array(
	'id' => 'footer-settings',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_303_temple_islam.png',
	'title' => esc_html__('Footer Settings', 'highstand'),
	'desc' => wp_kses( __('<p class="description">Select footer layouts, Add analytics embed..etc.. to footer</p>', 'highstand'), array( 'p' =>array() )),
	'parent' => 'general-settings',
	'fields' => array(
		array(
			'id' => 'footer_style',
			'type' => 'footer_styles',
			'title' => esc_html__('Select Footer Styles', 'highstand'),
			'sub_desc' => wp_kses( __('<br />Select footer for all pages, You can also go to each page to select specific.', 'highstand'), array( 'br'=>array() )),
			'options' => $list_footers_style,
			'std' => 'default'
		),
	)
);



$sections[] = array(
	'id' => 'seo',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_236_zoom_in.png',
	'title' => esc_html__('SEO', 'highstand'),
	'desc' => wp_kses( __('<p class="description">Help your site more friendly with Search Engine<br /> After active theme, we will enable all <strong>permalinks</strong> and meta descriptions.</p>', 'highstand'), array('p'=>array(),'strong'=>array(),'br'=>array())),
	'fields' => array(

		array(
			'id' => 'ogmeta',
			'type' => 'button_set',
			'title' => esc_html__('Open Graph Meta', 'highstand'),
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'std' => '1',
			'sub_desc' => esc_html__('elements that describe the object in different ways and are represented by meta tags included on the object page', 'highstand'),
		),

		array(
			'id' => 'metatag',
			'type' => 'button_set',
			'title' => esc_html__('Meta Tag', 'highstand'),
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'std' => '1',
			'sub_desc' => esc_html__('Show meta tags into head of website.', 'highstand'),
		),

		array(
			'id' => 'homeTitle',
			'type' => 'text',
			'title' => esc_html__('Homepage custom title', 'highstand'),
			'desc' => wp_kses( __('<br />Default is:  <strong>%Site Title% - %Tagline%</strong> from General Settings', 'highstand'), array('br'=>array())),
			'sub_desc' => esc_html__('The title will be displayed in homepage between &lt;title>&lt;/title> tags', 'highstand'),
		),

		array(
			'id' => 'homeTitleFm',
			'type' => 'select',
			'title' => esc_html__('Home Title Format', 'highstand'),
			'options' => array('1' => 'Blog Name | Blog description','2' => 'Blog description | Blog Name', '3' => 'Blog Name only'),
			'desc' => wp_kses( __('<br />If <b>Homepage custom title</b> not set', 'highstand'),array('br'=>array(),'b'=>array())),
			'std' => '1'
		),

		array(
			'id' => 'postTitleFm',
			'type' => 'select',
			'title' => esc_html__('Single Post Page Title Format', 'highstand'),
			'options' => array('1' => 'Post title | Blog Name','2' => 'Blog Name | Post title', '3' => 'Post title only'),
			'std' => '1'
		),

		array(
			'id' => 'archivesTitleFm',
			'type' => 'select',
			'title' => esc_html__('Archives Title Format', 'highstand'),
			'options' => array('1' => 'Category name | Blog Name','2' => 'Blog Name | Category name', '3' => 'Category name only'),
			'std' => '1'
		),

		array(
			'id' => 'titleSeparate',
			'type' => 'text',
			'title' => esc_html__('Separate Character', 'highstand'),
			'sub_desc' => esc_html__('a Character to separate BlogName and Post title', 'highstand'),
			'std' => '|'
		),

		array(
			'id' => 'homeMetaKeywords',
			'type' => 'textarea',
			'title' => esc_html__('Home Meta Keywords', 'highstand'),
			'sub_desc' => esc_html__('Add  tags for the search engines and especially Google', 'highstand'),
		),
		array(
			'id' => 'homeMetaDescription',
			'type' => 'textarea',
			'title' => esc_html__('Home Meta Description', 'highstand'),

		),
		array(
			'id' => 'authorMetaKeywords',
			'type' => 'textarea',
			'title' => esc_html__('Author Meta Description', 'highstand'),
			'std' => 'king-theme.com'
		),
		array(
			'id' => 'contactMetaKeywords',
			'type' => 'textarea',
			'title' => esc_html__('Contact Meta Description', 'highstand'),
			'std' => 'contact@king-theme.com'
		),
		array(
			'id' => 'otherMetaKeywords',
			'type' => 'textarea',
			'title' => esc_html__('Other Page Meta Keywords', 'highstand'),

		),
		array(
			'id' => 'otherMetaDescription',
			'type' => 'textarea',
			'title' => esc_html__('Other Page Meta Description', 'highstand'),

		),
	)

);


$sections[] = array(
	'id' => 'blog',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_087_log_book.png',
	'title' => esc_html__('Blog', 'highstand'),
	'desc' => esc_html__('Blog Settings', 'highstand'),
	'fields' => array(		
		array(
			'id' => 'blog',
			'type' => 'blog'
		)
	)
);


$sections[] = array(
	'id' => 'article-settings',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_061_keynote.png',
	'title' => esc_html__('Article Settings', 'highstand'),
	'desc' => wp_kses( __('<p class="description">Settings for Single post or Page</p>', 'highstand'),array('p'=>array())),
	'fields' => array(
		array(
			'id' => 'article_sidebar',
			'type' => 'select',
			'title' => esc_html__('Select Sidebar', 'highstand'),
			'options' => $sidebars,
			'std' => '',
			'sub_desc' => esc_html__( 'Select template from single article at right side', 'highstand' ),
			'desc' => '<br /><br />'.esc_html__( 'Select a dynamic sidebar what you created in theme-panel to display under page layout.', 'highstand' )
		),
		array(
			'id' => 'excerptImage',
			'type' => 'button_set',
			'title' => esc_html__('Featured Image', 'highstand'), 
			'sub_desc' => esc_html__('Display Featured image before of content', 'highstand'),
			'options' => array('1' => 'Display','2' => 'Hide'),
			'std' => '1'
		),		
		array(
			'id' => 'navArticle',
			'type' => 'button_set',
			'title' => esc_html__('Next/Prev Article Direction', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showMeta',
			'type' => 'button_set',
			'title' => esc_html__('Meta Box', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showAuthorMeta',
			'type' => 'button_set',
			'title' => esc_html__('Author Meta', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showDateMeta',
			'type' => 'button_set',
			'title' => esc_html__('Date Meta', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showCateMeta',
			'type' => 'button_set',
			'title' => esc_html__('Categories Meta', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showCommentsMeta',
			'type' => 'button_set',
			'title' => esc_html__('Comments Meta', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showTagsMeta',
			'type' => 'button_set',
			'title' => esc_html__('Tags Meta', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showShareBox',
			'type' => 'button_set',
			'title' => esc_html__('Share Box', 'highstand'), 
			'sub_desc' => esc_html__('Display box socials button below', 'highstand'),
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showShareFacebook',
			'type' => 'button_set',
			'title' => esc_html__('Facebook Button', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showShareTwitter',
			'type' => 'button_set',
			'title' => esc_html__('Tweet Button', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showShareGoogle',
			'type' => 'button_set',
			'title' => esc_html__('Google Button', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showSharePinterest',
			'type' => 'button_set',
			'title' => esc_html__('Pinterest Button', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'showShareLinkedin',
			'type' => 'button_set',
			'title' => esc_html__('LinkedIn Button', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'std' => '1'
		),
		array(
			'id' => 'archiveAboutAuthor',
			'type' => 'button_set',
			'title' => esc_html__('About Author', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'sub_desc' => esc_html__('About author box with avatar and description', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'archiveRelatedPosts',
			'type' => 'button_set',
			'title' => esc_html__('Related Posts', 'highstand'), 
			'options' => array('1' => 'Show','0' => 'Hide'),
			'sub_desc' => esc_html__('List related posts after the content.', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'archiveNumberofPosts',
			'type' => 'text',
			'title' => esc_html__('Number of posts related to show', 'highstand'), 
			'validate' => 'numeric',
			'std' => '3'
		),
		array(
			'id' => 'archiveRelatedQuery',
			'type' => 'button_set',
			'title' => esc_html__('Related Query Type', 'highstand'), 
			'options' => array('category' => 'Category','tag' => 'Tag','author'=>'Author'),
			'std' => 'category'
		)
	)

);


//  coming soon

$sections[] = array(
	'id' => 'coming-soon',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_022_fire.png',
	'title' => esc_html__('Coming soon', 'highstand'),
	'desc' => esc_html__('Set your socials and will be displayed icons at header and footer, Leave blank to hide icons from front-end', 'highstand'),
	'fields' => array(
		array(
			'id' => 'cs_logo',
			'type' => 'upload',
			'title' => esc_html__('Upload Logo', 'highstand'), 
			'sub_desc' => esc_html__('This will be display as logo at header of every page', 'highstand'),
			'desc' => esc_html__('Upload new or from media library to use as your logo. We recommend that you use images without borders and throughout.', 'highstand'),
			'std' => THEME_URI.'/assets/images/logo.png'
		),
		array(
			'id' => 'cs_text_after_logo',
			'type' => 'text',
			'title' => esc_html__('Text after logo', 'highstand'),
			'sub_desc' => esc_html__('Will show "We\'re Launching Soon" if you leave empty', 'highstand'),
			'std' => 'We\'re Launching Soon'
		),
		array(
			'id' => 'cs_timedown',
			'type' => 'text',
			'title' => esc_html__('Date time for countdown', 'highstand'),
			'sub_desc' => esc_html__('Format  "F d, Y H:i:s" for example "October 18, 2015 08:30:30"', 'highstand'),
			'std' => 'October 18, 2025 08:30:30'
		),
		array(
			'id' => 'cs_description',
			'type' => 'textarea',
			'title' => esc_html__('Description', 'highstand'), 
			'std' => 'Our website is under construction. We\'ll be here soon with our new awesome site. Get best experience with this one.'
		),
		array(
			'id' => 'cs_slider1',
			'type' => 'upload',
			'title' => esc_html__('Background Slider image 1', 'highstand'), 
			'sub_desc' => esc_html__('This will be display as slide at coming soon slider ', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'cs_slider2',
			'type' => 'upload',
			'title' => esc_html__('Background Slider image 2', 'highstand'), 
			'sub_desc' => esc_html__('This will be display as slide at coming soon slider ', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'cs_slider3',
			'type' => 'upload',
			'title' => esc_html__('Background Slider image 3', 'highstand'), 
			'sub_desc' => esc_html__('This will be display as slide at coming soon slider ', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'cs_slider4',
			'type' => 'upload',
			'title' => esc_html__('Background Slider image 4', 'highstand'), 
			'sub_desc' => esc_html__('This will be display as slide at coming soon slider ', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'cs_slider5',
			'type' => 'upload',
			'title' => esc_html__('Background Slider image 5', 'highstand'), 
			'sub_desc' => esc_html__('This will be display as slide at coming soon slider ', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		
	)

);


$listBreadcrumbs = array();
if ( $handle = opendir( THEME_PATH.DS.'templates'.DS.'breadcrumb' ) ){
	while ( false !== ( $entry = readdir($handle) ) ) {
		if( $entry != '.' && $entry != '..' && strpos($entry, '.php') !== false  ){
			$title  = ucwords( str_replace( '-', ' ', basename( $entry, '.php' ) ) );
			$listBreadcrumbs[ 'templates'.DS.'breadcrumb'.DS.$entry ] = $title;
		}
	}
}
//  Post Types
$sections[] = array(
	'id' => 'custom-post-types',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_145_folder_plus.png',
	'title' => esc_html__('Custom Post Types', 'highstand'),
	'desc' => esc_html__('Setting title, slugs for post types', 'highstand'),
	'fields' => array(
		array(
			'id' => 'our_works_status',
			'type' => 'button_set',
			'title' => esc_html__('Our Work', 'highstand'),
			'sub_desc' => esc_html__('Select to active post type Our Work', 'highstand'),
			'options' => array('enable' => 'Enable','disable' => 'Disable'),
			'std' => '1'
		),
		array(
			'id' => 'our_works_visit_link',
			'type' => 'button_set',
			'title' => esc_html__('Our Work visit link', 'highstand'),
			'sub_desc' => esc_html__('Show or hide visit link on Our Work single page', 'highstand'),
			'options' => array('enable' => 'Enable','disable' => 'Disable'),
			'std' => '1'
		),
		array(
			'id' => 'our_team_status',
			'type' => 'button_set',
			'title' => esc_html__('Our Team', 'highstand'),
			'sub_desc' => esc_html__('Select to active post type Team', 'highstand'),
			'options' => array('enable' => 'Enable','disable' => 'Disable'),
			'std' => '1'
		),
		array(
			'id' => 'testimonials_status',
			'type' => 'button_set',
			'title' => esc_html__('Testimonials', 'highstand'),
			'sub_desc' => esc_html__('Select to active post type Testimonials', 'highstand'),
			'options' => array('enable' => 'Enable','disable' => 'Disable'),
			'std' => '1'
		),
		array(
			'id' => 'faqs_status',
			'type' => 'button_set',
			'title' => esc_html__('FAQs', 'highstand'),
			'sub_desc' => esc_html__('Select to active post type FAQs', 'highstand'),
			'options' => array('enable' => 'Enable','disable' => 'Disable'),
			'std' => '1'
		),
		array(
			'id' => 'pricing_tables_status',
			'type' => 'button_set',
			'title' => esc_html__('Pricing Tables', 'highstand'),
			'sub_desc' => esc_html__('Select to active post type Pricing', 'highstand'),
			'options' => array('enable' => 'Enable','disable' => 'Disable'),
			'std' => '1'
		),

		array(
			'id' => 'our_works_title',
			'type' => 'text',
			'title' => esc_html__('Our Works Title', 'highstand'),
			'sub_desc' => esc_html__('This will replace \'Our Works\' menu, breadcrumb text', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'our_works_slug',
			'type' => 'text',
			'title' => esc_html__('Our Works Slug', 'highstand'),
			'sub_desc' => esc_html__('This will replace /our-works/ on url', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'our_works_field',
			'type' => 'multi_text',
			'title' => esc_html__('Our Works Custom fields', 'highstand'),
			'sub_desc' => esc_html__('Add more extra field in our work post', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id'       => 'our_works_breadcrumb',
			'type'     => 'select',
			'title'    => esc_html__('Our Works Breadcrumb', 'highstand'),
			'sub_desc' => esc_html__('Select breadcrumb for our works.', 'highstand'),
			'options'  => $listBreadcrumbs,
			'std'      => 'templates/breadcrumb/empty.php'
		),
		array(
			'id'    => 'our_works_breadcrumb_bg',
			'type'  => 'upload',
			'title' => esc_html__('Our Works Breadcrumb Background Image', 'highstand'),
			'desc'  => esc_html__('Upload your background image for Breadcrumb', 'highstand'),
			'std'   => ''
		),
		array(
			'id' => 'our_team_title',
			'type' => 'text',
			'title' => esc_html__('Our Team Title', 'highstand'),
			'sub_desc' => esc_html__('This will replace \'Our Team\' menu, breadcrumb text', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'our_team_slug',
			'type' => 'text',
			'title' => esc_html__('Our Team Slug', 'highstand'),
			'sub_desc' => esc_html__('This will replace /our-team/ on url', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'faq_title',
			'type' => 'text',
			'title' => esc_html__('FAQ Title', 'highstand'),
			'sub_desc' => esc_html__('This will replace \'FAQ\' menu, breadcrumb text', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),
		array(
			'id' => 'faq_slug',
			'type' => 'text',
			'title' => esc_html__('FAQ Slug', 'highstand'), 
			'sub_desc' => esc_html__('This will replace /faq/ on url', 'highstand'),
			'desc' => esc_html__('', 'highstand'),
			'std' => ''
		),	
	)

);


$sections[] = array('divide'=>true);	


$sections[] = array(
	'id' => 'dynamic-sidebars',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_037_credit.png',
	'title' => esc_html__('Dynamic Sidebars', 'highstand'),
	'desc' => esc_html__('You can create unlimited sidebars and use it in any page you want.','highstand'),
	'parent' => 'general-settings',
	'fields' => array(
		array(
			'id' => 'sidebars',
			'type' => 'multi_text',
			'title' => esc_html__('List of Sidebars Created', 'highstand'),
			'sub_desc' => esc_html__('Add name of sidebar', 'highstand'),
			'std' => array('Nav Sidebar')
		),
	)

);
 
$sections[] = array(
	'id' => 'styling',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_273_drink.png',
	'title' => esc_html__('Styling', 'highstand'),
	'desc' => wp_kses( __('<p class="description">Setting up global style and background</p>', 'highstand'), array('p'=>array())),
	'fields' => array(
		array(
			'id' => 'colorStyle',
			'type' => 'colorStyle',
			'title' => esc_html__('Color Style', 'highstand'), 
			'sub_desc' => esc_html__('Predefined Color Skins', 'highstand'),
			'desc' => esc_html__( 'Primary css file has been located at: /wp_content/themes/__name__/assets/css/colors/color-primary.css', 'highstand' ),
			'std'	=> ''
		),
		array(
			'type' => 'color',
			'id' => 'backgroundColor',
			'title' =>  esc_html__('Background Color', 'highstand'),
			'desc' =>  esc_html__(' Background body for layout wide and background box for layout boxed', 'highstand'), 
			'css' => '<?php if($value!="")echo "body{background-color: ".$value.";}"; ?>',
			'std' => '#ffffff'
		),	
		array(
			'type' => 'upload',
			'id' => 'backgroundCustom',
			'title' =>  esc_html__('Custom Background Image', 'highstand'),
			'sub_desc' => esc_html__('Only be used for Boxed Type.', 'highstand'),
			'desc' =>  esc_html__(' Upload your custom background image, or you can also use the Pattern available below.', 'highstand'),
			'std' => '',
			'css' => '<?php if($value!="")echo "body{background-image: url(".$value.") !important;}"; ?>'
		
		),
		array(
			'id' => 'useBackgroundPattern',
			'type' => 'checkbox_hide_below',
			'title' => esc_html__('Use Pattern for background', 'highstand'), 
			'sub_desc' => esc_html__('Tick on checkbox to show list of Patterns', 'highstand'),
			'desc' => esc_html__('If you do not have background image, you can also use our Pattern.', 'highstand'),
			'std' => 0,
		),
		array(
			'id' => 'backgroundImage',
			'type' => 'radio_img',
			'title' => esc_html__('Select background', 'highstand'), 
			'sub_desc' => esc_html__('Only be used for Boxed Type.', 'highstand'),
			'options' => $patterns,
			'std' => '',
			'css' => '<?php if($value!="")echo "body{background-image: url('.THEME_URI.'/assets/images/elements/".$value.");}"; ?>'
		),		
		array(
			'id' => 'linksDecoration',
			'type' => 'select',
			'title' => esc_html__('Links Decoration', 'highstand'), 
			'sub_desc' => esc_html__('Set decoration for all links.', 'highstand'),
			'options' => array('default'=>'Default','none'=>'None','underline'=>'Underline','overline'=>'Overline','line-through'=>'Line through'),
			'std' => 'default',
			'css' => '<?php if($value!="")echo "a{text-decoration: ".$value.";}"; ?>'
		),		
		array(
			'id' => 'linksHoverDecoration',
			'type' => 'select',
			'title' => esc_html__('Links Hover Decoration', 'highstand'), 
			'sub_desc' => esc_html__('Set decoration for all links when hover.', 'highstand'),
			'options' => array('default'=>'Default','none'=>'None','underline'=>'Underline','overline'=>'Overline','line-through'=>'Line through'),
			'std' => 'default',
			'css' => '<?php if($value!="")echo "a:hover{text-decoration: ".$value.";}"; ?>'
		),		
		array(
			'id' => 'cssGlobal',
			'type' => 'textarea',
			'title' => esc_html__('Global CSS', 'highstand'), 
			'sub_desc' => esc_html__('CSS for all screen size, only CSS without &lt;style&gt; tag', 'highstand'),
			'css' => '<?php if($value!="")print( $value ); ?>'
		),
		array(
			'id' => 'cssTablets',
			'type' => 'textarea',
			'title' => esc_html__('Tablets CSS', 'highstand'), 
			'sub_desc' => esc_html__('Width from 768px to 985px, only CSS without &lt;style&gt; tag', 'highstand'),
			'css' => '<?php if($value!="")echo "@media (min-width: 768px) and (max-width: 985px){".$value."}"; ?>'
		),
		array(
			'id' => 'cssPhones',
			'type' => 'textarea',
			'title' => esc_html__('Wide Phones CSS', 'highstand'), 
			'sub_desc' => esc_html__('Width from 480px to 767px, only CSS without &lt;style&gt; tag', 'highstand'),
			'css' => '<?php if($value!="")echo "@media (min-width: 480px) and (max-width: 767px){".$value."}"; ?>'
		),
		
	)

);

$sections[] = array(
	'id' => 'typography',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_107_text_resize.png',
	'title' => esc_html__('Typography', 'highstand'),
	'desc' => wp_kses( __('<p class="description">Set the color, font family, font size, font weight and font style.</p>', 'highstand'),array('p'=>array())),
	'fields' => array(
		array(
			'id' => 'generalTypography',
			'type' => 'typography',
			'title' => esc_html__('General Typography', 'highstand'), 
			'std' => array(),
			'css' => 'body,.dropdown-menu,body p{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),				
		array(
			'id' => 'generalHoverTypography',
			'type' => 'typography',
			'title' => esc_html__('General Link Hover', 'highstand'), 
			'css' => 'body * a:hover, body * a:active, body * a:focus{<?php if($value[color]!="")echo "color:".$value[color]." !important;"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),		
		array(
			'id' => 'mainMenuTypography',
			'type' => 'typography',
			'title' => esc_html__('Main Menu', 'highstand'),
			'css' => 'body .navbar-default .navbar-nav>li>a{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\' !important;"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight]." !important;"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),		
		array(
			'id' => 'mainMenuHoverTypography',
			'type' => 'typography',
			'title' => esc_html__('Main Menu Hover', 'highstand'), 
			'css' => 'body .navbar-default .navbar-nav>li>a:hover,.navbar-default .navbar-nav>li.current-menu-item>a{<?php if($value[color]!="")echo "color:".$value[color]." !important;"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\' !important;"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),			
		array(
			'id' => 'mainMenuSubTypography',
			'type' => 'typography',
			'title' => esc_html__('Sub Main Menu', 'highstand'), 
			'css' => '.dropdown-menu>li>a{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),			
		array(
			'id' => 'mainMenuSubHoverTypography',
			'type' => 'typography',
			'title' => esc_html__('Sub Main Menu Hover', 'highstand'), 
			'css' => '.dropdown-menu>li>a:hover{<?php if($value[color]!="")echo "color:".$value[color]." !important;"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),	
		array(
			'id' => 'postMetaTypography',
			'type' => 'typography',
			'title' => esc_html__('Post Meta', 'highstand'), 
			'std' => array(),
			'css' => '.post_meta_links{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'postMatalinkTypography',
			'type' => 'typography',
			'title' => esc_html__('Post Meta Link', 'highstand'), 
			'css' => '.post_meta_links li a{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'postTitleTypography',
			'type' => 'typography',
			'title' => esc_html__('Post Title', 'highstand'), 
			'css' => '.blog_post h3.entry-title a{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'postEntryTypography',
			'type' => 'typography',
			'title' => esc_html__('Post Entry', 'highstand'), 
			'css' => 'article .blog_postcontent,article .blog_postcontent p{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'widgetTitlesTypography',
			'type' => 'typography',
			'title' => esc_html__('Widget Titles', 'highstand'),
			'css' => 'h3.widget-title,#reply-title,#comments-title{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'footerWidgetTitlesTypography',
			'type' => 'typography',
			'title' => esc_html__('Footer Widgets Titles', 'highstand'), 
			'std'	=> array(),
			'css' => '.footer h3.widget-title{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'h1Typography',
			'type' => 'typography',
			'title' => esc_html__('H1 Typography', 'highstand'), 
			'std' => array(),
			'css' => '.entry-content h1{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'h2Typography',
			'type' => 'typography',
			'title' => esc_html__('H2 Typography', 'highstand'), 
			'std' => array(),
			'css' => '.entry-content h2{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'h3Typography',
			'type' => 'typography',
			'title' => esc_html__('H3 Typography', 'highstand'), 
			'std' => array(),
			'css' => '.entry-content h3{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'h4Typography',
			'type' => 'typography',
			'title' => esc_html__('H4 Typography', 'highstand'), 
			'std' => array(),
			'css' => '.entry-content h4{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'h5Typography',
			'type' => 'typography',
			'title' => esc_html__('H5 Typography', 'highstand'), 
			'std' => array(),
			'css' => '.entry-content h5{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		),
		array(
			'id' => 'h6Typography',
			'type' => 'typography',
			'title' => esc_html__('H6 Typography', 'highstand'), 
			'std' => array(),
			'css' => '.entry-content h6{<?php if($value[color]!="")echo "color:".$value[color].";"; ?><?php if($value[font]!="")echo "font-family:\'".$value[font]."\';"; ?><?php if($value[size]!="")echo "font-size:".$value[size]."px;"; ?><?php if($value[weight]!="")echo "font-weight:".$value[weight].";"; ?><?php if($value[style]!="")echo "font-style:".$value[style].";"; ?>}'
		)
		
	)

);


$sections[] = array(
	'id' => 'social-accounts',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_050_link.png',
	'title' => esc_html__('Social Accounts', 'highstand'),
	'desc' => esc_html__('Set your socials and will be displayed icons at header and footer, Leave blank to hide icons from front-end', 'highstand'),
	'fields' => array(
		array(
			'id' => 'feed',
			'type' => 'text',
			'title' => esc_html__('Your Feed RSS', 'highstand'),
			'sub_desc' => esc_html__('Enter full link e.g: http://yoursite.com/feed', 'highstand'),
			'std' => 'feed'
		),
		array(
			'id' => 'facebook',
			'type' => 'text',
			'title' => esc_html__('Your Facebook Account', 'highstand'),
			'sub_desc' => esc_html__('Social icon will not display if you leave empty', 'highstand'),
			'std' => 'highstand'
		),
		array(
			'id' => 'twitter',
			'type' => 'text',
			'title' => esc_html__('Your Twitter Account', 'highstand'),
			'sub_desc' => esc_html__('Social icon will not display if you leave empty', 'highstand'),
			'std' => 'highstand'
		),
		array(
			'id' => 'google',
			'type' => 'text',
			'title' => esc_html__('Your Google+ Account', 'highstand'),
			'sub_desc' => esc_html__('Social icon will not display if you leave empty', 'highstand'),
			'std' => 'highstand'
		),
		array(
			'id' => 'linkedin',
			'type' => 'text',
			'title' => esc_html__('Your LinkedIn Account', 'highstand'),
			'sub_desc' => esc_html__('Social icon will not display if you leave empty', 'highstand'),
			'std' => 'highstand'
		),
		array(
			'id' => 'flickr',
			'type' => 'text',
			'title' => esc_html__('Your Flickr Account', 'highstand'),
			'sub_desc' => esc_html__('Social icon will display if you leave empty', 'highstand'),
			'std' => 'highstand'
		),
		array(
			'id' => 'instagram',
			'type' => 'text',
			'title' => esc_html__('Your Instagram Account', 'highstand'),
			'sub_desc' => esc_html__('Social icon will not display if you leave empty', 'highstand'),
			'std' => 'highstand'
		),		
		array(
			'id' => 'pinterest',
			'type' => 'text',
			'title' => esc_html__('Your Pinterest Account', 'highstand'),
			'sub_desc' => esc_html__('Social icon will not display if you leave empty', 'highstand'),
			'std' => 'highstand'
		),
		array(
			'id' => 'youtube',
			'type' => 'text',
			'title' => esc_html__('Your Youtube Chanel', 'highstand'),
			'sub_desc' => esc_html__('Social icon will not display if you leave empty', 'highstand'),
			'std' => 'highstand'
		)
		
	)
);


$sections[] = array(
	'id' => 'twitter-api-key',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_322_twitter.png',
	'title' => esc_html__('Twitter API Key', 'highstand'),
	'desc' => esc_html__('Enter your twitter API key for twitter widget feed updates', 'highstand'),
	'parent' => 'social-accounts',
	'fields' => array(
		array(
			'id' => 'twitter_consumer_key',
			'type' => 'text',
			'title' => esc_html__('Consumer Key (API Key)', 'highstand'),
			'sub_desc' => esc_html__('Get consumer key in https://apps.twitter.com', 'highstand'),
			'std' => 'tHWsp0yQQioooQZJfXJdGP3d4'
		),
		array(
			'id' => 'twitter_consumer_secret',
			'type' => 'text',
			'title' => esc_html__('Consumer Secret (API Secret)', 'highstand'),
			'sub_desc' => esc_html__('Get consumer Secret in https://apps.twitter.com', 'highstand'),
			'std' => 'bl1kN9xH6nf167d0SJXnv9V5ZXuGXSShr5CeimLXaIGcUEQnsp'
		),
		array(
			'id' => 'twitter_oauth_access_token',
			'type' => 'text',
			'title' => esc_html__('Access Token', 'highstand'),
			'sub_desc' => esc_html__('Get Access Token in https://apps.twitter.com', 'highstand'),
			'std' => ' 120290116-vmLx4sPp5O3hjhRxjpl28i0APJkCpg04YVsoZyb7'
		),
		array(
			'id' => 'twitter_oauth_access_token_secret',
			'type' => 'text',
			'title' => esc_html__('Access Token Secret', 'highstand'),
			'sub_desc' => esc_html__('Get Access Token Secret in https://apps.twitter.com', 'highstand'),
			'std' => 'B9mAhgZhQG0cspt1doF2cxDky40OEatjftRI5NCmQh1pE'
		),
	)
);


$sections[] = array(
	'id'     => 'newsletter-settings',
	'icon'   => highstand_options_URL.'img/glyphicons/glyphicons_037_credit.png',
	'title'  => esc_html__('Newsletter', 'highstand'),
	'desc'   => esc_html__('Select your newsletter method on website.','highstand'),
	'fields' => array(
		array(
			'id'      => 'newsletter_method',
			'type'    => 'button_set',
			'title'   => esc_html__('Method', 'highstand'),
			'options' => array('mc' => 'Mailchimp', 'self' => 'Theme Functions'),
			'std'     => 'mc'
		),

		array(
			'id'       => 'mc_api',
			'type'     => 'text',
			'title'    => esc_html__('Mailchimp API Key', 'highstand'),
			'sub_desc' => esc_html__('Your API key which you can grab from http://admin.mailchimp.com/account/api/', 'highstand'),
			'std'      => ''
		),

		array(
			'id'       => 'mc_list_id',
			'type'     => 'text',
			'title'    => esc_html__('Mailchimp List ID', 'highstand'),
			'sub_desc' => esc_html__('The ID of list which you want to customers signup. You can grab your List Id by going to http://admin.mailchimp.com/lists/ click the "settings" link for the list - the Unique Id is at the bottom of that page. ', 'highstand'),
			'std'      => ''
		),

	)

);


$sections[] = array('divide'=>true);	

//  Woo Admin
$sections[] = array(
	'id' => 'wooecommerce',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_202_shopping_cart.png',
	'title' => esc_html__('WooEcommerce', 'highstand'),
	'desc' => esc_html__('Setting for your Shop!', 'highstand'),
	'fields' => array(
		array(
			'id' => 'product_number',
			'type' => 'text',
			'title' => esc_html__('Number of Products per Page', 'highstand'),
			'desc' => esc_html__('Insert the number of products to display per page.', 'highstand'),
			'std' => '12'
		),
		array(
			'id' => 'woo_grids',
			'type' => 'select',
			'title' => esc_html__('Items per row', 'highstand'), 
			'desc' => esc_html__('Set number products per row (for Grids layout)', 'highstand'),
			'options' => array('4'=>'4 (Shop layout without sidebar)','3'=>'3 (Shop layout with sidebar)'),
			'std' => '3'
		),			
		array(
			'id' => 'woo_layout',
			'type' => 'select',
			'title' => esc_html__('Shop Layout', 'highstand'), 
			'desc' => esc_html__('Set layout for your shop page.', 'highstand'),
			'options' => array('full'=>'No sidebar - Full width', 'left'=>'With Sidebar on Left', 'right'=>'With Sidebar on Right'),
			'std' => 'left'
		),		
		array(
			'id' => 'woo_product_layout',
			'type' => 'select',
			'title' => esc_html__('Product Layout', 'highstand'), 
			'desc' => esc_html__('Set layout for your product detail page.', 'highstand'),'options' => array('full'=>'No sidebar - Full width', 'left'=>'With Sidebar on Left', 'right'=>'With Sidebar on Right'),
			'std' => 'right'
		),	
		array(
			'id' => 'woo_product_display',
			'type' => 'select',
			'title' => esc_html__('Product Display', 'highstand'), 
			'desc' => esc_html__('Display products by grid or list.', 'highstand'),
			'options' => array('grid'=>'Grid','list'=>'List'),
			'std' => 'grid'
		),	
		array(
			'id' => 'woo_filter',
			'type' => 'button_set',
			'title' => esc_html__('Filter Products', 'highstand'), 
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'desc' => esc_html__('Enable filter products by price, categories, attributes..', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'woo_cart',
			'type' => 'button_set',
			'title' => esc_html__('Show Woocommerce Cart Icon in Top Menu', 'highstand'), 
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'desc' => esc_html__('Enable Woocommerce Cart show on top menu', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'woo_social',
			'type' => 'button_set',
			'title' => esc_html__('Show Woocommerce Social Icons', 'highstand'), 
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'desc' => esc_html__('Show Woocommerce Social Icons in Single Product Page', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'woo_message_1',
			'type' => 'textarea',
			'title' => esc_html__('Account Message 1', 'highstand'), 
			'desc' => esc_html__('Insert your message to appear in the first message box on the acount page.', 'highstand'),
			'std' => 'Call us in 000-000-000 If you need our support. Happy to help you !'
		),
		array(
			'id' => 'woo_message_2',
			'type' => 'textarea',
			'title' => esc_html__('Account Message 2', 'highstand'), 
			'desc' => esc_html__('Insert your message to appear in the second message box on the acount page.', 'highstand'),
			'std' => 'Send us a email in devn@support.com'
		),
		
	)

);

// Woo Magnifier
$sections[] = array(
	'id' => 'woo-magnifier',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_027_search.png',
	'title' => esc_html__('Woo Magnifier', 'highstand'),
	'desc' => esc_html__('Setting Magnifier effect for images product in single product page!', 'highstand'),
	'parent' => 'wooecommerce',
	'fields' => array(
		array(
			'id' => 'mg_active',
			'type' => 'button_set',
			'title' => esc_html__('Magnifier Active', 'highstand'), 
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'desc' => esc_html__('Enable magnifier for product images/ Disable magnifier to use default lightbox for product images', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'mg_zoom_width',
			'type' => 'text',
			'title' => esc_html__('Zoom Width', 'highstand'),
			'desc' => esc_html__('Set width of magnifier box ( default: auto )', 'highstand'),
			'std' => 'auto'
		),
		array(
			'id' => 'mg_zoom_height',
			'type' => 'text',
			'title' => esc_html__('Zoom Height', 'highstand'),
			'desc' => esc_html__('Set height of magnifier box ( default: auto )', 'highstand'),
			'std' => 'auto'
		),
		array(
			'id' => 'mg_zoom_position',
			'type' => 'select',
			'title' => esc_html__('Zoom Position', 'highstand'), 
			'desc' => esc_html__('Set magnifier position ( default: Right )', 'highstand'),
			'options' => array('right'=>'Right','inside'=>'Inside'),
			'std' => 'right'
		),	
		array(
			'id' => 'mg_zoom_position_mobile',
			'type' => 'select',
			'title' => esc_html__('Zoom Position on Mobile', 'highstand'), 
			'desc' => esc_html__('Set magnifier position on mobile devices (iPhone, Android, etc.)', 'highstand'),
			'options' => array('default'=>'Default','inside'=>'Inside','disable'=>'Disable'),
			'std' => 'default'
		),	
		array(
			'id' => 'mg_loading_label',
			'type' => 'text',
			'title' => esc_html__('Loading Label', 'highstand'),
			'desc' => esc_html__('Set text for magnifier loading...', 'highstand'),
			'std' => 'Loading...'
		),
		array(
			'id' => 'mg_lens_opacity',
			'type' => 'text',
			'title' => esc_html__('Lens Opacity', 'highstand'),
			'desc' => esc_html__('Set opacity for Lens (0 - 1)', 'highstand'),
			'std' => '0.5'
		),
		array(
			'id' => 'mg_blur',
			'type' => 'button_set',
			'title' => esc_html__('Blur Effect', 'highstand'), 
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'desc' => esc_html__('Blur effect when Lens hover on product images', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'mg_thumbnail_slider',
			'type' => 'button_set',
			'title' => esc_html__('Active Slider', 'highstand'), 
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'desc' => esc_html__('Enable slider for product thumbnail images', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'mg_slider_item',
			'type' => 'text',
			'title' => esc_html__('Items', 'highstand'),
			'desc' => esc_html__('Number items of Slide', 'highstand'),
			'default' => 3
		),
		array(
			'id' => 'mg_thumbnail_circular',
			'type' => 'button_set',
			'title' => esc_html__('Circular Thumbnail', 'highstand'), 
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'desc' => esc_html__('Continue slide as a circle', 'highstand'),
			'std' => '1'
		),
		array(
			'id' => 'mg_thumbnail_infinite',
			'type' => 'button_set',
			'title' => esc_html__('Infinite Thumbnail', 'highstand'), 
			'options' => array('1' => 'Enable','0' => 'Disable'),
			'desc' => esc_html__('Back to first image when end of list', 'highstand'),
			'std' => '1'
		),
		
		
		
		
	)

);


$sections[] = array('divide'=>true);	

$sections[] = array(
	'id' => 'license',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_044_keys.png',
	'title' => esc_html__('Product License Key', 'highstand'),
	'desc' => esc_html__('Submit Theme License Key to get auto-update HighStand Theme and Plugins', 'highstand'),
	'fields' => array(
		array(
			'id' => 'license',
			'type' => 'license'
		),
	)
);

$sections[] = array(
	'id' => 'import-export',
	'icon' => highstand_options_URL.'img/glyphicons/glyphicons_082_roundabout.png',
	'title' => esc_html__('Import / Export', 'highstand'),
	'desc' => esc_html__('Import or Export theme options and widgets data', 'highstand'),
	'fields' => array(
		array(
			'id' => 'import_data',
			'type' => 'import_data',
			'title' => esc_html__('Import From File', 'highstand'), 
			'warning_text' => esc_html__( 'WARNING! This will overwrite all existing option values, please proceed with caution!', 'highstand' ),
			'desc' => esc_html__('', 'highstand')
		),
		array(
			'id' => 'export_data',
			'type' => 'export_data',
			'title' => esc_html__('Export To File', 'highstand'), 
			'desc' => esc_html__('Here you can copy/download your current option settings. Keep this safe as you can use it as a backup should anything go wrong, or you can use it to restore your settings on this site (or any other site).', 'highstand')
		),
	)
);

			
	$tabs = array();
			
	if (function_exists('wp_get_theme')){
		$theme_data = wp_get_theme();
		$theme_uri = $theme_data->get('ThemeURI');
		$description = $theme_data->get('Description');
		$author = $theme_data->get('Author');
		$version = $theme_data->get('Version');
		$tags = $theme_data->get('Tags');
	}else{
		$theme_data = wp_get_theme(trailingslashit(get_stylesheet_directory()).'style.css');
		$theme_uri = $theme_data['URI'];
		$description = $theme_data['Description'];
		$author = $theme_data['Author'];
		$version = $theme_data['Version'];
		$tags = $theme_data['Tags'];
	}	
	
	
	
	if(file_exists(trailingslashit(get_stylesheet_directory()).'README.html')){
		$tabs['theme_docs'] = array(
						'icon' => highstand_options_URL.'img/glyphicons/glyphicons_071_book.png',
						'title' => esc_html__('Documentation', 'highstand'),
						'content' => nl2br(devnExt::file( 'get', trailingslashit(get_stylesheet_directory()).'README.html'))
						);
	}//if

	global $highstand, $highstand_options;
	
	$highstand_options = new highstand_options($sections, $args, $tabs);
	$highstand->cfg = get_option( $args['opt_name'] );

}//function
add_action('init', 'setup_framework_options', 0);

/*
 * 
 * Custom function for the callback referenced above
 *
 */
function video_get_start($field, $value){
	
	switch( $field['id'] ){
		case 'inspector':
		  echo '<ifr'.'ame width="560" height="315" src="http://www.youtube.com/embed/rO8HYqUUbL8?vq=hd720&rel=0&start=76" frameborder="0" allowfullscreen></ifr'.'ame>';
		break;
		case 'grid':
			echo '<ifr'.'ame width="560" height="315" src="http://www.youtube.com/embed/rO8HYqUUbL8?vq=hd720&rel=0" frameborder="0" allowfullscreen></ifr'.'ame>';
		break;
	}

}//function

/*
 * 
 * Custom function for the callback validation referenced above
 *
 */
function validate_callback_function($field, $value, $existing_value){
	
	$error = false;
	$value =  'just testing';	
	$return['value'] = $value;
	if($error == true){
		$return['error'] = $field;
	}
	return $return;
	
}//function

function highstand_get_template_content( $path ){
	
}
?>