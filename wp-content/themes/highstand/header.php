<?php
/**
 *
 * (c) king-theme.com
 *
 * The Header of theme.
 *
 */


$post = highstand::globe('post');
$highstand = highstand::globe();
$post_data = highstand_post_meta_options();

if ( ! isset( $content_width ) ) $content_width = 1170;

?><!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
<?php
	wp_head();
?>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 ga('create', 'UA-24347826-1', 'auto');
 ga('require', 'GTM-WF7F8WM');
 ga('send', 'pageview');
</script>
<script type="text/javascript" defer="defer" src="https://extend.vimeocdn.com/ga/56901836.js"></script>
<script type="text/javascript" language="javascript"> 
      var sf14gv = 30674; 
      (function() { 
      var sf14g = document.createElement('script'); sf14g.type = 'text/javascript'; sf14g.async = true; 
      sf14g.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 't.sf14g.com/sf14g.js'; 
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sf14g, s); 
      })(); 
</script>
</head>
<body <?php body_class('bg-cover'); ?> <?php body_style(); ?>>
	<div id="main" class="layout-<?php
		if(isset($highstand->cfg['layout'])){
			$mclass =  $highstand->cfg['layout'];
		}else{
			$mclass = '';
		}

		$boxed_style = '';

		if(isset($post_data['page_layout']) && $post_data['page_layout'] == 'boxed'){
			$mclass = $post_data['page_layout'];

			// if(!empty($post_data['page_boxed_width'])){
			// 	$boxed_style .= 'width:'. intval($post_data['page_boxed_width']).'px;';
			// }
			if(!empty($post_data['boxed_margin_top'])){
				$boxed_style .= 'margin-top: '. intval($post_data['boxed_margin_top']) .'px !important;';
			}
		}

		if(isset($post_data['page_layout']) && $post_data['page_layout'] == 'onepage'){
			$mclass = $post_data['page_layout'];
		}

		if( !empty( $post->post_name ) ){
			if( $post->post_type == 'page' ) $mclass .= ' page-'.$post->post_name;
		}
		echo esc_attr( $mclass );
		echo ' '.esc_attr( $highstand->main_class ); ?> site_wrapper" style="<?php echo esc_attr( $boxed_style ); ?>">

	<?php
		
		highstand::path( 'header' );

	?>