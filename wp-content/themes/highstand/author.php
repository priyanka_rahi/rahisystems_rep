<?php
/**
 * (c) king-theme.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$highstand = highstand::globe();

get_header();

?>

	<?php highstand::path( 'blog_breadcrumb' ); ?>

	<div id="primary" class="site-content container-content content content_fullwidth less2">
		<div id="content" class="row row-content container">
			<div class="col-md-9 content_left">


				<?php if ( have_posts() ) : ?>

					<?php

						the_post();
					?>

					<header>
						<h1 class="page-title author"><?php printf( esc_html__( 'Author Archives: %s', 'highstand' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' ); ?></h1>
					</header>

					<?php

						rewind_posts();
					?>

					<?php
					// If a user has filled out their description, show a bio on their entries.
					if ( get_the_author_meta( 'description' ) ) : ?>
						<div class="about_author">

							<?php echo get_avatar( get_the_author_meta( 'user_email' ) ); ?>

							<h3><?php printf( esc_html__( 'About %s', 'highstand' ), get_the_author() ); ?></h3>

							<?php the_author_meta( 'description' ); ?>
						</div><!-- #author-info -->
						<div class="clearfix divider_dashed3"></div>
					<?php endif; ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

							get_template_part( 'content' );

						?>

					<?php endwhile; ?>

					<?php $highstand->pagination(); ?>

				<?php else : ?>

					<article id="post-0" class="post no-results not-found">
						<header class="entry-header">
							<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'highstand' ); ?></h1>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<p><?php esc_html_e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'highstand' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-0 -->

				<?php endif; ?>

			</div>
			<div class="col-md-3 right_sidebar">
				<?php get_sidebar( ); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>