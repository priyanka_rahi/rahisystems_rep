CREATE TABLE `rh_wfThrottleLog` (  `IP` binary(16) NOT NULL,  `startTime` int(10) unsigned NOT NULL,  `endTime` int(10) unsigned NOT NULL,  `timesThrottled` int(10) unsigned NOT NULL,  `lastReason` varchar(255) NOT NULL,  PRIMARY KEY (`IP`),  KEY `k2` (`endTime`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `rh_wfThrottleLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `rh_wfThrottleLog` ENABLE KEYS */;
