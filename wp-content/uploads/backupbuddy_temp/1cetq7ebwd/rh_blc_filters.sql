CREATE TABLE `rh_blc_filters` (  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,  `params` text COLLATE utf8mb4_unicode_520_ci NOT NULL,  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `rh_blc_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `rh_blc_filters` ENABLE KEYS */;
