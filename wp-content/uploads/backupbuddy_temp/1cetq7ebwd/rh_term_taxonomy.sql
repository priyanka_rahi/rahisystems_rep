CREATE TABLE `rh_term_taxonomy` (  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',  `count` bigint(20) NOT NULL DEFAULT '0',  PRIMARY KEY (`term_taxonomy_id`),  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),  KEY `taxonomy` (`taxonomy`)) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `rh_term_taxonomy` DISABLE KEYS */;
INSERT INTO `rh_term_taxonomy` VALUES('1', '1', 'category', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('2', '2', 'nav_menu', '', '0', '35');
INSERT INTO `rh_term_taxonomy` VALUES('34', '99', 'time-line-category', '', '0', '8');
INSERT INTO `rh_term_taxonomy` VALUES('41', '106', 'pricing_tables-category', '', '0', '5');
INSERT INTO `rh_term_taxonomy` VALUES('42', '107', 'pricing_tables-category', '', '0', '3');
INSERT INTO `rh_term_taxonomy` VALUES('43', '108', 'pricing_tables-category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('44', '109', 'pricing_tables-category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('45', '110', 'pricing_tables-category', '', '0', '3');
INSERT INTO `rh_term_taxonomy` VALUES('46', '111', 'pricing_tables-category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('51', '116', 'testimonials-category', '', '0', '5');
INSERT INTO `rh_term_taxonomy` VALUES('69', '134', 'nav_menu', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('70', '135', 'nav_menu', '', '0', '5');
INSERT INTO `rh_term_taxonomy` VALUES('71', '136', 'nav_menu', '', '0', '10');
INSERT INTO `rh_term_taxonomy` VALUES('72', '137', 'nav_menu', '', '0', '14');
INSERT INTO `rh_term_taxonomy` VALUES('73', '138', 'nav_menu', '', '0', '9');
INSERT INTO `rh_term_taxonomy` VALUES('74', '139', 'nav_menu', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('75', '140', 'nav_menu', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('76', '141', 'nav_menu', '', '0', '2');
INSERT INTO `rh_term_taxonomy` VALUES('77', '142', 'nav_menu', '', '0', '3');
INSERT INTO `rh_term_taxonomy` VALUES('78', '143', 'nav_menu', '', '0', '7');
INSERT INTO `rh_term_taxonomy` VALUES('79', '144', 'nav_menu', '', '0', '6');
INSERT INTO `rh_term_taxonomy` VALUES('80', '145', 'nav_menu', '', '0', '5');
INSERT INTO `rh_term_taxonomy` VALUES('81', '146', 'nav_menu', '', '0', '7');
INSERT INTO `rh_term_taxonomy` VALUES('151', '216', 'nav_menu', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('152', '217', 'nav_menu', '', '0', '12');
INSERT INTO `rh_term_taxonomy` VALUES('153', '218', 'post_format', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('154', '219', 'events-category', '', '0', '8');
INSERT INTO `rh_term_taxonomy` VALUES('155', '220', 'our-team-category', '', '0', '10');
INSERT INTO `rh_term_taxonomy` VALUES('156', '221', 'post_tag', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('157', '222', 'post_tag', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('160', '225', 'our-works-category', '', '0', '7');
INSERT INTO `rh_term_taxonomy` VALUES('161', '226', 'our-works-category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('162', '227', 'our-works-category', '', '0', '5');
INSERT INTO `rh_term_taxonomy` VALUES('163', '228', 'our-works-category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('164', '229', 'our-works-category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('165', '230', 'our-works-category', '', '0', '5');
INSERT INTO `rh_term_taxonomy` VALUES('166', '231', 'our-works-category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('167', '232', 'our-works-category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('168', '233', 'category', '', '0', '15');
INSERT INTO `rh_term_taxonomy` VALUES('170', '235', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('171', '236', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('172', '237', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('173', '238', 'post_tag', '', '0', '3');
INSERT INTO `rh_term_taxonomy` VALUES('174', '239', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('176', '241', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('177', '242', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('178', '243', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('179', '244', 'category', '', '280', '1');
INSERT INTO `rh_term_taxonomy` VALUES('180', '245', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('181', '246', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('182', '247', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('183', '248', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('184', '249', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('185', '250', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('186', '251', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('187', '252', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('188', '253', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('189', '254', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('190', '255', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('191', '256', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('192', '257', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('193', '258', 'category', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('194', '259', 'post_tag', '', '0', '8');
INSERT INTO `rh_term_taxonomy` VALUES('195', '260', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('196', '261', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('197', '262', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('198', '263', 'category', '', '0', '3');
INSERT INTO `rh_term_taxonomy` VALUES('199', '264', 'post_tag', '', '0', '5');
INSERT INTO `rh_term_taxonomy` VALUES('200', '265', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('201', '266', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('202', '267', 'kbe_taxonomy', '', '0', '6');
INSERT INTO `rh_term_taxonomy` VALUES('203', '268', 'kbe_taxonomy', '', '0', '8');
INSERT INTO `rh_term_taxonomy` VALUES('204', '269', 'kbe_taxonomy', '', '0', '6');
INSERT INTO `rh_term_taxonomy` VALUES('205', '270', 'kbe_taxonomy', '', '0', '8');
INSERT INTO `rh_term_taxonomy` VALUES('209', '274', 'category', '', '0', '2');
INSERT INTO `rh_term_taxonomy` VALUES('210', '275', 'category', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('212', '277', 'category', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('213', '278', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('214', '279', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('215', '280', 'category', '', '0', '3');
INSERT INTO `rh_term_taxonomy` VALUES('216', '281', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('217', '282', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('218', '283', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('219', '284', 'category', '', '0', '3');
INSERT INTO `rh_term_taxonomy` VALUES('220', '285', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('221', '286', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('222', '287', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('223', '288', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('224', '289', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('225', '290', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('226', '291', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('227', '292', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('228', '293', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('229', '294', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('230', '295', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('231', '296', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('232', '297', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('233', '298', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('234', '299', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('235', '300', 'post_tag', '', '0', '9');
INSERT INTO `rh_term_taxonomy` VALUES('236', '301', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('237', '302', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('238', '303', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('239', '304', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('240', '305', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('241', '306', 'category', '', '280', '0');
INSERT INTO `rh_term_taxonomy` VALUES('242', '307', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('243', '308', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('244', '309', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('245', '310', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('246', '311', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('247', '312', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('248', '313', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('249', '314', 'category', '', '0', '2');
INSERT INTO `rh_term_taxonomy` VALUES('250', '315', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('251', '316', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('252', '317', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('253', '318', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('254', '319', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('255', '320', 'testimonials-category', '', '0', '6');
INSERT INTO `rh_term_taxonomy` VALUES('256', '321', 'category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('257', '322', 'category', '', '280', '0');
INSERT INTO `rh_term_taxonomy` VALUES('258', '323', 'category', '', '280', '1');
INSERT INTO `rh_term_taxonomy` VALUES('259', '324', 'category', '', '280', '0');
INSERT INTO `rh_term_taxonomy` VALUES('260', '325', 'category', '', '280', '0');
INSERT INTO `rh_term_taxonomy` VALUES('261', '326', 'category', '', '258', '1');
INSERT INTO `rh_term_taxonomy` VALUES('262', '327', 'category', '', '0', '2');
INSERT INTO `rh_term_taxonomy` VALUES('263', '328', 'category', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('264', '329', 'category', '', '280', '0');
INSERT INTO `rh_term_taxonomy` VALUES('265', '330', 'category', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('266', '331', 'category', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('267', '332', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('268', '333', 'post_tag', '', '0', '2');
INSERT INTO `rh_term_taxonomy` VALUES('269', '334', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('270', '335', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('271', '336', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('272', '337', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('273', '338', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('274', '339', 'category', '', '0', '48');
INSERT INTO `rh_term_taxonomy` VALUES('275', '340', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('276', '341', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('277', '342', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('278', '343', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('279', '344', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('280', '345', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('281', '346', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('282', '347', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('283', '348', 'kbe_taxonomy', '', '0', '0');
INSERT INTO `rh_term_taxonomy` VALUES('284', '349', 'category', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('285', '350', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('286', '351', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('287', '352', 'post_tag', '', '0', '2');
INSERT INTO `rh_term_taxonomy` VALUES('288', '353', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('289', '354', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('290', '355', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('291', '356', 'category', '', '0', '4');
INSERT INTO `rh_term_taxonomy` VALUES('292', '357', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('293', '358', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('294', '359', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('295', '360', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('296', '361', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('297', '362', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('298', '363', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('299', '364', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('300', '365', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('301', '366', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('302', '367', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('303', '368', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('304', '369', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('305', '370', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('306', '371', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('307', '372', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('308', '373', 'post_tag', '', '0', '1');
INSERT INTO `rh_term_taxonomy` VALUES('309', '374', 'post_tag', '', '0', '1');
/*!40000 ALTER TABLE `rh_term_taxonomy` ENABLE KEYS */;
