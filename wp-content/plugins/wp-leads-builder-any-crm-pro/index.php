<?php
/******************************
Plugin Name: WP Leads Builder For Any CRM Pro 
Description: Sync data from Webforms (contact 7 , Ninja & Gravity ) and WP User data to SalesForce , Zoho CRM , Vtiger CRM , Sugar CRM & Freshsales CRM.  Embed forms as Posts , Pages & Widgets.
Version: 1.7.2
Author: smackcoders.com
Plugin URI: http://www.smackcoders.com/store/
Author URI: http://www.smackcoders.com
 * filename: index.php
 */

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

ob_start();
define('WP_CONST_ULTIMATE_CRM_CPT_URL_PRO', 'http://www.smackcoders.com/store/');
define('WP_CONST_ULTIMATE_CRM_CPT_NAME_PRO', 'WP Leads Builder For Any CRM Pro');
define('WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO', 'wp-leads-builder-any-crm-pro');
define('WP_CONST_ULTIMATE_CRM_CPT_SETTINGS_PRO', 'WP Leads Builder For Any CRM Pro');
define('WP_CONST_ULTIMATE_CRM_CPT_VERSION_PRO', '1.7.1');
define('WP_CONST_ULTIMATE_CRM_CPT_DIR_PRO', WP_PLUGIN_URL . '/' . WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO . '/');
define('WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO', plugin_dir_path( __FILE__ ));
define('WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO',site_url().'/wp-admin/admin.php?page='.WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO.'/index.php');

add_action('plugins_loaded','wp_lead_builder_pro_load_lang_files');
function wp_lead_builder_pro_load_lang_files(){
	$wp_lead_builder_pro_lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
	load_plugin_textdomain( WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO , false, $wp_lead_builder_pro_lang_dir );
}
if (!class_exists('SkinnyControllerCommonCrmPRO')) {
        require_once('lib/skinnymvc/controller/SkinnyController.php');
}

	require_once("includes/Data.php");
	require_once("includes/ContactFormPlugins.php");
	$ContactFormPlugins = new ContactFormPROPlugins();
	$ActivePlugin = $ContactFormPlugins->getActivePlugin();
	$get_debug_option = get_option("wp_{$ActivePlugin}_settings");

        if(!isset($get_debug_option['debugmode'])) {
                error_reporting(0);
                 ini_set('display_errors', 'Off');
        }

	if(isset( $_REQUEST['crmtype'] ))
	{
		require_once("includes/{$_REQUEST['crmtype']}Functions.php");
	}
	else
	{
		require_once("includes/{$ActivePlugin}Functions.php");
	}

	require_once('lib/skinnymvc/controller/SkinnyController.php');
	require_once('includes/WPCapture_includes_helper.php');
	require_once("templates/SmackContactFormGenerator.php");
	require_once('includes/Functions.php');

	# Activation & Deactivation 
	register_activation_hook(__FILE__, array('WPCapture_includes_helper_PRO', 'activate') );
	register_deactivation_hook(__FILE__, array('WPCapture_includes_helper_PRO', 'deactivate') );

	function action_admin_menu_crm_pro()
	{
		add_menu_page(WP_CONST_ULTIMATE_CRM_CPT_SETTINGS_PRO, WP_CONST_ULTIMATE_CRM_CPT_NAME_PRO, 'manage_options',  __FILE__, array('WPCapture_includes_helper_PRO','output_fd_page'), WP_CONST_ULTIMATE_CRM_CPT_DIR_PRO . "/images/leadsIcon24.png");
	}
	add_action ( "admin_menu", "action_admin_menu_crm_pro" );

	function action_crm_init_pro()
	{
		if (isset($_REQUEST['page']) && (sanitize_text_field($_REQUEST['page']) == WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO.'/index.php' || $_REQUEST['page'] == 'page')) {
			wp_enqueue_style('main-style', plugins_url('css/mainstyle.css', __FILE__));
			wp_enqueue_style('jquery-ui', plugins_url('css/jquery-ui.css', __FILE__));
			wp_enqueue_style('common-crm-free-bootstrap-css', plugins_url('css/bootstrap.css', __FILE__));
			wp_enqueue_style('common-crm-free-font-awesome-css', plugins_url('css/font-awesome/css/font-awesome.css', __FILE__));
			wp_enqueue_style('magnific-popup.css', plugins_url('css/magnific-popup.css', __FILE__));
			wp_enqueue_style('jquery-confirm.min.css', plugins_url('css/jquery-confirm.min.css', __FILE__));
			wp_enqueue_style('jquery-ui-11.4-css', plugins_url('css/jquery-ui-11.4.css', __FILE__));
			wp_register_script('basic-action-js', plugins_url('js/basicaction.js', __FILE__));
			wp_enqueue_script('basic-action-js');
			wp_register_script('common-crm-free-bootstrap-min-js', plugins_url('js/bootstrap.min.js', __FILE__));
			wp_enqueue_script('common-crm-free-bootstrap-min-js');
			wp_register_script('basic-action-js', plugins_url('js/basicaction.js', __FILE__));
			wp_enqueue_script('basic-action-js');
			wp_register_script('jquery-js', plugins_url('js/jquery.js', __FILE__));
			wp_enqueue_script('jquery-js');
			wp_register_script('jquery-min-js', plugins_url('js/jquery.min.js', __FILE__));
			wp_enqueue_script('jquery-min-js');
			wp_register_script('jquery-ui-11.4-js', plugins_url('js/jquery-ui-11.4.js', __FILE__));
                        wp_enqueue_script('jquery-ui-11.4-js');
			wp_register_script('boot.min-js', plugins_url('js/bootstrap-modal.min.js', __FILE__));
                        wp_enqueue_script('boot.min-js');
			wp_register_script('magnific-popup', plugins_url('js/jquery.magnific-popup.js', __FILE__));
                        wp_enqueue_script('magnific-popup');
			wp_register_script('jquery-confirm.min.js', plugins_url('js/jquery-confirm.min.js', __FILE__));
                        wp_enqueue_script('jquery-confirm.min.js');
		}
	}

	function frontend_init_pro()
	{
		if(!is_admin())
		{
			global $HelperObj;
			$HelperObj = new WPCapture_includes_helper_PRO;
			$activatedplugin = $HelperObj->ActivatedPlugin;
			$config = get_option("wp_captcha_settings");
			if($config['smack_recaptcha']=='yes')
			{
				wp_register_script( 'google-captcha-js' , "https://www.google.com/recaptcha/api.js" );
				wp_enqueue_script( 'google-captcha-js' );
			}
			wp_enqueue_script('jquery-ui-datepicker');
			wp_enqueue_style('jquery-ui' , plugins_url('css/jquery-ui.css', __FILE__) );
			wp_enqueue_style('front-end-styles' , plugins_url('css/frontendstyles.css', __FILE__) );
			wp_enqueue_style('datepicker' , plugins_url('css/datepicker.css', __FILE__) );
		//	wp_enqueue_style('normalize' , plugins_url('css/normalize.css', __FILE__) );
		}
	}

	add_action('init' , 'frontend_init_pro');
	add_action('admin_init', 'action_crm_init_pro');
	$check_sync_value = get_option( 'Sync_value_on_off' );
	if( $check_sync_value == "On" ){	
	add_action( 'profile_update', array( 'CapturingProcessClassPRO' , 'capture_updating_users' ) );
	add_action( 'user_register', array( 'CapturingProcessClassPRO' , 'capture_registering_users' ) );
	}
	$active_plugins = get_option( "active_plugins" );
	if( in_array( "ninja-forms/ninja-forms.php" , $active_plugins)  )	{
		add_action( 'init', 'ninja_forms_register_example' );
		require_once('templates/nija_form_field_handling.php');
	}

	if( in_array( "contact-form-7/wp-contact-form-7.php" , $active_plugins) )	{
		require_once('templates/contact_form_field_handling.php');
	} 

	if( in_array( "gravityforms/gravityforms.php" , $active_plugins) )  {
		require_once('templates/gravity_form_field_handling.php');
	}

	if( in_array( "woocommerce/woocommerce.php" , $active_plugins) )
	{
		require_once( 'templates/ecom_wc_field_handling.php' );
	}

	function wp_usersync_assignedto()
	{
		require_once( "templates/wp_assignedtouser.php" );
		die;
	}
	add_action( "wp_ajax_wp_usersync_assignedto" , "wp_usersync_assignedto");
	function TFA_auth_save()
	{
		$TFA_Authtoken_value = sanitize_text_field( $_REQUEST['authtoken']);	
		update_option("TFA_zoho_authtoken" , $TFA_Authtoken_value );
		print_r( $TFA_Authtoken_value);
		die;
	}
	add_action('wp_ajax_TFA_auth_save' , 'TFA_auth_save' );

	function mappingmodulepro()
	{
		$map_module = $_REQUEST['postdata'];
		update_option( 'WpMappingModule' , $map_module );
		die;
	}
	add_action( 'wp_ajax_mappingmodulepro' , 'mappingmodulepro' );

	function SaveCRMconfig( )
	{
		require_once( 'modules/Settings/actions/saveCRMConfig.php' );
		die;
	}
	add_action( 'wp_ajax_SaveCRMconfig' , 'SaveCRMconfig' );
	
	function Sync_settings_PRO( )
	{
		require_once( 'templates/Sync-settings.php' );
		die;
	}
	add_action( 'wp_ajax_Sync_settings_PRO' , 'Sync_settings_PRO' );

	function saveSyncValue()
	{
		require_once( 'templates/save-sync-value.php' );
		die;
	}
	add_action( 'wp_ajax_saveSyncValue' , 'saveSyncValue' );

	function send_mapping_configuration()
        {
                require_once( 'templates/thirdparty_mapping.php' );
		$module = sanitize_text_field( $_REQUEST['thirdparty_module'] );
		$thirdparty_form = sanitize_text_field( $_REQUEST['thirdparty_plugin'] );
                $mapping_ui_fields = new thirdparty_mapping();
                $mapping_ui_fields->mapping_form_fields($module , $thirdparty_form );
        }
        add_action( 'wp_ajax_send_mapping_configuration' , 'send_mapping_configuration' );


	function get_thirdparty_fields()
	{
		require_once( 'templates/thirdparty_mapping.php' );
		$mapping_ui_fields = new thirdparty_mapping();
                $mapping_ui_fields->get_thirdparty_form_fields();
	}
	add_action( 'wp_ajax_get_thirdparty_fields' , 'get_thirdparty_fields' );

	function map_thirdparty_fields()
	{
		require_once( 'templates/thirdparty_mapping.php' );
                $mapping_ui_fields = new thirdparty_mapping();
                $mapping_ui_fields->map_thirdparty_form_fields();

	}
	add_action( 'wp_ajax_map_thirdparty_fields' , 'map_thirdparty_fields' );	

	function save_thirdparty_form_title()
	{
		$thirdparty_title_key = sanitize_text_field($_REQUEST['tp_title_key']);
		$thirdparty_title_value = sanitize_text_field( $_REQUEST['tp_title_val'] );
		update_option( $thirdparty_title_key , $thirdparty_title_value );
		die;
	}
	add_action( 'wp_ajax_save_thirdparty_form_title' , 'save_thirdparty_form_title' );	

	function send_mapped_config()
	{
		require_once( 'templates/thirdparty_mapping.php' );
                $mapping_ui_fields = new thirdparty_mapping();
                $mapping_ui_fields->show_mapped_config();
	}
	add_action( 'wp_ajax_send_mapped_config' , 'send_mapped_config' );
	
	function delete_mapped_config()
	{
		require_once( 'templates/thirdparty_mapping.php' );
                $mapping_ui_fields = new thirdparty_mapping();
                $mapping_ui_fields->delete_mapped_configuration();
	}
	add_action( 'wp_ajax_delete_mapped_config' , 'delete_mapped_config' );

	function saveSFSettings() {
		require_once('includes/wpsalesforceproFunctions.php');
		$key = sanitize_text_field($_POST['key']);
		$value = sanitize_text_field($_POST['value']);
		$exist_config = get_option("wp_wpsalesforcepro_settings");
		$config = $current_config = array();
		switch ($key) {
			case 'key':
				$current_config['key'] = $value;
				break;
			case 'secret':
				$current_config['secret'] = $value;
				break;
			case 'callback':
				$current_config['callback'] = $value;
				break;
		}
		if(!empty($exist_config))
			$config = array_merge($exist_config, $current_config);
		else
			$config = $current_config;
		update_option('wp_wpsalesforcepro_settings', $config);
		die;
	}
	add_action( 'wp_ajax_saveSFSettings', 'saveSFSettings' );

	function selectplugpro()
	{
		require_once("templates/plugin-select.php");
		die;
	}   


//Round Robin User sync
	function save_usersync_RR_option()
	{
		$usersync_RR_value = sanitize_text_field( $_REQUEST['user_rr_val'] );
		update_option('usersync_rr_value' , $usersync_RR_value );
		die;
	}
	add_action( 'wp_ajax_save_usersync_RR_option' , 'save_usersync_RR_option' );

//Round Robin ecom

	function selectthirdpartypro()	{
		require_once("templates/third-plugin.php");
		die;
	}

	function customfieldpro()
	{	
		$custom_plugin = sanitize_text_field($_REQUEST['postdata']);
		$active_plugins = get_option( "active_plugins" );
        	switch( $custom_plugin )
        	{
                case 'acf':
                        if( in_array( "advanced-custom-fields/acf.php" , $active_plugins ) ) {
			update_option('custom_plugin',$custom_plugin);
                        $activated = "yes" ;
			}
                        else {
                        $activated = "no" ;
			}
                break;

                case 'wp-members':
                        if( in_array( "wp-members/wp-members.php" , $active_plugins) ) {
			update_option('custom_plugin',$custom_plugin);
                        $activated = "yes" ;
			}
                        else {
                        $activated = "no" ;
			}
                break;
	
		case 'member-press':
                        if( in_array( "memberpress/memberpress.php" , $active_plugins) ) {
                        update_option('custom_plugin',$custom_plugin);
                        $activated = "yes" ;
                        }
                        else {
                        $activated = "no" ;
                        }
                break;

		case 'none':
		update_option('custom_plugin',$custom_plugin);
		$activated = "yes" ;
		break;
        	}
		print_r( $activated );die;
	}

	add_action( 'wp_ajax_customfieldpro','customfieldpro' );
	function smack_leads_builder_pro_change_menu_order( $menu_order ) {
           return array(
               'index.php',
               'edit.php',
               'edit.php?post_type=page',
               'upload.php',
               'wp-leads-builder-any-crm-pro/index.php',
           );
        }
        add_filter( 'custom_menu_order', '__return_true' );
        add_filter( 'menu_order', 'smack_leads_builder_pro_change_menu_order' );

	add_action('wp_ajax_selectplugpro', 'selectplugpro');
	add_action('wp_ajax_selectthirdpartypro', 'selectthirdpartypro');

	WPCapture_includes_helper_PRO::checkVersion();


//Woocommerce Integration



add_action('woocommerce_checkout_order_processed', 'send_order_info');

function send_order_info($order_id) {
    $order = new WC_Order( $order_id );
    $customer = new WC_Customer( $order_id );
//print_r( $customer );
//echo 'POST';
//print_r( $_POST );
    $items = $order->get_items();
//echo '<pre>';
//print_r($order );
//print_r( $order_id );

//    print_r($items);
$myuser_id = (int)$order->user_id;
    $user_info = get_userdata($myuser_id);
    $items = $order->get_items();
    foreach ($items as $item) {
        if ($item['product_id']==24) {
          // Do something clever
        }
    }
  //  die();
}

function change_ecom_module_config()
{
	require_once( 'templates/ecom_config.php');
	$ecom_obj = new ecom_configuration();
	$ecom_obj->change_module_config();
}
add_action( 'wp_ajax_change_ecom_module_config' , 'change_ecom_module_config' );

function save_convert_lead()
{
	$convert_val = sanitize_text_field( $_REQUEST['convert_lead']);
	update_option( 'ecom_wc_convert_lead' , $convert_val );
}

add_action( 'wp_ajax_save_convert_lead' , 'save_convert_lead' );

function map_ecom_fields()
{
	require_once( 'templates/ecom_config.php');
        $ecom_obj = new ecom_configuration();
        $ecom_obj->map_ecom_module_configuration();
}
add_action( 'wp_ajax_map_ecom_fields' , 'map_ecom_fields' );


add_action( 'wpcf7_mail_sent', 'your_wpcf7_mail_sent_function' ); 
  
function your_wpcf7_mail_sent_function( $contact_form ) {
    $title = $contact_form->title;
    $submission = WPCF7_Submission::get_instance();
    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
    }
    if ( 'myTestForm' == $title ) {
        $firstName = $posted_data['first-name'];
        $lastName = $posted_data['last-name'];
   } 
}
?>
