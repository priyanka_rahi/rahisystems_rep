<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

global $IncludedPluginsPRO , $DefaultActivePluginPRO , $crmdetailsPRO , $ThirdPartyPlugins , $custom_plugins;
$IncludedPluginsPRO = Array(
		'wptigerpro' =>  "VtigerCRM",		
		'wpsugarpro'  => "SugarCRM" ,		
		'wpzohopro' => "ZohoCRM" ,	
		'wpsalesforcepro' => "SalesforceCRM" ,
		'freshsales'    => 'Fresh Sales',
	);
$ThirdPartyPlugins = array('none' => "None",
			   'ninjaform' => "Ninja Forms",
			   'contactform' => "Contact Form",
			   'gravityform' => "Gravity Form" ,
			);

$WpMappingModule = array(
			'Leads' => 'Leads',
			'Contacts' => 'Contacts',
			);

$custom_plugins = array('none' => "None",
			'wp-members' => "Wp-members",
			'acf' => "ACF" ,
			'member-press' => "MemberPress" ,
		      ); 

$crmdetailsPRO =array( 
'wptigerpro'=> array("Label" => "WP Tiger pro" , "crmname" => "VtigerCRM" , "modulename" => array("Leads" => "Leads" ,"Contacts" => "Contacts") ),
'wpsugarpro' => array( "Label" => "WP Sugar pro" , "crmname" => "SugarCRM" , "modulename" => array("Leads" => "Leads" ,"Contacts" => "Contacts") ),
'wpzohopro' => array("Label" => "WP Zoho pro" , "crmname" => "ZohoCRM" , "modulename" => array("Leads" => "Leads" ,"Contacts" => "Contacts")),  
'wpsalesforcepro' => array("Label" => "WP Salesforce pro" , "crmname" => "SalesforceCRM" , "modulename" => array("Leads" => "Lead" ,"Contacts" => "Contact") ),
'freshsales'=> array("Label" => "Fresh Sales" , "crmname" => "FreshSales" , "modulename" => array("Leads" => "Leads" ,"Contacts" => "Contacts") ),
	);

$DefaultActivePluginPRO = "wptigerpro";

