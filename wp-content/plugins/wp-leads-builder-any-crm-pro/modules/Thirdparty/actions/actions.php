<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class ThirdpartyActions extends SkinnyActions {

    public function __construct()
    {
    }

  /**
   * The actions index method
   * @param array $request
   * @return array
   */
    public function executeIndex($request)
    {
        // return an array of name value pairs to send data to the template
        $data = array();
        return $data;
    }

    public function executeView($request)
    {
        // return an array of name value pairs to send data to the template
        $data = array();
	foreach( $request as $key => $REQUESTS )
	{
		foreach( $REQUESTS as $REQUESTS_KEY => $REQUESTS_VALUE )
		{
			$data['REQUEST'][$REQUESTS_KEY] = $REQUESTS_VALUE;
		}
	}
		$data['activatedplugin'] = 'Thirdparty';
        	$data['action'] = $data['activatedplugin']."Settings";
		$data['HelperObj'] = new WPCapture_includes_helper_PRO();
                $data['module'] = $data['HelperObj']->Module;
                $data['moduleslug'] = $data['HelperObj']->ModuleSlug;
                $data['activatedpluginlabel'] = $data['HelperObj']->ActivatedPluginLabel;
                $data['plugin_dir']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
                $data['plugins_url'] = WP_CONST_ULTIMATE_CRM_CPT_DIR_PRO;
                $data['siteurl'] = site_url();
	if( isset($data['REQUEST']["posted"]) && ($data['REQUEST']["posted"] == "posted") )
	{
		$result = $this->saveSettings( $data );
		if($result['error'] == 1)
		{
			$data['display'] = "<p class='display_failure'> Please Fill all details </p>";
		}
		else
		{
			$data['display'] = "<p class='display_success'> Settings Successfully Saved </p>";
		}
	}
		return $data;
    }

    public function saveSettings( $data )
    {
		update_option("WpLeadBuilderProFirstTimeWarning" , "false");
		$request['action'] = 'ThirdParty';
		$HelperObj = $data['HelperObj'];
                $module = $HelperObj->Module;
                $moduleslug = $HelperObj->ModuleSlug;
                $activatedplugin = $HelperObj->ActivatedPlugin;
                $activatedpluginlabel = $HelperObj->ActivatedPluginLabel;
                $fieldNames = array(
                        'recaptcha_public_key' => __('Recaptcha Public Key', "wp-leads-builder-any-crm-pro" ),
                        'recaptcha_private_key' => __('Recaptcha Private Key', "wp-leads-builder-any-crm-pro" ),
                        'smack_recaptcha' => __('Recaptcha', "wp-leads-builder-any-crm-pro" ),
			'email'           => __( 'Email' , "wp-leads-builder-any-crm-pro") ,
			'emailcondition'  => __( 'Email Condition' , "wp-leads-builder-any-crm-pro" ),
			'debugmode'       => __( 'Debug Mode' , "wp-leads-builder-any-crm-pro" )
                );
                foreach ($fieldNames as $field=>$value){
                if(isset($data['REQUEST'][$field]))
                        {
                                $config[$field] = $data["REQUEST"][$field];
                        }
                        else
                        {
                                $config[$field] = "";
                        }
                }

		if( empty( $config['recaptcha_public_key']) || empty($config['recaptcha_private_key'] ) )
		{
			if( $config['smack_recaptcha'] == 'yes' ) {
				$result['error'] = "1";
			}
			else
			{
				$result['error'] = "0";
			}
		}
		else
		{
			$result['error'] = "0";
		}
                update_option("wp_captcha_settings", $config );
		return $result ;
    }
}


