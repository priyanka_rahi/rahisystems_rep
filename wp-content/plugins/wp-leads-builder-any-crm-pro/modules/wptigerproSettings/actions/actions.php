<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class WptigerSettingsActions extends SkinnyActions {

	public function __construct()
	{
	}

	/**
	 * The actions index method
	 * @param array $request
	 * @return array
	 */

	public function executeIndex($request)
	{
		// return an array of name value pairs to send data to the template
		$data = array();
		return $data;
	}

	public function executeView($request)
	{
		$data['plugin_url']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
		$data['onAction'] = 'onCreate';
		$data['siteurl'] = site_url();
		$data['HelperObj'] = new WPCapture_includes_helper_PRO();
		$data['module'] = $data["HelperObj"]->Module;
		$data['option'] = $data['options'] = "smack_{$data['activatedplugin']}_{$data['moduleslug']}_fields-tmp";
	}


	public function saveSettings($sett_array)
	{

		$fieldNames = array(

				'url' => __('Vtiger Url' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'username' => __('Vtiger User Name' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'accesskey' => __('Vtiger Access Key' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'smack_email' => __('Smack Email' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'email' => __('Email id' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'emailcondition' => __('Emailcondition' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'debugmode' => __('Debug Mode' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
					);

		foreach ($fieldNames as $field=>$value){
			if(isset($sett_array[$field]))
			{
				$config[$field] = trim($sett_array[$field]);
			}
		}

		$FunctionsObj = new PROFunctions( );
		$testlogin_result = $FunctionsObj->testLogin( $sett_array['url'] , $sett_array['username'] , $sett_array['accesskey'] );

		if($testlogin_result == 1)
		{

			$successresult = "<p  class='display_success' style='color: green;'> Settings Saved </p>";
			$result['error'] = 0;
			$result['success'] = $successresult;
			$WPCapture_includes_helper_Obj = new WPCapture_includes_helper_PRO();
			$activateplugin = $WPCapture_includes_helper_Obj->ActivatedPlugin;
			update_option("wp_{$activateplugin}_settings", $config);

		}
		else
		{
			$vtigercrmerror = "<p  class='display_failure' style='color:red;' >Please Verify your Vtiger Credentials.</p>";
			$result['error'] = 1;
			$result['errormsg'] = $vtigercrmerror ;
			$result['success'] = 0;
		}
		return $result;
	}
}
