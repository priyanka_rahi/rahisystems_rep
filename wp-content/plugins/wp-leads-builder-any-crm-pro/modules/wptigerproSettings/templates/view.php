<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$siteurl = site_url();
$siteurl = esc_url( $siteurl );
$config = get_option("wp_{$skinnyData['activatedplugin']}_settings");

if( $config == "" )
{
	$config_data = 'no';
}
else
{
	$config_data = 'yes';
}

/* define the plugin folder url */
#define('WP_PLUGIN_URL', plugin_dir_url(__FILE__));
$help_img = $siteurl."/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png";
$callout_img = $siteurl."/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif";
$help="<img src='$help_img'>"; 
$call="<img src='$callout_img'>";
?>
<input type="hidden" id="get_config" value="<?php echo $config_data ?>" >
<input type="hidden" id="revert_old_crm_pro" value="wptigerpro">
<span id="save_config" style="font:14px;width:200px;"> </span>

<span id="Fieldnames" style="font-size: 14px;font-weight:bold;float:right;padding-right:10px;padding-top:12px;padding-left:12px;"></span>
<script>
        $( document ).ready( function( ) {
                $( "#Fieldnames" ).hide(  );
        });

</script>
<div>
<!--  Start -->
	<form id="smack-vtiger-settings-form" action="" method="post">
		<input type="hidden" name="smack-vtiger-settings-form" value="smack-vtiger-settings-form" />
		<input type="hidden" id="plug_URL" value="<?php echo esc_url(WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO);?>" />
		<div class="wp-common-crm-content" style="width: 1000px;float: left;">
		<table>
			<tr>
				<td><label id="inneroptions" style="margin-top:6px;margin-right:4px;font-weight:bold;"><?php echo esc_html__('Select the CRM you use' , "wp-leads-builder-any-crm-pro" ); ?></label></td>
				<td>

			<?php
				$ContactFormPluginsObj = new ContactFormPROPlugins();
				echo $ContactFormPluginsObj->getPluginActivationHtml();
			?>
				</td>
			</tr>
		</table>
		<br>
		<label id="inneroptions" style="font-weight:bold;">VTiger CRM Settings :</label>
		<table class="settings-table">
			<tr><td></td></tr>
			<tr>
				<td style="width:172px;padding-left:40px;">
					<label id="innertext"> <?php echo esc_html__('CRM Url' , "wp-leads-builder-any-crm-pro" ); ?> </label><div style='float:right;'> : </div>
				</td>
				<td colspan="3" >
					<input type='text' class='smack-vtiger-settings' name='url' id='smack_tiger_host_address' style="width:480px;" value="<?php echo esc_url($config['url']) ?>"/>

				</td>
		</tr>

		<tr>
			<td style="width:172px;padding-left:40px;">
				<label id="innertext"> <?php echo esc_html__('Username' , "wp-leads-builder-any-crm-pro" ); ?> </label><div style='float:right;'> : </div>
			</td>
			<td>
				<input type='text' class='smack-vtiger-settings' name='username' id='smack_host_username' value="<?php echo sanitize_text_field($config['username']) ?>"/>
			</td>
			<td style='width:150px;padding-left:40px;'>
				<label id="innertext"> <?php echo esc_html__('Access Key' , "wp-leads-builder-any-crm-pro" ); ?> </label><div style='float:right;'> : </div>
			</td>
			<td style="width:100px;padding-left:0px;">
				<input type='text' class='smack-vtiger-settings' name='accesskey' id='smack_host_access_key' value="<?php echo sanitize_text_field($config['accesskey']) ?>"/>
			</td>
		</tr>

        </table>
        <br/><br><br><br>

		<table style="float:right;margin-right:150px;">
			<tr>
				<td>
				<input type="hidden" name="posted" value="<?php echo 'posted';?>">
					
				<input type="hidden" id="site_url" name="site_url" value="<?php echo esc_attr($siteurl) ;?>">
               			<input type="hidden" id="active_plugin" name="active_plugin" value="<?php echo esc_attr($skinnyData['activatedplugin']); ?>">
                		<input type="hidden" id="leads_fields_tmp" name="leads_fields_tmp" value="smack_wptigerpro_leads_fields-tmp">
                		<input type="hidden" id="contact_fields_tmp" name="contact_fields_tmp" value="smack_wptigerpro_contacts_fields-tmp">

					<p class="submit" style='position:absolute;'>
					<span id="SaveCRMConfig" style="position:absolute;bottom:70px;margin-top:10px;">
        <input type="button" value="<?php echo esc_attr__('Save CRM Confiuration' , "wp-leads-builder-any-crm-pro" );?>" id="save" class="button-primary" onclick="saveCRMConfiguration(this.id);" />
</span> 
					</p>
				</td>
			</tr>
		</table>
		</div>
	
	</form>
<!-- End-->
</div>
<div id="loading-sync" style="display: none; background:url(<?php echo esc_url(WP_PLUGIN_URL);?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Syncing' , 'wp-leads-builder-any-crm-pro' ); ?>...</div>

<div id="loading-image" style="display: none; background:url(<?php echo esc_url(WP_PLUGIN_URL);?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Please Wait...' , "wp-leads-builder-any-crm-pro"  ); ?> </div>

