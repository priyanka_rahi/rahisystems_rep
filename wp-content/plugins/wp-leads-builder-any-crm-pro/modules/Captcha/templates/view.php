<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$config = get_option("wp_captcha_settings");
?>
	<form id="smack-<?php echo sanitize_text_field($skinnyData['activatedplugin']);?>-captcha-form" action="" method="post">
	<input type="hidden" name="smack-<?php echo esc_attr($skinnyData['activatedplugin']);?>-captcha-form" value="smack-<?php echo sanitize_text_field($skinnyData['activatedplugin']);?>-captcha-form" />
		<h3 id="innerheader"><?php echo esc_html__('Google reCAPTCHA Settings' , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?></h3>
	<div class = "wp-common-crm-content">
		<table>
			<tr>
				<td>
					<label id="inneroptions" style=" margin-top: 7px; margin-right: 10px;" ><?php echo esc_html__("Do you need captcha to visible" , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?> : </label>
				</td>
				<td>
				<span id="circlecheck">
					<input type='radio' class='smack-vtiger-settings-radio-captcha circlecheckbox' name='smack_recaptcha' id='smack_recaptcha_no'  value="no"
		<?php
		if($config['smack_recaptcha']=='no' || !isset($config['smack_recaptcha']))
		{
			echo "checked";
		}
		?>
		 onclick="showOrHideRecaptchaPRO('no');"> <label for="smack_recaptcha_no" class="circle-label" id="innertext" style='margin-left: 5px; margin-bottom: 0px; margin-right: 10px; margin-top: 0px;'> <?php echo esc_html__('No' , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?> </label>
					<input type='radio' class='smack-vtiger-settings-radio-captcha circlecheckbox' name='smack_recaptcha' id='smack_recaptcha_yes'  value="yes" 
		<?php
		if($config['smack_recaptcha']=='yes')
		{
			echo "checked";
		}
		?>
		 onclick="showOrHideRecaptchaPRO('yes');"> <label for="smack_recaptcha_yes" class="circle-label" id="innertext" style='margin-left: 5px; margin-bottom: 0px; margin-right: 10px; margin-top: 0px;'> <?php echo esc_html__('Yes' , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?> </label>
				</span>
				</td>
			</tr>
		</table>
		<table>
		
			<tr id="recaptcha_public_key"
		<?php

		if($config['smack_recaptcha']=='no' || !isset($config['smack_recaptcha']))
		{
			echo 'style="display:none"';
		}
		else
		{
			echo 'style="display:block;margin-top:18px;"';
		}

		?>
		>
				<td style='width:150px;'>
					<label id="innertext"><?php echo esc_html__('Recaptcha Public Key' , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?> </label><div style='float:right;'> : </div>
				</td>
				<td>
					<div style ="position:relative;left:9px;">
						<input type='text' style="width: 250px;" class='smack-vtiger-settings-text' placeholder='<?php echo esc_attr__('Enter your recaptcha public key here', WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?>' name='recaptcha_public_key' id='smack_public_key' value="<?php echo sanitize_text_field($config['recaptcha_public_key']) ?>"/>
					</div>
				</td>
				<td >
					<div style ="padding-left:20px; position:relative;top:-9px;">
					<a class="tooltip"  href="#">
					<img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
					<span class="tooltipPostStatus">
					<img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
					<?php echo __('Enter your recaptcha public key here.', WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?>
					<img style="margin-top: 6px;float:right;" src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
					</span> </a>
					</div>
				</td>
			</tr>
			
			<tr id="recaptcha_private_key" <?php

		if($config['smack_recaptcha']=='no' || !isset($config['smack_recaptcha']))
		{
			echo 'style="display:none"';
		}
		else
		{
			echo 'style="display:block;margin-top:13px"';
		}

		?>
		>
				<td style='width:150px;'>
					<label id="innertext"><?php echo esc_html__("Recaptcha Private Key", WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?></label><div style='float:right;'> : </div>
				</td>
				<td>
					<div style = "position:relative;left:9px;">
						<input type='text' style="width: 250px;" class='smack-vtiger-settings-text' placeholder='<?php echo esc_attr__("Enter your recaptcha private key here" , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?>' name='recaptcha_private_key' id='smack_private_key' value="<?php echo $config['recaptcha_private_key'] ?>"/>
					</div>
				</td>
				<td>
					<div style ="padding-left:20px; position:relative;top:-9px;">
					<a class="tooltip"  href="#">
					<img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
					<span class="tooltipPostStatus">
					<img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
					<?php echo __("Enter your recaptcha private key here." , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?>
					<img style="margin-top: 6px;float:right;" src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
					</span> </a>
					</div>
				</td>

			</tr>
		</table>
		<table>
			<tr>
				<td>
					<input type="hidden" name="posted" value="<?php echo 'posted';?>">
					<p class="submit">
						<input type="submit" value="<?php echo esc_attr__('Save Settings' , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO );?>" id="innersave" class="button-primary"/>
					</p>
				</td>
			</tr>
		</table>
		</div>
		</form>
