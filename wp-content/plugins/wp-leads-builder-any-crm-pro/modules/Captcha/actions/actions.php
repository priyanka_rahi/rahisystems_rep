<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class CaptchaActions extends SkinnyActions {

	public $nonceKey = null;
	public function __construct()
	{
		$helperObj = new OverallFunctionsPRO();
		$this->nonceKey = $helperObj->smack_create_wplbpro_nonce_key();
	}

	/**
	* The actions index method
	* @param array $request
	* @return array
	*/

	public function executeIndex($request)
	{
		// return an array of name value pairs to send data to the template
		$data = array();
		return $data;
	}
	public function executeView($request)
	{
	 	$data = array();
	 	foreach( $request as $key => $REQUESTS )
			{
				foreach( $REQUESTS as $REQUESTS_KEY => $REQUESTS_VALUE )
				{
					$data['REQUEST'][$REQUESTS_KEY] = $REQUESTS_VALUE;
				}
			}
	  	$data['HelperObj'] = new WPCapture_includes_helper_PRO();
		$data['module'] = $data['HelperObj']->Module;
		$data['moduleslug'] = $data['HelperObj']->ModuleSlug;
		$data['activatedplugin'] = $data['HelperObj']->ActivatedPlugin;
		$data['activatedpluginlabel'] = $data['HelperObj']->ActivatedPluginLabel;
		$data['plugin_dir']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
		$data['plugins_url'] = WP_CONST_ULTIMATE_CRM_CPT_DIR_PRO;
		$data['nonce_key'] = $this->nonceKey;
		$data['siteurl'] = site_url();
	 	if( isset($data['REQUEST']["smack-{$data['activatedplugin']}-captcha-form"]) )
		{
			 $this->saveSettingArray($data);
		}
		return $data;
	}
	public function saveSettingArray($data)
	{
		$HelperObj = $data['HelperObj'];
		$module = $HelperObj->Module;
		$moduleslug = $HelperObj->ModuleSlug;
		$activatedplugin = $HelperObj->ActivatedPlugin;
		$activatedpluginlabel = $HelperObj->ActivatedPluginLabel;		
		$fieldNames = array(
			'recaptcha_public_key' => __('Recaptcha Public Key', WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ),
			'recaptcha_private_key' => __('Recaptcha Private Key', WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ),
			'smack_recaptcha' => __('Recaptcha', WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ),
		);
		foreach ($fieldNames as $field=>$value){
		if(isset($data['REQUEST'][$field]))
			{
                        	$config[$field] = $data["REQUEST"][$field];
			}
			else
			{
				$config[$field] = "";
			}		
 
		}
		update_option("wp_captcha_settings", $config);
	}
}


