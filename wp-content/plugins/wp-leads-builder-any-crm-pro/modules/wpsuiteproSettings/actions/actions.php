<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class WpsuiteSettingsActions extends SkinnyActions {

	public function __construct()
	{
	}

	/**
	* The actions index method
	* @param array $request
	* @return array
	*/
	public function executeIndex($request)
	{
		// return an array of name value pairs to send data to the template
		$data = array();
		return $data;
	}

	public function saveSettings($suiteSettArray)
	{
		$fieldNames = array(
			'url' => __('Suite Host Address'),
			'username' => __('Suite Username'),
			'password' => __('Suite Password'),

			'user_capture' => __('User Capture'),
			'smack_email' => __('Smack Email'),
                         'email' => __('Email id'),
			 'emailcondition' => __('Emailcondition'),
			'debugmode' => __('Debug Mode'),
		);
		foreach ($fieldNames as $field=>$value){
		if(isset($suiteSettArray[$field]))
		{
			$config[$field] = $suiteSettArray[$field];
		}
		}	

		$FunctionsObj = new PROFunctions( );
                $testlogin_result = $FunctionsObj->testlogin( $config['url'] , $config['username'] , $config['password'] );
                if(isset($testlogin_result['login']) && ($testlogin_result['login']['id'] != -1) && is_array($testlogin_result['login']))
                {
                        $successresult = "<p  class='display_success' style='color: green;'> Settings Saved </p>";
                        $result['error'] = 0;
                        $result['success'] = $successresult;
                        $WPCapture_includes_helper_Obj = new WPCapture_includes_helper_PRO();
                        $activateplugin = $WPCapture_includes_helper_Obj->ActivatedPlugin;
                        update_option("wp_{$activateplugin}_settings", $config);
                }
                else
                {
                        $suitecrmerror = "<p  class='display_failure' style='color:red;' >Please Verify Your Salesforce Credentials </p>";
                        $result['error'] = 1;
                        $result['errormsg'] = $suitecrmerror ;
                        $result['success'] = 0;
                }
                return $result;
                $WPCapture_includes_helper_Obj = new WPCapture_includes_helper_PRO();
                $activateplugin = $WPCapture_includes_helper_Obj->ActivatedPlugin;
                update_option("wp_{$activateplugin}_settings", $config);
	}	
}

class CallSuiteSettingsCrmObj extends WpsuiteSettingsActions
{
	private static $_instance = null;
	public static function getInstance()
	{
		if( !is_object(self::$_instance) )  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
			self::$_instance = new WpsuiteSettingsActions();
		return self::$_instance;
	}
}
