<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$siteurl = site_url();
$config = get_option('smack_wp_suite_settings');
$chkUpgrade = get_option('smack_wp_suite_lead_fields');
$config = get_option("wp_{$skinnyData['activatedplugin']}_settings");
?>
<form id="smack-suite-settings-form" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
	<h3 id="innerheader">Suite CRM Settings :</h3>
	<input type="hidden" name="smack-suite-settings-form" value="smack-suite-settings-form" />
	<input type="hidden" id="plug_URL" value="<?php echo WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO;?>" />
	<div class="wp-common-crm-content" style="width: 800px;float: left;">
	<table>
		<tr>
			<td><label id="inneroptions" style="margin-top:6px;margin-right:4px">Select Plugin</label></td>
			<td>
			<?php
				$ContactFormPluginsObj = new ContactFormPROPlugins();
				echo $ContactFormPluginsObj->getPluginActivationHtml();
			?>
			</td>
			<td>
                                <div style="position:relative;top:-11px;left:10px;">
                                <a class="tooltip"  href="#">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
                                <span class="tooltipPostStatus" style="width:130px;">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
                                 Choose your CRM.
                                </span> </a>
                                </div>
                        </td>

		</tr>
	</table>
	<table  class="settings-table">
		<tr>
			<td colspan=3>
				<span style="color: red; font-weight:bold;"> Note :- This module is beta. No Immediate support will be available. </span>
			</td>
		</tr>
		<tr>
			<td style='width:172px;'>
				<label id="innertext"> Suite Host Address </label><div style='float:right;'> : </div>
			</td>
			<td>
				<input type='text' class='smack-vtiger-settings' name='url' id='smack_host_address' value="<?php echo $config['url'] ?>"/>

			</td>
			 <td>
                                <div style="position:relative;top:-11px;">
                                <a class="tooltip"  href="#">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
                                <span class="tooltipPostStatus" style="width:335px;">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
                                  This is the full URL path to access your crm via browser.
                                </span> </a>
                                </div>
                        </td>

		</tr>
		<tr>
			<td style='width:172px;'>
				<label id="innertext"> Suite Username </label><div style='float:right;'> : </div>
			</td>
			<td>
				<input type='text' class='smack-vtiger-settings' name='username' id='smack_host_username' value="<?php echo $config['username'] ?>"/>

			</td>
			 <td>
                                <div style="position:relative;top:-11px;">
                                <a class="tooltip"  href="#">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
                                <span class="tooltipPostStatus">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
                                 This is CRM admin in general, but you can also use other users with admin privilege.
                                </span> </a>
                                </div>
                        </td>

		</tr>
		<tr>
			<td style='width:172px;'>
				<label id="innertext"> Suite Password </label><div style='float:right;'> : </div>
			</td>
			<td>
				<input type='password' class='smack-vtiger-settings' name='password' id='smack_host_access_key' value="<?php echo $config['password'] ?>"/>
			</td>
			 <td>
                                <div style="position:relative;top:-11px;">
                                <a class="tooltip"  href="#">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
                                <span class="tooltipPostStatus" style="width:335px;">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
                                 Password for the username as given in CRM username as above.
                                </span> </a>
                                </div>
                        </td>

		</tr>
		<tr>
			<td style='width:172px;'>
				<label id="innertext" style='float:left;'> Capture User Registration</label><div style='float:right;'> : </div>
			</td>
			<td>
				<div class="switch">
					<input type='checkbox' class='smack-vtiger-settings cmn-toggle cmn-toggle-yes-no' name='user_capture' id='user_capture' value="on" <?php if(isset($config['user_capture']) && $config['user_capture'] == 'on') { echo "checked=checked"; } ?>/>
					<label for="user_capture" id="innertext" data-on="Yes" data-off="No"></label>
                        </div>
<!--			</td>
			<td style = "width:18%;">-->
                                <div style="margin-left:60px">
                                <a class="tooltip"  href="#" style="margin-top:-29px">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
                                <span class="tooltipPostStatus" style="width:263px;">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
                                (Enabling will sync users to CRM contacts).
                                </span> </a>
                                </div>
                        </td>

		</tr>
	<tr><td>
	<label  id="inneroptions">Email Notification :</label>
	</td></tr>
                <tr>
                        <td style='width:172px;'>
                                <label id="innertext" style='float:left;'> Email All Captured Data</label><div style='float:right;'> : </div>
                        </td>
                        <td>
				<div class="switch">
	                                <input type='checkbox' class='smack-vtiger-settings cmn-toggle cmn-toggle-yes-no' name='smack_email' id='smack_email' value="on" <?php if(isset($config['smack_email']) && $config['smack_email'] == 'on') { echo "checked=checked"; } ?> onclick="enablesmackemail(this.id)"/>
					<label for="smack_email" id="innertext" data-on="Yes" data-off="No"></label>
                        </div>
<!--                        </td>
			  <td style = "width:18%;">-->
                                <div style="margin-left:60px">
                                <a class="tooltip"  href="#" style="margin-top:-29px">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
                                <span class="tooltipPostStatus" style="width:300px;">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
                                (Get emails for each form data feed with details).
                                </span> </a>
                                </div>
                        </td>

                </tr>
                <tr>
                        <td style='width:172px;'>
                                <label id="innertext" style='float:left;'> Email Id </label><div style='float:right;'> : </div>
                        </td>
                        <td>
                     <input type='text' class='smack-vtiger-settings' name='email' id='email' value="<?php if(isset($config['email'])) { echo $config['email']; } ?>" <?php if( !isset( $config['email'] ) || $config['smack_email'] != 'on' ){ ?> disabled="disabled" <?php } ?> />
                        </td>
			<td>
                                <div style="position:relative;top:-11px;left:6px;">
                                <a class="tooltip"  href="#">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
                                <span class="tooltipPostStatus">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
                                  (by default admin enable to get mail alerts, you can add one more email here).
                                </span> </a>
                                </div>
                        </td>
	
                </tr> 
		<tr>
			 <td>
			 <span id="circlecheck">
                                <input type = 'radio' name='emailcondition' class='circlecheckbox' id = 'successemailcondition' value ="success" <?php if(isset($config['smack_email']) && $config['emailcondition'] == 'success' && $config['smack_email'] == 'on') { echo "checked=checked"; } ?> >
<label for="successemailcondition" class="circle-label" id="innertext" style="position:relative;top:3px;left:4px;">Success</label>
			</span>
                        </td>
                        <td>
			<div id="circlecheck" style="position:relative;right:10px;">
                                 <input type = 'radio' name='emailcondition' class='circlecheckbox' id = 'failureemailcondition' value ="failure" <?php if(isset($config['smack_email']) && $config['emailcondition'] == 'failure' && $config['smack_email'] == 'on') { echo "checked=checked"; } ?> ><label for="failureemailcondition" class="circle-label" id="innertext" style="position:relative;top:3px;left:4px;">Failure</label>
                        </div>
			</td>
                        <td>
			<div id="circlecheck" style="position:relative;left:100px;">
                        	<input type = 'radio' name='emailcondition' class='circlecheckbox' id = 'bothemailcondition' value ="both" <?php if(isset($config['smack_email']) && $config['emailcondition'] == 'both' && $config['smack_email'] == 'on') { echo "checked=checked"; } ?> >
<label for="bothemailcondition" class="circle-label" id="innertext" style="position:relative;top:3px;right:150px;">Both</label>
			</div>
                        </td>
                </tr>

		<tr>
			<td>
                                <label id="inneroptions">Debug Mode :</label>
                        </td>
		</tr>
		<tr>
                        <td style='width:172px;'>
                                <label id="innertext" style='float:left;'> Enable Debug Mode</label><div style='float:right;'> : </div>
                        </td>
                        <td>
                                <div class="switch">
                                        <input type='checkbox' class='smack-vtiger-settings-text cmn-toggle cmn-toggle-yes-no' name='debugmode' id='debugmode' value="on" <?php if(isset($config['debugmode']) && $config['debugmode'] == 'on') { echo "checked=checked"; } ?> onclick="debugmode(this.id)"/>
                                        <label for="debugmode" id="innertext" data-on="Yes" data-off="No"></label>
                                </div>
                                <div style="margin-left:60px">
                                <a class="tooltip"  href="#" style="margin-top:-29px">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/help.png">
                                <span class="tooltipPostStatus" style="width:300px;">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/<?php echo WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO;?>/images/callout.gif" class="callout">
                                (Debug Mode is No to get Error_reporting is zero and also ini_set is Off).
                                </span> </a>
                                </div>
                        </td>
                </tr>
 
        </table>
	<table>
		<tr>
			<td>
				<input type="hidden" name="posted" value="<?php echo 'posted';?>">
				<p class="submit">
					<input type="submit" value="<?php _e('Save Settings');?>" id="innersave" class="button-primary" onclick="document.getElementById('loading-image').style.display = 'block'"/>
				</p>
			</td>
		</tr>
	</table>
	</div>
</form>
<div id="loading-image" style="display: none; background:url(<?php echo WP_PLUGIN_URL;?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;">Please Wait...</div>
