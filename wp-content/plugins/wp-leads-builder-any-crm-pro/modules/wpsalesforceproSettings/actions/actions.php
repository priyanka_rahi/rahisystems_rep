<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class WpsalesforceSettingsActions extends SkinnyActions {

	public function __construct()
	{
	}

	/**
	 * The actions index method
	 * @param array $request
	 * @return array
	 */
	public function executeIndex($request)
	{
		// return an array of name value pairs to send data to the template
		$data = array();
		return $data;
	}

	public function executeView($request)
	{
		$data['plugin_url']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
		$data['onAction'] = 'onCreate';
		$data['siteurl'] = site_url();
		$data['HelperObj'] = new WPCapture_includes_helper_PRO();
		$data['module'] = $data["HelperObj"]->Module;
	}

	public function saveSettings($sugarSettArray)
	{
		$fieldNames = array(
				'key' => __('Consumer Key' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'secret' => __('Consumer Secret' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'callback' => __('Callback URL' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'smack_email' => __('Smack Email' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'email' => __('Email id' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'emailcondition' => __('Emailcondition' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
				'debugmode' => __('Debug Mode', WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
					);

		foreach ($fieldNames as $field=>$value){

			if(isset($sugarSettArray[$field]))
			{
				$config[$field] = $sugarSettArray[$field];
			}
		}
		$WPCapture_includes_helper_Obj = new WPCapture_includes_helper_PRO();
		$activateplugin = $WPCapture_includes_helper_Obj->ActivatedPlugin;
		update_option("wp_{$activateplugin}_settings", $config);
	}
}

class CallSalesforceSettingsCrmObj extends WpsalesforceSettingsActions
{
	private static $_instance = null;
	public static function getInstance()
	{
		if( !is_object(self::$_instance) )
			self::$_instance = new WpsalesforceSettingsActions();
		return self::$_instance;
	}
}
