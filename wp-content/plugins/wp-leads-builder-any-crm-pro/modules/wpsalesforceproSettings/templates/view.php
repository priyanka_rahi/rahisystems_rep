<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$config = get_option("wp_{$skinnyData['activatedplugin']}_settings");

if( $config == "" )
{
        $config_data = 'no';
}
else
{
        $config_data = 'yes';
}

$site_url = site_url();
$page_scheme = parse_url($site_url,PHP_URL_SCHEME);
$sales = $page_scheme."://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$sales_url = trim( strtok( $sales , '?' ));
$sales_query_string = $_SERVER['QUERY_STRING'] ;
$remove_code = remove_query_arg( 'code' , $sales_query_string );
$sales_callback_url = $sales_url."?".$remove_code;

if(isset( $_REQUEST['code'] ) && (sanitize_text_field($_REQUEST['code']) != '') )
{
	include_once(WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO."/lib/SmackSalesForceApi.php");
	$code = sanitize_text_field( $_GET['code'] );
	$response = Getaccess_token( $config , $code);
	$access_token = $response['access_token'];
	$instance_url = $response['instance_url'];
	if (!isset($access_token) || $access_token == "") {
		die("Error - access token missing from response!");
	}
	if (!isset($instance_url) || $instance_url == "") {
		die("Error - instance URL missing from response!");
	}
	$_SESSION['access_token'] = $access_token;
	$_SESSION['instance_url'] = $instance_url;
	$config['access_token'] = $access_token;
	$config['instance_url'] = $instance_url;
	$config['id_token'] = $response['id_token'];
	$config['signature'] = $response['signature'];
}

$siteurl = site_url(); 
$help_img = $siteurl."/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png";
$callout_img = $siteurl."/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif";
$help="<img src='$help_img'>";
$call="<img src='$callout_img'>";
update_option("wp_{$skinnyData['activatedplugin']}_settings" , $config );
?>
<input type="hidden" id="get_config" value="<?php echo $config_data ?>" >
<span id="save_config" style="font:14px;width:200px;"> </span>

<span id="Fieldnames" style="font-size: 14px;font-weight:bold;float:right;padding-right:10px;padding-top:12px;padding-left:12px;"></span>
<script>
	$( document ).ready( function( ) {
		save_salesforece_settings('callback', "<?php echo $sales_callback_url ;?>");
		$( "#Fieldnames" ).hide(  );
	});

</script>
<input type="hidden" id="revert_old_crm_pro" value="wpsalesforcepro">
<form id="smack-salesforce-settings-form" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>" method="post">

	<input type="hidden" name="smack-salesforce-settings-form" value="smack-salesforce-settings-form" />
	<input type="hidden" id="plug_URL" value="<?php echo esc_url(WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO);?>" />
	<div class="wp-common-crm-content" style="width: 1000px;float: left;">
	
		<table>
			<tr>
				<td><label id="inneroptions" style="margin-top:6px;margin-right:4px;font-weight:bold;"><?php echo esc_html__('Select the CRM you use', 'wp-leads-builder-any-crm-pro' ); ?></label></td>

				<td>
					<?php
					$ContactFormPluginsObj = new ContactFormPROPlugins();
					echo $ContactFormPluginsObj->getPluginActivationHtml();
					?>
				</td>
			</tr>
		</table>
		<br>
		<label id="inneroptions" style="font-weight:bold;">Salesforce CRM Settings :</label>
		<table  class="settings-table">
			<tr><td></td></tr>
			<tr>
				<td style='width:150px;padding-left:40px;'>
					<label id="innertext"> <?php echo esc_attr__('Consumer Key', 'wp-leads-builder-any-crm-pro' ); ?>  </label><div style='float:right;'> : </div>
				</td>
				<td>
					<input type='text' class='smack-vtiger-settings' name='key' id='smack_host_address' value="<?php echo sanitize_text_field($config['key']) ?>" onblur="save_salesforece_settings('key', this.value);"/>

					<div style="position:relative;top:-20px;margin-left:189px;">
						<div class="tooltip">
					<?php echo $help ?>	
                                <span class="tooltipPostStatus">
                                <h5>Consumer Key</h5>Get the Consumer Key from your Salesforce account and specify here.
<a target="_blank" href="https://help.salesforce.com/apex/HTViewSolution?id=000205876&language=en_US">Refer Salesforce help</a>
                                </span> </div>
					</div>
				</td>
			</tr>
			<tr>
				<td style='width:100px;padding-left:40px;'>
					<label id="innertext"> <?php echo esc_html__('Consumer Secret', 'wp-leads-builder-any-crm-pro' ); ?> </label><div style='float:right;'> : </div>
				</td>
				<td>
					<input type='password' class='smack-vtiger-settings' name='secret' id='smack_host_username' value="<?php echo sanitize_text_field($config['secret']) ?>" onblur="save_salesforece_settings('secret', this.value);"/>
					<div style="position:relative;top:-20px;margin-left:189px;">
						<div class="tooltip">
						<?php echo $help ?>
                                <span class="tooltipPostStatus" style="width:330px;">
                                <h5>Consumer Secret</h5>Get the Consumer Secret from your Salesforce account and specify here. 
				<a target="_blank" href="https://help.salesforce.com/apex/HTViewSolution?id=000205876&language=en_US">Refer Salesforce Help</a>				
</span> </div>
					</div>
				</td>

			</tr>
			<tr>
				<td style='width:172px;padding-left:40px;'>
					<label id="innertext"> <?php echo esc_html__('Callback URL' , 'wp-leads-builder-any-crm-pro' ); ?> </label><div style='float:right;'> : </div>
				</td>
				<td colspan="2">
					<input type='text' class='smack-vtiger-settings' name='callback' style="width:350px;" id='copy_smack_host_access_key' value="<?php echo esc_url($sales_callback_url); ?>"  disabled="disabled"/>
<!-- 

<?php echo esc_url($sales_callback_url); ?>
-->
				
				<img src="<?php echo esc_url($siteurl); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/copy.png" id="copy_to_clipboard" value="Copy"  data-clipboard-action="copy" data-clipboard-target="#copy_smack_host_access_key">
						<div style="position:relative;top:-21px;padding-left:390px;">
						<div class="tooltip"  href="#">
				<?php echo $help ?>		
                                <span class="tooltipPostStatus"style="width:300px;">
                                <h5>Callback URL</h5> 
	                                <?php echo esc_html__('This URL to be specified in SalesForce Callback url field.' , 'wp-leads-builder-any-crm-pro' ); ?><a target="_blank" href="https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/intro_defining_remote_access_applications.htm">Refer Salesforce Help</a>
                                </span> </a>
					</div>
				</td>

			</tr>
			<tr>
				  <td style="padding-left:250px;"colspan="4">
					  <?php
                                                $auth_url =  "https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=" . $config['key'] . "&redirect_uri=" . urlencode($config['callback']);
                                                $auth_url = esc_url( $auth_url );
                                                ?>

                                                <a href="<?php echo "$auth_url"?>" ><input name="submit" type="button" value="<?php echo esc_attr__('Authenticate' , 'wp-leads-builder-any-crm-pro' ); ?>" class="button-primary" style="margin-left:0px;float:left;"/> </a>

                        </td>
		</tr>
		<tr>
			<td style="padding-left:40px;" colspan="4">
                                        <label style="color:red;"><?php echo esc_html__('Note : The included API add-on supports only Enterprise edition, Unlimited edition and Developer edition of sales force.' , 'wp-leads-builder-any-crm-pro'  ); ?></label>
                                </td></tr>
		</table>
		<br><br><br>
		<table style="float:right;margin-right:150px;">
			<tr>
		<!--	<td>

			<?php
                                                $auth_url =  "https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=" . $config['key'] . "&redirect_uri=" . urlencode($config['callback']);
                                                $auth_url = esc_url( $auth_url );
                                                ?>
                                
                                                <a href="<?php echo "$auth_url"?>" ><input name="submit" type="button" value="<?php echo esc_attr__('Authenticate' , 'wp-leads-builder-any-crm-pro' ); ?>" class="button-primary" style="margin-left:0px;float:left;"/> </a>

			</td> --!>
				<td>
				<input type="hidden" id="posted" name="posted" value="<?php echo 'posted';?>">
				<input type="hidden" id="site_url" name="site_url" value="<?php echo esc_attr($siteurl) ;?>">
                		<input type="hidden" id="active_plugin" name="active_plugin" value="<?php echo esc_attr($skinnyData['activatedplugin']); ?>">
                		<input type="hidden" id="leads_fields_tmp" name="leads_fields_tmp" value="smack_wpsalesforcepro_leads_fields-tmp">
                		<input type="hidden" id="contact_fields_tmp" name="contact_fields_tmp" value="smack_wpsalesforcepro_contacts_fields-tmp">
		<p class='submit' style='position:absolute'>
        <span style="float:right;padding-right:10px;bottom:70px;position:absolute"><input type="button" id="Save_crm_config" value="<?php echo esc_attr__('Save CRM Confiuration' , 'wp-leads-builder-any-crm-pro' );?>" id="save"  class="button-primary" onclick="saveCRMConfiguration(this.id);" />
				</span> </p>
				</td>
			</tr> 
		</table>
	</div>
</form>

<div id="loading-sync" style="display: none; background:url(<?php echo esc_url(WP_PLUGIN_URL);?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Syncing' , 'wp-leads-builder-any-crm-pro' ); ?>...</div>
<div id="loading-image" style="display: none; background:url(<?php echo esc_url(WP_PLUGIN_URL);?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Please Wait' , 'wp-leads-builder-any-crm-pro' ); ?>...</div>


