<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class FieldOperations
{
	public $nonceKey = null;
	public function __construct() {
		$helperObj = new OverallFunctionsPRO();
	}
	function saveFormFields( $options , $onAction , $editShortCodes , $formtype = "post" )
	{
		$HelperObj = new WPCapture_includes_helper_PRO();
		$module = $HelperObj->Module;
		$moduleslug = $HelperObj->ModuleSlug;
		$activatedplugin = $HelperObj->ActivatedPlugin;
		$activatedpluginlabel = $HelperObj->ActivatedPluginLabel;
		$save_field_config = array();
		$crmtype = sanitize_text_field($_REQUEST['crmtype']);
		$module = sanitize_text_field($_REQUEST['module']);
		$moduleslug = rtrim( strtolower($module) , "s");
		$options = "smack_fields_shortcodes";
		if( isset($_POST ['savefields'] ) && (sanitize_text_field($_POST ['savefields']) == "GenerateShortcode"))
		{
			$config_fields = get_option("smack_{$crmtype}_{$moduleslug}_fields-tmp");
			$config_contact_shortcodes = get_option($options);
		}
		else
		{
			$options = "smack_fields_shortcodes";
			$config_contact_shortcodes = get_option($options);
			$config_fields = $config_contact_shortcodes[$_REQUEST['EditShortcode']];

		}
		foreach( $config_fields as $shortcode_attributes => $fields )
		{
			if($shortcode_attributes == "fields")
			{
				foreach( $fields as $key => $field )
				{
					$save_field_config["fields"][$key] = $field;

					if( !isset($field['mandatory']) || $field['mandatory'] != 2 )
					{
						if(isset($_POST['select'.$key]))
						{
							$save_field_config['fields'][$key]['publish'] = 1;
						}
						else
						{
							$save_field_config['fields'][$key]['publish'] = 0;
						}
					}
					else
					{
						$save_field_config['fields'][$key]['publish'] = 1;
					}

					if( !isset($field['mandatory']) || $field['mandatory'] != 2 )
					{
						if(isset($_POST['mandatory'.$key]))
						{
							$save_field_config['fields'][$key]['wp_mandatory'] = 1;
							$save_field_config['fields'][$key]['publish'] = 1;
						}
						else
						{
							$save_field_config['fields'][$key]['wp_mandatory'] = 0;
						}
					}
					else
					{

						$save_field_config['fields'][$key]['wp_mandatory'] = 1;
					}

					$save_field_config['fields'][$key]['display_label'] = sanitize_text_field($_POST['fieldlabel'.$key]);
				}
			}
			else
			{
				$save_field_config[$shortcode_attributes] = $fields;
			}
		}
		if(!isset($save_fields_config["check_duplicate"]))
		{
			$save_fields_config["check_duplicate"] = 'none';
		}
		else if(isset($save_fields_config["check_duplicate"]) && ($save_fields_config["check_duplicate"] === 1))
		{
			$save_fields_config["check_duplicate"] === 'skip';
		}
		else if(isset($save_fields_config["check_duplicate"]) && ($save_fields_config["check_duplicate"] === 0))
		{
			$save_fields_config["check_duplicate"] = 'none';
		}

		$extra_fields = array( "formtype" , "enableurlredirection" , "redirecturl" , "errormessage" , "successmessage" , "assignedto" , "check_duplicate" , "enablecaptcha");

		foreach( $extra_fields as $extra_field )
		{
			if(isset( $_POST[$extra_field]))
			{
				$save_field_config[$extra_field] = $_POST[$extra_field];
			}
			else
			{
				unset($save_field_config[$extra_field]);
			}
		}
		for( $i = 0; $i < $_REQUEST['no_of_rows']; $i++ )
		{
			$REQUEST_DATA[$i] = $_REQUEST['position'.$i];
		}

		asort($REQUEST_DATA);

		$i = 0;
		foreach( $REQUEST_DATA as $key => $value )
		{
			$Ordered_field_config['fields'][$i] = $save_field_config['fields'][$key];
			$i++;
		}
		$save_field_config['fields'] = $Ordered_field_config['fields'];
		$save_field_config['crm'] = $_REQUEST['crmtype'];
		if( isset($_POST ['savefields'] ) && (sanitize_text_field($_POST ['savefields']) == "GenerateShortcode"))
		{
			$OverallFunctionObj = new OverallFunctionsPRO();
			$random_string = $OverallFunctionObj->CreateNewFieldShortcode( $_REQUEST['crmtype'] , $_REQUEST['module'] );
			$config_contact_shortcodes[$random_string] = $config_fields;
			update_option("smack_fields_shortcodes", $config_contact_shortcodes);
			update_option("smack_{$crmtype}_{$moduleslug}_fields-tmp" , $save_field_config);
			wp_redirect("".WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO."&__module=ManageShortcodes&__action=ManageFields&crmtype=$crmtype&module=$module&EditShortcode=$random_string&nonce_key=$this->nonceKey");
			exit;
		}
		else
		{
			$config_contact_shortcodes[$_REQUEST['EditShortcode']] = $save_field_config;
			update_option("smack_fields_shortcodes", $config_contact_shortcodes);
			update_option("smack_{$crmtype}_{$moduleslug}_fields-tmp" , $save_field_config);
		}
		$data['display'] = "";
		return $data;
	}

	function formFields( $options, $onAction, $editShortCodes , $formtype = "post" )
	{
		$siteurl= site_url();
		$CaptureData = new CaptureData();
		$module =$module_options ='Leads';
		$content1='';
		$config_leads_fields = $CaptureData->formfields_settings( $editShortCodes );
		$imagepath=$siteurl.'/wp-content/plugins/wp-leads-builder-any-crm-pro/images/' ;
		$imagepath = esc_url( $imagepath );
		$content='
		<input type="hidden" name="field-form-hidden" value="field-form" />
		<div>';
		$i = 0; 
		if(!isset($config_leads_fields['fields'][0]))
		{
			$content.='<p style="color:red;font-size:20px;text-align:center;margin-top:-22px;margin-bottom:20px;">'.__("Crm fields are not yet synchronised", "wp-leads-builder-any-crm-pro" ).' </p>';
		}
		else
		{
			$content.='<table style="background-color: #F1F1F1; border: 1px solid #dddddd;width:98%;margin-bottom:26px;margin-top:0px"><tr class="smack_highlight smack_alt" style="border-bottom: 1px solid #dddddd;"><th class="smack-field-td-middleit" align="left" style="width: 50px;"><input type="checkbox" name="selectall" id="selectall" onclick="selectAllPRO'."('field-form','".$module."')".';" style="margin-top:-3px"/></th><th align="left" style="width: 200px;"><h5>'.__('Field Name', 'wp-leads-builder-any-crm-pro' ).'</h5></th><th class="smack-field-td-middleit" align="left" style="width: 100px;"><h5>'.__('Show Field', 'wp-leads-builder-any-crm-pro' ).'</h5></th><th class="smack-field-td-middleit" align="left" style="width: 100px;"><h5>'.__('Order', 'wp-leads-builder-any-crm-pro' ).'</h5></th><th class="smack-field-td-middleit" style="width: 100px;" align="left"><h5>'.__('Mandatory' , 'wp-leads-builder-any-crm-pro' ).'</h5></th><th class="smack-field-td-middleit" style="width: 100px;" align="left"><h5>'.__('Field Label Display' , 'wp-leads-builder-any-crm-pro' ).'</h5></th></tr>';

			for($i=0;$i<count($config_leads_fields['fields']);$i++)
			{
				if( $config_leads_fields['fields'][$i]['wp_mandatory'] == 1 )
				{
					$madantory_checked = 'checked="checked"';
				}
				else
				{
					$madantory_checked = "";
				}

				if( isset($config_leads_fields['fields'][$i]['mandatory']) && $config_leads_fields['fields'][$i]['mandatory'] == 2)
				{

					if($i % 2 == 1)
						$content1.='<tr class="smack_highlight smack_alt">';
					else
						$content1.='<tr class="smack_highlight">';

					$content1.='
					<td class="smack-field-td-middleit"><input type="checkbox" name="select'.$i.'" id="select'.$i.'" disabled=disabled checked=checked ></td>
					<td>'.$config_leads_fields['fields'][$i]['label'].' *</td>
					<td class="smack-field-td-middleit">';
					{
						$content1.='<a name="publish'.$i.'" id="publish'.$i.'" onclick="'."alert('".__('This field is mandatory, cannot hide' , 'wp-leads-builder-any-crm-pro' )."')".'">
						<img src="'.$imagepath.'tick_strict.png"/>
						</a>';
					}
					$content1.='</td>
					<td class="smack-field-td-middleit">';
					$content1.= "<input class='position-text-box' type='textbox' name='position{$i}' value='".($i+1)."' >";
					$content1.='</td> 
					<td class="smack-field-td-middleit"><input type="checkbox" name="mandatory'.$i.'" id="mandatory'.$i.'" disabled=disabled checked=checked ></td>';
					$content1.='<td class="smack-field-td-middleit" id="field_label_display'.$i.'"><input type="text" id="field_label_display'.$i.'" name="fieldlabel'.$i.'" value="'.$config_leads_fields['fields'][$i]['display_label'].'"></td>

							</tr>';
				}
				else
				{
					if($i % 2 == 1)
						$content1.='<tr class="smack_highlight smack_alt">';
					else
						$content1.='<tr class="smack_highlight">';

					$content1.='<td class="smack-field-td-middleit">';
					if($config_leads_fields['fields'][$i]['publish'] == 1){
						$content1.= '<input type="checkbox" name="select'.$i.'" id="select'.$i.'">';
					}
					else
					{
						$content1.= '<input type="checkbox" name="select'.$i.'" id="select'.$i.'">';
					}
					$content1.='</td>
					<td>'.$config_leads_fields['fields'][$i]['label'].'</td>
					<td class="smack-field-td-middleit">';

					if($config_leads_fields['fields'][$i]['publish'] == 1){
						$content1.='<p name="publish'.$i.'" id="publish'.$i.'" >
						
						<span class="is_show_widget" style="color: #019E5A;">Yes</span>
						</p>';
					}
					else{
						$content1.='<p name="publish'.$i.'" id="publish'.$i.'" >
						
						<span class="not_show_widget" style="color: #FF0000;">No</span>
						</p>';
					}
					$content1.='</td>
					<td class="smack-field-td-middleit">';
					$content1.= "<input class='position-text-box' type='textbox' name='position{$i}' value='".($i+1)."' ></td>";
					$content1.=' <td class="smack-field-td-middleit">';
					if($config_leads_fields['fields'][$i]["wp_mandatory"] == 1)
					{
						$content1 .= '<p name="mandatory'.$i.'" id="mandatory'.$i.'" >
						
						<span class="is_show_widget" style="color: #019E5A;">'.__("Yes", "wp-leads-builder-any-crm-pro" ).'</span>
						</p>';
					}
					else
					{
						$content1 .= '<p name="mandatory'.$i.'" id="mandatory'.$i.'" >
						<span class="not_show_widget" style="color: #FF0000;">'.__("No", "wp-leads-builder-any-crm-pro" ).'</span>
						</p>';
					}
					$content1 .= '</td>';

					$content1.='<td class="smack-field-td-middleit" id="field_label_display'.$i.'"><input type="text" id="field_label_display_'.$i.'" name ="fieldlabel'.$i.'" value="'.$config_leads_fields['fields'][$i]['display_label'].'"></td>
			
	</tr>';
				}
			}
		}
		$content1.="<input type='hidden' name='no_of_rows' id='no_of_rows' value={$i} />";
		$content.=$content1;
		$content.= '</table>
		</div>';
		return $content;
	}

	function enableMandatoryFields( $selectedfields , $shortcode_name )
	{
		global $wpdb;
		$string = "";
		$enable_mandtry = $wpdb->get_results("select ffm.form_field_sequence , ffm.rel_id , sm.shortcode_id from wp_smackleadbulider_form_field_manager as ffm inner join wp_smackleadbulider_shortcode_manager as sm on ffm.shortcode_id = sm.shortcode_id where sm.shortcode_name = '$shortcode_name' order by ffm.form_field_sequence");
		if(!empty($selectedfields)) {
			foreach ( $selectedfields as $fields ) {
				$string .= "'" . $enable_mandtry[ $fields ]->rel_id . "',";
			}
		}
		$trim = rtrim($string, ',');
		$mandatory = $enable_mandtry[0]->shortcode_id;
		$enable_flds = $wpdb->query("update wp_smackleadbulider_form_field_manager set wp_field_mandatory = '1', state = '1' where rel_id in ($trim) and shortcode_id = '$mandatory'");
	}

	function disableMandatoryFields( $selectedfields , $shortcode_name )
	{
		global $wpdb;
		$string1 = "";

		$disable_mandtry = $wpdb->get_results( "select ffm.form_field_sequence , ffm.rel_id , sm.shortcode_id from wp_smackleadbulider_form_field_manager as ffm inner join wp_smackleadbulider_shortcode_manager as sm on ffm.shortcode_id = sm.shortcode_id where sm.shortcode_name = '$shortcode_name' order by ffm.form_field_sequence" );

		foreach($selectedfields as $fields)
		{
			$string1 .= "'" . $disable_mandtry[$fields]->rel_id . "',";
		}
		$trim1 = rtrim($string1, ',');
		$wps_mandatory = $disable_mandtry[0]->shortcode_id;
		$disable_flds = $wpdb->query("update wp_smackleadbulider_form_field_manager set wp_field_mandatory = '0' where rel_id in ($trim1) and shortcode_id = '$wps_mandatory'");
	}

	function saveFieldLabelDisplay( $fieldDisplayLabels , $shortcode_name )
	{
		global $wpdb;

		$fieldLabel = $wpdb->get_results( "select ffm.form_field_sequence , ffm.rel_id , sm.shortcode_id from wp_smackleadbulider_form_field_manager as ffm inner join wp_smackleadbulider_shortcode_manager as sm on ffm.shortcode_id = sm.shortcode_id where sm.shortcode_name = '$shortcode_name' order by ffm.form_field_sequence" );
		$wps_mandatory = $fieldLabel[0]->shortcode_id;
		foreach( $fieldDisplayLabels as $key => $fields )
		{
			$update_label = $wpdb->query( "update wp_smackleadbulider_form_field_manager set display_label = '{$fields}' where rel_id = {$fieldLabel[$key]->rel_id} and shortcode_id = '$wps_mandatory'" );
		}
	}

	function enableFields( $selectedfields , $shortcode_name )
	{
		global $wpdb;
		$string2 = "";
		$enable_showfields = $wpdb->get_results("select ffm.form_field_sequence , ffm.rel_id , sm.shortcode_id from wp_smackleadbulider_form_field_manager as ffm inner join wp_smackleadbulider_shortcode_manager as sm on ffm.shortcode_id = sm.shortcode_id where sm.shortcode_name = '$shortcode_name' order by ffm.form_field_sequence");
		if( isset( $selectedfields ) ) {
		foreach($selectedfields as $fields)
		{
			$string2 .= "'" . $enable_showfields[$fields]->rel_id . "',";
		}
		}
		$trim2 = rtrim($string2, ',');
		$wps_enablefields = $enable_showfields[0]->shortcode_id;
		$enable_crmfields = $wpdb->query("update wp_smackleadbulider_form_field_manager set state = '1' where rel_id in ($trim2) and shortcode_id = '$wps_enablefields'");
	}

	function disableFields( $selectedfields , $shortcode_name )
	{
		global $wpdb;
		$string3 = "";
		$disable_showfields = $wpdb->get_results("select ffm.form_field_sequence , ffm.rel_id , sm.shortcode_id from wp_smackleadbulider_form_field_manager as ffm inner join wp_smackleadbulider_shortcode_manager as sm on ffm.shortcode_id = sm.shortcode_id where sm.shortcode_name = '$shortcode_name' order by ffm.form_field_sequence");
		if( isset( $selectedfields ) ) {
		foreach($selectedfields as $fields)
		{
			$string3 .= "'" . $disable_showfields[$fields]->rel_id . "',";
		}
		}
		$trim3 = rtrim($string3, ',');
		$wps_disablefields = $disable_showfields[0]->shortcode_id;
		$disable_crmfields = $wpdb->query("update wp_smackleadbulider_form_field_manager set state = '0' where rel_id in ($trim3) and shortcode_id = '$wps_disablefields'");
	}

	function updateFieldsOrder( $selectedfields , $shortcode_name )
	{
		global $wpdb;
		for( $i = 0; $i < count($selectedfields); $i++ )
		{
			$REQUEST_DATA[$i+1] = $selectedfields[$i];
		}
		asort($REQUEST_DATA);
		$i = 1;
		foreach( $REQUEST_DATA as $key => $value )
		{
			$REQUEST_DATA_1[$key] = $i;
			$i++;
		}
		$update_field_order = $wpdb->get_results("select ffm.rel_id , ffm.form_field_sequence from wp_smackleadbulider_form_field_manager as ffm inner join wp_smackleadbulider_shortcode_manager as sm on sm.shortcode_id = ffm.shortcode_id  where sm.shortcode_name = '$shortcode_name' order by ffm.form_field_sequence");
		$newarray = array();
		$i = 1;
		foreach( $update_field_order as $fieldkey => $fieldvalue)
		{
			$newarray[$fieldvalue->rel_id] = $REQUEST_DATA_1[$i]; //$REQUEST_DATA[$fieldvalue->form_field_sequence];
			$i++;
		}
		foreach( $newarray as $key => $value )
		{
			$update_order = $wpdb->query("update wp_smackleadbulider_form_field_manager set form_field_sequence = {$value} where rel_id = {$key}");
		}
	}
}
class ManageShortcodesActions extends SkinnyActions {

	public $nonceKey = null;
	public function __construct()
	{
		$helperObj = new OverallFunctionsPRO();
	}

	/**
	 * The actions index method
	 * @param array $request
	 * @return array
	 */

	public function executeIndex($request)
	{
		// return an array of name value pairs to send data to the template
		$data = array();
		return $data;
	}

	public function executeView($request)
	{
		$data = array();
		$data['plugin_url']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
		$data['onAction'] = 'onCreate';
		$data['siteurl'] = site_url();
		$data['nonce_key'] = $this->nonceKey;
		return $data;
	}

	public function executeManageFields1($request)
	{
		$data = $request;
		return $data;
	}
	public function executeManageFields($request)
	{
		$FieldOperation = new FieldOperations();
		if( isset( $request['POST']['bulkaction'] ) )
		{
			$selectedfields = array();
			for( $i = 0 ; $i < $request['POST']['no_of_rows'] ; $i++ )
			{
				if( isset( $request['POST']['select'.$i] ) && ( $request['POST']['select'.$i] == 'on' ) )
				{
					$selectedfields[] = $i;
				}
				$fieldpostions[] = $request['POST']['position'.$i];
				$fieldLabelDisplay[] = $request['POST']['fieldlabel'.$i];
			}

			$bulkaction = $request['POST']['bulkaction'];
			$shortcode_name = $request['GET']['EditShortcode'];
			switch( $bulkaction )
			{
				case 'enable_field':
					$FieldOperation->enableFields( $selectedfields , $shortcode_name );
					break;
				case 'disable_field':
					$FieldOperation->disableFields( $selectedfields , $shortcode_name );
					break;
				case 'update_order':
					$FieldOperation->updateFieldsOrder( $fieldpostions , $shortcode_name );
					break;
				case 'enable_mandatory':
					$FieldOperation->enableMandatoryFields( $selectedfields , $shortcode_name );
					break;
				case 'disable_mandatory':
					$FieldOperation->disableMandatoryFields( $selectedfields , $shortcode_name );
					break;
				case 'save_field_label_display':
					$FieldOperation->saveFieldLabelDisplay( $fieldLabelDisplay , $shortcode_name );
					break;
			}

		//Action 1
		//support for Ninja forms 
		//first create the  ninjs form title  in wp_ninja_forms table
	
		//check the selected Third party plugin
		
		$get_edit_shortcode = $request['GET']['EditShortcode'];
		$thirdPartyPlugin = get_option('Thirdparty_'.$get_edit_shortcode);
		$get_thirdparty_title = get_option( $get_edit_shortcode );
		if($thirdPartyPlugin == 'ninjaform' && (!empty($request['POST']) || ($request['GET']))) 	{
			if( !empty( $get_thirdparty_title ) )
			{
				$title = $get_thirdparty_title;
			}
			else
			{
				$title = $request['GET']['crmtype'] . '-'.$request['GET']['module'].'-'.$request['GET']['EditShortcode'];
			}	
			$obj = new CallManageShortcodesCrmObj();
			// create the form in ninja form table
			$nin_form_id = $obj->ninjaFromTitle($title,$thirdPartyPlugin,$request['GET']['EditShortcode']);
		}

		if($thirdPartyPlugin == 'contactform' && (!empty($request['POST']) || ($request['GET'])))     {
			$title = $request['GET']['crmtype'] . '-'.$request['GET']['module'].'-'.$request['GET']['EditShortcode'];
			$obj = new CallManageShortcodesCrmObj();
		}

		if($thirdPartyPlugin == 'gravityform' && (!empty($request['POST']) || ($request['GET']))) 	{
			if( !empty( $get_thirdparty_title ) )
			{
				$title = $get_thirdparty_title;	
			}
			else
			{
				$title = $request['GET']['crmtype'] . '-'.$request['GET']['module'].'-'.$request['GET']['EditShortcode'];		
			}
			$obj = new CallManageShortcodesCrmObj();
			//create the form in gravity form table
			$gravity_form_id = $obj->gravityFromTitle($title,$thirdPartyPlugin,$request['GET']['EditShortcode']);

		}
	
		//Action 2
		// ninjs form field format
		if($thirdPartyPlugin == 'ninjaform' && !empty($request['POST']))        {
			$obj->formatNinjaFields($thirdPartyPlugin,$request['GET']['EditShortcode'],$nin_form_id,$bulkaction);
		}

		if($thirdPartyPlugin == 'contactform' && !empty($request['POST']))        {
			$get_edit_shortcode = $request['GET']['EditShortcode'];
 	               $get_thirdparty_title = get_option( $get_edit_shortcode );
	
			if( !empty($get_thirdparty_title  ))
			{
				$title = $get_thirdparty_title;
			}
			else
			{
				$title = $get_edit_shortcode;
			}	
			$obj->formatContactFields($thirdPartyPlugin, $title , $request['GET']['EditShortcode']);
		}

		if($thirdPartyPlugin == 'gravityform' && !empty($request['POST']))
		{
			if( !empty( $get_thirdparty_title ) )
                        {
                                $title = $get_thirdparty_title;
                        }
                        else
                        {
                                $title = $request['GET']['crmtype'] . '-'.$request['GET']['module'].'-'.$request['GET']['EditShortcode'];
                        }
			$obj->formatGravityFields($thirdPartyPlugin,$request['GET']['EditShortcode'],$gravity_form_id,$title);
		}

		}
		$data = array();

		foreach( $request as $key => $REQUESTS )
		{
			foreach( $REQUESTS as $REQUESTS_KEY => $REQUESTS_VALUE )
			{
				$data['REQUEST'][$REQUESTS_KEY] = $REQUESTS_VALUE;
			}
		}


		$data['HelperObj'] = new WPCapture_includes_helper_PRO();
		$data['module'] = $data["HelperObj"]->Module;
		$data['moduleslug'] = $data['HelperObj']->ModuleSlug;
		$data['activatedplugin'] = $data["HelperObj"]->ActivatedPlugin;
		$data['activatedpluginlabel'] = $data["HelperObj"]->ActivatedPluginLabel;
		$data['plugin_url']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
		$data['onAction'] = 'onCreate';
		$data['siteurl'] = site_url();
		$data['nonce_key'] = $this->nonceKey;
		if(isset($data['REQUEST']['formtype']))
		{
			$data['formtype'] = $data['REQUEST']['formtype'];
		}
		else
		{
			$data['formtype'] = "post";
		}

		if(isset($data['REQUEST']['EditShortcode']) && ( $data['REQUEST']['EditShortcode'] != "" ))
		{
			$data['option'] = $data['options'] = "smack_fields_shortcodes";
		}
		else
		{
			$data['option'] = $data['options'] = "smack_{$data['activatedplugin']}_{$data['moduleslug']}_fields-tmp";
		}

		if(isset($data['REQUEST']['EditShortcode']) && ( $data['REQUEST']['EditShortcode'] != "" ) )
		{
			$data['onAction'] = 'onEditShortCode';
		}
		else
		{
			$data['onAction'] = 'onCreate';
		}

		return $data;
	}

	public function executeCreateShortcode($request)
	{
		$data['HelperObj'] = new WPCapture_includes_helper_PRO();
		$crmtype = $data["HelperObj"]->ActivatedPlugin;
		$module = $request['GET']['moduletype'];
		$moduleslug = rtrim( strtolower($module) , "s");
		$tmp_option = "smack_{$crmtype}_{$moduleslug}_fields-tmp";
		// Function call
		$shortcodeObj = new CaptureData();
		$OverallFunctions = new OverallFunctionsPRO();
		$randomstring = $OverallFunctions->CreateNewFieldShortcode( $crmtype , $module );
		$config_fields['crm'] = $crmtype;
		$users_list = get_option('crm_users');
		$assignee = $users_list[$crmtype]['id'][0];
		$shortcode_details['name'] = $randomstring;
		$shortcode_details['type'] = 'post';
		$shortcode_details['assignto'] = $assignee;
		$shortcode_details['isredirection'] = $is_redirection;
		$shortcode_details['urlredirection'] = $url_redirection;
		$shortcode_details['captcha'] = $google_captcha;
		$shortcode_details['crm_type'] = $crmtype;
		$shortcode_details['module'] = $module;
		$shortcode_id = $shortcodeObj->formShorcodeManager($shortcode_details);
		$config_fields = $shortcodeObj->get_crmfields_by_settings($crmtype, $module);
		foreach( $config_fields as $field )
		{
			$shortcodeObj->insertFormFieldManager( $shortcode_id , $field->field_id , $field->field_mandatory , '1' , $field->field_type, $field->field_values , $field->field_sequence, $field->field_label );
		}

		$config_shortcodes = get_option("smack_fields_shortcodes");
		$config_shortcodes[$randomstring] = $config_fields;
		wp_redirect("".WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO."&__module=ManageShortcodes&__action=ManageFields&crmtype=$crmtype&module=$module&EditShortcode=$randomstring&nonce_key=$this->nonceKey");
		exit;
	}

	public function executeDelete($request)
	{
		global $wpdb;
		// return an array of name value pairs to send data to the template
		$data =array();
		$delete_short = $request['GET']['DeleteShortcode'];
		$deletedata = $wpdb->get_results("select shortcode_id from wp_smackleadbulider_shortcode_manager where shortcode_name = '$delete_short'");
		$deleteid = $deletedata[0]->shortcode_id;
		$delete_shortcode = $wpdb->query("delete from wp_smackleadbulider_shortcode_manager where shortcode_id = '$deleteid'");
		$delete_shortcode_fields = $wpdb->query( "delete from wp_smackleadbulider_form_field_manager where shortcode_id = '$deleteid'" );
		unset( $deletedata[$request["GET"]['DeleteShortcode']] );
		wp_redirect(WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO."&__module=ManageShortcodes&__action=view&nonce_key=$this->nonceKey");
		exit;
	}
}

class CallManageShortcodesCrmObj extends ManageShortcodesActions
{
	private static $_instance = null;
	public static function getInstance()
	{
		if( !is_object(self::$_instance) ) 
			self::$_instance = new CallManageShortcodesCrmObj();
		return self::$_instance;
	}

	public function ninjaFromTitle($title,$thirdparty,$shortcode)	{

		global $wpdb;
		$form_title = array();
		$form_title['form_title'] = $title;
		$formTitle = serialize($form_title);
		//get Ninja form fields
		//Todo for ninja form table prefix calculation
		$formid = $wpdb->get_var("select id from ". $wpdb->prefix ."nf_objects order by id desc limit 1");
		//  insert into title
		$id = $formid + 1;
		$date =  date("Y-m-d h:i:sa");
		$checkid = $wpdb->get_results("select * from wp_smackformrelation where  shortcode='{$shortcode}' and thirdparty='ninjaform'");
		if(empty($checkid)){
			$response = $wpdb->insert( $wpdb->prefix .'nf_objects' , array( 'id' => $id , 'type' => 'form' ) );
			$response1 = $wpdb->insert( $wpdb->prefix .'nf_objectmeta' , array( 'object_id' => $id ,'meta_key' => 'form_title' ,'meta_value' => $title ) );
			$wpdb->insert( $wpdb->prefix ."nf_objectmeta" , array( 'object_id' => $id , 'meta_key' => 'date_updated' , 'meta_value' => $date ) );
			$wpdb->insert( $wpdb->prefix ."nf_objectmeta" , array( 'object_id' => $id , 'meta_key' => 'clear_complete', 'meta_value' => '1' ) );
			$wpdb->insert( $wpdb->prefix ."nf_objectmeta" , array( 'object_id' => $id , 'meta_key' => 'hide_complete', 'meta_value' => '0' ) );
			$wpdb->insert( $wpdb->prefix ."nf_objectmeta" , array( 'object_id' => $id, 'meta_key' => 'name' , 'meta_value' => 'Success message' ) );
			$wpdb->insert( $wpdb->prefix ."nf_objectmeta" , array( 'object_id' => $id, 'meta_key' => 'type' , 'meta_value' => 'success_message' ) );
			$wpdb->insert( $wpdb->prefix ."nf_objectmeta" , array( 'object_id' => $id , 'meta_key' => 'success_msg' , 'meta_value' => 'Your form has been successfully submitted !' ) );

			//TODO need to create new table for relate the lead builder form and ninja forms
			//check id is already present in the wp_smackformrelation table
			$wpdb->insert( 'wp_smackformrelation' , array(  'shortcode' => $shortcode , 'thirdparty' => $thirdparty , 'thirdpartyid' => $id ) );
			return $id;
		}
		else {
			$thirdparty_formid = $checkid[0]->thirdpartyid;
			$wpdb->update( $wpdb->prefix ."nf_objectmeta" , array( 'meta_value' => $title ) , array( 'meta_key' => 'form_title' , 'object_id' => $thirdparty_formid ) );
			$id = $wpdb->get_var( $wpdb->prepare( "select object_id from ". $wpdb->prefix ."nf_objectmeta where meta_value=%s " , $title ) );
			//Update Title
			return $id;
		}
	}

	public function formatNinjaFields($thirdparty_form,$shortcode,$id,$action)	{
	
		global $wpdb;
		$ninja_array = array();
		//get fields from
		//TODO for every action (table entry changes and update option)
		switch($action)	{
			case 'enable_field':
				break;
			case 'disable_field':
				break;
			case 'update_order':
				break;
			case 'enable_mandatory':
				break;
			case 'disable_mandatory':
				break;
			case 'save_field_label_display':
				break;
			default :
				break;
		}
		$word_form_enable_fields = $wpdb->get_results("select a.rel_id,a.wp_field_mandatory,a.custom_field_type,a.custom_field_values,a.display_label from wp_smackleadbulider_form_field_manager as a join wp_smackleadbulider_shortcode_manager as b where b.shortcode_id=a.shortcode_id and b.shortcode_name='{$shortcode}' and a.state=1 order by form_field_sequence");


		//first delete the all reecord related to corressponding form
		//TODO don't do like that need to change it
		///check id exists in that table
		$checkid = $wpdb->get_results( $wpdb->prepare( "select * from wp_smackformrelation where thirdpartyid=%d and thirdparty=%s" , $id , 'ninjaform' ) );

		$active_crm = get_option( 'WpLeadBuilderProActivatedPlugin' );

		if(!empty($checkid)) {
			$wpdb->query( $wpdb->prepare( "delete from ". $wpdb->prefix ."ninja_forms_fields where form_id=%d" , $id ) );
			//delete the thungs from smackfieldrelation table  TODO this is not good
			$wpdb->query( $wpdb->prepare( "delete from wp_smackthirdpartyformfieldrelation where thirdpartyformid=%d" , $id ) );
			foreach($word_form_enable_fields as $key=>$value) {
				$type = $value->custom_field_type;
/*				$picklist_array = unserialize($value->custom_field_values) ;
			if( $active_crm == 'freshsales' && ($type == 'picklist' || $type == 'multipicklist' ) )
			{
				foreach( $picklist_array as $list_key => $list_val )
				{
					$picklist_array[$list_key] = array( 'id' => $list_val['id'] , 'label' => $list_val['label'] , 'value' => $list_val['id'] );	
				}
			}
*/
				$label = $value->display_label;	
				if( $active_crm == 'freshsales' && ($label == 'Time zone' ))
				{
					//Skip the time zone entry
				}			
				else
				{	
				switch($type) {
					case 'picklist':
						$type = 'list';
						$ninja_array['list']['options'] = unserialize($value->custom_field_values);
						$ninja_array['list_type'] = 'dropdown';
						break;
					case 'multipicklist':
						$type = 'list';
                                                $ninja_array['list']['options'] = unserialize($value->custom_field_values);
						$ninja_array['list_type'] = 'dropdown';
						break;

					case 'date':
						$type = 'text';
						$ninja_array['datepicker'] = '1';
						break;
					case 'string':
					case 'phone':
					case 'currency':
					case 'url':
					case 'email':
					case 'integer':
						$type = 'text';
						$ninja_array['datepicker'] = "";
						break;
					case 'boolean':
						$type = 'checkbox';
						$ninja_array['datepicker'] = "";
						break;
					case '':
						$type = 'text';
						$ninja_array['datepicker'] = "";
						break;
					default :
						$type = $type;
						$ninja_array['datepicker'] = "";
						break;
				}
				$type = '_'.$type;
				$ninja_array['label'] = $value->display_label;
				$ninja_array['label_pos'] = 'above';
				$ninja_array['user_info_field_group'] = '1';
				$ninja_array['req'] = $value->wp_field_mandatory;
				$ninja_arra = serialize($ninja_array);
				//insert into wp_ninja_forms_fields tables
						$wpdb->query("insert into ". $wpdb->prefix ."ninja_forms_fields (form_id,type,data,fav_id,def_id) values('{$id}','{$type}','{$ninja_arra}','NULL','NULL')");
				//get the current field id fomr wp_ninja_forms_fields tables

						$getCurrentId = $wpdb->get_var("select id from ". $wpdb->prefix ."ninja_forms_fields order by id desc limit 1");
				//get third party plugin name
				$thirdpartypluginname = $thirdparty_form;
				//map the smackc form anr ninja forms
							$wpdb->query("insert into wp_smackthirdpartyformfieldrelation (smackshortcodename,smackfieldid,smackfieldslable,thirdpartypluginname,thirdpartyformid,thirdpartyfieldids) values('{$shortcode}','{$value->rel_id}','{$value->display_label}','{$thirdpartypluginname}','{$id}','{$getCurrentId}')");
				
			}// Check for fresh sales time zone .. skip
			}
			$submit_array = array();
			$submit_array['label'] = 'Submit';
			$submit_array['show_help'] = 0;
			$submit_arr = serialize($sumit_arr);
					$wpdb->query("insert into ". $wpdb->prefix ."ninja_forms_fields (form_id,type,data,fav_id,def_id) values('{$id}','_submit','{$submit_arra}','NULL','NULL')");
		}

	}

	public function formatContactFields($thirdparty_form,$title,$shortcode){
		global $wpdb;
		$word_form_enable_fields = $wpdb->get_results("select a.rel_id,a.wp_field_mandatory,a.custom_field_type,a.custom_field_values,a.display_label from wp_smackleadbulider_form_field_manager as a join wp_smackleadbulider_shortcode_manager as b where b.shortcode_id=a.shortcode_id and b.shortcode_name='{$shortcode}' and a.state=1 order by form_field_sequence");
		$checkid = $wpdb->get_var( $wpdb->prepare( "select thirdpartyid from wp_smackformrelation where shortcode =%s and thirdparty=%s" , $shortcode , 'contactform' ) );

		if(!empty($checkid))
		{
			$wpdb->query( $wpdb->prepare( "delete from wp_smackthirdpartyformfieldrelation where thirdpartyformid=%d" , $checkid ) );
		}
		$contact_array = '';
		foreach($word_form_enable_fields as $key=>$value) {
			$type = $value->custom_field_type;
			$labl = $value->display_label;
			$label = preg_replace('/[^a-zA-Z]+/','_',$labl);
			$label = ltrim($label,'_');
			$mandatory = $value->wp_field_mandatory;
			$cont_array = array();
			$cont_array = unserialize($value->custom_field_values);
			$string ="";
			if( !empty( $cont_array ) )
			{
				foreach($cont_array as $val) {
					$string .= "\"{$val['label']}\" ";
				}
			}
			$str = rtrim($string,',');
			if($mandatory == 0)
			{
				$man ="";
			}
			else
			{
				$man ="*";
			}
			switch($type)
			{
				case 'phone':
				case 'currency':
				case 'text':
				case 'integer':
				case 'string':
					$contact_array .= "<p>".  $label ."".$man. "<br />
                                		 [text".$man." ".  $label."] </p>" ;
					break;

				case 'email':
					$contact_array .= "<p>".  $label ."".$man. "<br />
                                                [email".$man." ". $label."] </p>" ;
					break;
				case 'url':
					$contact_array .= "<p>".  $label ."".$man. "<br />
                                                [url".$man." ". $label."] </p>" ;
					break;
				case 'picklist':
					$contact_array .= "<p>".  $label ."".$man. "<br />
                                                [select".$man." ". $label." " .$str."] </p>" ;
					$str ="";
					break;
				case 'boolean':
					$contact_array .= "<p>
                                                [checkbox".$man." ". $label." "."label_first "."\" $label\""."] </p>" ;
					break;
				case 'date':
					$contact_array .= "<p>".  $label ."".$man. "<br />
                                            [date".$man." ". $label." min:1950-01-01 max:2050-12-31 placeholder \"YYYY-MM-DD\"] </p>" ;
					break;
				case '':
					$contact_array .= "<p>".  $label ."".$man. "<br />
                                                 [text".$man." ".  $label."] </p>" ;
					break;

				default:

					break;
			}
		}
		$contact_array .= "<p><br /> [submit "." \"Submit\""."]</p>";
		$meta = $contact_array;
//		$checkid = $wpdb->get_var( $wpdb->prepare( "select thirdpartyid from wp_smackformrelation where shortcode =%s and thirdparty=%s" , $shortcode , 'contactform' ) );
		$checkid = $wpdb->get_var( $wpdb->prepare( "select thirdpartyid from wp_smackformrelation inner join {$wpdb->prefix}posts on {$wpdb->prefix}posts.ID = wp_smackformrelation.thirdpartyid and {$wpdb->prefix}posts.post_status='publish' where shortcode =%s and thirdparty=%s" , $shortcode , 'contactform'  ) );

		if(empty($checkid))
		{
			$contform = array (
					'post_title'  => $title,
					'post_content'=> $contact_array,
					'post_type'   => 'wpcf7_contact_form',
					'post_status' => 'publish',
					'post_name'   => $shortcode
			);
			$id = wp_insert_post($contform);
			$content2 = "[contact-form-7 id=\"$id\" title=\"$shortcode\"]";
			$contform2 = array (
					'post_title'  => $id,
					'post_content'=> $content2,
					'post_type'   => 'post',
					'post_status' => 'publish',
					'post_name'   => $id
			);
			wp_insert_post($contform2);

			$post_id = $id;
			$meta_key ='_form';
			$meta_value = $meta;
			update_post_meta($post_id,$meta_key,$meta_value);
			$wpdb->query( "update wp_smackformrelation set thirdpartyid = {$id} where thirdparty='contactform' and shortcode ='{$shortcode}'" );
		}
		else
		{
			
			$wpdb->update( $wpdb->posts , array( 'post_content' => $contact_array , 'post_title' => $title ) , array( 'ID' => $checkid ) );
			$wpdb->update( $wpdb->postmeta , array( 'meta_value' => $meta ) , array( 'post_id' => $checkid , 'meta_key' => '_form'));
			$id = $checkid;
		}
		$thirdPartyPlugin = $thirdparty_form;
		$obj = new CallManageShortcodesCrmObj();
		$obj->contactFormRelation($shortcode,$id,$thirdPartyPlugin,$word_form_enable_fields);
	}

	public function contactFormRelation($shortcode,$id,$thirdparty,$enablefields)
	{
		global $wpdb;
		//TODO update tables
		$checkid = $wpdb->get_var( $wpdb->prepare( "select thirdpartyid from wp_smackformrelation where shortcode =%s" , $shortcode ) );
		if(empty($checkid))
		{
			$wpdb->insert( 'wp_smackformrelation' , array( 'shortcode' => $shortcode, 'thirdparty' => $thirdparty , 'thirdpartyid' => $id ) );
		}
		foreach($enablefields as $value)
		{
			$labl = $value->display_label;
			$labid = preg_replace('/[^a-zA-Z]+/','_',$labl);
			$labid = ltrim($labid,'_');
			$wpdb->insert( 'wp_smackthirdpartyformfieldrelation' , array( 'smackshortcodename' => $shortcode , 'smackfieldid' => $value->rel_id , 'smackfieldslable' => $value->display_label , 'thirdpartypluginname' => $thirdparty , 'thirdpartyformid' => $id , 'thirdpartyfieldids' => $labid ) );
		}
	}

	public function gravityFromTitle($title,$thirdparty,$shortcode)
	{
		global $wpdb;
		$formid = $wpdb->get_var("select id from ".$wpdb->prefix."rg_form order by id desc limit 1");
		$id = $formid + 1;

		$date =  date("Y-m-d h:i:sa");
				$checkid = $wpdb->get_results("select * from wp_smackformrelation where  shortcode='{$shortcode}' and thirdparty='gravityform'");
		if(empty($checkid))
		{
					$response = $wpdb->query("insert into ".$wpdb->prefix."rg_form(id,title,date_created) values('{$id}','{$title}','{$date}')");

					$wpdb->query("insert into wp_smackformrelation (shortcode,thirdparty,thirdpartyid) values('{$shortcode}','{$thirdparty}','{$id}')");
			return $id;
		}
		else
		{
			$thirdparty_formid = $checkid[0]->thirdpartyid;
			$wpdb->update( $wpdb->prefix ."rg_form" , array( 'title' => $title ) , array( 'id' => $thirdparty_formid ) );	
			$id = $wpdb->get_var("select id from ". $wpdb->prefix ."rg_form where title='{$title}'");
			return $id;
		}

	}

	public function formatGravityFields($thirdparty_form,$shortcode,$id,$title)
	{
		global $wpdb;
		$word_form_enable_fields = $wpdb->get_results("select a.rel_id,a.wp_field_mandatory,a.custom_field_type,a.custom_field_values,a.display_label from wp_smackleadbulider_form_field_manager as a join wp_smackleadbulider_shortcode_manager as b where b.shortcode_id=a.shortcode_id and b.shortcode_name='{$shortcode}' and a.state=1 order by form_field_sequence");
				$checkid = $wpdb->get_results("select * from wp_smackformrelation where thirdpartyid='{$id}' and thirdparty='gravityform'");

		if(!empty($checkid))
		{
			$wpdb->query("delete from ". $wpdb->prefix ."rg_form_meta where form_id='{$id}'");
			$wpdb->query("delete from wp_smackthirdpartyformfieldrelation where thirdpartyformid='{$id}'");
			$gravity_array['title'] = $title;
			$gravity_array['description'] = $shortcode;
			$gravity_array['labelPlacement'] = 'top_label';
			$gravity_array['descriptionPlacement'] = 'below';
			$gravity_array['button'] = array('type' =>'text' , 'text'=>'Submit' ,'imageUrl'=>'');
			$i = 0; $j =0;
			$active_crm = get_option( 'WpLeadBuilderProActivatedPlugin' );
			foreach($word_form_enable_fields as $key=>$value)
			{
				$i++;
				$type = $value->custom_field_type;
				$labl = $value->display_label;
				$label = array();

				$mandatory = $value->wp_field_mandatory;
				if($mandatory == 0)
				{
					$man ='';
				}
				else
				{
					$man ='1';
				}

				if( $active_crm == 'freshsales' && $labl == 'Time zone' )
				{
					// Skip for time zone
				}
				else
				{
				switch($type)
				{
					case 'phone':
					case 'currency':
					case 'text':
					case 'integer':
					case 'email':
					case 'url':
					case 'string':
					case '':
						$gravity_array['fields'][] =
								array(	'type'=>'text',
								          'id'=> $i ,
								          'label'=> $labl ,
								          'adminLabel'=>'',
								          'isRequired'=> $man ,
								          'size'=>'medium' ,
									  'errorMessage'=>'',
                                                                          'inputs'=>null,
                                                                          'labelPlacement'=>'',
                                                                          'descriptionPlacement'=>'',
                                                                          'subLabelPlacement'=>'',
                                                                          'placeholder'=>'',

								);
						break;
					case 'date':
						$gravity_array['fields'][] =
								array(
										'type' => 'date',
										'id' => $i ,
										'label' => $labl ,
										'adminLabel' => '' ,
										'isRequired' => $man ,
										'size' => 'medium' ,
										'dateType' => 'datepicker' ,
										'calendarIconType' => 'calander' ,
										'dateFormat' => 'ymd_dash' ,
										'pageNumber' => 1 ,
										'formId' => $id ,
								);
						break;
					case 'boolean':
						$bool_choice[] =array(
                                                                'text' => $labl,
                                                                'value' => $labl,
                                                                'isSelected' =>''
                                                );
						 $bool_input[] = array(
                                                                'id' => 1,
                                                                'label' => $labl,
                                                                'name' => ''
						);

						$gravity_array['fields'][] =
								array(  'type'=>'checkbox',
								        'id'=> $i ,
								        'label'=> $labl ,
								        'adminLabel'=>'',
								        'isRequired'=> $man ,
								        'size'=>'medium' ,
								        'pageNumber' => 1,
								        'choices' => $bool_choice,
								        'inputs' => $bool_input,
								        'formId'=>$id
								);
						unset( $bool_choice );
						unset( $bool_input );
						break;
					case 'picklist':
						$choice_array = array();
						$picklist = unserialize($value->custom_field_values);
						$j = 0;
						foreach($picklist as $val)
                                                { $j++;
							if( $active_crm == 'freshsales' )
							{
								$choice_array[] = array (
                                                                        'text'      => $val['label'],
                                                                        'value'     => $val['id'],
                                                                        'isSelected'=> ''
                                                        );
							}
							else {
								$choice_array[] = array (
										'text'      => $val['label'],
										'value'     => $val['label'],
										'isSelected'=> ''

                                                        );
							}
                                                }
						$gravity_array['fields'][] =
								array( 'type' => 'select' ,
								       'id' => $i ,
								       'label' =>$labl ,
								       'adminLabel' =>'' ,
								       'isRequired' => $man ,
								       'size' => 'medium',
								       'pageNumber' =>1 ,
								       'choices' => $choice_array,
								       'formId' => $id
								);
						unset( $choice_array );
						break;

				}

				$gravity_array['id'] = $id;
				$gravity_array['useCurrentUserAsAuthor'] = '1';
				$gravity_array['useCurrentUserAsAuthor'] = 'true';
                                $gravity_array['postContentTemplateEnabled'] = 'false';
                                $gravity_array['postTitleTemplateEnabled'] = 'false';
                                $gravity_array['postTitleTemplate'] = '';
                                $gravity_array['postContentTemplate'] = '';
                                $gravity_array['lastPageButton'] = null;
                                $gravity_array['pagination'] = null;
                                $gravity_array['firstPageCssClass'] = null;
				$grav_array = array();
				$grav_array = json_encode($gravity_array);
				//get third party plugin name
				$thirdpartypluginname = $thirdparty_form;
					$wpdb->query("insert into wp_smackthirdpartyformfieldrelation (smackshortcodename,smackfieldid,smackfieldslable,thirdpartypluginname,thirdpartyformid,thirdpartyfieldids) values('{$shortcode}','{$value->rel_id}','{$value->display_label}','{$thirdpartypluginname}','{$id}','{$labl}')");
			} // Skip fresh sales time zone
			}
			$confirm_array = array();
			$confirm_array[$id] = array(
					'id'          => $id ,
					'name'        => 'Default Confirmation',
					'isDefault'   => 1,
					'type'        => 'message' ,
					'message'     => 'Successfully Submitted !!' ,
					'url'         => '' ,
					'pageId'      => '' ,
					'queryString' => ''

			);
			$confirm = serialize($confirm_array);
			
			$notify_array = array();
			$notify_array[$id] = array(
					'id'     => $id ,
					'to'     => '{admin_email}' ,
					'name'   => 'Admin Notification' ,
					'event'  => 'form_submission' ,
					'toType' => 'email' ,
					'subject'=> 'New submission from {form_title}' ,
					'message'=> '{all_fields}'
			);
			$notify = serialize($notify_array);

					$wpdb->query("insert into ". $wpdb->prefix ."rg_form_meta (form_id,display_meta,confirmations,notifications) values('{$id}','{$grav_array}','{$confirm}','{$notify}')");

		}
	}

}
