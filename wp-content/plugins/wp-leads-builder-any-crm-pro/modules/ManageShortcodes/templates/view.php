<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$OverallFunctionsPROObj = new OverallFunctionsPRO();
$result = $OverallFunctionsPROObj->CheckFetchedDetails();

require_once( ABSPATH."wp-content/plugins/".WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO."/templates/thirdparty_mapping.php" );
if( !$result['status'] )
{
	$display_content = "<br>". $result['content']." to create Forms <br><br>";
	echo "<div style='font-weight:bold;  color:red; font-size:16px;text-align:center'> $display_content </div>";
}
else
{
	global $crmdetailsPRO;
	global $attrname;
	global $migrationmap;
	global $wpdb;
	$skinnyObj = CallManageShortcodesCrmObj::getInstance();
	$HelperObj = new WPCapture_includes_helper_PRO();
	$module = $HelperObj->Module;
	$moduleslug = $HelperObj->ModuleSlug;
	$activatedplugin = $HelperObj->ActivatedPlugin;
	$activatedpluginlabel = $HelperObj->ActivatedPluginLabel;
	$plugin_url= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
	$onAction= 'onCreate';
	$siteurl= site_url();
	$crm_users = get_option("crm_users");
	$users_detail = array();
	foreach( $crm_users[$activatedplugin]['id'] as $key => $value )
	{
		$users_detail[$value] = array( 'user_name' => $crm_users[$activatedplugin]['user_name'][$key] , 'first_name' => $crm_users[$activatedplugin]['first_name'][$key] , 'last_name' => $crm_users[$activatedplugin]['last_name'][$key]  );
	}


		$content1 = "";
		$content1 .= "<h3 style='margin-left:15px;'>".__('Forms and Shortcodes' , "wp-leads-builder-any-crm-pro" )." ( {$crmdetailsPRO[$activatedplugin]['Label']} ) : </h3>
			<div class='wp-common-crm-content'>
			<table style='margin-right:20px;margin-bottom:20px;border: 1px solid #dddddd;'>
				<tr style='border-top: 1px solid #dddddd;'>
				</tr>
				<tr class='smack-crm-pro-highlight smack-crm-pro-alt' style='border-top: 1px solid #dddddd;'>
					<th class='smack-crm-free-list-view-th' style='width: 300px;'>".__('Shortcode / Title' , 'wp-leads-builder-any-crm-pro' )."</th>
					<th class='smack-crm-free-list-view-th' style='width: 200px;'>".__('Assignee' , 'wp-leads-builder-any-crm-pro' )."</th>
					<th class='smack-crm-free-list-view-th' style='width: 200px;'>".__('Module' , 'wp-leads-builder-any-crm-pro' )."</th>
					<th class='smack-crm-free-list-view-th' style='width: 200px;'>".__('Thirdparty' , 'wp-leads-builder-any-crm-pro' )."</th>			

					<th class='smack-crm-free-list-view-th' style='width: 200px;'>".__('Actions' , 'wp-leads-builder-any-crm-pro' )."</th>
				</tr>";
						
			$shortcodemanager = $wpdb->get_results("select *from wp_smackleadbulider_shortcode_manager where crm_type = '{$activatedplugin}'");
			foreach($shortcodemanager as $shortcode_fields)
			{
				$content1 .= "<tr>";
				$shortcode_name = "[" . $shortcode_fields->crm_type . "-web-form name='" . $shortcode_fields->shortcode_name . "']";

				if( $shortcode_fields->assigned_to == "Round Robin" )
				{
					$assigned_to = "Round Robin";
				}
				else
				{
					$assigned_to = $users_detail[$shortcode_fields->assigned_to]['first_name']." ".$users_detail[$shortcode_fields->assigned_to]['last_name'];
				}
				$oldshortcodename = "";
				$oldshortcode_reveal_html = "";
				$oldshortcode_html = "";
				if( $shortcode_fields->old_shortcode_name != NULL )
				{
					$oldshortcodename = $shortcode_fields->old_shortcode_name;
					$oldshortcode_reveal_html = "<p><a style='cursor:pointer;' id='oldshortcodename_reveal{$shortcode_fields->shortcode_id}' onclick='jQuery(\"#oldshortcodename\"+{$shortcode_fields->shortcode_id}).show(); jQuery(\"#oldshortcodename_reveal\"+{$shortcode_fields->shortcode_id}).hide(); '> Click here to reveal old shortcode </a></p>";
					$oldshortcode_html = "<p style='display:none;' id='oldshortcodename{$shortcode_fields->shortcode_id}'> $oldshortcodename </p>";
				}

				$content1 .= "<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'>" . $shortcode_name . "$oldshortcode_reveal_html $oldshortcode_html</td>";
				$content1 .= "<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'>" . $assigned_to . "</td>";
				$content1 .= "<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'>" . $shortcode_fields->module . "</td>";
				$content1 .= "<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> None </td>";
				
				$content1 .= "<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align:center;'>";
				$content1 .= "<a href='".esc_url(WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO)."&__module=ManageShortcodes&__action=ManageFields&crmtype=". $shortcode_fields->crm_type ."&module=". $shortcode_fields->module ."&EditShortcode=". $shortcode_fields->shortcode_name ."' > Edit </a>";
				$content1 .= "<a href='".esc_url(WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO)."&__module=ManageShortcodes&__action=delete&DeleteShortcode=". $shortcode_fields->shortcode_name ."'style='padding-left:10px;' > Delete </a>";
				$content1 .= "</td>";
				$content1 .= "</tr>";
			}
		
			//Codes for getting Thirdparty existing forms
			$existing_content = '';
			$save_gravity_form_id = array();
			$gravity_option_name = $activatedplugin."_wp_gravity";
                        $list_of_shortcodes = $wpdb->get_results( $wpdb->prepare( "select option_name from {$wpdb->prefix}options where option_name like %s" , "$gravity_option_name%" ) );
                        if( !empty( $list_of_shortcodes ))
                        {
                                foreach( $list_of_shortcodes as $list_key => $list_val )
                                {
                                        $shortcode_name = $list_val->option_name;
                                        $form_id = explode( $gravity_option_name , $shortcode_name );
                                        $save_gravity_form_id[] = $form_id[1];
					
                                }
                        }
			foreach( $save_gravity_form_id as $grav_val )
			{
				$get_config = get_option($gravity_option_name."".$grav_val);
				$exist_module = $get_config['third_module'];
				$exist_assignee = $get_config['thirdparty_assignedto_name'];
				$get_form_title = $wpdb->get_results( $wpdb->prepare( "select title from {$wpdb->prefix}rg_form where id=%d" , $grav_val ) );
				$gravity_form_title = $get_form_title[0]->title;
				$third_plugin = $get_config['third_plugin'];
				$third_roundrobin = $get_config['tp_roundrobin'];

				$existing_content .= "<tr>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $gravity_form_title</td>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $exist_assignee</td>				
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $exist_module</td>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> Gravity Form</td>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'>"; 
				$existing_content .= "<a href='#' onclick='return show_map_config(\"$exist_module\" , \"$gravity_form_title\" , \"$grav_val\" , \"$third_plugin\" , \"$third_roundrobin\")'> Edit </a>";
                                $existing_content .= "<a href='#' onclick='return delete_map_config(\"$third_plugin\" , \"$grav_val\" );' style='padding-left:10px;'> Delete </a>";

				$existing_content .="</td></tr>";
			}

		//NINJA MAPPED FIELDS
			$save_ninja_form_id = array();
			$ninja_option_name = $activatedplugin."_wp_ninja";
                        $list_of_shortcodes = $wpdb->get_results( $wpdb->prepare( "select option_name from {$wpdb->prefix}options where option_name like %s" , "$ninja_option_name%" ) );
                        if( !empty( $list_of_shortcodes ))
                        {
                                foreach( $list_of_shortcodes as $list_key => $list_val )
                                {
                                        $shortcode_name = $list_val->option_name;
                                        $form_id = explode( $ninja_option_name , $shortcode_name );
                                        $save_ninja_form_id[] = $form_id[1];
					
                                }
                        }

			foreach( $save_ninja_form_id as $ninja_val )
			{
				$get_config = get_option($ninja_option_name."".$ninja_val);
				$exist_module = $get_config['third_module'];
				$exist_assignee = $get_config['thirdparty_assignedto_name'];
				$get_form_title = $wpdb->get_results( $wpdb->prepare( "select meta_value from {$wpdb->prefix}nf_objectmeta where object_id=%d and meta_key=%s" , $ninja_val, "form_title" ) );	
				$ninja_form_title = $get_form_title[0]->meta_value;
				$third_plugin = $get_config['third_plugin'];
				$third_roundrobin = $get_config['tp_roundrobin'];

				$existing_content .= "<tr><td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $ninja_form_title</td>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $exist_assignee</td>				
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $exist_module</td>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> Ninja Forms</td>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'>"; 
				$existing_content .= "<a href='#' onclick='return show_map_config(\"$exist_module\" , \"$ninja_form_title\" , \"$ninja_val\" , \"$third_plugin\" , \"$third_roundrobin\")'> Edit </a>";
                                $existing_content .= "<a href='#' onclick='return delete_map_config(\"$third_plugin\" , \"$ninja_val\");' style='padding-left:10px;'> Delete </a>";

				$existing_content .="</td></tr>";
			}
		//CONTACT FORM MAPPING
		
			$save_contact_form_id = array();
			$contact_option_name = $activatedplugin."_wp_contact";
                        $list_of_shortcodes = $wpdb->get_results( $wpdb->prepare( "select option_name from {$wpdb->prefix}options where option_name like %s" , "$contact_option_name%" ) );
                        if( !empty( $list_of_shortcodes ))
                        {
                                foreach( $list_of_shortcodes as $list_key => $list_val )
                                {
                                        $shortcode_name = $list_val->option_name;
                                        $form_id = explode( $contact_option_name , $shortcode_name );
                                        $save_contact_form_id[] = $form_id[1];
					
                                }
                        }

			foreach( $save_contact_form_id as $contact_val )
			{
				$get_config = get_option($contact_option_name."".$contact_val);
				$exist_module = $get_config['third_module'];
				$exist_assignee = $get_config['thirdparty_assignedto_name'];
				$get_form_title = $wpdb->get_results( $wpdb->prepare( "select post_title from $wpdb->posts where post_type=%s and ID=%d" , 'wpcf7_contact_form' , $contact_val ) );

				$contact_form_title = $get_form_title[0]->post_title;
				$third_plugin = $get_config['third_plugin'];
				$third_roundrobin = $get_config['tp_roundrobin'];

				$existing_content .= "<tr>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $contact_form_title</td>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $exist_assignee</td>			
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> $exist_module</td>	
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'> Contact Form7</td>
				<td class='smack-crm-pro-highlight' style='border-top: 1px solid #dddddd; text-align: center'>"; 
				$existing_content .= "<a href='#' onclick='return show_map_config(\"$exist_module\" , \"$contact_form_title\" , \"$contact_val\" , \"$third_plugin\" , \"$third_roundrobin\")'> Edit </a>";
                                $existing_content .= "<a href='#' onclick='return delete_map_config(\"$third_plugin\" , \"$contact_val\" );' style='padding-left:10px;'> Delete </a>";

				$existing_content .="</td></tr>";
			}
	
		$content1 .= $existing_content;	
		$content1 .= "</table></div>";
	echo $content1;
?> 
	<input class="button-primary" style="float:left;marginThirdparty1px;margin-left:25%;" type="submit" value="<?php echo esc_attr__('Create Lead Form' , "wp-leads-builder-any-crm-pro" ); ?>" onclick ="window.location='admin.php?page=wp-leads-builder-any-crm-pro/index.php&__module=ManageShortcodes&__action=CreateShortcode&moduletype=Leads'" id="generateleadsshortcode"/>

	<input class="button-primary" style="float:left;margin-top:-1px;margin-left:50px;" type="submit" value="<?php echo esc_attr__('Create Contact Form' , "wp-leads-builder-any-crm-pro" ); ?>" onclick ="window.location='admin.php?page=wp-leads-builder-any-crm-pro/index.php&__module=ManageShortcodes&__action=CreateShortcode&moduletype=Contacts'" id="generatecontactsshortcode"/>

	<input class="button-primary" style="float:left;margin-top:-1px;margin-left:65px;" type="button" id="thirdparty_map" value="<?php echo esc_attr__('Use Existing Form' , "wp-leads-builder-any-crm-pro" ); ?>" />

<head>
  <meta charset="utf-8">
</head>
<body>

<div class="container" style="width:100%;">
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="mapping-modalbox" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" id="close_map_modal" data-dismiss="modal" onclick="remove_map_contents();">&times;</button>
          <h4 class="modal-title" style="text-align:center;color:green;">Configure an existing Form</h4>
        </div>
        <div class="modal-body">
	<div style='padding-left:60px;' id="clear_contents">
          <p id='show_form_list'>
		<?php
			$mapping_ui_fields = new thirdparty_mapping();
			echo $mapping_ui_fields->get_mapping_config();
		?>
	  </p>
	</div>	

	  <p style='padding-left:60px;' id="display_form_lists">
	  </p>
	  <p style='padding-left:60px;' id="mapping_options">
	  </p>
	
	<form name='mapping_fields' id='mapping_fields' onsubmit="return false;">
	<div id="CRM_field_mapping" style="margin-top:50px;padding-left:60px;">
	

	 	</div>
	<div class="modal-footer">
	  <input type="button" class="btn btn-primary" name="map_crm_fields" value="Configure" id="map_fields" onclick="map_thirdparty_crm_fields();"> 
          <button type="button" id="close" class="btn btn-primary" data-dismiss="modal" onclick="remove_map_contents();">Close</button>
        </div>
	</form>
        </div>
       
      </div>
      
    </div>
  </div>
  
</div>


<script>

jQuery(document).ready(function(){
    jQuery( ".mapping-modalbox" ).hide();	
    jQuery("#thirdparty_map").click(function(){
        jQuery("#mapping-modalbox").modal();
	jQuery( "#clear_contents" ).show();
    });
});
</script>

</body>

<?php
}
?>
