<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$OverallFunctionsPROObj = new OverallFunctionsPRO();
$result = $OverallFunctionsPROObj->CheckFetchedDetails();

if( !$result['status'] )
{
        echo "<div style='font-weight:bold; padding-left:20px; color:red;'> {$result['content']} </div>";
}
else
{
	$siteurl = $skinnyData['siteurl'];
	$plug_url = WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO;
	$field_form_action = add_query_arg( array( '__module' => 'ManageShortcodes' , '__action' => 'ManageFields' , 'crmtype' => $skinnyData['REQUEST']['crmtype'] , 'module' => $skinnyData['REQUEST']['module'] , 'EditShortcode' => $skinnyData['REQUEST']['EditShortcode']) , $plug_url );
?>
	<form id="field-form" action="<?php echo $field_form_action .''; ?>" method="post">

	<h3 style="height: 42px;">

<?php
		global $crmdetailsPRO;
		$content = "";
if(isset($skinnyData['REQUEST']['EditShortcode']))
{
		$content .= "<span id='inneroptions' style='position:relative;left:5px;margin-left:15px;'>CRM Type  : ";
		$content .= "<span> ";
		foreach( $crmdetailsPRO as $crm_key => $crm_value )
		{
			if(isset($skinnyData['REQUEST']['crmtype']) && ($crm_key == $skinnyData['REQUEST']['crmtype'])){
				$select_option = " {$crm_value['crmname']} ";
			}
		}
		$content .= $select_option;
		$content .= "</span>";
		$content .= "</span>";

		echo $content;
}
else
{
                $content.= "<span id='inneroptions' style='position:relative;left:5px;margin-left:10px;'>CRM Type  : <select id='crmtype' name='crmtype' style='margin-left:8px;height:27px;' class=''
onchange = \"SelectFieldsPRO('{$skinnyData['siteurl']}','{$skinnyData['module']}','{$skinnyData['options']}', '{$skinnyData['onAction']}')\">";
                $select_option = "";
                $skinnyData['crmtype'] = "" ;

                $select_option .= "<option> --".__('Select' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO )."-- </option>";
                foreach( $crmdetailsPRO as $crm_key => $crm_value )
                {
			  if(isset($skinnyData['REQUEST']['crmtype']) && ($crm_key == $skinnyData['REQUEST']['crmtype'])){
                                $select_option.= "<option value='{$crm_key}' selected=selected > {$crm_value['crmname']} </option>";

                        }
                        else
                        {
                                $select_option.= "<option value='{$crm_key}'> {$crm_value['crmname']} </option>";
                        }
                }

                $content.= $select_option;

                $content.= "</select></span>";
                echo $content;
}
	?>
	<?php
		global $crmdetailsPRO;
		global $DefaultActivePluginPRO;

		$content = "";
if(isset($skinnyData['REQUEST']['EditShortcode']))
{
		$content .= "<span id='inneroptions' style='position:relative;left:40px;'>Module Type  : ";
                $content .= "<span> ";
                foreach( $crmdetailsPRO[$skinnyData['activatedplugin']]['modulename'] as $key => $value )
                {
                        if(isset($skinnyData['REQUEST']['module']) && ($skinnyData['REQUEST']['module'] == $key ) ){
                                $select_option = " {$value} ";
                        }
                }
                $content .= $select_option;
                $content .= "</span>";
                $content .= "</span>";
                echo $content;
}
else
{
                $content.= "<span id='inneroptions' style='position:relative;left:40px;'>Module Type  : <select id='module' name='module' style='margin-left:8px;height:27px;' onchange = \"SelectFieldsPRO('{$skinnyData['siteurl']}','{$skinnyData['module']}','{$skinnyData['options']}', '{$skinnyData['onAction']}')\" >";
                $select_option = "";
                $select_option .= "<option> --".__('Select' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO )."-- </option>";
                foreach( $crmdetailsPRO[$skinnyData['activatedplugin']]['modulename'] as $key => $value)
                {
                        if(isset($skinnyData['REQUEST']['module']) && ($skinnyData['REQUEST']['module'] == $key ) )
                        {
                                $select_option.= "<option value = '{$key}' selected=selected  > {$value}</option>";
                        }
                        else
                        {
                                $select_option.= "<option value = '{$key}' > {$value}</option>";
                        }
                }
                $content.= $select_option;

                $content.= "</select></span>";
                echo $content;
}
	?> 
	</h3>
	<?php
 	if(isset($skinnyData['REQUEST']['EditShortcode']) )
        {
        $skinnyData['onAction']='onEditShortCode';
        ?>
        <h3 id="innerheader" style="margin-bottom: 0px;">[<?php echo sanitize_text_field($skinnyData['activatedplugin']);?>-web-form name='<?php echo $skinnyData['REQUEST']['EditShortcode'];?>']</h3>
        <?php
        	$skinnyData['onAction']='onEditShortCode';
        }
        else
        {
	        $skinnyData['onAction']='onCreate';
        }
        ?>


<div class="wp-common-crm-content" style="background-color: white;" >
<div class="content" style="padding: 20px 0px;">
<div id="settingsavedmessage" style="height: 42px; display:none; color:red;">	</div>
<div id="savedetails" style="height: 90px; display:none; color:blue;">   </div>
<div id="url_post_id" style="display:none; color:blue;">  </div>
<h3 id="formtext" style=" margin:0px; padding: 10px 0px; "> <?php echo esc_html__('Form Settings' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?> :</h3>
<table>
	<tbody>

		<tr>
	<?php
		$formObj = new CaptureData();

                if(isset($skinnyData['REQUEST']['EditShortcode']) && ( $skinnyData['REQUEST']['EditShortcode'] != "" ))
                {
			$config_fields = $formObj->getFormSettings( $skinnyData['REQUEST']['EditShortcode'] );	// Get form settings 
		}

		$content = "";
		$content.= "<td id='inneroptions' style='width: 25%;'>".__('Form Type' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO )."  : </td><td style='width: 25%;'><select name='formtype'>";
		$formtypes = array( 'post' => __("Post" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ) , 'widget' => __("Widget" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ) );
		$select_option = "";
		foreach( $formtypes as $formtype_key => $formtype_value )
		{
			if( $formtype_key == $config_fields->form_type )
			{
				$select_option.= "<option value='{$formtype_key}' selected > {$formtype_value} </option>";
			}
			else
			{
				$select_option.= "<option value='{$formtype_key}'> {$formtype_value} </option>";
			}
		}

		$content.= $select_option;

		$content.= "</select></td>";

		echo $content;
	?>
		</tr>
		</tbody>
	</table>

	<table><tbody>
		<tr><td><br></td></tr>
 		<tr>
                <td style="width:138px">
                        <label><div id='innertext' style='float:left;padding-right: 5px;'><?php echo esc_html__("Duplicate handling" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></div><div style='float:right;'>:</div> </label>
                </td>
                <td style="width:90px">
			<span id="circlecheck">
                        <input type='radio'  name='check_duplicate' id='smack_capture_duplicates' value="skip" 
<?php
if( isset($config_fields->duplicate_handling) && ($config_fields->duplicate_handling == 'skip'))
{
        echo "checked=checked";
}
?>
>
			<label for="smack_capture_duplicates" style="margin-top:9px;" id='innertext'><?php echo esc_html__("Skip" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></label>
                        </span>
                </td>
                <td style="width:90px">
			<span id="circlecheck">
                        <input type='radio'  name='check_duplicate' id='smack_update_records' value= "update"
<?php
if(isset($config_fields->duplicate_handling ) && ($config_fields->duplicate_handling == 'update'))
{
        echo "checked=checked";
}
?>
>
			<label for="smack_update_records" style="margin-top:9px;" id='innertext'><?php echo esc_html__('Update' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></label>
                        </span>

                </td>
		<?php
		$activated_crm = get_option( 'WpLeadBuilderProActivatedPlugin' );
		if($activated_crm != 'freshsales' || ($activated_crm == 'freshsales' && $skinnyData['module'] != 'Contacts')) {
		?>
 		<td style="width:90px">
			<span id="circlecheck">
                        <input type='radio'  name='check_duplicate' id='smack_none_records' value="none"
<?php
if(!isset($config_fields->duplicate_handling ) || ( isset($config_fields->duplicate_handling) && ($config_fields->duplicate_handling=='none')))
{
        echo "checked=checked";
}
?>
>
			<label for="smack_none_records" style="margin-top:9px;" id='innertext'><?php echo esc_html__("None" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></label>
                        </span>

                </td>
		<?php } ?>
        </tr>
</tbody></table>

<table><tbody>
<br>
	<tr>
<td id='innertext' style="width:198px"><label><?php 
	$HelperObj = new WPCapture_includes_helper_PRO();
	$module = $HelperObj->Module;
	echo esc_html__("Assign" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO )." {$module} ".esc_html__("to User" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO );

?></label><div style='float:right;'> : </div>
</td>
<td  id="assignedto_td" style="padding-left:10px;">
<?php  
	$FunctionsObj = new PROFunctions();

	if(isset($skinnyData['REQUEST']['EditShortcode']))
	{
		$UsersListHtml = $FunctionsObj->getUsersListHtml( $skinnyData['REQUEST']['EditShortcode'] );
	}
	else
	{
        	$UsersListHtml = $FunctionsObj->getUsersListHtml();
	}

	echo " $UsersListHtml";

?>
	<input type='hidden' id='rr_first_userid' value="<?php echo $first_userid ;?>">
</td>
</tr>
<tr><td><br></td></tr>
<tr>
		    <td>
			<label id='innertext'>Error Message Submission</label><div style='float:right;'> : </div>
		    </td>
		    <td style="padding-left:10px;">
			<input type="text" name="errormessage" value="<?php if(isset($config_fields->error_message)) echo $config_fields->error_message; ?>" placeholder ="<?php echo esc_html__("Sorry, submission failed. Kindly try again after some time" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>" />
		    </td>
                   <td>
                        <div style ="position:relative;top:-9px;">
                        <a class="tooltip"  href="#" style="padding-left:8px">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                        <?php echo esc_html__("Message Displayed For Failed Submission." ,WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>
                        </span> </a>
                        </div>
                   </td>
		</tr><tr><td><br></td></tr>
		<tr>
		    <td>
			<label id='innertext'><?php echo esc_html__('Success Message Submission' , 'wp-leads-builder-any-crm-pro' ); ?></label><div style='float:right;'> : </div> 
		    </td>
		    <td style="padding-left:10px;">
			<input type="text" name="successmessage" value="<?php if(isset($config_fields->success_message)) echo $config_fields->success_message; ?>" placeholder ="Thanks for Submitting"/>
		   </td>
                   <td>
                        <div style ="position:relative;top:-9px;">
                        <a class="tooltip" href="#" style="padding-left:8px">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                         <?php echo esc_html__('Message Displayed For Successful Submission.' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>
                        </span> </a>
                        </div>

                   </td>
		</tr>
		<tr><td><br></td></tr>
		</tbody></table>
		<table><tbody>
		<tr>
		    <td style="width:199px">
			<label id='innertext'><?php echo esc_html__('Enable URL Redirection' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?> </label><div style='float:right;'> : </div>
		    </td><td style="width:90;padding-left:10px;">
			<div class="switch">
				<input type="checkbox" id='enableurlredirection' name="enableurlredirection" class="cmn-toggle cmn-toggle-yes-no" onclick="enableredirecturl(this.id);" value="on" <?php if(isset($config_fields->is_redirection) && ($config_fields->is_redirection == '1')){ echo "checked=checked"; } ?> />
				<label for="enableurlredirection" data-on="Yes" data-off="No"></label>
			</div>			
		    </td>
		    <td style="padding-left:30px;">
			<input id="redirecturl" type="text" name="redirecturl" <?php if(!isset($config_fields->is_redirection) == '1'){ echo "disabled=disabled";} ?> value="<?php if(isset($config_fields->url_redirection)) echo $config_fields->url_redirection; ?>" placeholder = "<?php echo esc_attr__('Page id or Post id' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>"/>
		    </td>
                    <td style="padding-left:10px;">
                        <div style ="position:relative;top:-9px;">
                        <a class="tooltip" href="#">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">      
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                        <?php echo esc_html__("(Give your custom success page url post id to redirect leads)." , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>
                        </span> </a>
                        </div>

                   </td>
		</tr>
<tr><td><br></td></tr>
		<tr>
		    <td>
			<label id='innertext'><?php echo esc_html__("Enable Google Captcha", "wp-leads-builder-any-crm-pro" ); ?> </label><div style='float:right;'> : </div>
		    </td>
		    <td style="padding-left:10px;">
			<div class="switch">
				<input type="checkbox" name="enablecaptcha" id="enablecaptcha"  class="cmn-toggle cmn-toggle-yes-no" value="on" <?php if(isset($config_fields->google_captcha ) && ($config_fields->google_captcha == '1'))  { echo "checked=checked"; } ?> />
				<label for="enablecaptcha" data-on="Yes" data-off="No"></label>	
			</div>
		    </td>
	            <td style="padding-left:36px;">
                        <div style="margin-left:-25px;margin-top:-11px">
                        <a class="tooltip" href="#">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">      
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                        <?php echo esc_html__('(Enable google recaptcha feature).' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>
                        </span> </a>
                        </div>

                   </td>

<!-- kjkkjkj -->
	<tr><td><br></td></tr>
<?php
$thirdparty_form = get_option( 'Thirdparty_'.$skinnyData['REQUEST']['EditShortcode'] );
?>
                <tr>
                    <td>
                        <label id='innertext'><?php echo esc_html__("Choose Thirdparty Form", "wp-leads-builder-any-crm-pro" ); ?> </label><div style='float:right;'> : </div>
                    </td>
                    
			<td> <select id='thirdparty_form_type' name='thirdparty_form_type' style='margin-left:10px;width:180px;' >";
                	<option value='none'>None</option>
                                <option value='ninjaform' <?php if($thirdparty_form == 'ninjaform') { echo "selected=selected"; }?>   >Ninja Forms</option>
                                <option value='contactform' <?php if($thirdparty_form == 'contactform') { echo "selected=selected"; }?> >Contact Form</option>
                                <option value='gravityform' <?php if($thirdparty_form == 'gravityform') { echo "selected=selected"; }?> >Gravity Forms</option>
                                </select></td> 

                        <td style="padding-left:36px;">
                        <div style="margin-left:-25px;margin-top:-11px">
                        <a class="tooltip" href="#">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">      
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                        <?php echo esc_html__('(Choose your Thirdparty form here).' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>
                        </span> </a>
                        </div>

                   </td>
<!-- lllklkll -->

<!-- Third party title  -->
<?php $thirdparty_title_key = $skinnyData['REQUEST']['EditShortcode'];
      $check_thirdparty_title = get_option( $thirdparty_title_key );
?>

                <tr>
                <td><br></td>
                </tr>
                <tr>
                    <td>
                        <label id='innertext'><?php echo esc_html__("Thirdparty Form Title", "wp-leads-builder-any-crm-pro" ); ?> </label><div style='float:right;'> : </div>
                    </td>
                    <td style="padding-left:10px;">
                        <div class="switch">
                                <input type="text" name="thirdparty_form_title" id="thirdparty_form_title" 
				<?php   if( !empty( $check_thirdparty_title ) )
					{ ?> value="<?php echo $check_thirdparty_title; }?>" />
                        </div>
                    </td>
                    <td style="padding-left:36px;">
                        <div style="margin-left:-25px;margin-top:-11px">
                        <a class="tooltip" href="#">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">      
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                        <?php echo esc_html__('(Enter thirdparty form title here).' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>
                        </span> </a>
                        </div>

                   </td>

                 </tr>


		 </tr>
		<tr>
			<td>
				<br>
				<input class="button button-primary" type="button" onclick="saveFormSettings('<?php echo esc_js($_REQUEST['EditShortcode']); ?>');" value="<?php echo esc_attr__("Save Form Settings" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>" name="SaveFormSettings" />
			</td>
		</tr>

	</tbody>
	</table>
        </div>
</div>

<span style="padding:10px; color:#FFFFFF; background-color: #37707D; text-align:center; float:right; font-weight:bold; cursor:pointer;" id ="showmore"><?php echo esc_html__("Form Options" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?> <i class="dashicons dashicons-arrow-down"></i></span>
<span style="padding:10px; color:#FFFFFF; background-color: #37707D; text-align:center; float:right; font-weight:bold; cursor:pointer;" id ="showless"><?php echo esc_html__("Form Options" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?> <i class="dashicons dashicons-arrow-up"></i></span>
<br>
<br>
<br>

<div class="wp-common-crm-content" style="background-color: white;" >	

<div>
<h3 id="formtext" style=" margin:0px; padding: 10px 0px; "> <?php echo esc_html__('Field Settings' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?> :</h3>

<select id="bulk-action-selector-top" name="bulkaction" style="margin: 0px 0px -67px;">
<option selected="selected" value="-1"><?php echo __('Bulk Actions' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></option>
<option value="enable_field"><?php echo __('Enable Field' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></option>
<option value="disable_field"><?php echo __('Disable Field' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></option>
<option value="update_order"><?php echo __('Update Order' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></option>
<option value="enable_mandatory"><?php echo __('Enable Mandatory' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></option>
<option value="disable_mandatory"><?php echo __('Disable Mandatory' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></option>
<option value="save_field_label_display"><?php echo __('Save Display Label' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?></option>
</select>


<input type="hidden" id="savefields" name="savefields" value="<?php echo esc_attr__('Apply' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>"/>
<?php
	if(isset($skinnyData['REQUEST']['EditShortcode']))
	{
		$content = "";
		$content.= "<input class='button-primary' type='submit' value='".__("Apply" , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO )."' style='background-color: #0074A2; border-color: #CCCCCC; color: #FFFFFF; font-weight:bold; margin-top: 30px; margin-right:78%; float:right;'  onclick =  \" return SaveCheckPRO('{$skinnyData['siteurl']}','{$skinnyData['module']}','{$skinnyData['options']}', '{$skinnyData['onAction']}')\" />";
		echo $content;
	}
?>

<div class="form-group"> <?php #echo esc_html($error); ?> </div>
<?php #echo $pagination; ?>

</div>
<script>
$(document).ready(function() {
        $( ".content" ).hide();
        $( "#showless" ).hide();

        $( "#showmore" ).click(function() {
        $( ".content" ).show( 500 );
        $( "#showless" ).show();
        $( "#showmore" ).hide();
        });

        $( "#showless" ).click(function() {
        $( ".content" ).hide( 500 );
        $( "#showless" ).hide();
        $( "#showmore" ).show();
        });
        
});
</script>

	<br> 
	<br>
		<div id="fieldtable">
	<?php
		$FieldOperations = new FieldOperations();
	if(isset($skinnyData['REQUEST']['EditShortcode']))
		echo $FieldOperations->formFields( "smack_fields_shortcodes" , "onEditShortCode" , $skinnyData['REQUEST']['EditShortcode'] , $skinnyData['formtype'] );
	else
		echo $FieldOperations->formFields( $skinnyData['option'] , $skinnyData['onAction'] , '' , $skinnyData['formtype'] );

	?>
	</div>
</div>

<script>
function showAccordion( id )
{
	if(jQuery("#advance_option_display").val() == 0 )
	{
		jQuery("#advance_option").css("display", "block");
		jQuery("#advance_option_display").val(1);
		jQuery("#accordion_arrow").removeClass( "fa-chevron-right" );
		jQuery("#accordion_arrow").addClass( "fa-chevron-down" );
	}
	else
	{
                jQuery("#advance_option").css("display", "none");
                jQuery("#advance_option_display").val(0);
		jQuery("#accordion_arrow").removeClass( "fa-chevron-down" );
		jQuery("#accordion_arrow").addClass( "fa-chevron-right" );
	}
}

</script>
<br>
</form>

<div id="loading-image" style="display: none; background:url(<?php echo esc_url($siteurl); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Please Wait' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ); ?>...</div>
<?php
}
?>
