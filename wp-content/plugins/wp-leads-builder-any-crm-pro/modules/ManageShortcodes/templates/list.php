<?php
/**
 * new table class that will extend the WP_List_Table
 */
class Traitify_List_Table extends WP_List_Table
{
	public $limit = 10;

	/**
	 * Prepare the items for the table to process
	 *
	 * @return Void
	 */
	public function prepare_items()
	{
		$columns = $this->get_columns();
		$hidden = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();

		$data = $this->table_data();
		usort( $data, array( &$this, 'sort_data' ) );

		$perPage = $this->limit;
        	$currentPage = $this->get_pagenum();
        	$totalItems = count($data);

		$this->set_pagination_args( array(
            		'total_items' => $totalItems,
            		'per_page'    => $perPage
        	));

		$data = array_slice($data, (($currentPage-1) * $perPage), $perPage);

		$this->_column_headers = array($columns, $hidden, $sortable);
		$this->items = $data;
	}

	/**
	 * Override the parent columns method. Defines the columns to use in your listing table
	 *
	 * @return Array
	 */
	public function get_columns($module = "")
	{
		$columns = array(
				'checkbox'		=> '<input class = checkboxclass" type = "checkbox" name ="selectall" id="selectall" onclick="selectAllPRO(this);">',
				'fieldname'          	=> 'Field Name',
				'showfield'       	=> 'Show Field',
				'order' 		=> 'Order',
				'mandatory'		=> 'Mandatory',
				'field_display'     	=> 'Field Label Display',
				);

		return $columns;
	}

	/**
	 * Allows you to sort the data by the variables set in the $_GET
	 *
	 * @return Mixed
	 */
	private function sort_data( $a, $b )
	{
		// Set defaults
		$orderby = 'field_display';
		$order = 'asc';

		// If orderby is set, use this as the sort column
		if(!empty($_GET['orderby']))
		{
			$orderby = $_GET['orderby'];
		}

		// If order is set use this as the order
		if(!empty($_GET['order']))
		{
			$order = $_GET['order'];
		}

		$result = strnatcmp( $a[$orderby], $b[$orderby] );

		if($order === 'asc')
		{
			return $result;
		}

		return -$result;
	}

	/**
	 * Define the sortable columns
	 *
	 * @return Array
	 */
	public function get_sortable_columns()
	{
		return array('field_display' => array('field_display', false));
	}

	/**
	 * Define which columns are hidden
	 *
	 * @return Array
	 */
	public function get_hidden_columns()
	{
		return array();
	}


	/**
	 * Get the table data
	 *
	 * @return Array
	 */
	private function table_data()
	{
		global $wpdb;
		$shortcode_name = $_REQUEST['EditShortcode'];
        	$get_fields = $wpdb->get_results("select a.field_name , a.field_label , a.field_mandatory , b.* , c.* from wp_smackleadbulider_field_manager as a inner join wp_smackleadbulider_form_field_manager as b on a.field_id = b.field_id inner join wp_smackleadbulider_shortcode_manager as c on c.shortcode_id = b.shortcode_id where c.shortcode_name = '{$shortcode_name}' order by b.form_field_sequence");

		$data = array();
		$i = 0;
		foreach($get_fields as $value)	
		{
			$data[] = array(
				'checkbox' => '<input class = "checkboxclass" type = "checkbox" style = "margin-left: 8px;" name ="select'.$i.'" id="select'.$i.'">',
				'fieldname' => $value->display_label,
				'showfield' => $value->state,
				'order' => $value->form_field_sequence,
				'mandatory' => $value->wp_field_mandatory,
				'field_display' => $value->field_label,
			);
			$i++;
		}
		return $data;
	}

	/**
	 * return list of tests
	 * @parma integer $user_id
	 * @return array $getTests;
	 */
	public function getListOfTest($user_id)	{
		global $wpdb, $traitify;
		$user_tests = $wpdb->get_results("select type, assessment_id from {$wpdb->prefix}{$traitify->table_name} where user_id = '$user_id'");
		return $user_tests;
	}

	/**
	 * return total test count
	 * @param integer $user_id
	 * @return integer $count
	 */
	public function getTotalTest($user_id)	{
		global $wpdb, $traitify;
		$count = 0;
		$getCount = $wpdb->get_results("select count(*) as count from {$wpdb->prefix}{$traitify->table_name} where user_id = '$user_id'");
		if(!empty($getCount))
			$count = $getCount[0]->count;

		return $count;
	}

	/**
         * return assessment id
         * @param string $type
         * @param integer $user_id
         * @return string $assessmentId
         */
        public function getUsersList($limit = 10, $page = 0)        {
                global $wpdb, $traitify;
	        $getUsersList = $wpdb->get_results("select * from {$wpdb->prefix}{$traitify->table_name} group by user_id");
                return $getUsersList;
        }

	// Used to display the value of the id column
	public function column_id($item)
	{
		return $item['id'];
	}

	/**
	 * Define what data to show on each column of the table
	 *
	 * @param  Array $item        Data
	 * @param  String $column_name - Current column name
	 *
	 * @return Mixed
	 */
	public function column_default($item, $column_name)
	{
		switch($column_name)	{
			case 'checkbox':
				return $item[$column_name];
			case 'fieldname':
				return $item[$column_name];
			case 'showfield':
				if($item[$column_name] == 1)	{
					$show_field ='<span class="is_show_widget" style="color: #019E5A;">Yes</span>';
				}
				else	{
					$show_field ='<span class="is_show_widget" style="color: #FF0000;">No</span>';
				}
				return $show_field;
			case 'order':
				$order_html = "<input type = 'text' class = 'position-text-box sequence-text' name = 'position' value = '{$item[$column_name]}'>";
				return $order_html;
			case 'mandatory':
				if($item[$column_name] == 1)	{
					$mandatory ='<span class="is_show_widget" style="color: #019E5A;">Yes</span>';
				}
				else	{
					$mandatory ='<span class="is_show_widget" style="color: #FF0000	;">No</span>';
				}
				return $mandatory;
			case 'field_display':	
				return $item[$column_name];
			default:
				return print_r($item, true);

		}
	}
}

