<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$siteurl = site_url();
$siteurl = esc_url( $siteurl );
$config = get_option("wp_thirdpartyplugin_settings");

/* define the plugin folder url */
define('WP_PLUGIN_URL', plugin_dir_url(__FILE__));

?>

</script>
<script>
$(document).ready( function(){
        $( "#dialog-confirm-thirdparty" ).hide();
	$( "#dialog-activate-thirdparty" ).hide();
});
</script>
<div id="dialog-confirm-thirdparty" title="Switch the plugin">
  <p><span  style="float:left; margin:0 7px 20px 0;"></span>Do you want to change Form Type??</p>
</div>

<div id="dialog-activate-thirdparty" title="Plugin inactive">
  <p><span  style="float:left; margin:0 7px 20px 0;"></span>You should activate the plugin first</p>
</div>

<?php
        $Thirdparty_plugin = get_option( "WpLeadThirdPartyPLugin" );			
?>
<input type="hidden" id="third_plugin_value" value='<?php echo $Thirdparty_plugin ;?>'>
<div>
<!--  Start -->
	<form id="smack-thirdparty-settings-form" action="" method="post">
		<input type="hidden" name="smack-thirdparty-settings-form" value="smack-thirdparty-settings-form" />
		<input type="hidden" id="plug_URL" value="<?php echo esc_url(WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO);?>" />
		<div class="wp-common-crm-content" style="width: 800px;float: left;">
<!--		<table>
			<tr>
				<div id ="dialog-modal"> 
					<p> Settings Saved Successfully ! </p>

				</div>

			</tr>
			<tr>
				<td><label id="inneroptions" style="margin-top:15px;margin-left:4px"><?php echo esc_html__('Choose your Form Type        ' , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?> <?php echo str_repeat( '&nbsp' , '20' ); ?>  </label></td>
				<td>
			<?php
				$ContactFormPluginsObj = new ContactFormPROPlugins();
				echo $ContactFormPluginsObj->getThirdpartyPLugins();
			?>
				</td>
			  <td>
                                <div style="position:relative;top:-11px;left:10px;">
                                <a class="tooltip"  href="#">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                                <span class="tooltipPostStatus" style="width:130px;">
                                <img src="<?php echo $siteurl; ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                                <?php echo esc_html__('Choose your Third party plugin.' , 'wp-leads-builder-any-crm-pro' ); ?>
                                </span> </a>
                                </div>
                          </td>

			</tr>

		</table> -->
		</div>
	
<script>
jQuery( "#dialog-modal" ).hide();
</script>
	<span id="Fields" style="margin-right:20px;"></span>
	</form>
<!-- End-->
</div>

<div id="loading-image" style="display: none; background:url(<?php echo esc_url(WP_PLUGIN_URL);?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Please Wait...' , 'wp-leads-builder-any-crm-pro'  ); ?> </div>

<?php
	echo str_repeat( '<br>' , 2 );   
	$config = get_option( "wp_captcha_settings" );
	$captcha = WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ;
	$captcha_form_url = add_query_arg( array( '__module' => 'Thirdparty' , '__action' => 'view'  ) , $captcha );
?>

	
         <div class = "captcha" style="margin-left:20px;"> 
	<form id="smack-<?php echo sanitize_text_field($skinnyData['activatedplugin']);?>-captcha-form" action="<?php echo esc_url( $captcha_form_url ) ; ?>" method="post">
        <input type="hidden" name="smack-<?php echo esc_attr($skinnyData['activatedplugin']);?>-captcha-form" value="smack-<?php echo sanitize_text_field($skinnyData['activatedplugin']);?>-captcha-form" />
		<table class="settings-table">
		<tr><td colspan="4">
                        <label id="inneroptions" ><?php echo esc_html__('Debug and Notification :' , 'wp-leads-builder-any-crm-pro' );?> </label>
                </td></tr>
			<tr>	
                        <td  style="width:250px;padding-left:40px;">
                        <label id="innertext" ><?php echo esc_html__('Which log do you need?' , 'wp-leads-builder-any-crm-pro' );?> </label>
                        <div style="float:right">:</div>
                        </td>
                        <td>
                        <span id="circlecheck">
                           <select name="emailcondition" id="emailcondition" onchange="enablesmackemail(this.id)">
                                <option value="none" id='smack_email'
                                <?php
                                if(isset($config['emailcondition']) && $config['emailcondition'] == 'none')
                                {
                                        echo "selected=selected";
                                }
                                ?>
                                >None</option>
                                <option value = "success" id= 'successemailcondition'
                                <?php
                                if(isset($config['emailcondition']) && $config['emailcondition'] == 'success')
                                {
                                        echo "selected=selected";
                                }
                                ?>
                                >Success</option>
                                <option value="failure"  id = 'failureemailcondition'
                                <?php
                                        if(isset($config['emailcondition']) && $config['emailcondition'] == 'failure')
                                {
                                        echo "selected=selected";
                                }
                                ?> 
                                >Failure</option>
                                <option value="both" id = 'bothemailcondition'
                                <?php
                                        if(isset($config['emailcondition']) && $config['emailcondition'] == 'both')
                                {
                                        echo "selected=selected";
                                }
                                ?> 
				>Both</option>
                        </select>
                        </td>
                </tr>
                 <tr>
                         <td style='width:50px;padding-left:40px;'>
                                <label id="innertext"> <?php echo esc_html__('Specify Email', "wp-leads-builder-any-crm-pro" ); ?> </label><div style='float:right;'> : </div>
                        </td>
                        <td>
                                 <input type='text' class='smack-vtiger-settings' name='email' id='email' value="<?php if(isset($config['email'])) { echo sanitize_email($config['email']); } ?>" <?php if( isset( $config['emailcondition']) && $config['emailcondition'] == 'none' ){ ?> disabled="disabled" <?php } ?> />
                        </td>
                </tr>
                <tr>
                        <td style='width:50px;padding-left:40px;'>
                                <label id="innertext"><?php echo esc_html__('Enable Debug mode ' , 'wp-leads-builder-any-crm-pro' ); ?></label><div style='float:right;'>:</div>
                        </td>
                        <td>
                                <div class="switch">
                                        <input type='checkbox' class='smack-vtiger-settings-text cmn-toggle cmn-toggle-yes-no' name='debugmode' id='debugmode'  <?php if(isset($config['debugmode']) && sanitize_text_field($config['debugmode']) == 'on') { echo "checked=checked"; } ?> onclick="debugmod(this.id)"/>
                                        <label for="debugmode" id="innertext" data-on="On" data-off="Off"></label>
                                </div>
                </tr>
        </table>
		<br><br>
                <table>
                        <tr>
                                <td>
                                        <label id="inneroptions" style=" margin-top: 0px; margin-left: 0px;" ><?php echo esc_html__("Do you want to enable the captcha " , "wp-leads-builder-any-crm-pro" ); ?> <?php echo str_repeat( '&nbsp' , 2 ); ?>  </label>
                                </td>
                                <td>
                                <span id="circlecheck">
                                        <input type='radio'  name='smack_recaptcha' id='smack_recaptcha_no'  value="no"
                <?php
                if($config['smack_recaptcha']=='no' || !isset($config['smack_recaptcha']))
                {
                        echo "checked";
                }
                ?>
                 onclick="showOrHideRecaptchaPRO('no');"> <label for="smack_recaptcha_no"  id="innertext" style='margin-left: 5px; margin-bottom: 0px;  margin-top: 0px;'> <?php echo esc_html__('No' , 'wp-leads-builder-any-crm-pro' ); ?> </label>
                                        <input type='radio'  name='smack_recaptcha' id='smack_recaptcha_yes' style="margin-left:20px;" value="yes" 
                <?php
                if($config['smack_recaptcha']=='yes')
                {
                        echo "checked";
                }
                ?>
                 onclick="showOrHideRecaptchaPRO('yes');"> <label for="smack_recaptcha_yes"  id="innertext" style=' margin-bottom: 0px; margin-top: 0px;'> <?php echo esc_html__('Yes' , 'wp-leads-builder-any-crm-pro' ); ?> </label>
                                </span>
                                </td>
                        </tr>
                </table>
		<table>

                        <tr id="recaptcha_public_key"
                <?php

                if($config['smack_recaptcha']=='no' || !isset($config['smack_recaptcha']))
                {
                        echo 'style="display:none"';
                }
                else
                {
                        echo 'style="display:block;margin-top:18px;"';
                }

                ?>
                >
                                <td style='width:275px;'>
                                        <label id="innertext"><?php echo esc_html__('Google Recaptcha Public Key' , 'wp-leads-builder-any-crm-pro' ); ?>  <?php echo str_repeat( '&nbsp;' , 50 );?>   </label>
                                </td>
                                <td>
                                        <div style ="position:relative;left:10px;">
                                                <input type='text' style="width: 250px;" class='smack-vtiger-settings-text' placeholder='<?php echo esc_attr__('Enter your recaptcha public key here', 'wp-leads-builder-any-crm-pro' ); ?>' name='recaptcha_public_key' id='smack_public_key' value="<?php echo sanitize_text_field($config['recaptcha_public_key']) ?>"/>
                                        </div>
                                </td>
                                <td >
                                        <div style ="padding-left:20px; position:relative;top:-9px;">
                                        <a class="tooltip"  href="#">
                                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                                        <span class="tooltipPostStatus">
                                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                                        <?php echo __('Enter your recaptcha public key here.', 'wp-leads-builder-any-crm-pro' ); ?>
                                        <img style="margin-top: 6px;float:right;" src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                                        </span> </a>
                                        </div>
                                </td>
                        </tr>

			<tr id="recaptcha_private_key" <?php

                if($config['smack_recaptcha']=='no' || !isset($config['smack_recaptcha']))
                {
                        echo 'style="display:none"';
                }
                else
                {
                        echo 'style="display:block;margin-top:13px"';
                }

                ?>
                >
                                <td style='width:283px;'>
                                        <label id="innertext"><?php echo esc_html__("Google Recaptcha Private Key", "wp-leads-builder-any-crm-pro" ); ?></label><?php echo str_repeat( '&nbsp;' , 50 ); ?>
                                </td>
                                <td>
                                        <div style = "position:relative;left:2px;">
                                                <input type='text' style="width: 250px;" class='smack-vtiger-settings-text' placeholder='<?php echo esc_attr__("Enter your recaptcha private key here" , "wp-leads-builder-any-crm-pro" ); ?>' name='recaptcha_private_key' id='smack_private_key' value="<?php echo $config['recaptcha_private_key'] ?>"/>
                                        </div>
                                </td>
                                <td>
                                        <div style ="padding-left:14px; position:relative;top:-12px;">
                                        <a class="tooltip"  href="#">
                                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                                        <span class="tooltipPostStatus">
                                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                                        <?php echo __("Enter your recaptcha private key here." , "wp-leads-builder-any-crm-pro" ); ?>
                                        <img style="margin-top: 6px;float:right;" src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                                        </span> </a>
                                        </div>
                                </td>

                        </tr>
                </table>
	<br><br><br>
		<table style="float:right;margin-right:30px;">
                        <tr>
                                <td>
                                        <input type="hidden" name="posted" value="<?php echo 'posted';?>">
                                        <p class="submit">
                                                <input type="submit" value="<?php echo esc_attr__('Save' , 'wp-leads-builder-any-crm-pro' );?>" id="innersave" class="button-primary"/>
                                        </p>
                                </td>
                        </tr>
                </table>
                  </form>
                </div>
