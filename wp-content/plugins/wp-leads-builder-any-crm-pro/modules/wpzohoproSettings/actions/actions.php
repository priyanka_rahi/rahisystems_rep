<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class WpzohoSettingsActions extends SkinnyActions {

	public function __construct()
	{
	}

	/**
	* The actions index method
	* @param array $request
	* @return array
	*/
	public function executeIndex($request)
	{
		// return an array of name value pairs to send data to the template
		$data = array();
		return $data;
	}

	public function executeView($request) 
	{
                $data['plugin_url']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
                $data['onAction'] = 'onCreate';
                $data['siteurl'] = site_url();
		$data['HelperObj'] = new WPCapture_includes_helper_PRO();
		$data['module'] = $data["HelperObj"]->Module;
	}


	public function saveSettings( $sugarSettArray )
	{
		$fieldNames = array(
			'username' => __('Zoho Username' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
			'password' => __('Zoho Password' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
			'smack_email' => __('Smack Email' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
			 'email' => __('Email id' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
			'emailcondition' => __('Emailcondition' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
			'debugmode' => __('Debug Mode' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
		);
		foreach ($fieldNames as $field=>$value){

			if(isset($sugarSettArray[$field]))
			{			
				$config[$field] = $sugarSettArray[$field];
			}

		}
		$FunctionsObj = new PROFunctions( );
		$jsonData = $FunctionsObj->getAuthenticationKey( $config['username'] , $config['password'] );
		if($jsonData['result'] == "TRUE")
		{
			$successresult = "<p class='display_success' style='color: green;'> Settings Saved </p>";
			$result['error'] = 0;
			$result['success'] = $successresult;
			$config['authtoken'] = $jsonData['authToken'];
			$WPCapture_includes_helper_Obj = new WPCapture_includes_helper_PRO();
			$activateplugin = $WPCapture_includes_helper_Obj->ActivatedPlugin;
	                update_option("wp_{$activateplugin}_settings", $config);
		}
		else
		{
			if($jsonData['cause'] == 'EXCEEDED_MAXIMUM_ALLOWED_AUTHTOKENS') {
				$zohocrmerror = "<p style='color:red; '>Please log in to <a target='_blank' href='https://accounts.zoho.com'>https://accounts.Zoho.com</a> - Click Active Authtoken - Remove unwanted Authtoken, so that you could generate new authtoken..</p>";
			}
			else{
				$zohocrmerror = "<p class='display_failure' style='color:red;' >Please Verify Username and Password.</p>";
			}
			$result['error'] = 1;
			$result['errormsg'] = $zohocrmerror ;
			$result['success'] = 0;
		} 
		return $result; 
	}	
}

class CallZohoSettingsCrmObj extends WpzohoSettingsActions
{
	private static $_instance = null;

	public static function getInstance()
	{
		if( !is_object(self::$_instance) ) 
			self::$_instance = new WpzohoSettingsActions();
		return self::$_instance;
	}
}
