<?php
global $wpdb;

$active_plugins = get_option( "active_plugins" );
if( !in_array("woocommerce/woocommerce.php" , $active_plugins))
{
        echo "<div style='margin-left:30%;'><a href='https://wordpress.org/plugins/woocommerce/' target='_blank' class='alert alert-danger'> You Should Install Woocommerce First</a> </div>";
	return false;
}
$ecom_lead_id = $get_lead_id[0]->crmid;

$activated_crm = get_option('WpLeadBuilderProActivatedPlugin');
$siteurl = site_url();
$siteurl = esc_url( $siteurl );
$config = get_option("wp_{$activated_crm}_settings");
$ecom_module = get_option( 'ecom_wc_module' );
$convert_lead = get_option( 'ecom_wc_convert_lead' );

if( empty($convert_lead) )
{
	update_option( 'ecom_wc_convert_lead' , 'on' );
	$convert_lead = 'on';
}
?>

<input type='hidden' id='ecom_module_value' value="<?php echo $ecom_module?>">
<div id='ecommerce_integration'> <!-- Start --> 
<form id='ecom_integration' method="post" action="">
<input type="hidden" id="plug_URL" value="<?php echo esc_url(WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO);?>" />
<div class="wp-common-crm-content" style="width: 1000px;float: left;height:auto;"> <!-- Common -->

<table>

<tr> <td> <br> <br> </td> </tr>

<tr>
<td>
<label id="inneroptions"> <?php echo esc_html__('You are now using WooCommerce store' , "wp-leads-builder-any-crm-pro" ); ?> </label>
</td>
</tr>
</table>

<table>

<tr> <td> <br> <br> </td> </tr>

<tr>
<td> 
<label id="inneroptions"> <?php echo esc_html__('Capture customer information as' , "wp-leads-builder-any-crm-pro" ); ?> </label>
</td>

<td style="width:172px;padding-left:40px;">
<select style="width:150px;" id='ecom_module' onchange="change_ecom_configuration(this.id)">

<option id='ecom_module_notenabled' value='Not Enabled'
<?php
if(  $ecom_module == 'Not Enabled' )
{
        echo "selected";
}
?>
> Not Enabled </option>


<option id='ecom_module_leads' value='Leads'
<?php
if(  $ecom_module == 'Leads' )
{
	echo "selected";	
}
?>
> Leads </option>
<option id='ecom_module_contacts' value='Contacts'
<?php
if(  $ecom_module == 'Contacts' )
{
        echo "selected";
}
?>
> Contacts </option>
</select>
</td>
</tr>

<tr> <td> <br> </td> </tr>
</table>

<div id='hide_convert'>
<label style="margin-top:6px;margin-right:4px;"><?php echo esc_html__('Convert Leads into Contacts automatically  on successful order' , "wp-leads-builder-any-crm-pro" ); ?></label>


                                <div class="switch" style='position:absolute;height:450px;margin-top:-24px;margin-left:393px;'>
                                        <input type='checkbox' class='smack-vtiger-settings-text cmn-toggle cmn-toggle-yes-no' name='ecom_convert_lead' id='ecom_convert_lead'  <?php if(isset($convert_lead) && $convert_lead == 'on') { ?> value='on' <?php echo "checked=checked"; } else { ?> value='off' <?php } ?> onclick="convert_lead(this.value)"/>
                                        <label for="ecom_convert_lead" id="innertext" data-on="On" data-off="Off"></label>
                                </div>
</div> <!--  CONVERT -->


<table>
<tr> <td> <br> </td> </tr>

<tr id='choose_owner'>
<td> 
<label id="inneroptions" class='module_owner'> </label>
</td>

<td style="width:172px;padding-left:98px;" id='change_ecom_owner'>
<?php	
	$activated_crm = $skinnyData['activatedplugin'];
	$ecom_option_name = "ecom_wc_{$activated_crm}_{$ecom_module}_config";
	$get_assignedto = new EcommerceSettingsActions();
	echo $get_assignedto->get_ecom_assignedto($ecom_option_name);
?>
</td>
</tr>

<input type='hidden' value='<?php echo $activated_crm; ?>' id='ecom_active_crm'>
<tr> <td><br></td></tr>
</table>



<div id='load_ecom_fields'>
<?php 

if( $ecom_module != 'Not Enabled' )
{
?>

<table id="ecommerce-mapping" class="ecommerce-mapping">
<tr>
<label id="inneroptions" style="margin-top:6px;margin-left:161px;font-weight:bold;"><?php echo esc_html__('Configure Field Mapping' , "wp-leads-builder-any-crm-pro" ); ?></label>
</tr>

<tr>
<td style="padding-left:80px;">
<label id="ecom_innertext"> <?php echo esc_html__('WooCommerce Fields' , "wp-leads-builder-any-crm-pro" ); ?> </label>
</td>

<td style='padding-left:70px;'>
</td>

<td> 
<label id="ecom_innertext"> <?php echo esc_html__('CRM Fields' , "wp-leads-builder-any-crm-pro" ); ?> </label>
</td>
</tr>

<?php
$wc_config = get_option( "ecom_wc_{$activated_crm}_{$ecom_module}_config" );
$wc_config_fields = $wc_config['fields'];
$third_module = $ecom_module;




// To fetch the checkout form fields.
global $woocommerce;
$checkout_fields = new WC_Countries();
$checkout_fields = $checkout_fields->get_address_fields();
	//Get Woocommerce Checkout Fields	
	$wc_field_labels = array();
	foreach( $checkout_fields as $check_key => $check_value )
	{
		if( $check_value['label'] != "" )
		{
			$wc_field_labels[] = $check_value['label'];	
		}
		else
		{
			if( $check_key == "billing_address_2" )
			{
				$wc_field_labels[] = "Address2";
			}
		}
	}
	
	//Get CRM Fields
	$CaptureDataObj = new CaptureData();
        $crm_fields = $CaptureDataObj->get_crmfields_by_settings( $activated_crm , $third_module );
        $j = 1;
        $js_mandatory_fields = array();
        foreach( $crm_fields as $crm_field_key => $crm_fields_vals ) {
               $crm_field_labels[$j] = $crm_fields_vals->field_name;
               if( $crm_fields_vals->field_mandatory == 1 )
               {
                        if(!in_array($crm_fields_vals->field_name, $js_mandatory_fields))
                        $js_mandatory_fields[] = $crm_fields_vals->field_name;
               }
               $j++;
               }
               $js_mandatory_array = json_encode($js_mandatory_fields);
               $crm_field_options = '';
               $crm_field_options .= "<option>--None--</option>";
               foreach( $crm_field_labels as $field_key => $crm_field_label )
               {
               		$crm_field_options .= "<option value='{$crm_field_label}'> $crm_field_label</option>";
               }

//COPY		
			$fields_html = "";
			$i = 1;
                        foreach( $wc_field_labels as $cont_id => $cont_label)
                        {
                        $fields_html .= "<tr>
                                        <td style='padding-left:80px;'><label> $cont_label </label></td><td style='padding-left:60px;'> </td>
                        <input type='hidden' name='ecom_field_$i' id='ecom_field_$i' value='$cont_label' />";


                        $fields_html .= "<td ><select style='width:150px;' name='ecom_crm_fields_$i' id='ecom_crm_fields_$i' >";
                                        $crm_field_options = '';
                        $crm_field_options .= "<option>--None--</option>";
                        foreach( $crm_field_labels as $field_key => $crm_field_label ) // Prepare crm fileds drop down
                        {

                                $crm_field_options .= "<option value='{$crm_field_label}'";
				if( !empty( $wc_config_fields )) {
                        	foreach( $wc_config_fields as $config_key => $config_val ) // configuration
                                {
                                        if( $cont_label == $config_key && $crm_field_label == $config_val ) //match label and fieldname
                                        {
                                                $crm_field_options .= "selected=selected";//select when the configuration exist
                                        }
                                }       
				}
                        $crm_field_options .= "> $crm_field_label</option>";
                        }
                        $fields_html .= $crm_field_options;
                        $fields_html .= "</select>
                                        </td>
                                        </tr>";
                        $i++;
                        }
			echo $fields_html;
//END COPY

?>

<tr><td> <br> </td></tr>
</table>




<?php

}
?>

</div> <!-- END ecom_module_fields --> <!-- Load Ecom fields -->
<input type='hidden' value="<?php echo $i ;?>" id='ecom_total_field_count'>
<input type='hidden' value="<?php echo $third_module; ?>" id='ecom_module'>
<input type='hidden' value='<?php echo $js_mandatory_array;?>' id='ecom_crm_mandatory_fields'>


<table>
<tr id='ecom_save'>
<td style='width:960px;'><div style='float:right'>
<input type="button" class="btn btn-primary" name="map_ecom_fields" value="Save" id="map_ecom_fields" onclick="map_ecom_crm_fields();">

</div>
</td>
</tr>
</table>


</div> <!-- Common -->
</form
</div><!-- END -->


<div id="loading-sync" style="display: none; background:url(<?php echo esc_url(WP_PLUGIN_URL);?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Syncing' , 'wp-leads-builder-any-crm-pro' ); ?>...</div>

<div id="loading-image" style="display: none; background:url(<?php echo esc_url(WP_PLUGIN_URL);?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Please Wait...' , "wp-leads-builder-any-crm-pro"  ); ?> </div>


