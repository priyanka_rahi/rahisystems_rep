<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class WpsugarSettingsActions extends SkinnyActions {

	public function __construct()
	{
	}

	/**
	* The actions index method
	* @param array $request
	* @return array
	*/
	public function executeIndex($request)
	{
		// return an array of name value pairs to send data to the template
		$data = array();
		return $data;
	}

	public function executeView($request) 
	{
                $data['plugin_url']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
                $data['onAction'] = 'onCreate';
                $data['siteurl'] = site_url();
		$data['HelperObj'] = new WPCapture_includes_helper_PRO();
		$data['module'] = $data["HelperObj"]->Module;
	}

	public function saveSettings($sugarSettArray)
	{
		$fieldNames = array(
			'url' => __('Sugar Host Address', WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
			'username' => __('Sugar Username' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
			'password' => __('Sugar Password' , WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ),
			'smack_email' => __('Smack Email'),
                         'email' => __('Email id'),
			 'emailcondition' => __('Emailcondition'),
			'debugmode' => __('Debug Mode'),
		);

		foreach ($fieldNames as $field=>$value){
		if(isset($sugarSettArray[$field]))
		{
			$config[$field] = $sugarSettArray[$field];
		}
		}	

		$FunctionsObj = new PROFunctions( );

                $testlogin_result = $FunctionsObj->testlogin( $config['url'] , $config['username'] , $config['password'] );
                if(isset($testlogin_result['login']) && ($testlogin_result['login']['id'] != -1) && is_array($testlogin_result['login']))
                {
                        $successresult = "<p  class='display_success' style='color: green;'> Settings Saved </p>";
                        $result['error'] = 0;
                        $result['success'] = $successresult;
                        $WPCapture_includes_helper_Obj = new WPCapture_includes_helper_PRO();
                        $activateplugin = $WPCapture_includes_helper_Obj->ActivatedPlugin;
                        update_option("wp_{$activateplugin}_settings", $config);

                }
                else
                {
                        $sugarcrmerror = "<p  class='display_failure' style='color:red;' >Please Verify Your Sugar CRM credentials</p>";
                        $result['error'] = 1;
                        $result['errormsg'] = $sugarcrmerror ;
                        $result['success'] = 0;
                }

                return $result;
                $WPCapture_includes_helper_Obj = new WPCapture_includes_helper_PRO();
                $activateplugin = $WPCapture_includes_helper_Obj->ActivatedPlugin;
                update_option("wp_{$activateplugin}_settings", $config);
	}	
}

class CallSugarSettingsCrmObj extends WpsugarSettingsActions
{
	private static $_instance = null;

	public static function getInstance()
	{
		if( !is_object(self::$_instance) ) 
			self::$_instance = new WpsugarSettingsActions();
		return self::$_instance;
	}
}
