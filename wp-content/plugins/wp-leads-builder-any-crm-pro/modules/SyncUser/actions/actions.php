<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class SyncUserActions extends SkinnyActions {

    public function __construct()
    {
    }

  /**
   * The actions index method
   * @param array $request
   * @return array
   */
    public function executeIndex($request)
    {
        // return an array of name value pairs to send data to the template
        $data = array();
        return $data;
    }

	public function executeUserModuleMapping( $request )
    	{
       		$HelperObj = new WPCapture_includes_helper_PRO();
       		$Activated_plugin = $HelperObj->ActivatedPlugin;
       		$config = get_option("smack_{$Activated_plugin}_user_capture_settings");
       		$module = $config['user_sync_module'];
       		$data = $this->UserModuleMapping( $request , $module  );
       		return $data;
    	}


    public function UserModuleMapping( $request , $module )
    {
	    // return an array of name value pairs to send data to the template
	    $data = array();
	    foreach( $request as $key => $REQUESTS )
	    {
		    foreach( $REQUESTS as $REQUESTS_KEY => $REQUESTS_VALUE )
		    {
			    $data['REQUEST'][$REQUESTS_KEY] = $REQUESTS_VALUE;
		    }
	    }

	    $data['HelperObj'] = new WPCapture_includes_helper_PRO();
	    $data['module'] = $module ;
	    $data['moduleslug'] = rtrim( strtolower($module) , "s");
	    $data['activatedplugin'] = $data['HelperObj']->ActivatedPlugin;
	    $data['activatedpluginlabel'] = $data['HelperObj']->ActivatedPluginLabel;
	    $data['plugin_dir']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
	    $data['plugins_url'] = WP_CONST_ULTIMATE_CRM_CPT_DIR_PRO;
	    $data['siteurl'] = site_url();
	    if( isset($data['REQUEST']["saveusermodulemap"]) )
	    {
		    $this->saveUserMapping($data);
		    $data['display'] = "<p class='display_success'> Settings Saved Successfully</p>";
	    }

	    $data['UserModuleMapping'] = get_option("User{$data['activatedplugin']}{$data['module']}ModuleMapping");
	    $CaptureDataObj = new CaptureData();
	    $leadFields = $CaptureDataObj->get_crmfields_by_settings( $data['activatedplugin'] , $data['module'] );
	    $data['fields'] = $leadFields;
	    return $data;
    }

    public function saveUserMapping( $data )
    {
	    $activated_plugin = get_option( "WpLeadBuilderProActivatedPlugin" );
	    $userfield = $data['REQUEST']['userfield'];
	    $module_field = $data['REQUEST'][$data['module'].'_module_field'];
	    $mapfields = array();

	    foreach( $userfield as $key => $value )
	    {
		    $mapfields[$value] = $module_field[$key];
	    }
	    update_option( "User{$activated_plugin}{$data['module']}ModuleMapping" , $mapfields );
    }

    public function executeView($request)
    {
	    // return an array of name value pairs to send data to the template
	    $data = array();
	    foreach( $request as $key => $REQUESTS )
	    {
		    foreach( $REQUESTS as $REQUESTS_KEY => $REQUESTS_VALUE )
		    {
			    $data['REQUEST'][$REQUESTS_KEY] = $REQUESTS_VALUE;
		    }
	    }

	    $data['HelperObj'] = new WPCapture_includes_helper_PRO();
	    $data['module'] = $data['HelperObj']->Module;
	    $data['moduleslug'] = $data['HelperObj']->ModuleSlug;
	    $data['activatedplugin'] = $data['HelperObj']->ActivatedPlugin;
	    $data['activatedpluginlabel'] = $data['HelperObj']->ActivatedPluginLabel;
	    $data['plugin_dir']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
	    $data['plugins_url'] = WP_CONST_ULTIMATE_CRM_CPT_DIR_PRO;
	    $data['siteurl'] = site_url();
	    if( isset($data['REQUEST']["smack-{$data['activatedplugin']}-user-capture-settings-form"]) )
	    {
		    $this->saveSettingArray($data);
	    }
	    return $data;
    }

	public function saveSettingArray($data)
	{
		$HelperObj = $data['HelperObj'];
		$module = $HelperObj->Module;
		$moduleslug = $HelperObj->ModuleSlug;
		$activatedplugin = $HelperObj->ActivatedPlugin;
		$activatedpluginlabel = $HelperObj->ActivatedPluginLabel;
		$fieldNames = array(
			'smack_user_capture' => __('Capture Registering User'),
			'smack_capture_duplicates' => __('Capture Duplicate users'),
			'user_sync_module' => __('User Sync Module'),
		);

		foreach ($fieldNames as $field => $value){
			if(isset($data['REQUEST'][$field]))
			{
				$config[$field] = $data["REQUEST"][$field];
			}
			else
			{
				$config[$field] = "";
			}
		}
		update_option("smack_{$activatedplugin}_user_capture_settings", $config);
	}
}

