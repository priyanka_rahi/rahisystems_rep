<?php
/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$OverallFunctionsPROObj = new OverallFunctionsPRO();
$result = $OverallFunctionsPROObj->CheckFetchedDetails();

if( !$result['status'] )
{
	$display_content = "<br>".$result['content']. " to configure WP User Sync <br><br>" ; 
        echo "<div style='font-weight:bold;  color:red;font-size:16px;text-align:center'> $display_content </div>";
}
else
{

$imagepath = $skinnyData['plugins_url'].'images/';
if( isset($_POST["smack_{$skinnyData['activatedplugin']}_user_capture_settings"]) ) {
	$skinnydata->saveSettingArray($_POST , $data['HelperObj']);
}
if( isset($data['display']) )
{
	echo $data['display'];
}
$config = get_option("smack_{$skinnyData['activatedplugin']}_user_capture_settings");

?>
<p class="display_success" style="margin-left:15px;font-size:16px;color:green;">  </p>

<div id="dialog-confirm" title="Switch the plugin">
  <p><span  style="float:left; margin:0 7px 20px 0;"></span>Do you want to change ??</p>
</div>

<div id="dialog-activate-custom" title="Plugin inactive">
  <p><span  style="float:left; margin:0 7px 20px 0;"></span>You should activate the plugin first</p>
</div>


<script>

$(document).ready( function(){
	$(".display_success" ).hide();
	$( "#dialog-confirm" ).hide();
	$( "#dialog-activate-custom" ).hide();
	$( "#display_sync_log" ).hide();
});
</script>
<?php 
	$custom_plugin = get_option( "custom_plugin" );
?>
<div id="display_sync_log" style="text-align:center;float:right;"> </div>
<input type="hidden" id="custom_plugin_value" value='<?php echo $custom_plugin ;?>'>
<form id="smack-<?php echo sanitize_text_field($skinnyData['activatedplugin']);?>-user-capture-settings-form" action="" method="post">
<input type="hidden" name="smack-<?php echo  esc_attr($skinnyData['activatedplugin']);?>-user-capture-settings-form" value="smack-<?php echo  $skinnyData['activatedplugin'];?>-user-capture-settings-form" />

<input type="hidden" name="activated_crm" id="activated_crm" value="<?php echo $skinnyData['activatedplugin']; ?>">
<h3 style="margin-left:15px;"><?php echo esc_html__("Capture WordPress users" , "wp-leads-builder-any-crm-pro" ); ?> </h3>
<div class = "wp-common-crm-content">
<table>
	<tr><td> <br> </td> </tr>
	<tr>

                        <td><label id="inneroptions" style="margin-top:4px;"><?php echo esc_html__( 'Select Plugin-Custom Fields :' , 'wp-leads-builder-any-crm-pro' ); ?></label></td>
                        <td>
                        <?php
				$ContactFormPluginsObj = new ContactFormPROPlugins();
                                echo $ContactFormPluginsObj->getCustomFieldPlugins();

                        ?>
                        </td>
                        </tr> 
		<tr><td> <br> </td></tr>
	<tr>
		<td>
			<label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="inneroptions" ><?php echo esc_html__("Sync Wordpress User as" , WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO ); ?><?php echo str_repeat( '&nbsp' , 4 );  ?> :</label>
		</td>

		<td style ="width:90px;">
			<select name="choose_module" id="choose_module" style="width:130px;" onchange="wpSyncSettingsPRO( this )">
				<option value="Leads" 
				<?php
					if( isset( $config['user_sync_module']  ) && $config['user_sync_module'] == "Leads" )
					{
						echo "selected=selected";			
					}
				?>
				> Leads </option>
				<option value="Contacts" 
				<?php
					if( isset( $config['user_sync_module']  ) && $config['user_sync_module'] == "Contacts" )
                                        {
                                                echo "selected=selected";
                                        }
				?>
				> Contacts </option>
			</select>
		</td>
	<tr>
		<td>
			<br>
		</td>
	</tr>

		<tr>
		<td style="width:250px;">
			<label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="inneroptions" ><?php echo esc_html__("Map WP User Data" , "wp-leads-builder-any-crm-pro" ); ?> <?php echo str_repeat( '&nbsp' , 14 );  ?>:</label>

		</td>
	
		<td>
			<?php  
			$module_name = rtrim( $config['user_sync_module'] , "s" ) ; 
  			?>			

			<input type="button" style="float:left;position:relative;top:3px;height:29px;width:130px;text-align:center;" value="<?php echo esc_attr__('Configure' , "wp-leads-builder-any-crm-pro" );?>" class="button-primary" onClick="window.location.href='<?php echo rtrim($skinnyData['siteurl'] , "/")."/wp-admin/admin.php?page=wp-leads-builder-any-crm-pro/index.php&__module=SyncUser&__action=UserModuleMapping" ?>'" />

		</td>	
		</tr>

	<tr>
		<td> <br> </td>
	</tr>

	<tr>
		<td style="width:250px;">
			<label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="inneroptions" ><?php echo esc_html__("On Duplicate Data" , "wp-leads-builder-any-crm-pro" ); ?> <?php echo str_repeat( '&nbsp' , 15 );  ?>:</label>
		</td>

		</td>

                <td style ="width:90px;">
                        <select name="duplicate_handling" id="duplicate_handling" style="width:130px;" onchange="wpSyncDuplicateSettingsPRO( this )">
                                <option value="skip"
				<?php
				if( isset( $config['smack_capture_duplicates'] ) && $config['smack_capture_duplicates'] == "skip"  ) 
				{
					echo "selected=selected";
				}
				?>
				> Skip </option>
                                <option value="update"

				<?php
                                if( isset( $config['smack_capture_duplicates'] ) && $config['smack_capture_duplicates'] == "update"  ) 
                                {
                                        echo "selected=selected";
                                }
                                ?>
				> Update </option>
				<?php 
				$activated_crm = get_option( 'WpLeadBuilderProActivatedPlugin' );
		                if($activated_crm != 'freshsales' || ($activated_crm == 'freshsales' && $config['user_sync_module'] != "Contacts")) { ?>
				<option value="create"
				<?php
                                if( isset( $config['smack_capture_duplicates'] ) && $config['smack_capture_duplicates'] == "create"  ) 
                                {
                                        echo "selected=selected";
                                }
                                ?>
				> Create </option>
				<?php } ?>
                        </select>
                </td>
	</tr>
</table>
<table>

	<tr>
		<td><input type="hidden" name="posted" value="<?php echo 'posted';?>"> <br>
	</td>

	</tr>

	<tr>
		<td style="width:250px;">
                        <label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="inneroptions" ><?php echo esc_html__("WP User Auto Sync" , "wp-leads-builder-any-crm-pro" ); ?> <?php echo str_repeat( '&nbsp' , 13 );  ?>:</label>
                </td>

		<td>
	<?php
		$switch_sync_button = get_option( "Sync_value_on_off" );
		$start = 1;
		$offset = 10;
		$wp_users_count = count( get_users() );		
		$check_sync_value = get_option( "Sync_value_on_off" );

		//Check mapping exist or not
		$activated_crm = get_option( 'WpLeadBuilderProActivatedPlugin' );
		$config_user_capture = get_option("smack_{$activated_crm}_user_capture_settings");
		$usersync_module = $config_user_capture['user_sync_module'];
		$check_mapping = get_option( "User{$activated_crm}{$usersync_module}ModuleMapping" );

		if( empty( $check_mapping ) )
		{
			$mapping_value = 'no';
		}
		else
		{
			$mapping_value = 'yes';
		}
	?>
	<input type="hidden" id="check_mapping_value" value="<?php echo $mapping_value ;?>">
	<input type="hidden" id='wp_start' value="0">
	<input type="hidden" id="wp_offset" value="10">
	<input type="hidden" id="wp_synced_count" value="0">
	<input type="hidden" id="wp_users_count" value="<?php echo $wp_users_count; ?>">
	<input type="hidden" id="site_url" value="<?php echo $skinnyData['siteurl'] ;?>">

		<label id="postlabel" title= "<?php echo __('Enable',WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO); ?>" <?php if($check_sync_value == 'On'){?> class="enablesetting" <?php }else { ?> class="disablesetting" <?php } ?> >	
		<input type="checkbox" style="display:none;" id="enableAutoSync" name="enableAutoSync" onclick="enableWPUserAutoSync(this.id);"  <?php if( $check_sync_value == "On" ){ echo "checked"; }  ?>> Enable</label>

		<label id="nopostlabel" title= "<?php echo __('Disable',WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO); ?>" <?php if($check_sync_value == 'Off'){?> class="enablesetting" <?php } else{ ?> class="disablesetting" <?php } ?> >           
                <input type="checkbox" style="display:none;" id="disableAutoSync" name="disableAutoSync"  onclick="enableWPUserAutoSync(this.id);"  <?php if( $check_sync_value == "Off" ){ echo "checked"; }  ?>> Disable</label>
		</td>

                <td>
		<input type="button" style="float:left;position:relative;top:-3px;height:29px;margin-left:50%;" id="OneTimeSync" class="OTMS button button-primary" <?php if( $switch_sync_button == "On" ) { echo "disabled"; } ?> value="<?php echo esc_attr__('One Time Manual Sync ', 'wp-leads-builder-any-crm-pro' );?>" class="button-secondary submit-add-to-menu innersave" onclick="captureAlreadyRegisteredUsersPRO('<?php echo esc_js($skinnyData['siteurl']); ?>');"/>
               </td>
	</tr>
</table>

<table>
<?php
//Assign Leads And Contacts to User

$crm_users_list = get_option( 'crm_users' );
$assignedtouser_config = get_option( "smack_{$activated_crm}_usersync_assignedto_settings" );
$assignedtouser_config_leads = $assignedtouser_config['usersync_assign_leads'];
$assignedtouser_config_contacts = $assignedtouser_config['usersync_assign_contacts'];
$Assigned_users_list = $crm_users_list[$activated_crm];
switch( $activated_crm )
{
	case 'wpzohopro':
		$html_leads = "";
		$html_leads = '<select name="usersync_assignedto_leads" id="usersync_assignedto_leads">';
		$content_option_leads = "";
		$content_option_leads = "<option id='select' value='--Select--'>--Select--</option>";
		if(isset($Assigned_users_list['user_name']))
			for($i = 0; $i < count($Assigned_users_list['user_name']) ; $i++)
			{
				$content_option_leads.="<option id='{$Assigned_users_list['user_name'][$i]}' value='{$Assigned_users_list['id'][$i]}'";
				if($Assigned_users_list['id'][$i] == $assignedtouser_config_leads )
				{
					$content_option_leads .=" selected";
				}
				$content_option_leads .=">{$Assigned_users_list['user_name'][$i]}</option>";
			}
		$content_option_leads .= "<option id='rr_usersync_owner' value='Round Robin'";
		if( $assignedtouser_config_leads == 'Round Robin' )
		{
			$content_option_leads .= "selected";
		}
		$content_option_leads .= "> Round Robin</option>";
		$html_leads .= $content_option_leads;
		$html_leads .= "</select> <span style='padding-left:15px; color:red;' id='assignedto_status'></span>";
		break;
	case 'wptigerpro':
		$html_leads = "";
		$html_leads = '<select name="usersync_assignedto_leads" id="usersync_assignedto_leads" style="min-width:69px;">';
		$content_option_leads = "";
		$content_option_leads = "<option id='select' value='--Select--'>--Select--</option>";
		if(isset($Assigned_users_list['user_name']))
			for($i = 0; $i < count($Assigned_users_list['user_name']) ; $i++)
			{
				$content_option_leads .="<option id='{$Assigned_users_list['id'][$i]}' value='{$Assigned_users_list['id'][$i]}'";
				if($Assigned_users_list['id'][$i] == $assignedtouser_config_leads)
				{
					$content_option_leads .=" selected";
				}
				$content_option_leads .=">{$Assigned_users_list['first_name'][$i]} {$Assigned_users_list['last_name'][$i]}</option>";
			}
		$content_option_leads .= "<option id='rr_usersync_owner' value='Round Robin'";
		if( $assignedtouser_config_leads == 'Round Robin' )
		{
			$content_option_leads .= "selected";
		}
		$content_option_leads .= "> Round Robin</option>";
		$html_leads .= $content_option_leads;
		$html_leads .= "</select> <span style='padding-left:15px; color:red;' id='assignedto_status'></span>";
		break;
	case 'wpsugarpro':
		$html_leads = "";
		$html_leads = '<select name="usersync_assignedto_leads" id="usersync_assignedto_leads" style="min-width:69px;">';
		$content_option_leads = "";
		$content_option_leads = "<option id='select' value='--Select--'>--Select--</option>";
		if(isset($Assigned_users_list['user_name']))
			for($i = 0; $i < count($Assigned_users_list['user_name']) ; $i++)
			{
				$content_option_leads .="<option id='{$Assigned_users_list['id'][$i]}' value='{$Assigned_users_list['id'][$i]}'";
				if($Assigned_users_list['id'][$i] == $assignedtouser_config_leads)
				{
					$content_option_leads .=" selected";
				}
				$content_option_leads .=">{$Assigned_users_list['first_name'][$i]} {$Assigned_users_list['last_name'][$i]}</option>";
			}
		$content_option_leads .= "<option id='rr_usersync_owner' value='Round Robin'";
		if( $assignedtouser_config_leads == 'Round Robin' )
		{
			$content_option_leads .= "selected";
		}
		$content_option_leads .= "> Round Robin</option>";
		$html_leads .= $content_option_leads;
		$html_leads .= "</select> <span style='padding-left:15px; color:red;' id='assignedto_status'></span>";
		break;
	case 'wpsalesforcepro':
		$html_leads = "";
		$html_leads = '<select name="usersync_assignedto_leads" id="usersync_assignedto_leads" style="min-width:69px;">';
		$content_option_leads = "";
		$content_option_leads = "<option id='select' value='--Select--'>--Select--</option>";
		if(isset($Assigned_users_list['user_name']))
			for($i = 0; $i < count($Assigned_users_list['user_name']) ; $i++)
			{
				$content_option_leads .="<option id='{$Assigned_users_list['user_name'][$i]}' value='{$Assigned_users_list['id'][$i]}'";
				if($Assigned_users_list['id'][$i]== $assignedtouser_config_leads)
				{
					$content_option_leads .=" selected";
				}
				$content_option_leads .=">{$Assigned_users_list['user_name'][$i]}</option>";
			}
		$content_option_leads .= "<option id='rr_usersync_owner' value='Round Robin'";
		if( $assignedtouser_config_leads == 'Round Robin' )
		{
			$content_option_leads .= "selected";
		}
		$content_option_leads .= "> Round Robin</option>";
		$html_leads .= $content_option_leads ;
		$html_leads .= "</select> <span style='padding-left:15px; color:red;' id='assignedto_status'></span>";
		break;
	case 'freshsales':
		$html_leads = "";
		$html_leads = '<select name="usersync_assignedto_leads" id="usersync_assignedto_leads" style="min-width:69px;">';
		$content_option_leads = "";
		$content_option_leads = "<option id='select' value='--Select--'>--Select--</option>";
		if(isset($Assigned_users_list['last_name']))
			for($i = 0; $i < count($Assigned_users_list['last_name']) ; $i++)
			{
				$content_option_leads .="<option id='{$Assigned_users_list['last_name'][$i]}' value='{$Assigned_users_list['id'][$i]}'";
				if($Assigned_users_list['id'][$i]== $assignedtouser_config_leads)
				{
					$content_option_leads .=" selected";
				}
				$content_option_leads .=">{$Assigned_users_list['last_name'][$i]}</option>";
			}
		$content_option_leads .= "<option id='rr_usersync_owner' value='Round Robin'";
		if( $assignedtouser_config_leads == 'Round Robin' )
		{
			$content_option_leads .= "selected";
		}
		$content_option_leads .= "> Round Robin</option>";
		$html_leads .= $content_option_leads ;
		$html_leads .= "</select> <span style='padding-left:15px; color:red;' id='assignedto_status'></span>";
		break;
}
?>
<tr>
<td><br>
</td></tr>
<tr >
<td id='Lead_owner' style="width:250px;">
                        <label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="inneroptions" ><?php echo esc_html__("Lead Owner" , "wp-leads-builder-any-crm-pro" ); ?> <?php echo str_repeat( '&nbsp' , 26);  ?>:</label>
</td>

<td id='Contact_owner' style="width:250px;">
                        <label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="inneroptions" ><?php echo esc_html__("Contact Owner" , "wp-leads-builder-any-crm-pro" ); ?> <?php echo str_repeat( '&nbsp' , 20 );  ?>:</label>
</td>

 
<td> <?php echo $html_leads; ?> </td>

</tr>

</table>

<table>
<tr>
<td> <br></td>

</tr>

<tr>
<td><label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="inneroptions" ><?php echo esc_html__("NOTE :" , "wp-leads-builder-any-crm-pro" ); ?> </label>
 </td> </tr>

<tr><td><br></td></tr>

<tr><td><label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="innertext" ><?php echo esc_html__("WP User Auto Sync" , "wp-leads-builder-any-crm-pro" ); ?><span style="padding-left:34px;">: </span> </label>
 </td> <td><label style ="position:relative;top:4px;left:5px;margin-top:-12px;padding-left:10px;"  id="innertext" >Automatically sync WordPress users to CRM When Creating Or Updating Users</label> </td> </tr>


<tr><td><label style = "position:relative;top:4px;left:5px;margin-top:-12px;" id="innertext" ><?php echo esc_html__("One Time Manual Sync " , "wp-leads-builder-any-crm-pro" ); ?> <span style="padding-left:9px;">: </span> </label>
 </td> <td> <label style ="position:relative;top:4px;left:5px;margin-top:-12px;padding-left:10px;"  id="innertext"> Sync existing Wordpress users as Leads or Contacts </label> </td></tr>



</table>

</div>
</form>
<div id="loading-image" style="display: none; background:url(<?php echo WP_PLUGIN_URL;?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;"><?php echo esc_html__('Please Wait' , 'wp-leads-builder-any-crm-pro' ); ?>...</div>
<?php
}
