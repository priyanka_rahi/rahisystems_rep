<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

$OverallFunctionsPROObj = new OverallFunctionsPRO();
$result = $OverallFunctionsPROObj->CheckFetchedDetails();
global $wpdb;
if( !$result['status'] )
{
        echo "<div style='font-weight:bold; padding-left:20px; color:red;'> {$result['content']} </div>";
}
else
{
	$imagepath = $skinnyData['plugins_url'].'images/';
	if( isset($_POST["smack_{$skinnyData['activatedplugin']}_user_capture_settings"]) ) {
		$skinnydata->saveSettingArray($_POST , $data['HelperObj']);
	}
	if( isset($skinnyData['display']) )
	{
		echo $skinnyData['display'];
	}
	$config = get_option("smack_{$skinnyData['activatedplugin']}_user_capture_settings");

	$args = array();
	$user_query = get_users();
	$user_fields = array( 'user_login' => __('Username' , 'wp-leads-builder-any-crm-pro' ) , 'role' => __('Role' , 'wp-leads-builder-any-crm-pro' ) , 'user_nicename' => __('Nicename' , 'wp-leads-builder-any-crm-pro' ) , 'user_email' => __('E-mail' , 'wp-leads-builder-any-crm-pro' ) , 'user_url' => __('Website' , 'wp-leads-builder-any-crm-pro' ) , 'display_name' => __('Display name publicly as' , 'wp-leads-builder-any-crm-pro' ) , 'nickname' => __('Nickname' , 'wp-leads-builder-any-crm-pro' ) , 'first_name' => __('First Name' , 'wp-leads-builder-any-crm-pro' ) , 'last_name' => __('Last Name' , 'wp-leads-builder-any-crm-pro' ) , 'description' => __('Biographical Info' , 'wp-leads-builder-any-crm-pro' ) );
	$wp_user_custom_plugin = get_option('custom_plugin');
	$wp_member_plugin = "wp-members/wp-members.php" ;
	$acf_plugin = "advanced-custom-fields/acf.php" ;
	$memberpress_plugin = "memberpress/memberpress.php";

	//wp-members custom_fields
	$active_plugins = get_option( "active_plugins" );
	if( in_array($wp_member_plugin , $active_plugins) && $wp_user_custom_plugin == 'wp-members' )
	{
		$wp_member_array = get_option("wpmembers_fields");
		$option = $custom_field_array = array();
		$i=0;
		if( !empty( $wp_member_array )) {
		foreach( $wp_member_array as $key=>$option_name )
		{	$i++;
			$option[$i]['label'] = $option_name['1'];
			$option[$i]['name'] = $option_name['2'];
		}

		foreach( $option as $opt_ke=>$opt_val  )
		{
			if( !array_key_exists( $opt_val['name'] , $user_fields ) ){

				$custom_field_array[$opt_val['name']] =   $opt_val['label'] ;

			}
		}
		$user_fields = array_merge( $user_fields , $custom_field_array );
		}
	}
	//End of wp-members custom fields


	//ACF-custom fields
	if( in_array($acf_plugin , $active_plugins) && $wp_user_custom_plugin == 'acf' )
	{
		$acf_vals = array();
		$acf = $wpdb->get_results($wpdb->prepare( "select * from ".$wpdb->posts." where post_type=%s and post_status=%s" , 'acf' , 'publish'),ARRAY_A );
		$i = 0;
		if( !empty( $acf )) {
		foreach( $acf as $idkey=>$idval )
		{

			$id = $idval["ID"] ;
			$meta_fields = $wpdb->get_results( $wpdb->prepare("select meta_value from ".$wpdb->postmeta." where post_id=%d and meta_key like %s" , $id , 'field_%'),ARRAY_A );

			foreach( $meta_fields as $mkey=>$mvalue )
			{
				$meta_values = unserialize( $mvalue['meta_value'] ) ;
				$acf_vals[$i]['key']   = $meta_values['key'];
				$acf_vals[$i]['label'] = $meta_values['label'] ;
				$acf_vals[$i]['name']  = $meta_values['name'] ;
				$i++;
			}
		}

		foreach( $acf_vals as $acfkey => $acf_vl )
		{
			$meta_key = $acf_vl['name'];
			$check_db = $wpdb->get_results( "select * from ". $wpdb->usermeta ." where meta_key='$meta_key'" );

			if( isset( $check_db ) && !empty($check_db) )
			{
				$acf_array[$acf_vl['name']] = $acf_vl['label'];
			}
		}
		if( isset( $acf_array ) && !empty($acf_array))
		{
			$user_fields = array_merge( $user_fields , $acf_array );
		}
		}
	}

	//End of ACF-custom fields

	//Memberpress support
	if( in_array($memberpress_plugin , $active_plugins) && $wp_user_custom_plugin == 'member-press' ) 
	{
		$member_press = get_option('mepr_options');
                $field_list = $member_press['custom_fields'];
		$i = 0;
		$mp_custom_field_list = array();
		if( !empty( $field_list )) {
		foreach( $field_list as $custom_fields )
		{
			$mp_custom_field_list[$i]['field_key'] = $custom_fields->field_key ;
			$mp_custom_field_list[$i]['field_name'] = $custom_fields->field_name;
			$mp_custom_field_list[$i]['options'] = $custom_fields->options;
			$i++;
		}
		foreach( $mp_custom_field_list as $mp_label => $mp_option )
		{
			$mp_fields[$mp_option['field_key']] = $mp_option['field_name'];
		}

		if( !empty( $mp_fields ) )
		{
			$user_fields = array_merge( $user_fields , $mp_fields );
		}
		}
	}
	//End of Memberpress support

	$fields = json_decode( json_encode( $skinnyData['fields'] ) , true );
	$select_fields = "";
	$field_options = "<option value=''>--none--</option>";
	$javascript_mandatory_array = "[";

	foreach( $fields as $key => $field )
	{
		$field_options .= "<option value='{$field['field_name']}'> {$field['field_label']} </option>";
		if( $field['field_mandatory'] == 1 )
		{
			$javascript_mandatory_array .= "'{$field['field_name']}' ,";
		}
	}
	$javascript_mandatory_array = rtrim( $javascript_mandatory_array , ' ,' );
	$javascript_mandatory_array .= "]";

	?>
	<h3 id="innerheader"><?php echo esc_html__("Map", "wp-leads-builder-any-crm-pro" )." {$skinnyData['module']} ".__("Fields" , "wp-leads-builder-any-crm-pro" ); ?> </h3>
	<form name="mapuserfields" id="mapuserfields" action="" method="post" onsubmit="return validateMapFields( '<?php echo $skinnyData['siteurl']; ?>' , '<?php echo $skinnyData['module']; ?>_module_field' , 'userfield' , <?php echo $javascript_mandatory_array; ?> ); ">
		<div class="wp-common-crm-content">
			<table class="ecommerce-mapping">
				<tr>
					<th style="font-size:14px;font-weight:bold;">
						<?php echo esc_html__("User Fields" , "wp-leads-builder-any-crm-pro" ); ?>
					</th>
					<th style="font-size:14px;font-weight:bold;">
						<?php echo esc_html($skinnyData['module']." ".__("Fields", "wp-leads-builder-any-crm-pro" )); ?>
					</th>
				</tr>
				<tr> <td> <br> </td></tr>
				<?php
				$i = 0;
				foreach( $user_fields as $fieldvalue => $field_label )
				{
					?>
					<tr>
						<td>
							<label>	<?php echo esc_html__( $field_label , "wp-leads-builder-any-crm-pro" ); ?>	</label>
							<input type="hidden" name="userfield[]" value="<?php echo $fieldvalue; ?>" />
						</td>
						<td>
							<select name="<?php echo esc_html($skinnyData['module']); ?>_module_field[]" >
								<?php
								echo $field_options;
								?>
							</select>
						</td>
					</tr>
					<?php
					$load_script .= "
					<script>
						document.getElementsByName('{$skinnyData['module']}_module_field[]')[{$i}].value = '{$skinnyData['UserModuleMapping'][$fieldvalue]}';
					</script>";

					$i++;
				}

				?>

			</table>
			<input type="hidden" id="totaluserfields" name="totaluserfields" value="<?php echo $i; ?>">
		<span style="margin-top:20px;">
		<input type="button" style="position:absolute;height:29px;margin-left:20px;margin-top:5px;" value="<?php echo esc_attr__('<< Cancel ' , 'wp-leads-builder-any-crm-pro' );?>" class="button-primary" onClick="window.location.href='<?php echo rtrim($skinnyData['siteurl'] , "/")."/wp-admin/admin.php?page=wp-leads-builder-any-crm-pro/index.php&__module=SyncUser&__action=view" ?>'" />
			
		<input type="submit" name="saveusermodulemap" value="Update" id="innersave" style="margin-left:25px;margin-top:5px;float:right;margin-right:40px;" class="button-primary" onclick="document.getElementById('loading-image').style.display = 'block'"> </span>

		</div>
	</form>
	<div id="loading-image" style="display: none; background:url(<?php echo esc_url(WP_PLUGIN_URL);?>/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif) no-repeat center #fff;">Please Wait...</div>
	<?php
	echo $load_script;

}
