<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

class ManageFieldsActions extends SkinnyActions {

	public $nonceKey = null;
	public function __construct()
	{
		$helperObj = new OverallFunctionsPRO();
		$this->nonceKey = $helperObj->smack_create_wplbpro_nonce_key();
	}

	/**
	* The actions index method
	* @param array $request
	* @return array
	*/

	public function executeIndex($request)
	{
		// return an array of name value pairs to send data to the template
		$data = array();
		return $data;
	}

	public function executeview($request)
	{
		$data = array();
		foreach( $request as $key => $REQUESTS )
		{
			foreach( $REQUESTS as $REQUESTS_KEY => $REQUESTS_VALUE )
			{
				$data['REQUEST'][$REQUESTS_KEY] = $REQUESTS_VALUE;
			}
		}
		$data['HelperObj'] = new WPCapture_includes_helper_PRO();
		$data['module'] = $data['HelperObj']->Module;
		$data['moduleslug'] = $data['HelperObj']->ModuleSlug;
		$data['activatedplugin'] = $data["HelperObj"]->ActivatedPlugin;
		$data['activatedpluginlabel'] = $data["HelperObj"]->ActivatedPluginLabel;
		$data['nonce_key'] = $this->nonceKey;
		$data['plugin_url']= WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO;
		$data['onAction'] = 'onCreate';
		$data['siteurl'] = site_url();
		if(isset($data['REQUEST']['formtype']))
		{
			$data['formtype'] = $data['REQUEST']['formtype'];
		}
		else
		{
			$data['formtype'] = "post";
		}
		if(isset($data['REQUEST']['EditShortcode']) && ( $data['REQUEST']['EditShortcode'] != "" ))
		{
			$data['option'] = $data['options'] = "smack_fields_shortcodes";
		}
		else
		{
			$data['option'] = $data['options'] = "smack_{$data['activatedplugin']}_{$data['moduleslug']}_fields-tmp";
		}

		if(isset($data['REQUEST']['EditShortcode']) && ( $data['REQUEST']['EditShortcode'] != "" ) )
		{
			$data['onAction'] = 'onEditShortCode';
		}
		else
		{
			$data['onAction'] = 'onCreate';
		}
                return $data;
	}
}
