<?php

/******************************************************************************************
 * Copyright (C) Smackcoders 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

function saveFormFields( $options , $onAction , $editShortCodes , $formtype = "post" )
{
	$HelperObj = new WPCapture_includes_helper_PRO();
	$module = $HelperObj->Module;
	$moduleslug = $HelperObj->ModuleSlug;
	$activatedplugin = $HelperObj->ActivatedPlugin;
	$activatedpluginlabel = $HelperObj->ActivatedPluginLabel;
	$save_field_config = array();
	$crmtype = sanitize_text_field($_POST['crmtype']);
	$module = sanitize_text_field($_POST['module']);
	$moduleslug = rtrim( strtolower($module) , "s");
	$options = "smack_fields_shortcodes";
	if( isset($_POST ['savefields'] ) && (sanitize_text_field($_POST ['savefields']) == "GenerateShortcode"))
	{
		$config_fields = get_option("smack_{$crmtype}_{$moduleslug}_fields-tmp");
		$config_contact_shortcodes = get_option($options);
	}
	else
	{
		$options = "smack_fields_shortcodes";
		$config_contact_shortcodes = get_option($options);
		$config_fields = $config_contact_shortcodes[sanitize_text_field($_REQUEST['EditShortcode'])];
	}

	foreach( $config_fields as $shortcode_attributes => $fields )
	{
		if($shortcode_attributes == "fields")
		{
			foreach( $fields as $key => $field )
			{
				$save_field_config["fields"][$key] = $field;
				if( !isset($field['mandatory']) || $field['mandatory'] != 2 )
				{
					if(isset($_POST['select'.$key]))
					{
					$save_field_config['fields'][$key]['publish'] = 1;
					}
					else
					{
						$save_field_config['fields'][$key]['publish'] = 0;
					}
				}
				else
				{
					$save_field_config['fields'][$key]['publish'] = 1;
				}

			 if( !isset($field['mandatory']) || $field['mandatory'] != 2 )
				{
					if(isset($_POST['mandatory'.$key]))
                                        {
                                                $save_field_config['fields'][$key]['wp_mandatory'] = 1;
						$save_field_config['fields'][$key]['publish'] = 1;
                                        }
                                        else
                                        {
                                                $save_field_config['fields'][$key]['wp_mandatory'] = 0;
                                        }
				}
				else
				{
		
				     $save_field_config['fields'][$key]['wp_mandatory'] = 1;
				}

				$save_field_config['fields'][$key]['display_label'] = sanitize_text_field($_POST['fieldlabel'.$key]);
			}
		}
		else
		{
			$save_field_config[$shortcode_attributes] = $fields;
		}
	}
	
	if(!isset($save_fields_config["check_duplicate"]))
	{
		$save_fields_config["check_duplicate"] = 'none';
	}
	else if(isset($save_fields_config["check_duplicate"]) && ($save_fields_config["check_duplicate"] === 1))
	{
		$save_fields_config["check_duplicate"] === 'skip';
	}
	else if(isset($save_fields_config["check_duplicate"]) && ($save_fields_config["check_duplicate"] === 0))
	{
		$save_fields_config["check_duplicate"] = 'none';
	}
	$extra_fields = array( "formtype" , "enableurlredirection" , "redirecturl" , "errormessage" , "successmessage" , "assignedto" , "check_duplicate" , "enablecaptcha");
	foreach( $extra_fields as $extra_field )
	{
		if(isset( $_POST[$extra_field]))
		{
			$save_field_config[$extra_field] = sanitize_text_field($_POST[$extra_field]);
		}
		else
		{
			unset($save_field_config[$extra_field]);
		}
	}

	for( $i = 0; $i < $_REQUEST['no_of_rows']; $i++ )
	{
		$REQUEST_DATA[$i] = sanitize_text_field($_REQUEST['position'.$i]);
	}

	asort($REQUEST_DATA);

	$i = 0;
	foreach( $REQUEST_DATA as $key => $value )
	{
		$Ordered_field_config['fields'][$i] = $save_field_config['fields'][$key];
		$i++;
	}

	$save_field_config['fields'] = $Ordered_field_config['fields']; 

	$save_field_config['crm'] = $_REQUEST['crmtype'];

	if( isset($_POST ['savefields'] ) && (sanitize_text_field($_POST ['savefields']) == "GenerateShortcode"))
	{
		$OverallFunctionObj = new OverallFunctionsPRO();
		$random_string = $OverallFunctionObj->CreateNewFieldShortcode( sanitize_text_field($_REQUEST['crmtype']) , sanitize_text_field($_REQUEST['module']) );
		$config_contact_shortcodes[$random_string] = $config_fields;
              	update_option("smack_fields_shortcodes", $config_contact_shortcodes);
                update_option("smack_{$crmtype}_{$moduleslug}_fields-tmp" , $save_field_config);
		wp_redirect("".WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO."&__module=ManageShortcodes&__action=ManageFields&crmtype=$crmtype&module=$module&EditShortcode=$random_string");
		exit;
	}
	else
	{
		$config_contact_shortcodes[sanitize_text_field($_REQUEST['EditShortcode'])] = $save_field_config;
		update_option("smack_fields_shortcodes", $config_contact_shortcodes);
		update_option("smack_{$crmtype}_{$moduleslug}_fields-tmp" , $save_field_config);
	}
	$data['display'] = "";
	return $data;
}

function formFields( $options, $onAction, $editShortCodes , $formtype = "post" )
{
	$siteurl= site_url();
	$module =$module_options ='Leads';
	$content1='';
	if( $onAction == "onEditShortCode" && $editShortCodes != '')
	{
		$shortcode = get_option($options);
		$config_leads_fields = $shortcode[$editShortCodes];
	}
	else
	{
		$config_leads_fields = get_option($options);
	}
	$imagepath=$siteurl.'/wp-content/plugins/'.WP_CONST_ULTIMATE_CRM_CPT_SLUG_PRO.'/images/' ;
	$content='
	<input type="hidden" name="field-form-hidden" value="field-form" />
	<div>';
	$i = 0;

	if(!isset($config_leads_fields['fields'][0]))
	{
		$content.='<p style="color:red;font-size:20px;text-align:center;margin-top:-22px;margin-bottom:20px;">Crm fields are not yet synchronised</p>';
	}
	else
	{
		$content.='<table style="border: 1px solid #dddddd;width:98%;margin-bottom:26px;margin-top:5px"><tr class="smack_highlight smack_alt" style="border-bottom: 1px solid #dddddd;"><th class="smack-field-td-middleit" align="left" style="width: 50px;"><input type="checkbox" name="selectall" id="selectall" onclick="selectAllPRO'."('field-form','".$module."')".';" style="margin-top:-3px"/></th><th align="left" style="width: 200px;"><h5>Field Name</h5></th><th class="smack-field-td-middleit" align="left" style="width: 100px;"><h5>Show Field</h5></th><th class="smack-field-td-middleit" align="left" style="width: 100px;"><h5>Order</h5></th><th class="smack-field-td-middleit" style="width: 100px;" align="left"><h5>Mandatory</h5></th><th class="smack-field-td-middleit" style="width: 100px;" align="left"><h5>Field Label Display</h5></th> </tr>';

		for($i=0;$i<count($config_leads_fields['fields']);$i++)
		{
			if( $config_leads_fields['fields'][$i]['wp_mandatory'] == 1 )
			{
				$madantory_checked = 'checked="checked"';
			}
			else
			{
				$madantory_checked = "";
			}

			if( isset($config_leads_fields['fields'][$i]['mandatory']) && $config_leads_fields['fields'][$i]['mandatory'] == 2)
			{

				if($i % 2 == 1)
				$content1.='<tr class="smack_highlight smack_alt">';
				else
				$content1.='<tr class="smack_highlight">';

				$content1.='
				<td class="smack-field-td-middleit"><input type="checkbox" name="select'.$i.'" id="select'.$i.'" disabled=disabled checked=checked ></td>
				<td>'.$config_leads_fields['fields'][$i]['label'].' *</td>
				<td class="smack-field-td-middleit">';
//				if($config_leads_fields['fields'][$i]['publish'] == 1)
				{
					$content1.='<a name="publish'.$i.'" id="publish'.$i.'" onclick="'."alert('This field is mandatory, cannot hide')".'">
					<img src="'.$imagepath.'tick_strict.png"/>
					</a>';
				}
				$content1.='</td>
				<td class="smack-field-td-middleit">';
				$content1.= "<input class='position-text-box' type='textbox' name='position{$i}' value='".($i+1)."' >";
				$content1.='</td> 
                                <td class="smack-field-td-middleit"><input type="checkbox" name="mandatory'.$i.'" id="mandatory'.$i.'" disabled=disabled checked=checked ></td>';
				 $content1.='<td class="smack-field-td-middleit" id="field_label_display'.$i.'"><input type="text" id="field_label_display'.$i.'" name="fieldlabel'.$i.'" value="'.$config_leads_fields['fields'][$i]['display_label'].'"></td>

						</tr>';
			}
			else
			{
				if($i % 2 == 1)
				$content1.='<tr class="smack_highlight smack_alt">';
				else
				$content1.='<tr class="smack_highlight">';

				$content1.='<td class="smack-field-td-middleit">';
				if($config_leads_fields['fields'][$i]['publish'] == 1){
					$content1.= '<input type="checkbox" name="select'.$i.'" id="select'.$i.'" checked=checked >';
				}
				else
				{
					$content1.= '<input type="checkbox" name="select'.$i.'" id="select'.$i.'">';
				}
				$content1.='</td>
				<td>'.$config_leads_fields['fields'][$i]['label'].'</td>
				<td class="smack-field-td-middleit">';
				if($config_leads_fields['fields'][$i]['publish'] == 1){
					$content1.='<a name="publish'.$i.'" id="publish'.$i.'" >
					
					<span class="is_show_widget" style="color: #019E5A;">Yes</span>
					</a>';
				}
				else{
					$content1.='<a name="publish'.$i.'" id="publish'.$i.'" >
					
					<span class="not_show_widget" style="color: #FF0000;">No</span>
					</a>';
				}
				$content1.='</td>
				<td class="smack-field-td-middleit">';
				$content1.= "<input class='position-text-box' type='textbox' name='position{$i}' value='".($i+1)."' ></td>";
				$content1.=' <td class="smack-field-td-middleit">';
				if($config_leads_fields['fields'][$i]["wp_mandatory"] == 1)
				{
					$content1 .= '<input type="checkbox" name="mandatory'.$i.'" id="mandatory'.$i.'"  checked=checked >';
				}
				else
				{
					$content1 .= '<input type="checkbox" name="mandatory'.$i.'" id="mandatory'.$i.'" >';
				}
				$content1 .= '</td>'; 
				
				$content1.='<td class="smack-field-td-middleit" id="field_label_display'.$i.'"><input type="text" id="field_label_display_'.$i.'" name ="fieldlabel'.$i.'" value="'.$config_leads_fields['fields'][$i]['display_label'].'"></td>
            	
</tr>';
			}
		}
	}
	$content1.="<input type='hidden' name='no_of_rows' id='no_of_rows' value={$i} />";
	$content.=$content1;
	$content.= '</table>
	</div>';
	return $content;
}

	if (isset ($_POST ['formtype'])) {
		$shortcode = "";
		if(isset($skinnyData['REQUEST']['EditShortcode']))
		{
			$shortcode = $skinnyData['REQUEST']['EditShortcode'];
		}
		$data = saveFormFields( $skinnyData['option'] , $skinnyData['onAction'] , $shortcode , $skinnyData['formtype'] );

		if( isset($data['display']) )
		{
			echo $data['display'];
		}
	}
	$plug_url = WP_CONST_ULTIMATE_CRM_CPT_PLUG_URL_PRO ;
	$fields_form = add_query_arg( array( '__module' => 'ManageShortcodes' , '__action' => 'ManageFields' ) , $plug_url );
?>

	<form id="field-form" action="<?php echo esc_url( $fields_form ); ?>" method="post">
	<h3 style="height: 42px;">

<?php
		global $crmdetailsPRO;
		$content = "";
if(isset($skinnyData['REQUEST']['EditShortcode']))
{
		$content .= "<span id='inneroptions' style='position:relative;left:5px;margin-left:15px;'>CRM Type  : ";
		$content .= "<input type='hidden' name='crmtype' id='crmtype' value ='{$skinnyData['REQUEST']['crmtype']}'/>";
		$content .= "<span> ";
		foreach( $crmdetailsPRO as $crm_key => $crm_value )
		{
			if(isset($skinnyData['REQUEST']['crmtype']) && ($crm_key == $skinnyData['REQUEST']['crmtype'])){
				$select_option = " {$crm_value['crmname']} ";

			}
		}
		$content .= $select_option;
		$content .= "</span>";
		$content .= "</span>";
		echo $content;
}
else
{

                $content.= "<span id='inneroptions' style='position:relative;left:5px;margin-left:10px;'>CRM Type  : <select id='crmtype' name='crmtype' style='margin-left:8px;height:27px;' class=''
onchange = \"SelectFieldsPRO('{$skinnyData['siteurl']}','{$skinnyData['module']}','{$skinnyData['options']}', '{$skinnyData['onAction']}')\">";
                $select_option = "";
                $skinnyData['crmtype'] = "" ;

                $select_option .= "<option> --Select-- </option>";
                foreach( $crmdetailsPRO as $crm_key => $crm_value )
                {
			  if(isset($skinnyData['REQUEST']['crmtype']) && ($crm_key == $skinnyData['REQUEST']['crmtype'])){
                                $select_option.= "<option value='{$crm_key}' selected=selected > {$crm_value['crmname']} </option>";
                        }
                        else
                        {
                                $select_option.= "<option value='{$crm_key}'> {$crm_value['crmname']} </option>";
                        }
                }
                $content.= $select_option;
                $content.= "</select></span>";
                echo $content;
}
	?>
	<?php
		global $crmdetailsPRO;
		global $DefaultActivePluginPRO;
		$content = "";
if(isset($skinnyData['REQUEST']['EditShortcode']))
{
		$content .= "<span id='inneroptions' style='position:relative;left:40px;'>Module Type  : ";
                $content .= "<input type='hidden' name='module' id='module' value ='".sanitize_text_field($skinnyData['REQUEST']['module'])."'/>";
                $content .= "<span> ";
                foreach( $crmdetailsPRO[$skinnyData['activatedplugin']]['modulename'] as $key => $value )
                {
                        if(isset($skinnyData['REQUEST']['module']) && ($skinnyData['REQUEST']['module'] == $key ) ){
                                $select_option = " {$value} ";

                        }
                }
                $content .= $select_option;
                $content .= "</span>";
                $content .= "</span>";
                echo $content;
}
else
{
                $content.= "<span id='inneroptions' style='position:relative;left:40px;'>Module Type  : <select id='module' name='module' style='margin-left:8px;height:27px;' onchange = \"SelectFieldsPRO('{$skinnyData['siteurl']}','{$skinnyData['module']}','{$skinnyData['options']}', '{$skinnyData['onAction']}')\" >";
                $select_option = "";
                $select_option .= "<option> --Select-- </option>";
                foreach( $crmdetailsPRO[$skinnyData['activatedplugin']]['modulename'] as $key => $value)
                {
                        if(isset($skinnyData['REQUEST']['module']) && ($skinnyData['REQUEST']['module'] == $key ) )
                        {
                                $select_option.= "<option value = '{$key}' selected=selected  > {$value}</option>";
                        }
                        else
                        {
                                $select_option.= "<option value = '{$key}' > {$value}</option>";
                        }
                }
                $content.= $select_option;

                $content.= "</select></span>";
                echo $content;
}				
	?> 
	</h3>
	<?php
 	if(isset($skinnyData['REQUEST']['EditShortcode']) )
        {
        $skinnyData['onAction']='onEditShortCode';
        ?>
        <h3 id="innerheader" style="margin-bottom:48px;margin-left:20px;">[<?php echo esc_html($skinnyData['activatedplugin']);?>-web-form name='<?php echo $skinnyData['REQUEST']['EditShortcode'];?>']</h3>
        <input type="hidden" name="shortcode" id="shortcode" value="<?php echo sanitize_text_field($skinnyData['REQUEST']['EditShortcode']);?>"/>
        <?php
        $skinnyData['onAction']='onEditShortCode';
        }
        else
        {
        $skinnyData['onAction']='onCreate';
        }
        ?>

	<div class="wp-common-crm-content" style="margin-top:-40px">
		<input id="LeadSearchID" type="search" value="" name="LeadSearch" style="margin-left: 800px;">
		<input id="LeadSearchSubmit" class="button" type="submit" value="Search" name="LeadSearchSubmit">

<select id="bulk-action-selector-top" name="action" style="margin: 0px 18px 2px;">
<option selected="selected" value="-1">Bulk Actions</option>
<option value="enable_field">Enable Field</option>
<option value="disable_field">Disable Field</option>
<option value="update_order">Update Order</option>
<option value="enable_mandatory">Enable Mandatory</option>
<option value="disable_mandatory">Disable Mandatory</option>
</select>


<input type="hidden" id="savefields" name="savefields" value="Apply"/>
<span class="subsubsub" style="margin-top: 0px;">
<a class="" href="">Reset Shortcode</a>
</span>

	<?php
	if(isset($skinnyData['REQUEST']['EditShortcode']))
	{
		$content = "";
		$content.= '<input type="hidden" id="EditShortcode" name="EditShortcode" value="'.esc_attr($skinnyData['REQUEST']['EditShortcode']).'"/>';
		$content.= "<input class='button-primary' type='submit' value='Apply' style='background-color: #F7F7F7; border-color: #CCCCCC; color: #555555; margin-left: 10px;' onclick =  \" return SaveCheckPRO('{$skinnyData['siteurl']}','{$skinnyData['module']}','{$skinnyData['options']}', '{$skinnyData['onAction']}')\" />";
		echo $content;
	}
    	else
	{
	?>
	 <input class="button-primary" type="submit" value="Generate Shortcode" onclick =" document.getElementById('savefields').value='GenerateShortcode'; return SaveCheckPRO('<?php echo esc_js($skinnyData['siteurl']); ?>','<?php echo esc_js($skinnyData['module']); ?>','<?php echo esc_js($skinnyData['options']); ?>', '<?php echo esc_js($skinnyData['onAction']); ?>');" id="innersave" >
	<?php
	}
	?>	
			<img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/ajax-loaders.gif" id="loading-image" style="display: none;margin-left: 500px;right:200px;padding-top: 5px; padding-left: 5px;">
		<div id="fieldtable">
	<?php
	if(isset($skinnyData['REQUEST']['EditShortcode']))
		echo formFields( "smack_fields_shortcodes" , "onEditShortCode" , $skinnyData['REQUEST']['EditShortcode'] , $skinnyData['formtype'] );
	else
		echo formFields( $skinnyData['option'] , $skinnyData['onAction'] , '' , $skinnyData['formtype'] );
	?>
	</div>
</div>
<div class="wp-common-crm-content">
<h3 id="formtext" style="width:margin-left:0px;"> Form Settings :</h3>
<table>
	<tbody>
	<tr>
		<?php
                                if(!isset($_REQUEST['formtype']))
                                {
					echo "<span><b>Alert:</b><label style='color: red;margin-left:6px;padding-top:2px;'> Careful on saving the form mode, This form options will be stored under the form type chosen on 'Form Type' picklist</label></span>"; }?> </tr>
		<tr><td><br></td></tr>
		<tr>
	<?php
                if(isset($skinnyData['REQUEST']['EditShortcode']) && ( $skinnyData['REQUEST']['EditShortcode'] != "" ))
                {
			
			$total_fields = get_option($skinnyData['option']);
			$config_fields = $total_fields[$skinnyData['REQUEST']['EditShortcode']];
		}
		else
		{
			$config_fields = get_option($skinnyData['option']);
		}
		$content = "";
		$content.= "<td id='inneroptions' style='width: 25%;'>Form Type  : </td><td style='width: 25%;'><select name='formtype'>";
		$formtypes = array('post' => "Post" , 'widget' => "Widget");
		$select_option = "";
		foreach( $formtypes as $formtype_key => $formtype_value )
		{
			if( $formtype_key == $config_fields['formtype'] )
			{
				$select_option.= "<option value='{$formtype_key}' selected > {$formtype_value} </option>";
			}
			else
			{
				$select_option.= "<option value='{$formtype_key}'> {$formtype_value} </option>";
			}
		}
		$content.= $select_option;
		$content.= "</select></td>";
		echo $content;
	?>
		<td id="inneroptions" style="width: 18%;">
			Form Mode : 
		</td>
		<td id='innertext'>
			<?php
				if(isset($_REQUEST['formtype']))
				{
					echo "Form builder is in Edit mode";
				}
				else
				{
					echo "Temporary form builder mode.";
				}
			?>
		</td>
		</tr>
		</tbody>
	</table>
	<table><tbody>
		<tr><td><br></td></tr>
 		<tr>
                <td style="width:138px">
                        <label><div id='innertext' style='float:left;padding-right: 5px;'>Duplicate handling</div><div style='float:right;'>:</div> </label>
                </td>
                <td style="width:90px">
			<span id="circlecheck">
                        <input type='radio'  name='check_duplicate' id='smack_capture_duplicates' value="skip" 
<?php
if(isset($config_fields['check_duplicate']) && (sanitize_text_field($config_fields['check_duplicate'])=='skip'))
{
        echo "checked=checked";
}
?>
>
			<label for="smack_capture_duplicates" id='innertext'>Skip</label>
                        </span>
                </td>
                <td style="width:90px">
			<span id="circlecheck">
                        <input type='radio'  name='check_duplicate' id='smack_update_records' value= "update"
<?php
if(isset($config_fields['check_duplicate'] ) && (sanitize_text_field($config_fields['check_duplicate'])=='update'))
{
        echo "checked=checked";
}
?>
>
			<label for="smack_update_records"  id='innertext'>Update</label>
                        </span>
                </td>
 		<td style="width:90px">
			<span id="circlecheck">
                        <input type='radio'  name='check_duplicate' id='smack_none_records' value="none"
<?php
if(!isset($config_fields['check_duplicate']) || ( isset($config_fields['check_duplicate']) && (sanitize_text_field($config_fields['check_duplicate'])=='none')))
{
        echo "checked=checked";
}
?>
>
			<label for="smack_none_records"  id='innertext'>None</label>
                        </span>
                </td>
        </tr>
</tbody></table>
<table><tbody>
<br>
<tr>
<td id='innertext' style="width:198px"><label><?php 
	$HelperObj = new WPCapture_includes_helper_PRO();
	$module = $HelperObj->Module;
	echo "Assign {$module} to User";

?></label><div style='float:right;'> : </div>
</td>
<td  id="assignedto_td" style="padding-left:10px;">
<?php  
	$FunctionsObj = new PROFunctions();
	if(isset($skinnyData['REQUEST']['EditShortcode']))
	{
		$UsersListHtml = $FunctionsObj->getUsersListHtml( $skinnyData['REQUEST']['EditShortcode'] );
	}
	else
	{
        	$UsersListHtml = $FunctionsObj->getUsersListHtml();
	}
	echo " $UsersListHtml"; 
?>
</td>
</tr>
<tr><td><br></td></tr>
		<tr>
		    <td>
			<label id='innertext'>Error Message Submission</label><div style='float:right;'> : </div>
		    </td>
		    <td style="padding-left:10px;">
			<input type="text" name="errormessage" value="<?php if(isset($config_fields['errormessage'])) echo sanitize_text_field($config_fields['errormessage']); ?>" placeholder ="Sorry, submission failed. Kindly try again after some time" />
		    </td>
                   <td>
                        <div style ="position:relative;top:-9px;">
                        <a class="tooltip"  href="#" style="padding-left:8px">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                        Message Displayed For Failed Submission.
                        <img style="margin-top: 6px;float:right;" src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                        </span> </a>
                        </div>
                   </td>
		</tr><tr><td><br></td></tr>
		<tr>
		    <td>
			<label id='innertext'>Success Message Submission</label><div style='float:right;'> : </div> 
		    </td>
		    <td style="padding-left:10px;">
			<input type="text" name="successmessage" value="<?php if(isset($config_fields['successmessage'])) echo sanitize_text_field($config_fields['successmessage']); ?>" placeholder ="Thanks for Submitting"/>
		   </td>
                   <td>
                        <div style ="position:relative;top:-9px;">
                        <a class="tooltip" href="#" style="padding-left:8px">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                         Message Displayed For Successful Submission.
                        <img style="margin-top: 6px;float:right;" src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                        </span> </a>
                        </div>
                   </td>
		</tr>
		<tr><td><br></td></tr>
		</tbody></table>
		<table></tbody>
		<tr>
		    <td style="width:199px">
			<label id='innertext'>Enable URL Redirection </label><div style='float:right;'> : </div>
		    </td><td style="width:90;padding-left:10px;">
			<div class="switch">
				<input type="checkbox" id='enableurlredirection' name="enableurlredirection" class="cmn-toggle cmn-toggle-yes-no" onclick="enableredirecturl(this.id);" value="on" <?php if(isset($config_fields['enableurlredirection']) == 'on'){ echo "checked=checked"; } ?> />
				<label for="enableurlredirection" data-on="Yes" data-off="No"></label>
			</div>			
		    </td>
		    <td style="padding-left:30px;">
			<input id="redirecturl" type="text" name="redirecturl" <?php if(!isset($config_fields['enableurlredirection']) == 'on'){ echo "disabled=disabled";} ?> value="<?php if(isset($config_fields['redirecturl'])) echo intval($config_fields['redirecturl']); ?>" placeholder = "Page id or Post id"/>
		    </td>
                    <td style="padding-left:10px;">
                        <div style ="position:relative;top:-9px;">
                        <a class="tooltip" href="#">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">      
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                        (Give your custom success page url post id to redirect leads).
                        <img style="margin-top: 6px;float:right;" src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                        </span> </a>
                        </div>

                   </td>
		</tr>
<tr><td><br></td></tr>
		<tr>
		    <td>
			<label id='innertext'>Enable Google Captcha </label><div style='float:right;'> : </div>
		    </td>
		    <td style="padding-left:10px;">
			<div class="switch">
				<input type="checkbox" name="enablecaptcha" id="enablecaptcha"  class="cmn-toggle cmn-toggle-yes-no" value="on" <?php if(isset($config_fields['enablecaptcha']) && (sanitize_text_field($config_fields['enablecaptcha'])== 'on'))  { echo "checked=checked"; } ?> />
				<label for="enablecaptcha" data-on="Yes" data-off="No"></label>	
			</div>
		    </td>
	            <td style="padding-left:36px;">
                        <div style="margin-left:-25px;margin-top:-11px">
                        <a class="tooltip" href="#">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">      
                        <span class="tooltipPostStatus">
                        <img src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/callout.gif" class="callout">
                        (Enable google recaptcha feature).
                        <img style="margin-top: 6px;float:right;" src="<?php echo esc_url($skinnyData['siteurl']); ?>/wp-content/plugins/wp-leads-builder-any-crm-pro/images/help.png">
                        </span> </a>
                        </div>
                   </td>
		 </tr>
	</tbody>
	</table>
        </div>
        <br>
	<br>
<script>
function showAccordion( id )
{
	if(jQuery("#advance_option_display").val() == 0 )
	{
		jQuery("#advance_option").css("display", "block");
		jQuery("#advance_option_display").val(1);
		jQuery("#accordion_arrow").removeClass( "fa-chevron-right" );
		jQuery("#accordion_arrow").addClass( "fa-chevron-down" );
	}
	else
	{
                jQuery("#advance_option").css("display", "none");
                jQuery("#advance_option_display").val(0);
		jQuery("#accordion_arrow").removeClass( "fa-chevron-down" );
		jQuery("#accordion_arrow").addClass( "fa-chevron-right" );
	}
}
</script>
        <div class="wp-common-crm-content" id="advance_option" style="display:none; " >
		<input type="hidden" id="advance_option_display" name="advance_option_display"  value=0 >
		<div class="version-warning">
			<span style="font-weight:bold; color:red;">Pro Version Only</span>
			<?php
				require_once(WP_CONST_ULTIMATE_CRM_CPT_DIRECTORY_PRO."modules/ManageFields/templates/Advance_Option.php");
			?>
		</div>		
        </div>
        <br>
        <br>
</form>
